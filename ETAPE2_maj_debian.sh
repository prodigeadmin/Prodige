#!/bin/bash

interactive()
{
echo "Ce script ne doit pas être exécuté
Lancer les commandes les unes après les autres"
exit
}
interactive

/etc/init.d/tomcat7 stop
pg_dropcluster --stop 9.4 main
apt-get remove -y --purge postgresql-9.4 postgresql-9.4-postgis postgresql-client-9.4 postgresql-contrib-9.4 postgresql-doc-9.4 phppgadmin libecwj2 gdal mapserv mapcache php5* apache2* tomcat* libtomcat7-java openjdk-7* postgis*  postgres* libproj0
rm -rf /etc/apache2 /usr/lib/cgi-bin /var/lib/postgresql/ /usr/share/phppgadmin /var/lib/jetty8/webapps /etc/jetty8 /etc/tomcat7 /usr/share/jetty8 /usr/share/tomcat7 /etc/php5 /var/lib/php5
rm /usr/bin/wkhtmltopdf > /dev/null 2>&1
rm /usr/local/bin/wkhtmltopdf > /dev/null 2>&1
rm /usr/lib/libproj.so.0 /usr/lib/libproj.so > /dev/null 2>&1
sed -i '/#tasks prodige4.0/,/#end tasks prodige4.0/d' /etc/crontab


echo '
deb http://ftp.fr.debian.org/debian stretch main contrib non-free
deb http://security.debian.org/ stretch/updates main' > /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive
echo 'libc6 libraries/restart-without-asking boolean true' | debconf-set-selections

apt-get update
apt-get -o Dpkg::Options::="--force-confold" --force-yes -fuy upgrade
apt-get -o Dpkg::Options::="--force-confold" --force-yes -fuy dist-upgrade

apt-get autoremove -y
apt-get install -y linux-image-4.9.0-8-amd64

reboot

