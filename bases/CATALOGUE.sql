--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 9.6.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: alkarto; Type: SCHEMA; Schema: -; Owner: user_p40
--

CREATE SCHEMA alkarto;


ALTER SCHEMA alkarto OWNER TO user_p40;

--
-- Name: bdterr; Type: SCHEMA; Schema: -; Owner: user_p40
--

CREATE SCHEMA bdterr;


ALTER SCHEMA bdterr OWNER TO user_p40;

--
-- Name: catalogue; Type: SCHEMA; Schema: -; Owner: user_p40
--

CREATE SCHEMA catalogue;


ALTER SCHEMA catalogue OWNER TO user_p40;

--
-- Name: SCHEMA catalogue; Type: COMMENT; Schema: -; Owner: user_p40
--

COMMENT ON SCHEMA catalogue IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: prodige_couche_type_stockage(integer); Type: FUNCTION; Schema: catalogue; Owner: user_p40
--

CREATE FUNCTION catalogue.prodige_couche_type_stockage(i integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $$
BEGIN
   IF i=0 THEN
    RETURN 'Raster';
   ELSIF i=1 THEN
    RETURN 'Vecteur';
   ELSIF i=-1 THEN
    RETURN 'Ensenble de séries de données';
   ELSIF i=-2 THEN
    RETURN 'Majic';
   ELSIF i=-3 THEN
    RETURN 'Tableau';
   ELSIF i=-4 THEN
    RETURN 'Vue';
  ELSE
     RETURN 'Type Inconnu';
END IF;        
END;
$$;


ALTER FUNCTION catalogue.prodige_couche_type_stockage(i integer) OWNER TO user_p40;

--
-- Name: st_aslatlontext(public.geometry); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.st_aslatlontext(public.geometry) RETURNS text
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$ SELECT ST_AsLatLonText($1, '') $_$;


ALTER FUNCTION public.st_aslatlontext(public.geometry) OWNER TO postgres;

--
-- Name: gist_geometry_ops; Type: OPERATOR FAMILY; Schema: public; Owner: user_p40
--

CREATE OPERATOR FAMILY public.gist_geometry_ops USING gist;


ALTER OPERATOR FAMILY public.gist_geometry_ops USING gist OWNER TO user_p40;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bdterr_champ_type; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_champ_type (
    champtype_id integer NOT NULL,
    champtype_nom character varying(255) DEFAULT NULL::character varying,
    champtype_incontenutier boolean DEFAULT true
);


ALTER TABLE bdterr.bdterr_champ_type OWNER TO user_p40;

--
-- Name: bdterr_contenus_tiers; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_contenus_tiers (
    contenu_id integer NOT NULL,
    lot_id integer,
    contenu_type integer,
    contenu_nom character varying(255) DEFAULT NULL::character varying,
    contenu_ordre integer,
    contenu_fiche boolean,
    contenu_resultats boolean,
    contenu_url character varying(255) NOT NULL
);


ALTER TABLE bdterr.bdterr_contenus_tiers OWNER TO user_p40;

--
-- Name: bdterr_couche_champ; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_couche_champ (
    champ_id integer NOT NULL,
    champ_lot_id integer NOT NULL,
    champ_type integer,
    champ_nom character varying(150) NOT NULL,
    champ_alias character varying(255) DEFAULT NULL::character varying,
    champ_ordre integer,
    champ_format character varying(255) DEFAULT NULL::character varying,
    champ_fiche boolean,
    champ_resultats boolean,
    champ_identifiant boolean,
    champ_label boolean
);


ALTER TABLE bdterr.bdterr_couche_champ OWNER TO user_p40;

--
-- Name: bdterr_donnee; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_donnee (
    donnee_id integer NOT NULL,
    donnee_lot_id integer,
    donnee_objet_id text,
    donnee_objet_nom text,
    donnee_geom text,
    donnee_geom_json text,
    donnee_attributs_fiche text,
    donnee_attributs_resultat text,
    donnee_ressources_resultat text,
    donnee_ressources_fiche text,
    donnee_insee text,
    donnee_insee_delegue text
);


ALTER TABLE bdterr.bdterr_donnee OWNER TO user_p40;

--
-- Name: bdterr_lot; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_lot (
    lot_id integer NOT NULL,
    lot_theme_id integer,
    lot_referentiel_id integer,
    lot_uuid character varying(255) DEFAULT NULL::character varying,
    lot_table character varying(255) DEFAULT NULL::character varying,
    lot_alias character varying(255) DEFAULT NULL::character varying,
    lot_carte character varying(255) DEFAULT NULL::character varying,
    lot_avertissement_message character varying(255) DEFAULT NULL::character varying,
    lot_statistiques boolean,
    lot_visualiseur boolean,
    lot_date_maj timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    lot_statistic text,
    lot_layer_name text
);


ALTER TABLE bdterr.bdterr_lot OWNER TO user_p40;

--
-- Name: bdterr_rapports; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_rapports (
    rapport_id integer NOT NULL,
    rapport_nom character varying(255) DEFAULT NULL::character varying,
    rapport_gabarit_html character varying(255) DEFAULT NULL::character varying,
    rapport_gabarit_odt character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE bdterr.bdterr_rapports OWNER TO user_p40;

--
-- Name: bdterr_rapports_profils; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_rapports_profils (
    id integer NOT NULL,
    bterr_rapport_id integer,
    prodige_profil_id integer
);


ALTER TABLE bdterr.bdterr_rapports_profils OWNER TO user_p40;

--
-- Name: bdterr_referentiel_carto; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_referentiel_carto (
    referentiel_id integer NOT NULL,
    referentiel_table text NOT NULL,
    referentiel_champs text
);


ALTER TABLE bdterr.bdterr_referentiel_carto OWNER TO user_p40;

--
-- Name: bdterr_referentiel_carto_referentiel_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.bdterr_referentiel_carto_referentiel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.bdterr_referentiel_carto_referentiel_id_seq OWNER TO user_p40;

--
-- Name: bdterr_referentiel_intersection; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_referentiel_intersection (
    referentiel_id integer NOT NULL,
    referentiel_table character varying(255) DEFAULT NULL::character varying,
    referentiel_insee character varying(255) DEFAULT NULL::character varying,
    referentiel_insee_deleguee character varying(255) DEFAULT NULL::character varying,
    referentiel_nom character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE bdterr.bdterr_referentiel_intersection OWNER TO user_p40;

--
-- Name: bdterr_referentiel_recherche; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_referentiel_recherche (
    referentiel_id integer NOT NULL,
    referentiel_table character varying(255) DEFAULT NULL::character varying,
    referentiel_insee character varying(255) DEFAULT NULL::character varying,
    referentiel_insee_delegue character varying(255) DEFAULT NULL::character varying,
    referentiel_commune character varying(255) DEFAULT NULL::character varying,
    referentiel_nom text,
    referentiel_api_champs text
);


ALTER TABLE bdterr.bdterr_referentiel_recherche OWNER TO user_p40;

--
-- Name: bdterr_themes; Type: TABLE; Schema: bdterr; Owner: user_p40
--

CREATE TABLE bdterr.bdterr_themes (
    theme_id integer NOT NULL,
    theme_parent integer,
    theme_nom character varying(255) NOT NULL,
    theme_alias text,
    theme_ordre integer
);


ALTER TABLE bdterr.bdterr_themes OWNER TO user_p40;

--
-- Name: champ_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.champ_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.champ_id_seq OWNER TO user_p40;

--
-- Name: champtype_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.champtype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.champtype_id_seq OWNER TO user_p40;

--
-- Name: contenu_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.contenu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.contenu_id_seq OWNER TO user_p40;

--
-- Name: donnee_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.donnee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.donnee_id_seq OWNER TO user_p40;

--
-- Name: lot_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.lot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.lot_id_seq OWNER TO user_p40;

--
-- Name: rapport_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.rapport_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.rapport_id_seq OWNER TO user_p40;

--
-- Name: rapports_profils_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.rapports_profils_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.rapports_profils_id_seq OWNER TO user_p40;

--
-- Name: referentiel_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.referentiel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.referentiel_id_seq OWNER TO user_p40;

--
-- Name: referentiel_recherche_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.referentiel_recherche_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.referentiel_recherche_id_seq OWNER TO user_p40;

--
-- Name: theme_id_seq; Type: SEQUENCE; Schema: bdterr; Owner: user_p40
--

CREATE SEQUENCE bdterr.theme_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bdterr.theme_id_seq OWNER TO user_p40;

--
-- Name: seq_acces_serveur; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_acces_serveur
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_acces_serveur OWNER TO user_p40;

--
-- Name: acces_serveur; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.acces_serveur (
    pk_acces_serveur integer DEFAULT nextval('catalogue.seq_acces_serveur'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    accs_id character varying(128) NOT NULL,
    accs_adresse character varying(128) NOT NULL,
    path_consultation character varying(128) DEFAULT '/PRRA/Consultation/?map='::character varying NOT NULL,
    path_administration character varying(128) DEFAULT '/PRRA/AdministrationCarto/?map='::character varying NOT NULL,
    path_carte_statique character varying(128) DEFAULT '/CartesStatiques/'::character varying,
    accs_adresse_admin text,
    accs_adresse_tele text,
    accs_login_admin text,
    accs_pwd_admin text,
    accs_service_admin integer,
    accs_adresse_data text
);


ALTER TABLE catalogue.acces_serveur OWNER TO user_p40;

--
-- Name: COLUMN acces_serveur.path_consultation; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON COLUMN catalogue.acces_serveur.path_consultation IS 'Chemin d''acces au site de consultation';


--
-- Name: COLUMN acces_serveur.path_administration; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON COLUMN catalogue.acces_serveur.path_administration IS 'Chemin d''acces au site d''administration';


--
-- Name: COLUMN acces_serveur.path_carte_statique; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON COLUMN catalogue.acces_serveur.path_carte_statique IS 'Chemin d''acces aux cartes statiques';


--
-- Name: seq_domaine; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_domaine
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_domaine OWNER TO user_p40;

--
-- Name: domaine; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.domaine (
    pk_domaine integer DEFAULT nextval('catalogue.seq_domaine'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    dom_id character varying(30) NOT NULL,
    dom_nom character varying(128) NOT NULL,
    dom_description character varying(1024),
    dom_rubric integer DEFAULT 0 NOT NULL
);


ALTER TABLE catalogue.domaine OWNER TO user_p40;

--
-- Name: seq_groupe_profil; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_groupe_profil
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_groupe_profil OWNER TO user_p40;

--
-- Name: groupe_profil; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.groupe_profil (
    pk_groupe_profil integer DEFAULT nextval('catalogue.seq_groupe_profil'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    grp_id character varying(30) NOT NULL,
    grp_nom character varying(128) NOT NULL,
    grp_description character varying(1024),
    grp_is_default_installation integer DEFAULT 0 NOT NULL,
    grp_nom_ldap character varying(128)
);


ALTER TABLE catalogue.groupe_profil OWNER TO user_p40;

--
-- Name: seq_grp_regroupe_usr; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_grp_regroupe_usr
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_grp_regroupe_usr OWNER TO user_p40;

--
-- Name: grp_regroupe_usr; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.grp_regroupe_usr (
    pk_grp_regroupe_usr integer DEFAULT nextval('catalogue.seq_grp_regroupe_usr'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    grpusr_fk_groupe_profil integer,
    grpusr_fk_utilisateur integer,
    grpusr_is_principal integer DEFAULT 0
);


ALTER TABLE catalogue.grp_regroupe_usr OWNER TO user_p40;

--
-- Name: seq_sous_domaine; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_sous_domaine
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_sous_domaine OWNER TO user_p40;

--
-- Name: seq_utilisateur; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_utilisateur
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_utilisateur OWNER TO user_p40;

--
-- Name: sous_domaine; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.sous_domaine (
    pk_sous_domaine integer DEFAULT nextval('catalogue.seq_sous_domaine'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    ssdom_id character varying(30) NOT NULL,
    ssdom_nom character varying(128) NOT NULL,
    ssdom_description character varying(1024),
    ssdom_fk_domaine integer,
    ssdom_admin_fk_groupe_profil integer,
    ssdom_createur_fk_groupe_profil integer,
    ssdom_editeur_fk_groupe_profil integer,
    ssdom_service_wms integer DEFAULT 0,
    ssdom_service_wms_uuid character varying(250)
);


ALTER TABLE catalogue.sous_domaine OWNER TO user_p40;

--
-- Name: utilisateur; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.utilisateur (
    pk_utilisateur integer DEFAULT nextval('catalogue.seq_utilisateur'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    usr_id character varying(128) NOT NULL,
    usr_nom character varying(128) NOT NULL,
    usr_prenom character varying(128),
    usr_email character varying(128),
    usr_telephone character varying(30),
    usr_telephone2 character varying(30),
    usr_service character varying(128),
    usr_description character varying(1024),
    usr_password character varying(128),
    usr_pwdexpire date DEFAULT now() NOT NULL,
    usr_generic integer DEFAULT 0 NOT NULL,
    usr_ldap integer DEFAULT 0,
    usr_signature text,
    date_expiration_compte date
);


ALTER TABLE catalogue.utilisateur OWNER TO user_p40;

--
-- Name: administrateurs_sous_domaine; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.administrateurs_sous_domaine AS
 SELECT domaine.pk_domaine,
    domaine.dom_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    groupe_profil.pk_groupe_profil,
    groupe_profil.grp_nom,
    utilisateur.pk_utilisateur,
    utilisateur.usr_id
   FROM ((((catalogue.domaine
     RIGHT JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
     LEFT JOIN catalogue.groupe_profil ON ((sous_domaine.ssdom_admin_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
     LEFT JOIN catalogue.grp_regroupe_usr ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
     LEFT JOIN catalogue.utilisateur ON ((grp_regroupe_usr.grpusr_fk_utilisateur = utilisateur.pk_utilisateur)));


ALTER TABLE catalogue.administrateurs_sous_domaine OWNER TO user_p40;

--
-- Name: VIEW administrateurs_sous_domaine; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON VIEW catalogue.administrateurs_sous_domaine IS 'Liste les adminitrateurs de chaque sous domaine';


--
-- Name: seq_carte_projet; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_carte_projet
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_carte_projet OWNER TO user_p40;

--
-- Name: carte_projet; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.carte_projet (
    pk_carte_projet integer DEFAULT nextval('catalogue.seq_carte_projet'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    cartp_id character varying(30) NOT NULL,
    cartp_nom character varying(255) NOT NULL,
    cartp_description text,
    cartp_format integer DEFAULT 0 NOT NULL,
    cartp_fk_stockage_carte integer,
    cartp_fk_fiche_metadonnees integer,
    cartp_utilisateur integer,
    cartep_etat integer DEFAULT 0,
    cartp_administrateur integer,
    cartp_utilisateur_email character varying(128),
    cartp_wms integer DEFAULT 0
);


ALTER TABLE catalogue.carte_projet OWNER TO user_p40;

--
-- Name: seq_fiche_metadonnees; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_fiche_metadonnees
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_fiche_metadonnees OWNER TO user_p40;

--
-- Name: fiche_metadonnees; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.fiche_metadonnees (
    pk_fiche_metadonnees integer DEFAULT nextval('catalogue.seq_fiche_metadonnees'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    fmeta_id character varying(128) NOT NULL,
    fmeta_description character varying(1024),
    fmeta_fk_couche_donnees integer,
    statut integer DEFAULT 1 NOT NULL,
    schema character varying(128)
);


ALTER TABLE catalogue.fiche_metadonnees OWNER TO user_p40;

--
-- Name: COLUMN fiche_metadonnees.statut; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON COLUMN catalogue.fiche_metadonnees.statut IS 'Statut de publication de la fiche (1 - projet, 2 - attente validation, 3- validee, 4 - publiee)';


--
-- Name: seq_ssdom_visualise_carte; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_ssdom_visualise_carte
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_ssdom_visualise_carte OWNER TO user_p40;

--
-- Name: seq_stockage_carte; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_stockage_carte
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_stockage_carte OWNER TO user_p40;

--
-- Name: ssdom_visualise_carte; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.ssdom_visualise_carte (
    pk_ssdom_visualise_carte integer DEFAULT nextval('catalogue.seq_ssdom_visualise_carte'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    ssdcart_fk_carte_projet integer,
    ssdcart_fk_sous_domaine integer
);


ALTER TABLE catalogue.ssdom_visualise_carte OWNER TO user_p40;

--
-- Name: stockage_carte; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.stockage_carte (
    pk_stockage_carte integer DEFAULT nextval('catalogue.seq_stockage_carte'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    stkcard_id character varying(30) NOT NULL,
    stkcard_path character varying(1024) NOT NULL,
    stk_server integer
);


ALTER TABLE catalogue.stockage_carte OWNER TO user_p40;

--
-- Name: metadata; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.metadata (
    id integer NOT NULL,
    uuid character varying(250) NOT NULL,
    schemaid character varying(32) NOT NULL,
    istemplate character(1) DEFAULT 'n'::bpchar NOT NULL,
    isharvested character(1) DEFAULT 'n'::bpchar NOT NULL,
    createdate character varying(30) NOT NULL,
    changedate character varying(30) NOT NULL,
    data text NOT NULL,
    source character varying(250) NOT NULL,
    title character varying(255),
    root character varying(255),
    harvestuuid character varying(250) DEFAULT NULL::character varying,
    owner integer NOT NULL,
    groupowner integer,
    harvesturi character varying(512) DEFAULT NULL::character varying,
    rating integer DEFAULT 0 NOT NULL,
    popularity integer DEFAULT 0 NOT NULL,
    displayorder integer,
    doctype character varying(255),
    extra character varying(255)
);


ALTER TABLE public.metadata OWNER TO user_p40;

--
-- Name: operationallowed; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.operationallowed (
    groupid integer NOT NULL,
    metadataid integer NOT NULL,
    operationid integer NOT NULL
);


ALTER TABLE public.operationallowed OWNER TO user_p40;

--
-- Name: cartes_sdom; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.cartes_sdom AS
 SELECT carte_projet.cartp_format,
    carte_projet.pk_carte_projet,
    carte_projet.cartp_description,
    carte_projet.cartp_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    stockage_carte.pk_stockage_carte,
    stockage_carte.stkcard_path,
    acces_serveur.accs_adresse,
    acces_serveur.path_consultation,
    acces_serveur.path_administration,
    acces_serveur.path_carte_statique,
    acces_serveur.accs_adresse_admin,
    acces_serveur.accs_login_admin,
    acces_serveur.accs_pwd_admin,
    acces_serveur.accs_service_admin,
    fiche_metadonnees.pk_fiche_metadonnees,
    fiche_metadonnees.fmeta_id,
        CASE
            WHEN (operationallowed.operationid = 0) THEN 4
            ELSE 1
        END AS statut,
    domaine.pk_domaine,
    domaine.dom_rubric AS dom_coherence,
    domaine.dom_nom,
    carte_projet.cartp_wms
   FROM ((((((((catalogue.domaine
     JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
     JOIN catalogue.ssdom_visualise_carte ON ((sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine)))
     JOIN catalogue.carte_projet ON ((ssdom_visualise_carte.ssdcart_fk_carte_projet = carte_projet.pk_carte_projet)))
     LEFT JOIN catalogue.fiche_metadonnees ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
     JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::bigint = metadata.id)))
     LEFT JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
     JOIN catalogue.stockage_carte ON ((carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte)))
     LEFT JOIN catalogue.acces_serveur ON ((acces_serveur.pk_acces_serveur = stockage_carte.stk_server)));


ALTER TABLE catalogue.cartes_sdom OWNER TO user_p40;

--
-- Name: cartes_sdom_pub; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.cartes_sdom_pub AS
 SELECT cartes_sdom.cartp_format,
    cartes_sdom.pk_carte_projet,
    cartes_sdom.cartp_description,
    cartes_sdom.cartp_nom,
    cartes_sdom.pk_sous_domaine,
    cartes_sdom.ssdom_nom,
    cartes_sdom.pk_stockage_carte,
    cartes_sdom.stkcard_path,
    cartes_sdom.accs_adresse,
    cartes_sdom.path_consultation,
    cartes_sdom.path_administration,
    cartes_sdom.path_carte_statique,
    cartes_sdom.accs_service_admin,
    cartes_sdom.pk_fiche_metadonnees,
    cartes_sdom.fmeta_id,
    cartes_sdom.statut,
    cartes_sdom.pk_domaine,
    cartes_sdom.dom_coherence,
    cartes_sdom.dom_nom,
    cartes_sdom.accs_adresse_admin,
    cartes_sdom.cartp_wms
   FROM catalogue.cartes_sdom
  WHERE (cartes_sdom.statut = 4);


ALTER TABLE catalogue.cartes_sdom_pub OWNER TO user_p40;

--
-- Name: seq_competence; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_competence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_competence OWNER TO user_p40;

--
-- Name: competence; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.competence (
    pk_competence_id integer DEFAULT nextval('catalogue.seq_competence'::regclass) NOT NULL,
    competence_nom character varying
);


ALTER TABLE catalogue.competence OWNER TO user_p40;

--
-- Name: seq_competence_accede_carte; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_competence_accede_carte
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_competence_accede_carte OWNER TO user_p40;

--
-- Name: competence_accede_carte; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.competence_accede_carte (
    pk_competence_accede_carte integer DEFAULT nextval('catalogue.seq_competence_accede_carte'::regclass) NOT NULL,
    competencecarte_fk_competence_id integer,
    competencecarte_fk_carte_projet integer
);


ALTER TABLE catalogue.competence_accede_carte OWNER TO user_p40;

--
-- Name: seq_competence_accede_couche; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_competence_accede_couche
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_competence_accede_couche OWNER TO user_p40;

--
-- Name: competence_accede_couche; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.competence_accede_couche (
    pk_competence_accede_couche integer DEFAULT nextval('catalogue.seq_competence_accede_couche'::regclass) NOT NULL,
    competencecouche_fk_competence_id integer,
    competencecouche_fk_couche_donnees integer
);


ALTER TABLE catalogue.competence_accede_couche OWNER TO user_p40;

--
-- Name: seq_couche_donnees; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_couche_donnees
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_couche_donnees OWNER TO user_p40;

--
-- Name: couche_donnees; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.couche_donnees (
    pk_couche_donnees integer DEFAULT nextval('catalogue.seq_couche_donnees'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    couchd_id character varying(128) NOT NULL,
    couchd_nom character varying(255) NOT NULL,
    couchd_description character varying(2048),
    couchd_type_stockage integer,
    couchd_emplacement_stockage character varying(1024),
    couchd_fk_acces_server integer,
    couchd_wms integer DEFAULT 0,
    couchd_wfs integer DEFAULT 0,
    couchd_download integer DEFAULT 0 NOT NULL,
    couchd_restriction integer DEFAULT 0 NOT NULL,
    couchd_restriction_exclusif integer DEFAULT 0 NOT NULL,
    couchd_zonageid_fk integer,
    couchd_restriction_buffer double precision,
    couchd_visualisable integer DEFAULT 1 NOT NULL,
    couchd_extraction_attributaire integer DEFAULT 0 NOT NULL,
    couchd_extraction_attributaire_champ character varying DEFAULT ''::character varying,
    changedate timestamp with time zone DEFAULT now() NOT NULL,
    couchd_alert_msg_id integer,
    couchd_wfs_uuid character varying(250) DEFAULT NULL::character varying,
    couchd_wms_sdom integer[]
);


ALTER TABLE catalogue.couche_donnees OWNER TO user_p40;

--
-- Name: seq_ssdom_dispose_couche; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_ssdom_dispose_couche
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_ssdom_dispose_couche OWNER TO user_p40;

--
-- Name: ssdom_dispose_couche; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.ssdom_dispose_couche (
    pk_ssdom_dispose_couche integer DEFAULT nextval('catalogue.seq_ssdom_dispose_couche'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    ssdcouch_fk_couche_donnees integer,
    ssdcouch_fk_sous_domaine integer
);


ALTER TABLE catalogue.ssdom_dispose_couche OWNER TO user_p40;

--
-- Name: couche_sdom; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.couche_sdom AS
 SELECT domaine.pk_domaine,
    domaine.dom_rubric AS dom_coherence,
    domaine.dom_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    fiche_metadonnees.pk_fiche_metadonnees,
    fiche_metadonnees.fmeta_id,
        CASE
            WHEN (operationallowed.operationid = 0) THEN 4
            ELSE 1
        END AS statut,
    couche_donnees.pk_couche_donnees,
    couche_donnees.couchd_nom,
    couche_donnees.couchd_description,
    couche_donnees.couchd_type_stockage,
    couche_donnees.couchd_emplacement_stockage,
    couche_donnees.couchd_wms,
    couche_donnees.couchd_wfs,
    couche_donnees.couchd_download,
    metadata.uuid,
    couche_donnees.couchd_restriction,
    couche_donnees.couchd_visualisable,
    couche_donnees.changedate
   FROM ((((((catalogue.domaine
     JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
     JOIN catalogue.ssdom_dispose_couche ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
     JOIN catalogue.fiche_metadonnees ON ((ssdom_dispose_couche.ssdcouch_fk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
     JOIN catalogue.couche_donnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
     JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
     LEFT JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));


ALTER TABLE catalogue.couche_sdom OWNER TO user_p40;

--
-- Name: dom_sdom; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.dom_sdom AS
 SELECT domaine.pk_domaine,
    domaine.dom_rubric,
    domaine.dom_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom
   FROM (catalogue.domaine
     LEFT JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)));


ALTER TABLE catalogue.dom_sdom OWNER TO user_p40;

--
-- Name: VIEW dom_sdom; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON VIEW catalogue.dom_sdom IS 'Liste des domaines/sous-domaines';


--
-- Name: editeurs_sous_domaine; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.editeurs_sous_domaine AS
 SELECT domaine.pk_domaine,
    domaine.dom_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    groupe_profil.pk_groupe_profil,
    groupe_profil.grp_nom,
    utilisateur.pk_utilisateur,
    utilisateur.usr_id
   FROM ((((catalogue.domaine
     RIGHT JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
     LEFT JOIN catalogue.groupe_profil ON ((sous_domaine.ssdom_editeur_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
     LEFT JOIN catalogue.grp_regroupe_usr ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
     LEFT JOIN catalogue.utilisateur ON ((grp_regroupe_usr.grpusr_fk_utilisateur = utilisateur.pk_utilisateur)));


ALTER TABLE catalogue.editeurs_sous_domaine OWNER TO user_p40;

--
-- Name: execution_report; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.execution_report (
    id integer NOT NULL,
    scheduled_command_id integer,
    date_time timestamp(0) without time zone NOT NULL,
    file character varying(255) NOT NULL,
    status character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE catalogue.execution_report OWNER TO user_p40;

--
-- Name: execution_report_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.execution_report_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.execution_report_id_seq OWNER TO user_p40;

--
-- Name: seq_grp_accede_competence; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_grp_accede_competence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_grp_accede_competence OWNER TO user_p40;

--
-- Name: grp_accede_competence; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.grp_accede_competence (
    pk_grp_accede_competence integer DEFAULT nextval('catalogue.seq_grp_accede_competence'::regclass) NOT NULL,
    fk_grp_id integer NOT NULL,
    fk_competence_id integer NOT NULL
);


ALTER TABLE catalogue.grp_accede_competence OWNER TO user_p40;

--
-- Name: seq_grp_accede_dom; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_grp_accede_dom
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_grp_accede_dom OWNER TO user_p40;

--
-- Name: grp_accede_dom; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.grp_accede_dom (
    pk_grp_accede_dom integer DEFAULT nextval('catalogue.seq_grp_accede_dom'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    grpdom_fk_domaine integer,
    grpdom_fk_groupe_profil integer
);


ALTER TABLE catalogue.grp_accede_dom OWNER TO user_p40;

--
-- Name: seq_grp_accede_ssdom; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_grp_accede_ssdom
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_grp_accede_ssdom OWNER TO user_p40;

--
-- Name: grp_accede_ssdom; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.grp_accede_ssdom (
    pk_grp_accede_ssdom integer DEFAULT nextval('catalogue.seq_grp_accede_ssdom'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    grpss_fk_sous_domaine integer,
    grpss_fk_groupe_profil integer
);


ALTER TABLE catalogue.grp_accede_ssdom OWNER TO user_p40;

--
-- Name: seq_grp_autorise_trt; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_grp_autorise_trt
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_grp_autorise_trt OWNER TO user_p40;

--
-- Name: grp_autorise_trt; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.grp_autorise_trt (
    pk_grp_autorise_trt integer DEFAULT nextval('catalogue.seq_grp_autorise_trt'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    grptrt_fk_traitement integer,
    grptrt_fk_groupe_profil integer
);


ALTER TABLE catalogue.grp_autorise_trt OWNER TO user_p40;

--
-- Name: seq_grp_trt_objet; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_grp_trt_objet
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_grp_trt_objet OWNER TO user_p40;

--
-- Name: grp_trt_objet; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.grp_trt_objet (
    pk_grp_trt_objet integer DEFAULT nextval('catalogue.seq_grp_trt_objet'::regclass) NOT NULL,
    grptrtobj_fk_grp_id integer,
    grptrtobj_fk_trt_id integer,
    grptrtobj_fk_objet_id integer,
    grptrtobj_fk_obj_type_id integer,
    grp_trt_objet_status integer DEFAULT 0
);


ALTER TABLE catalogue.grp_trt_objet OWNER TO user_p40;

--
-- Name: harves_opendata_node; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.harves_opendata_node (
    pk_node_id integer NOT NULL,
    node_name text,
    node_logo text,
    api_url text,
    node_last_modification text,
    url_dcat text,
    node_log_file text,
    command_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE catalogue.harves_opendata_node OWNER TO user_p40;

--
-- Name: TABLE harves_opendata_node; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON TABLE catalogue.harves_opendata_node IS 'moissonnage DCAT';


--
-- Name: harves_opendata_node_params; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.harves_opendata_node_params (
    pk_params_id integer NOT NULL,
    fk_node_id integer NOT NULL,
    key text,
    value text
);


ALTER TABLE catalogue.harves_opendata_node_params OWNER TO user_p40;

--
-- Name: TABLE harves_opendata_node_params; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON TABLE catalogue.harves_opendata_node_params IS 'paramètres nœuds de moissonnage DCAT';


--
-- Name: harves_opendata_node_params_FK_node_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue."harves_opendata_node_params_FK_node_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue."harves_opendata_node_params_FK_node_id_seq" OWNER TO user_p40;

--
-- Name: harves_opendata_node_params_FK_node_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_p40
--

ALTER SEQUENCE catalogue."harves_opendata_node_params_FK_node_id_seq" OWNED BY catalogue.harves_opendata_node_params.fk_node_id;


--
-- Name: harves_opendata_node_params_pk_params_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.harves_opendata_node_params_pk_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.harves_opendata_node_params_pk_params_id_seq OWNER TO user_p40;

--
-- Name: harves_opendata_node_params_pk_params_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_p40
--

ALTER SEQUENCE catalogue.harves_opendata_node_params_pk_params_id_seq OWNED BY catalogue.harves_opendata_node_params.pk_params_id;


--
-- Name: harves_opendata_node_pk_node_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.harves_opendata_node_pk_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.harves_opendata_node_pk_node_id_seq OWNER TO user_p40;

--
-- Name: harves_opendata_node_pk_node_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_p40
--

ALTER SEQUENCE catalogue.harves_opendata_node_pk_node_id_seq OWNED BY catalogue.harves_opendata_node.pk_node_id;


--
-- Name: incoherence_carte_fiche_metadonnees; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.incoherence_carte_fiche_metadonnees AS
 SELECT carte_projet.pk_carte_projet,
    to_char(carte_projet.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS "date de création de la carte",
    carte_projet.cartp_nom AS "nom de la métadonnée",
    carte_projet.cartp_description AS "résumé de la métadonnée",
    carte_projet.cartp_format AS "format de la carte",
    carte_projet.cartp_fk_stockage_carte AS "identifiant stockage mapfile",
    carte_projet.cartp_fk_fiche_metadonnees AS "identifiant de la fiche métadonnée"
   FROM catalogue.carte_projet
  WHERE ((NOT (carte_projet.cartp_fk_fiche_metadonnees IN ( SELECT fiche_metadonnees.pk_fiche_metadonnees
           FROM catalogue.fiche_metadonnees))) OR ((carte_projet.cartp_fk_fiche_metadonnees IS NULL) AND (carte_projet.cartp_utilisateur IS NULL)));


ALTER TABLE catalogue.incoherence_carte_fiche_metadonnees OWNER TO user_p40;

--
-- Name: incoherence_carte_sous_domaines; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.incoherence_carte_sous_domaines AS
 SELECT carte_projet.pk_carte_projet,
    to_char(carte_projet.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS "date de création de la carte",
    carte_projet.cartp_nom AS "nom de la métadonnée",
    carte_projet.cartp_description AS "résumé de la métadonnée",
    carte_projet.cartp_format AS "format de la carte",
    carte_projet.cartp_fk_stockage_carte AS "identifiant stockage mapfile",
    carte_projet.cartp_fk_fiche_metadonnees AS "identifiant de la fiche métadonnée"
   FROM catalogue.carte_projet
  WHERE ((NOT (carte_projet.pk_carte_projet IN ( SELECT ssdom_visualise_carte.ssdcart_fk_carte_projet
           FROM catalogue.ssdom_visualise_carte
          WHERE (ssdom_visualise_carte.ssdcart_fk_carte_projet IS NOT NULL)))) AND (carte_projet.cartp_format = ANY (ARRAY[0, 1])));


ALTER TABLE catalogue.incoherence_carte_sous_domaines OWNER TO user_p40;

--
-- Name: incoherence_couche_fiche_metadonnees; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.incoherence_couche_fiche_metadonnees AS
 SELECT couche_donnees.pk_couche_donnees,
    to_char(couche_donnees.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS "date de création de la couche",
    couche_donnees.couchd_nom AS "nom de la métadonnée",
    couche_donnees.couchd_description AS "résumé de la métadonnée",
    catalogue.prodige_couche_type_stockage(couche_donnees.couchd_type_stockage) AS "type de stockage",
    couche_donnees.couchd_emplacement_stockage AS "emplacement de stockage",
    couche_donnees.couchd_fk_acces_server AS "identifiant du serveur",
    couche_donnees.couchd_wms AS "publiée en wms",
    couche_donnees.couchd_wfs AS "publiée en wfs"
   FROM catalogue.couche_donnees
  WHERE (NOT (couche_donnees.pk_couche_donnees IN ( SELECT fiche_metadonnees.fmeta_fk_couche_donnees
           FROM catalogue.fiche_metadonnees
          WHERE (fiche_metadonnees.fmeta_fk_couche_donnees IS NOT NULL))));


ALTER TABLE catalogue.incoherence_couche_fiche_metadonnees OWNER TO user_p40;

--
-- Name: incoherence_couche_sous_domaines; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.incoherence_couche_sous_domaines AS
 SELECT couche_donnees.pk_couche_donnees,
    to_char(couche_donnees.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS "date de création de la couche",
    couche_donnees.couchd_nom AS "nom de la métadonnée",
    couche_donnees.couchd_description AS "résumé de la métadonnée",
    catalogue.prodige_couche_type_stockage(couche_donnees.couchd_type_stockage) AS "type de stockage",
    couche_donnees.couchd_emplacement_stockage AS "emplacement de stockage",
    couche_donnees.couchd_fk_acces_server AS "identifiant du serveur",
    couche_donnees.couchd_wms AS "publiée en wms",
    couche_donnees.couchd_wfs AS "publiée en wfs"
   FROM catalogue.couche_donnees
  WHERE (NOT (couche_donnees.pk_couche_donnees IN ( SELECT ssdom_dispose_couche.ssdcouch_fk_couche_donnees
           FROM catalogue.ssdom_dispose_couche
          WHERE (ssdom_dispose_couche.ssdcouch_fk_couche_donnees IS NOT NULL))));


ALTER TABLE catalogue.incoherence_couche_sous_domaines OWNER TO user_p40;

--
-- Name: incoherence_fiche_metadonnees_metadata; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.incoherence_fiche_metadonnees_metadata AS
 SELECT fiche_metadonnees.pk_fiche_metadonnees,
    to_char(fiche_metadonnees.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS "date de création de la fiche",
    fiche_metadonnees.fmeta_id AS "identifiant géosource",
    fiche_metadonnees.statut
   FROM catalogue.fiche_metadonnees
  WHERE (((fiche_metadonnees.fmeta_id)::text = ''::text) OR (NOT ((fiche_metadonnees.fmeta_id)::bigint IN ( SELECT metadata.id
           FROM public.metadata))));


ALTER TABLE catalogue.incoherence_fiche_metadonnees_metadata OWNER TO user_p40;

--
-- Name: statistique_liste_carte_publiees; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_liste_carte_publiees AS
 SELECT carte_projet.pk_carte_projet,
    carte_projet.cartp_nom AS "nom de la carte",
    carte_projet.cartp_description AS "résumé",
    carte_projet.cartp_format AS format,
    stockage_carte.stkcard_path AS stockage,
    metadata.changedate,
    metadata.createdate,
    metadata.id AS "id metadata",
    metadata.uuid AS "uuid metadata"
   FROM ((((catalogue.carte_projet
     JOIN catalogue.stockage_carte ON ((carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte)))
     JOIN catalogue.fiche_metadonnees ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
     JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::text = (metadata.id)::text)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));


ALTER TABLE catalogue.statistique_liste_carte_publiees OWNER TO user_p40;

--
-- Name: statistique_liste_couche_publiees; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_liste_couche_publiees AS
 SELECT couche_donnees.pk_couche_donnees,
    couche_donnees.couchd_nom AS "nom de la couche",
    couche_donnees.couchd_description AS "résumé",
    couche_donnees.couchd_type_stockage AS "couche vecteur ?",
    couche_donnees.couchd_emplacement_stockage AS "emplacement de stockage",
    couche_donnees.couchd_wms AS "Publiée en WMS ?",
    couche_donnees.couchd_wfs AS "Publiée en WFS ?",
    metadata.changedate,
    metadata.createdate,
    metadata.id AS "id metadata",
    metadata.uuid AS "uuid metadata"
   FROM (((catalogue.couche_donnees
     JOIN catalogue.fiche_metadonnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
     JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::text = (metadata.id)::text)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));


ALTER TABLE catalogue.statistique_liste_couche_publiees OWNER TO user_p40;

--
-- Name: metadata_list; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.metadata_list AS
 SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."uuid metadata" AS id
   FROM catalogue.statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."uuid metadata" AS id
   FROM catalogue.statistique_liste_carte_publiees;


ALTER TABLE catalogue.metadata_list OWNER TO user_p40;

--
-- Name: metadonnees_sdom; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.metadonnees_sdom AS
 SELECT domaine.pk_domaine,
    domaine.dom_rubric AS dom_coherence,
    domaine.dom_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    fiche_metadonnees.pk_fiche_metadonnees,
    fiche_metadonnees.fmeta_id,
    fiche_metadonnees.fmeta_fk_couche_donnees
   FROM (((catalogue.domaine
     JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
     JOIN catalogue.ssdom_dispose_couche ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
     JOIN catalogue.fiche_metadonnees ON ((ssdom_dispose_couche.ssdcouch_fk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)));


ALTER TABLE catalogue.metadonnees_sdom OWNER TO user_p40;

--
-- Name: metadonnees_sdom_pub; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.metadonnees_sdom_pub AS
 SELECT domaine.pk_domaine,
    domaine.dom_rubric AS dom_coherence,
    domaine.dom_nom,
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    fiche_metadonnees.pk_fiche_metadonnees,
    fiche_metadonnees.fmeta_id,
    fiche_metadonnees.fmeta_fk_couche_donnees
   FROM (((((catalogue.domaine
     JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
     JOIN catalogue.ssdom_dispose_couche ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
     JOIN catalogue.fiche_metadonnees ON ((ssdom_dispose_couche.ssdcouch_fk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
     JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));


ALTER TABLE catalogue.metadonnees_sdom_pub OWNER TO user_p40;

--
-- Name: objet; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.objet AS
 SELECT couche_donnees.pk_couche_donnees AS objet_id,
    couche_donnees.couchd_nom AS objet_nom,
    '1'::text AS objet_type_id
   FROM catalogue.couche_donnees
UNION
 SELECT carte_projet.pk_carte_projet AS objet_id,
    carte_projet.cartp_nom AS objet_nom,
    '2'::text AS objet_type_id
   FROM catalogue.carte_projet
UNION
 SELECT meta.pk_fiche_metadonnees AS objet_id,
    couche.couchd_nom AS objet_nom,
    '3'::text AS objet_type_id
   FROM ((catalogue.fiche_metadonnees meta
     LEFT JOIN catalogue.couche_donnees couche ON ((meta.fmeta_fk_couche_donnees = couche.pk_couche_donnees)))
     LEFT JOIN public.metadata ON (((meta.fmeta_id)::text = (metadata.id)::text)))
  WHERE ((meta.fmeta_fk_couche_donnees IS NOT NULL) AND (metadata.istemplate = 'n'::bpchar))
UNION
 SELECT meta.pk_fiche_metadonnees AS objet_id,
    carte.cartp_nom AS objet_nom,
    '3'::text AS objet_type_id
   FROM ((catalogue.fiche_metadonnees meta
     LEFT JOIN catalogue.carte_projet carte ON ((meta.pk_fiche_metadonnees = carte.cartp_fk_fiche_metadonnees)))
     LEFT JOIN public.metadata ON (((meta.fmeta_id)::text = (metadata.id)::text)))
  WHERE ((carte.cartp_fk_fiche_metadonnees IS NOT NULL) AND (metadata.istemplate = 'n'::bpchar))
UNION
 SELECT meta.pk_fiche_metadonnees AS objet_id,
    couche.couchd_nom AS objet_nom,
    '4'::text AS objet_type_id
   FROM ((catalogue.fiche_metadonnees meta
     LEFT JOIN catalogue.couche_donnees couche ON ((meta.fmeta_fk_couche_donnees = couche.pk_couche_donnees)))
     LEFT JOIN public.metadata ON (((meta.fmeta_id)::text = (metadata.id)::text)))
  WHERE ((meta.fmeta_fk_couche_donnees IS NOT NULL) AND (metadata.istemplate = 'y'::bpchar))
UNION
 SELECT meta.pk_fiche_metadonnees AS objet_id,
    carte.cartp_nom AS objet_nom,
    '4'::text AS objet_type_id
   FROM ((catalogue.fiche_metadonnees meta
     LEFT JOIN catalogue.carte_projet carte ON ((meta.pk_fiche_metadonnees = carte.cartp_fk_fiche_metadonnees)))
     LEFT JOIN public.metadata ON (((meta.fmeta_id)::text = (metadata.id)::text)))
  WHERE ((carte.cartp_fk_fiche_metadonnees IS NOT NULL) AND (metadata.istemplate = 'y'::bpchar))
UNION
 SELECT groupe_profil.pk_groupe_profil AS objet_id,
    groupe_profil.grp_nom AS objet_nom,
    '5'::text AS objet_type_id
   FROM catalogue.groupe_profil;


ALTER TABLE catalogue.objet OWNER TO user_p40;

--
-- Name: seq_objet_type; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_objet_type
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_objet_type OWNER TO user_p40;

--
-- Name: objet_type; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.objet_type (
    pk_objet_type_id integer DEFAULT nextval('catalogue.seq_objet_type'::regclass) NOT NULL,
    objet_type_nom character varying(128) NOT NULL
);


ALTER TABLE catalogue.objet_type OWNER TO user_p40;

--
-- Name: seq_perimetre; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_perimetre
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_perimetre OWNER TO user_p40;

--
-- Name: perimetre; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.perimetre (
    pk_perimetre_id integer DEFAULT nextval('catalogue.seq_perimetre'::regclass) NOT NULL,
    perimetre_code character varying,
    perimetre_nom character varying,
    perimetre_zonage_id integer
);


ALTER TABLE catalogue.perimetre OWNER TO user_p40;

--
-- Name: prodige_carto_colors_prodige_carto_color_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.prodige_carto_colors_prodige_carto_color_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.prodige_carto_colors_prodige_carto_color_id_seq OWNER TO user_p40;

--
-- Name: prodige_carto_colors; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_carto_colors (
    prodige_carto_color_id integer DEFAULT nextval('catalogue.prodige_carto_colors_prodige_carto_color_id_seq'::regclass) NOT NULL,
    color_theme character varying(10),
    color_white character varying(6),
    color_light character varying(6),
    color_dark character varying(6),
    color_light_grey character varying(6),
    color_tables character varying(6),
    color_grey character varying(6),
    color_theme_extjs text
);


ALTER TABLE catalogue.prodige_carto_colors OWNER TO user_p40;

--
-- Name: prodige_colors_prodige_color_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.prodige_colors_prodige_color_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.prodige_colors_prodige_color_id_seq OWNER TO user_p40;

--
-- Name: prodige_colors; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_colors (
    prodige_color_id integer DEFAULT nextval('catalogue.prodige_colors_prodige_color_id_seq'::regclass) NOT NULL,
    color_theme character varying(10),
    color_back character varying(7),
    color_tables character varying(7),
    color_text character varying(7),
    color_text_light character varying(7),
    volet_url character varying(30),
    css_file text
);


ALTER TABLE catalogue.prodige_colors OWNER TO user_p40;

--
-- Name: prodige_database_requests_prodige_database_request_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.prodige_database_requests_prodige_database_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.prodige_database_requests_prodige_database_request_id_seq OWNER TO user_p40;

--
-- Name: prodige_database_requests; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_database_requests (
    prodige_database_request_id integer DEFAULT nextval('catalogue.prodige_database_requests_prodige_database_request_id_seq'::regclass) NOT NULL,
    request_description character varying(30),
    sql_request text,
    viewname character varying(128)
);


ALTER TABLE catalogue.prodige_database_requests OWNER TO user_p40;

--
-- Name: seq_prodige_external_access; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_prodige_external_access
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_prodige_external_access OWNER TO user_p40;

--
-- Name: prodige_external_access; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_external_access (
    pk_prodige_external_access integer DEFAULT nextval('catalogue.seq_prodige_external_access'::regclass) NOT NULL,
    prodige_extaccess_referer text NOT NULL,
    prodige_extaccess_fk_utilisateur integer NOT NULL
);


ALTER TABLE catalogue.prodige_external_access OWNER TO user_p40;

--
-- Name: prodige_fonts_font_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.prodige_fonts_font_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.prodige_fonts_font_id_seq OWNER TO user_p40;

--
-- Name: prodige_fonts; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_fonts (
    font_id integer DEFAULT nextval('catalogue.prodige_fonts_font_id_seq'::regclass) NOT NULL,
    font1 character varying(20),
    font2 character varying(20),
    font3 character varying(20)
);


ALTER TABLE catalogue.prodige_fonts OWNER TO user_p40;

--
-- Name: prodige_geosource_colors_prodige_geosource_color_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.prodige_geosource_colors_prodige_geosource_color_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.prodige_geosource_colors_prodige_geosource_color_id_seq OWNER TO user_p40;

--
-- Name: prodige_geosource_colors; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_geosource_colors (
    prodige_geosource_color_id integer DEFAULT nextval('catalogue.prodige_geosource_colors_prodige_geosource_color_id_seq'::regclass) NOT NULL,
    geosource_color_theme character varying(10),
    color_commontext character varying(6),
    color_a character varying(6),
    color_a_hover character varying(6),
    color_left_menu_context character varying(6),
    color_border character varying(6),
    color_background_content character varying(6),
    color_left_menu_text character varying(6)
);


ALTER TABLE catalogue.prodige_geosource_colors OWNER TO user_p40;

--
-- Name: seq_prodige_help; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_prodige_help
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_prodige_help OWNER TO user_p40;

--
-- Name: prodige_help; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_help (
    pk_prodige_help_id integer DEFAULT nextval('catalogue.seq_prodige_help'::regclass) NOT NULL,
    prodige_help_title text NOT NULL,
    prodige_help_desc text,
    prodige_help_url text,
    prodige_help_row_number integer
);


ALTER TABLE catalogue.prodige_help OWNER TO user_p40;

--
-- Name: seq_prodige_help_group; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_prodige_help_group
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_prodige_help_group OWNER TO user_p40;

--
-- Name: prodige_help_group; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_help_group (
    hlpgrp_fk_help_id integer NOT NULL,
    hlpgrp_fk_groupe_profil integer NOT NULL,
    pk_prodige_help_group integer DEFAULT nextval('catalogue.seq_prodige_help_group'::regclass) NOT NULL
);


ALTER TABLE catalogue.prodige_help_group OWNER TO user_p40;

--
-- Name: prodige_param_pk_prodige_param_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.prodige_param_pk_prodige_param_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.prodige_param_pk_prodige_param_seq OWNER TO user_p40;

--
-- Name: prodige_param; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_param (
    pk_prodige_param integer DEFAULT nextval('catalogue.prodige_param_pk_prodige_param_seq'::regclass) NOT NULL,
    prodige_title character varying(255),
    prodige_subtitle character varying(255),
    prodige_catalog_intro text,
    logo_url character varying(255),
    top_image_url character varying(255),
    font_id integer,
    prodige_color_id integer,
    prodige_geosource_color_id integer,
    prodige_carto_color_id integer,
    logo_right_url character varying(255)
);


ALTER TABLE catalogue.prodige_param OWNER TO user_p40;

--
-- Name: prodige_server_server_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.prodige_server_server_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.prodige_server_server_id_seq OWNER TO user_p40;

--
-- Name: prodige_session_user; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_session_user (
    pk_session_user character varying(128) NOT NULL,
    date_connexion timestamp without time zone NOT NULL
);


ALTER TABLE catalogue.prodige_session_user OWNER TO user_p40;

--
-- Name: seq_prodige_settings; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_prodige_settings
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_prodige_settings OWNER TO user_p40;

--
-- Name: prodige_settings; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.prodige_settings (
    pk_prodige_settings_id integer DEFAULT nextval('catalogue.seq_prodige_settings'::regclass) NOT NULL,
    prodige_settings_constant text NOT NULL,
    prodige_settings_title text,
    prodige_settings_value text,
    prodige_settings_desc text,
    prodige_settings_type text DEFAULT 'textfield'::text NOT NULL,
    prodige_settings_param text
);


ALTER TABLE catalogue.prodige_settings OWNER TO user_p40;

--
-- Name: raster_info; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.raster_info (
    id bigint NOT NULL,
    name character varying,
    srid integer,
    ulx double precision NOT NULL,
    uly double precision NOT NULL,
    lrx double precision NOT NULL,
    lry double precision NOT NULL,
    bands integer DEFAULT 1,
    cols bigint DEFAULT 0 NOT NULL,
    rows bigint DEFAULT 0 NOT NULL,
    resx double precision DEFAULT 1.0 NOT NULL,
    resy double precision DEFAULT 1 NOT NULL,
    format character varying DEFAULT 'GTIFF'::character varying NOT NULL,
    vrt_path character varying NOT NULL,
    compression character varying DEFAULT 'NONE'::character varying,
    color_interp integer DEFAULT 1
);


ALTER TABLE catalogue.raster_info OWNER TO user_p40;

--
-- Name: seq_rawgraph_couche_config; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_rawgraph_couche_config
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_rawgraph_couche_config OWNER TO user_p40;

--
-- Name: rawgraph_couche_config; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.rawgraph_couche_config (
    id integer DEFAULT nextval('catalogue.seq_rawgraph_couche_config'::regclass) NOT NULL,
    titre character varying(255) NOT NULL,
    resume text NOT NULL,
    serie_donnees_uuid character varying(255) NOT NULL,
    couche character varying(255) NOT NULL,
    colonnes text NOT NULL,
    uuid character varying(255) NOT NULL,
    config json
);


ALTER TABLE catalogue.rawgraph_couche_config OWNER TO user_p40;

--
-- Name: rubric_param_rubric_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.rubric_param_rubric_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.rubric_param_rubric_id_seq OWNER TO user_p40;

--
-- Name: rubric_param; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.rubric_param (
    rubric_id integer DEFAULT nextval('catalogue.rubric_param_rubric_id_seq'::regclass) NOT NULL,
    rubric_name character varying
);


ALTER TABLE catalogue.rubric_param OWNER TO user_p40;

--
-- Name: scheduled_command; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.scheduled_command (
    id integer NOT NULL,
    name text,
    command text,
    arguments text,
    cron_expression text,
    last_execution timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    last_return_code integer DEFAULT 0,
    log_file text,
    priority integer DEFAULT 0,
    execute_immediately boolean DEFAULT false,
    disabled boolean DEFAULT false,
    locked boolean DEFAULT false,
    is_periodic boolean NOT NULL,
    year integer,
    frequency text
);


ALTER TABLE catalogue.scheduled_command OWNER TO user_p40;

--
-- Name: scheduled_command_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.scheduled_command_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.scheduled_command_id_seq OWNER TO user_p40;

--
-- Name: seq_commune; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_commune
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_commune OWNER TO user_p40;

--
-- Name: seq_departement; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_departement
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_departement OWNER TO user_p40;

--
-- Name: seq_dico; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_dico
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_dico OWNER TO user_p40;

--
-- Name: seq_dico_majic2; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_dico_majic2
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_dico_majic2 OWNER TO user_p40;

--
-- Name: seq_export; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_export
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_export OWNER TO user_p40;

--
-- Name: seq_incoherence_perso; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_incoherence_perso
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_incoherence_perso OWNER TO user_p40;

--
-- Name: seq_mapfile; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_mapfile
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_mapfile OWNER TO user_p40;

--
-- Name: seq_mcd_field; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_mcd_field
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_mcd_field OWNER TO user_p40;

--
-- Name: seq_mcd_layer; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_mcd_layer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_mcd_layer OWNER TO user_p40;

--
-- Name: seq_menu; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_menu
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_menu OWNER TO user_p40;

--
-- Name: seq_profil; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_profil
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_profil OWNER TO user_p40;

--
-- Name: seq_ssdom_dispose_metadata; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_ssdom_dispose_metadata
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_ssdom_dispose_metadata OWNER TO user_p40;

--
-- Name: seq_statistique; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_statistique
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_statistique OWNER TO user_p40;

--
-- Name: seq_tache_import_donnees; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_tache_import_donnees
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_tache_import_donnees OWNER TO user_p40;

--
-- Name: seq_traitement; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_traitement
    START WITH 200
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_traitement OWNER TO user_p40;

--
-- Name: seq_trt_autorise_objet; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_trt_autorise_objet
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_trt_autorise_objet OWNER TO user_p40;

--
-- Name: seq_trt_objet; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_trt_objet
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_trt_objet OWNER TO user_p40;

--
-- Name: seq_usr_accede_perimetre; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_usr_accede_perimetre
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_usr_accede_perimetre OWNER TO user_p40;

--
-- Name: seq_usr_accede_perimetre_edition; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_usr_accede_perimetre_edition
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_usr_accede_perimetre_edition OWNER TO user_p40;

--
-- Name: seq_usr_alerte_perimetre_edition; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_usr_alerte_perimetre_edition
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_usr_alerte_perimetre_edition OWNER TO user_p40;

--
-- Name: seq_utilisateur_carte; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_utilisateur_carte
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_utilisateur_carte OWNER TO user_p40;

--
-- Name: seq_utilisateur_mapfile; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_utilisateur_mapfile
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_utilisateur_mapfile OWNER TO user_p40;

--
-- Name: seq_zonage; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.seq_zonage
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.seq_zonage OWNER TO user_p40;

--
-- Name: ssdom_dispose_metadata; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.ssdom_dispose_metadata (
    pk_ssdom_dispose_metadata integer DEFAULT nextval('catalogue.seq_ssdom_dispose_metadata'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    uuid character varying(250) NOT NULL,
    ssdcouch_fk_sous_domaine integer
);


ALTER TABLE catalogue.ssdom_dispose_metadata OWNER TO user_p40;

--
-- Name: standards_conformite; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.standards_conformite (
    metadata_id integer NOT NULL,
    standard_id integer NOT NULL,
    conformite_prefix character varying(100),
    conformite_suffix character varying(100),
    conformite_success boolean DEFAULT true,
    conformite_report_url text
);


ALTER TABLE catalogue.standards_conformite OWNER TO user_p40;

--
-- Name: standards_fournisseur; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.standards_fournisseur (
    fournisseur_id integer NOT NULL,
    fournisseur_name text NOT NULL,
    fournisseur_url text NOT NULL
);


ALTER TABLE catalogue.standards_fournisseur OWNER TO user_p40;

--
-- Name: standards_fournisseur_fournisseur_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.standards_fournisseur_fournisseur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.standards_fournisseur_fournisseur_id_seq OWNER TO user_p40;

--
-- Name: standards_fournisseur_fournisseur_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_p40
--

ALTER SEQUENCE catalogue.standards_fournisseur_fournisseur_id_seq OWNED BY catalogue.standards_fournisseur.fournisseur_id;


--
-- Name: standards_standard; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.standards_standard (
    standard_id integer NOT NULL,
    fournisseur_id integer NOT NULL,
    standard_name text NOT NULL,
    standard_url text NOT NULL,
    standard_database text,
    standard_schema text,
    fmeta_id integer
);


ALTER TABLE catalogue.standards_standard OWNER TO user_p40;

--
-- Name: standards_standard_standard_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_p40
--

CREATE SEQUENCE catalogue.standards_standard_standard_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE catalogue.standards_standard_standard_id_seq OWNER TO user_p40;

--
-- Name: standards_standard_standard_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_p40
--

ALTER SEQUENCE catalogue.standards_standard_standard_id_seq OWNED BY catalogue.standards_standard.standard_id;


--
-- Name: statistique_dernieres_mises_a_jour; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_dernieres_mises_a_jour AS
 SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."résumé",
    statistique_liste_couche_publiees.changedate AS date,
    'donnée'::text AS type,
    statistique_liste_couche_publiees."uuid metadata" AS id,
    statistique_liste_couche_publiees."couche vecteur ?" AS data_type
   FROM catalogue.statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."résumé",
    statistique_liste_carte_publiees.changedate AS date,
    'carte'::text AS type,
    statistique_liste_carte_publiees."uuid metadata" AS id,
    statistique_liste_carte_publiees.format AS data_type
   FROM catalogue.statistique_liste_carte_publiees
  ORDER BY 3 DESC
 LIMIT 30;


ALTER TABLE catalogue.statistique_dernieres_mises_a_jour OWNER TO user_p40;

--
-- Name: statistique_dernieres_nouveautes; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_dernieres_nouveautes AS
 SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."résumé",
    statistique_liste_couche_publiees.changedate AS date,
    statistique_liste_couche_publiees.createdate,
    'donnée'::text AS type,
    statistique_liste_couche_publiees."uuid metadata" AS id,
    statistique_liste_couche_publiees."couche vecteur ?" AS data_type
   FROM catalogue.statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."résumé",
    statistique_liste_carte_publiees.changedate AS date,
    statistique_liste_carte_publiees.createdate,
    'carte'::text AS type,
    statistique_liste_carte_publiees."uuid metadata" AS id,
    statistique_liste_carte_publiees.format AS data_type
   FROM catalogue.statistique_liste_carte_publiees
  ORDER BY 4 DESC
 LIMIT 30;


ALTER TABLE catalogue.statistique_dernieres_nouveautes OWNER TO user_p40;

--
-- Name: statistique_liste_metadata_publiees; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_liste_metadata_publiees AS
 SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."résumé",
    statistique_liste_couche_publiees.changedate AS date,
    statistique_liste_couche_publiees.createdate,
    'donnée'::text AS type,
    statistique_liste_couche_publiees."id metadata" AS id,
    statistique_liste_couche_publiees."couche vecteur ?" AS data_type
   FROM catalogue.statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."résumé",
    statistique_liste_carte_publiees.changedate AS date,
    statistique_liste_carte_publiees.createdate,
    'carte'::text AS type,
    statistique_liste_carte_publiees."id metadata" AS id,
    statistique_liste_carte_publiees.format AS data_type
   FROM catalogue.statistique_liste_carte_publiees;


ALTER TABLE catalogue.statistique_liste_metadata_publiees OWNER TO user_p40;

--
-- Name: statistique_metadata_date_creation; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_metadata_date_creation AS
 SELECT metadata.id,
    substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47)) AS date
   FROM ((( SELECT metadata_1.id,
            replace(metadata_1.data, ' '::text, ''::text) AS data
           FROM ( SELECT metadata_2.id,
                    replace(metadata_2.data, '

'::text, ''::text) AS data
                   FROM public.metadata metadata_2) metadata_1) metadata
     JOIN catalogue.fiche_metadonnees ON (((metadata.id)::text = (fiche_metadonnees.fmeta_id)::text)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
  WHERE ((strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) > 0) AND (strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType>'::text) > 0) AND (length(substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47))) < 11));


ALTER TABLE catalogue.statistique_metadata_date_creation OWNER TO user_p40;

--
-- Name: statistique_metadata_date_publication; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_metadata_date_publication AS
 SELECT metadata.id,
    substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="publication"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47)) AS date
   FROM (( SELECT metadata_1.id,
            replace(metadata_1.data, ' '::text, ''::text) AS data
           FROM ( SELECT metadata_2.id,
                    replace(metadata_2.data, '

'::text, ''::text) AS data
                   FROM public.metadata metadata_2) metadata_1) metadata
     JOIN catalogue.fiche_metadonnees ON (((metadata.id)::text = (fiche_metadonnees.fmeta_id)::text)))
  WHERE ((strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) > 0) AND (strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="publication"/></gmd:dateType>'::text) > 0) AND (fiche_metadonnees.statut = 4) AND (length(substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="publication"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47))) < 11));


ALTER TABLE catalogue.statistique_metadata_date_publication OWNER TO user_p40;

--
-- Name: statistique_metadata_date_revision; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_metadata_date_revision AS
 SELECT metadata.id,
    substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47)) AS date
   FROM ((( SELECT metadata_1.id,
            replace(metadata_1.data, ' '::text, ''::text) AS data
           FROM ( SELECT metadata_2.id,
                    replace(metadata_2.data, '

'::text, ''::text) AS data
                   FROM public.metadata metadata_2) metadata_1) metadata
     JOIN catalogue.fiche_metadonnees ON (((metadata.id)::text = (fiche_metadonnees.fmeta_id)::text)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
  WHERE ((strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) > 0) AND (strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType>'::text) > 0) AND (length(substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList="./resources/codeList.xml#CI_DateTypeCode"codeListValue="creation"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47))) < 11));


ALTER TABLE catalogue.statistique_metadata_date_revision OWNER TO user_p40;

--
-- Name: statistique_liste_metadonnees_prod_date_couche; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_liste_metadonnees_prod_date_couche AS
 SELECT metadata.id,
    (xpath('//gmd:contact/gmd:CI_ResponsibleParty/gmd:organisationName/gco:CharacterString/text()'::text, (('<?xml version="1.0" encoding="utf-8"?>'::text || metadata.data))::xml, ARRAY[ARRAY['gmd'::text, 'http://www.isotc211.org/2005/gmd'::text], ARRAY['gco'::text, 'http://www.isotc211.org/2005/gco'::text]]))::text AS "Producteur",
    statistique_metadata_date_creation.date AS "date de création",
    statistique_metadata_date_revision.date AS "date de révision",
    statistique_metadata_date_publication.date AS "date de publication",
    couche_donnees.couchd_nom AS "nom de la couche",
    couche_donnees.couchd_description AS "Résumé"
   FROM ((((((public.metadata
     JOIN catalogue.fiche_metadonnees ON (((metadata.id)::text = (fiche_metadonnees.fmeta_id)::text)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
     JOIN catalogue.couche_donnees ON ((fiche_metadonnees.fmeta_fk_couche_donnees = couche_donnees.pk_couche_donnees)))
     LEFT JOIN catalogue.statistique_metadata_date_creation ON ((statistique_metadata_date_creation.id = metadata.id)))
     LEFT JOIN catalogue.statistique_metadata_date_revision ON ((statistique_metadata_date_revision.id = metadata.id)))
     LEFT JOIN catalogue.statistique_metadata_date_publication ON ((statistique_metadata_date_publication.id = metadata.id)))
  WHERE ((NOT (metadata.data IS NULL)) AND (metadata.data <> ''::text));


ALTER TABLE catalogue.statistique_liste_metadonnees_prod_date_couche OWNER TO user_p40;

--
-- Name: statistique_nombre_cartes_publiees_domaine; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_nombre_cartes_publiees_domaine AS
 SELECT rubric_param.rubric_name AS "nom de la rubrique",
    domaine.pk_domaine,
    domaine.dom_nom AS "nom du domaine",
    count(*) AS "Nombre de cartes publiées"
   FROM (((((((catalogue.fiche_metadonnees
     JOIN catalogue.carte_projet ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
     JOIN catalogue.ssdom_visualise_carte ON ((carte_projet.pk_carte_projet = ssdom_visualise_carte.ssdcart_fk_carte_projet)))
     JOIN catalogue.sous_domaine ON ((sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine)))
     JOIN catalogue.domaine ON ((sous_domaine.ssdom_fk_domaine = domaine.pk_domaine)))
     JOIN catalogue.rubric_param ON ((domaine.dom_rubric = rubric_param.rubric_id)))
     JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
  WHERE (fiche_metadonnees.fmeta_fk_couche_donnees IS NULL)
  GROUP BY rubric_param.rubric_name, domaine.dom_nom, domaine.pk_domaine
  ORDER BY rubric_param.rubric_name, domaine.dom_nom;


ALTER TABLE catalogue.statistique_nombre_cartes_publiees_domaine OWNER TO user_p40;

--
-- Name: statistique_nombre_cartes_publiees_sousdomaine; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_nombre_cartes_publiees_sousdomaine AS
 SELECT rubric_param.rubric_name AS "nom de la rubrique",
    domaine.dom_nom AS "nom du domaine",
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom AS "nom du sous-domaine",
    count(*) AS "Nombre de cartes publiées"
   FROM (((((((catalogue.fiche_metadonnees
     JOIN catalogue.carte_projet ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
     JOIN catalogue.ssdom_visualise_carte ON ((carte_projet.pk_carte_projet = ssdom_visualise_carte.ssdcart_fk_carte_projet)))
     JOIN catalogue.sous_domaine ON ((sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine)))
     JOIN catalogue.domaine ON ((sous_domaine.ssdom_fk_domaine = domaine.pk_domaine)))
     JOIN catalogue.rubric_param ON ((domaine.dom_rubric = rubric_param.rubric_id)))
     JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
  WHERE (fiche_metadonnees.fmeta_fk_couche_donnees IS NULL)
  GROUP BY rubric_param.rubric_name, domaine.dom_nom, sous_domaine.ssdom_nom, sous_domaine.pk_sous_domaine
  ORDER BY rubric_param.rubric_name, domaine.dom_nom, sous_domaine.ssdom_nom;


ALTER TABLE catalogue.statistique_nombre_cartes_publiees_sousdomaine OWNER TO user_p40;

--
-- Name: statistique_nombre_couches_publiees_domaine; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_nombre_couches_publiees_domaine AS
 SELECT rubric_param.rubric_name AS "nom de la rubrique",
    domaine.pk_domaine,
    domaine.dom_nom AS "nom du domaine",
    count(*) AS "Nombre de couches publiées"
   FROM (((((((catalogue.fiche_metadonnees
     JOIN catalogue.couche_donnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
     JOIN catalogue.ssdom_dispose_couche ON ((couche_donnees.pk_couche_donnees = ssdom_dispose_couche.ssdcouch_fk_couche_donnees)))
     JOIN catalogue.sous_domaine ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
     JOIN catalogue.domaine ON ((sous_domaine.ssdom_fk_domaine = domaine.pk_domaine)))
     JOIN catalogue.rubric_param ON ((domaine.dom_rubric = rubric_param.rubric_id)))
     JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
  GROUP BY rubric_param.rubric_name, domaine.dom_nom, domaine.pk_domaine
  ORDER BY rubric_param.rubric_name, domaine.dom_nom;


ALTER TABLE catalogue.statistique_nombre_couches_publiees_domaine OWNER TO user_p40;

--
-- Name: statistique_nombre_couches_publiees_sousdomaine; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_nombre_couches_publiees_sousdomaine AS
 SELECT rubric_param.rubric_name AS "nom de la rubrique",
    domaine.dom_nom AS "nom du domaine",
    sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom AS "nom du sous-domaine",
    count(*) AS "Nombre de couches publiées"
   FROM (((((((catalogue.fiche_metadonnees
     JOIN catalogue.couche_donnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
     JOIN catalogue.ssdom_dispose_couche ON ((couche_donnees.pk_couche_donnees = ssdom_dispose_couche.ssdcouch_fk_couche_donnees)))
     JOIN catalogue.sous_domaine ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
     JOIN catalogue.domaine ON ((sous_domaine.ssdom_fk_domaine = domaine.pk_domaine)))
     JOIN catalogue.rubric_param ON ((domaine.dom_rubric = rubric_param.rubric_id)))
     JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
  GROUP BY rubric_param.rubric_name, domaine.dom_nom, sous_domaine.pk_sous_domaine, sous_domaine.ssdom_nom
  ORDER BY rubric_param.rubric_name, domaine.dom_nom;


ALTER TABLE catalogue.statistique_nombre_couches_publiees_sousdomaine OWNER TO user_p40;

--
-- Name: statistique_ogc; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.statistique_ogc AS
 SELECT couche_donnees.pk_couche_donnees,
    couche_donnees.couchd_nom,
    couche_donnees.couchd_type_stockage,
    couche_donnees.couchd_emplacement_stockage,
    couche_donnees.couchd_wms,
    couche_donnees.couchd_wfs,
    metadata.changedate,
    metadata.createdate,
    metadata.id,
    metadata.uuid
   FROM (((catalogue.couche_donnees
     JOIN catalogue.fiche_metadonnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
     JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::text = (metadata.id)::text)))
     JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
  WHERE ((couche_donnees.couchd_wms = 1) OR (couche_donnees.couchd_wfs = 1));


ALTER TABLE catalogue.statistique_ogc OWNER TO user_p40;

--
-- Name: tache_import_donnees; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.tache_import_donnees (
    pk_tache_import_donnees integer DEFAULT nextval('catalogue.seq_tache_import_donnees'::regclass) NOT NULL,
    uuid character varying(250) NOT NULL,
    pk_couche_donnees integer,
    user_id integer NOT NULL,
    import_date_request timestamp without time zone DEFAULT now() NOT NULL,
    import_date_execution timestamp without time zone,
    import_type character varying(20) DEFAULT 'import_differe'::character varying NOT NULL,
    import_frequency integer,
    import_table character varying(1024) NOT NULL,
    import_data_type character varying(250) DEFAULT 'VECTOR'::character varying NOT NULL,
    import_data_source text NOT NULL,
    import_extra_params text
);


ALTER TABLE catalogue.tache_import_donnees OWNER TO user_p40;

--
-- Name: test_requests; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.test_requests (
    test_id integer NOT NULL,
    test_description character varying(30)
);


ALTER TABLE catalogue.test_requests OWNER TO user_p40;

--
-- Name: traitement; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.traitement (
    pk_traitement integer DEFAULT nextval('catalogue.seq_traitement'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    trt_id character varying(30) NOT NULL,
    trt_nom character varying(128) NOT NULL,
    trt_description character varying(1024),
    trt_administre boolean NOT NULL,
    trt_accede boolean NOT NULL,
    trt_edite boolean DEFAULT false
);


ALTER TABLE catalogue.traitement OWNER TO user_p40;

--
-- Name: traitements_utilisateur; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.traitements_utilisateur AS
 SELECT utilisateur.pk_utilisateur,
    utilisateur.usr_nom,
    groupe_profil.pk_groupe_profil,
    groupe_profil.grp_nom,
    traitement.trt_id,
    traitement.trt_administre,
    traitement.trt_accede,
    traitement.trt_edite
   FROM ((((catalogue.utilisateur
     JOIN catalogue.grp_regroupe_usr ON ((utilisateur.pk_utilisateur = grp_regroupe_usr.grpusr_fk_utilisateur)))
     JOIN catalogue.groupe_profil ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
     JOIN catalogue.grp_autorise_trt ON ((groupe_profil.pk_groupe_profil = grp_autorise_trt.grptrt_fk_groupe_profil)))
     JOIN catalogue.traitement ON ((grp_autorise_trt.grptrt_fk_traitement = traitement.pk_traitement)));


ALTER TABLE catalogue.traitements_utilisateur OWNER TO user_p40;

--
-- Name: trt_autorise_objet; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.trt_autorise_objet (
    pk_trt_autorise_objet integer DEFAULT nextval('catalogue.seq_trt_autorise_objet'::regclass) NOT NULL,
    trtautoobjet_fk_trt_id integer,
    trtautoobjet_fk_obj_type_id integer
);


ALTER TABLE catalogue.trt_autorise_objet OWNER TO user_p40;

--
-- Name: trt_objet; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.trt_objet (
    pk_trt_objet integer DEFAULT nextval('catalogue.trt_objet'::regclass) NOT NULL,
    trt_id character varying(255) NOT NULL,
    trt_nom character varying(255) NOT NULL
);


ALTER TABLE catalogue.trt_objet OWNER TO user_p40;

--
-- Name: usr_accede_perimetre; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.usr_accede_perimetre (
    pk_usr_accede_perimetre integer DEFAULT nextval('catalogue.seq_usr_accede_perimetre'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    usrperim_fk_utilisateur integer,
    usrperim_fk_perimetre integer
);


ALTER TABLE catalogue.usr_accede_perimetre OWNER TO user_p40;

--
-- Name: usr_accede_perimetre_edition; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.usr_accede_perimetre_edition (
    pk_usr_accede_perimetre_edition integer DEFAULT nextval('catalogue.seq_usr_accede_perimetre_edition'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    usrperim_fk_utilisateur integer,
    usrperim_fk_perimetre integer
);


ALTER TABLE catalogue.usr_accede_perimetre_edition OWNER TO user_p40;

--
-- Name: usr_alerte_perimetre_edition; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.usr_alerte_perimetre_edition (
    pk_usr_alerte_perimetre_edition integer DEFAULT nextval('catalogue.seq_usr_alerte_perimetre_edition'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    usralert_fk_utilisateur integer,
    usralert_fk_couchedonnees integer,
    usralert_fk_perimetre integer
);


ALTER TABLE catalogue.usr_alerte_perimetre_edition OWNER TO user_p40;

--
-- Name: utilisateur_carte; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.utilisateur_carte (
    id bigint DEFAULT nextval('catalogue.seq_utilisateur_carte'::regclass) NOT NULL,
    fk_utilisateur integer NOT NULL,
    fk_stockage_carte integer NOT NULL
);


ALTER TABLE catalogue.utilisateur_carte OWNER TO user_p40;

--
-- Name: utilisateurs_domaine; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.utilisateurs_domaine AS
 SELECT domaine.pk_domaine,
    domaine.dom_nom,
    groupe_profil.pk_groupe_profil,
    groupe_profil.grp_nom,
    utilisateur.pk_utilisateur,
    utilisateur.usr_id
   FROM ((((catalogue.domaine
     JOIN catalogue.grp_accede_dom ON ((domaine.pk_domaine = grp_accede_dom.grpdom_fk_domaine)))
     JOIN catalogue.groupe_profil ON ((grp_accede_dom.grpdom_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
     JOIN catalogue.grp_regroupe_usr ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
     JOIN catalogue.utilisateur ON ((grp_regroupe_usr.grpusr_fk_utilisateur = utilisateur.pk_utilisateur)));


ALTER TABLE catalogue.utilisateurs_domaine OWNER TO user_p40;

--
-- Name: VIEW utilisateurs_domaine; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON VIEW catalogue.utilisateurs_domaine IS 'Liste des utilisateurs du domaine';


--
-- Name: utilisateurs_sous_domaine; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.utilisateurs_sous_domaine AS
 SELECT sous_domaine.pk_sous_domaine,
    sous_domaine.ssdom_nom,
    groupe_profil.pk_groupe_profil,
    groupe_profil.grp_nom,
    utilisateur.pk_utilisateur,
    utilisateur.usr_id
   FROM ((((catalogue.sous_domaine
     JOIN catalogue.grp_accede_ssdom ON ((sous_domaine.pk_sous_domaine = grp_accede_ssdom.grpss_fk_sous_domaine)))
     JOIN catalogue.groupe_profil ON ((grp_accede_ssdom.grpss_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
     JOIN catalogue.grp_regroupe_usr ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
     JOIN catalogue.utilisateur ON ((grp_regroupe_usr.grpusr_fk_utilisateur = utilisateur.pk_utilisateur)));


ALTER TABLE catalogue.utilisateurs_sous_domaine OWNER TO user_p40;

--
-- Name: VIEW utilisateurs_sous_domaine; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON VIEW catalogue.utilisateurs_sous_domaine IS 'Liste les utilisateurs de chaque sous domaine';


--
-- Name: v_acces_couche; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.v_acces_couche AS
 SELECT cd.pk_couche_donnees AS couche_pk,
    cd.couchd_id AS couche_id,
    cd.couchd_nom AS couche_nom,
    cd.couchd_description AS couche_desc,
    cd.couchd_emplacement_stockage AS couche_table,
    cd.couchd_type_stockage AS couche_type,
    acces_serveur.accs_adresse AS couche_srv,
    acces_serveur.accs_service_admin AS couche_service_admin,
    acces_serveur.accs_adresse_admin AS couche_srv_admin,
    acces_serveur.accs_login_admin,
    acces_serveur.accs_pwd_admin
   FROM (catalogue.couche_donnees cd
     LEFT JOIN catalogue.acces_serveur ON ((cd.couchd_fk_acces_server = acces_serveur.pk_acces_serveur)));


ALTER TABLE catalogue.v_acces_couche OWNER TO user_p40;

--
-- Name: v_desc_metadata; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.v_desc_metadata AS
 SELECT m.id AS metadataid,
    cd.couchd_nom AS couchenom,
    cd.couchd_type_stockage AS couchetype,
    acs.accs_adresse AS couchesrv,
    acs.accs_service_admin AS couchesrv_service,
    acs.accs_adresse_tele AS couchesrv_tele,
    cd.couchd_emplacement_stockage AS couchetable,
    m.data AS metadataxml,
    fm.schema
   FROM (((public.metadata m
     LEFT JOIN catalogue.fiche_metadonnees fm ON (((m.id)::text = (fm.fmeta_id)::text)))
     JOIN catalogue.couche_donnees cd ON ((fm.fmeta_fk_couche_donnees = cd.pk_couche_donnees)))
     JOIN catalogue.acces_serveur acs ON ((cd.couchd_fk_acces_server = acs.pk_acces_serveur)));


ALTER TABLE catalogue.v_desc_metadata OWNER TO user_p40;

--
-- Name: v_desc_raster; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.v_desc_raster AS
 SELECT m.id AS metadataid,
    r.id AS layerid,
    r.name,
    r.srid,
    r.ulx,
    r.uly,
    r.lrx,
    r.lry,
    r.bands,
    r.cols,
    r.rows,
    r.resx,
    r.resy,
    r.format,
    r.vrt_path,
    r.compression,
    r.color_interp
   FROM ((public.metadata m
     JOIN catalogue.fiche_metadonnees fm ON (((m.id)::text = (fm.fmeta_id)::text)))
     JOIN catalogue.raster_info r ON ((fm.fmeta_fk_couche_donnees = r.id)));


ALTER TABLE catalogue.v_desc_raster OWNER TO user_p40;

--
-- Name: VIEW v_desc_raster; Type: COMMENT; Schema: catalogue; Owner: user_p40
--

COMMENT ON VIEW catalogue.v_desc_raster IS 'Vue liant fiche de mÃ©tadonnÃ©e et descrition raster de la couche associÃ©e';


--
-- Name: v_utilisateur; Type: VIEW; Schema: catalogue; Owner: user_p40
--

CREATE VIEW catalogue.v_utilisateur AS
 SELECT utilisateur.pk_utilisateur,
    utilisateur.usr_id,
    utilisateur.usr_nom,
    utilisateur.usr_prenom,
    utilisateur.usr_email
   FROM catalogue.utilisateur
  WHERE ((utilisateur.date_expiration_compte IS NULL) OR ((utilisateur.date_expiration_compte IS NOT NULL) AND (utilisateur.date_expiration_compte > now())));


ALTER TABLE catalogue.v_utilisateur OWNER TO user_p40;

--
-- Name: zonage; Type: TABLE; Schema: catalogue; Owner: user_p40
--

CREATE TABLE catalogue.zonage (
    pk_zonage_id integer DEFAULT nextval('catalogue.seq_zonage'::regclass) NOT NULL,
    zonage_nom character varying,
    zonage_minimal integer DEFAULT 0,
    zonage_field_id character varying,
    zonage_field_name character varying,
    zonage_fk_couche_donnees integer,
    zonage_field_liaison character varying
);


ALTER TABLE catalogue.zonage OWNER TO user_p40;

--
-- Name: address; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.address (
    id integer NOT NULL,
    address character varying(128),
    city character varying(128),
    state character varying(32),
    zip character varying(16),
    country character varying(128)
);


ALTER TABLE public.address OWNER TO user_p40;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.categories (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.categories OWNER TO user_p40;

--
-- Name: categoriesdes; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.categoriesdes (
    iddes integer NOT NULL,
    langid character varying(5) NOT NULL,
    label character varying(255) NOT NULL
);


ALTER TABLE public.categoriesdes OWNER TO user_p40;

--
-- Name: cswservercapabilitiesinfo; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.cswservercapabilitiesinfo (
    idfield integer NOT NULL,
    langid character varying(5) NOT NULL,
    field character varying(32) NOT NULL,
    label text
);


ALTER TABLE public.cswservercapabilitiesinfo OWNER TO user_p40;

--
-- Name: customelementset; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.customelementset (
    xpath character varying(1000) NOT NULL,
    xpathhashcode integer NOT NULL
);


ALTER TABLE public.customelementset OWNER TO user_p40;

--
-- Name: email; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.email (
    user_id integer NOT NULL,
    email character varying(128)
);


ALTER TABLE public.email OWNER TO user_p40;

--
-- Name: files; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.files (
    _id integer NOT NULL,
    _content text NOT NULL,
    _mimetype character varying(255) NOT NULL
);


ALTER TABLE public.files OWNER TO user_p40;

--
-- Name: group_category; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.group_category (
    group_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.group_category OWNER TO user_p40;

--
-- Name: groups; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.groups (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    description character varying(255),
    email character varying(32),
    referrer integer,
    logo character varying(255),
    website character varying(255),
    defaultcategory_id integer,
    enablecategoriesrestriction character(1) DEFAULT 'n'::bpchar
);


ALTER TABLE public.groups OWNER TO user_p40;

--
-- Name: groupsdes; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.groupsdes (
    iddes integer NOT NULL,
    langid character varying(5) NOT NULL,
    label character varying(96) NOT NULL
);


ALTER TABLE public.groupsdes OWNER TO user_p40;

--
-- Name: harvesterdata; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.harvesterdata (
    harvesteruuid character varying(255) NOT NULL,
    key character varying(255) NOT NULL,
    value character varying(255) NOT NULL,
    keyvalue character varying(255) NOT NULL
);


ALTER TABLE public.harvesterdata OWNER TO user_p40;

--
-- Name: harvestersettings; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.harvestersettings (
    id integer NOT NULL,
    parentid integer,
    name character varying(64) NOT NULL,
    value text
);


ALTER TABLE public.harvestersettings OWNER TO user_p40;

--
-- Name: harvesthistory; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.harvesthistory (
    id integer NOT NULL,
    harvestdate character varying(30),
    harvesteruuid character varying(250),
    harvestername character varying(128),
    harvestertype character varying(128),
    deleted character(1) DEFAULT 'n'::bpchar NOT NULL,
    info text,
    params text,
    elapsedtime integer
);


ALTER TABLE public.harvesthistory OWNER TO user_p40;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: user_p40
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 40000
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO user_p40;

--
-- Name: inspireatomfeed; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.inspireatomfeed (
    id integer NOT NULL,
    atom text,
    atomdatasetid character varying(255),
    atomdatasetns character varying(255),
    atomurl character varying(255),
    authoremail character varying(255),
    authorname character varying(255),
    lang character varying(3),
    metadataid integer NOT NULL,
    rights character varying(255),
    subtitle character varying(255),
    title character varying(255)
);


ALTER TABLE public.inspireatomfeed OWNER TO user_p40;

--
-- Name: inspireatomfeed_entrylist; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.inspireatomfeed_entrylist (
    inspireatomfeed_id integer NOT NULL,
    crs character varying(255),
    id integer NOT NULL,
    lang character varying(3),
    title character varying(255),
    type character varying(255),
    url character varying(255)
);


ALTER TABLE public.inspireatomfeed_entrylist OWNER TO user_p40;

--
-- Name: isolanguages; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.isolanguages (
    id integer NOT NULL,
    code character varying(3) NOT NULL,
    shortcode character varying(2)
);


ALTER TABLE public.isolanguages OWNER TO user_p40;

--
-- Name: isolanguagesdes; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.isolanguagesdes (
    iddes integer NOT NULL,
    langid character varying(5) NOT NULL,
    label character varying(96) NOT NULL
);


ALTER TABLE public.isolanguagesdes OWNER TO user_p40;

--
-- Name: languages; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.languages (
    id character varying(5) NOT NULL,
    name character varying(32) NOT NULL,
    isinspire character(1) DEFAULT 'n'::bpchar,
    isdefault character(1) DEFAULT 'n'::bpchar
);


ALTER TABLE public.languages OWNER TO user_p40;

--
-- Name: mapservers; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.mapservers (
    id integer NOT NULL,
    configurl character varying(255) NOT NULL,
    description character varying(255),
    name character varying(32) NOT NULL,
    namespace character varying(255),
    namespaceprefix character varying(255),
    password character varying(128),
    stylerurl character varying(255),
    username character varying(128),
    wcsurl character varying(255),
    wfsurl character varying(255),
    wmsurl character varying(255),
    pushstyleinworkspace character(1)
);


ALTER TABLE public.mapservers OWNER TO user_p40;

--
-- Name: metadatacateg; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.metadatacateg (
    metadataid integer NOT NULL,
    categoryid integer NOT NULL
);


ALTER TABLE public.metadatacateg OWNER TO user_p40;

--
-- Name: metadatafiledownloads; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.metadatafiledownloads (
    id integer NOT NULL,
    downloaddate character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    fileuploadid integer NOT NULL,
    metadataid integer NOT NULL,
    requestercomments character varying(255),
    requestermail character varying(255),
    requestername character varying(255),
    requesterorg character varying(255),
    username character varying(255)
);


ALTER TABLE public.metadatafiledownloads OWNER TO user_p40;

--
-- Name: metadatafileuploads; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.metadatafileuploads (
    id integer NOT NULL,
    deleteddate character varying(255),
    filename character varying(255) NOT NULL,
    filesize double precision NOT NULL,
    metadataid integer NOT NULL,
    uploaddate character varying(255) NOT NULL,
    username character varying(255) NOT NULL
);


ALTER TABLE public.metadatafileuploads OWNER TO user_p40;

--
-- Name: metadataidentifiertemplate; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.metadataidentifiertemplate (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    isprovided character(1) DEFAULT 'n'::bpchar NOT NULL,
    template character varying(255) NOT NULL
);


ALTER TABLE public.metadataidentifiertemplate OWNER TO user_p40;

--
-- Name: metadatanotifications; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.metadatanotifications (
    metadataid integer NOT NULL,
    notifierid integer NOT NULL,
    notified character(1) DEFAULT 'n'::bpchar NOT NULL,
    metadatauuid character varying(250) NOT NULL,
    action integer NOT NULL,
    errormsg text
);


ALTER TABLE public.metadatanotifications OWNER TO user_p40;

--
-- Name: metadatanotifiers; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.metadatanotifiers (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    url character varying(255) NOT NULL,
    enabled character(1) DEFAULT 'n'::bpchar NOT NULL,
    username character varying(32),
    password character varying(32)
);


ALTER TABLE public.metadatanotifiers OWNER TO user_p40;

--
-- Name: metadatarating; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.metadatarating (
    metadataid integer NOT NULL,
    ipaddress character varying(32) NOT NULL,
    rating integer NOT NULL
);


ALTER TABLE public.metadatarating OWNER TO user_p40;

--
-- Name: metadatastatus; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.metadatastatus (
    metadataid integer NOT NULL,
    statusid integer DEFAULT 0 NOT NULL,
    userid integer NOT NULL,
    changedate character varying(30) NOT NULL,
    changemessage character varying(2048) NOT NULL
);


ALTER TABLE public.metadatastatus OWNER TO user_p40;

--
-- Name: operations; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.operations (
    id integer NOT NULL,
    name character varying(32) NOT NULL
);


ALTER TABLE public.operations OWNER TO user_p40;

--
-- Name: operationsdes; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.operationsdes (
    iddes integer NOT NULL,
    langid character varying(5) NOT NULL,
    label character varying(96) NOT NULL
);


ALTER TABLE public.operationsdes OWNER TO user_p40;

--
-- Name: params; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.params (
    id integer NOT NULL,
    requestid integer,
    querytype integer,
    termfield character varying(128),
    termtext character varying(128),
    similarity double precision,
    lowertext character varying(128),
    uppertext character varying(128),
    inclusive character(1)
);


ALTER TABLE public.params OWNER TO user_p40;

--
-- Name: regions; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.regions (
    id integer NOT NULL,
    north double precision NOT NULL,
    south double precision NOT NULL,
    west double precision NOT NULL,
    east double precision NOT NULL
);


ALTER TABLE public.regions OWNER TO user_p40;

--
-- Name: regionsdes; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.regionsdes (
    iddes integer NOT NULL,
    langid character varying(5) NOT NULL,
    label character varying(96) NOT NULL
);


ALTER TABLE public.regionsdes OWNER TO user_p40;

--
-- Name: relations; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.relations (
    id integer NOT NULL,
    relatedid integer NOT NULL
);


ALTER TABLE public.relations OWNER TO user_p40;

--
-- Name: requests; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.requests (
    id integer NOT NULL,
    requestdate character varying(30),
    ip character varying(128),
    query text,
    hits integer,
    lang character varying(16),
    sortby character varying(128),
    spatialfilter text,
    type text,
    service character varying(128),
    autogenerated boolean,
    simple boolean
);


ALTER TABLE public.requests OWNER TO user_p40;

--
-- Name: schematron; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.schematron (
    id integer NOT NULL,
    displaypriority integer NOT NULL,
    filename character varying(255) NOT NULL,
    schemaname character varying(255) NOT NULL
);


ALTER TABLE public.schematron OWNER TO user_p40;

--
-- Name: schematroncriteria; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.schematroncriteria (
    id integer NOT NULL,
    type character varying(255) NOT NULL,
    uitype character varying(255),
    uivalue character varying(255),
    value character varying(255) NOT NULL,
    group_name character varying(255) NOT NULL,
    group_schematronid integer NOT NULL
);


ALTER TABLE public.schematroncriteria OWNER TO user_p40;

--
-- Name: schematroncriteriagroup; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.schematroncriteriagroup (
    name character varying(255) NOT NULL,
    schematronid integer NOT NULL,
    requirement character varying(255) NOT NULL
);


ALTER TABLE public.schematroncriteriagroup OWNER TO user_p40;

--
-- Name: schematrondes; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.schematrondes (
    iddes integer NOT NULL,
    label character varying(96) NOT NULL,
    langid character varying(5) NOT NULL
);


ALTER TABLE public.schematrondes OWNER TO user_p40;

--
-- Name: selections; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.selections (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    iswatchable character(1) NOT NULL
);


ALTER TABLE public.selections OWNER TO user_p40;

--
-- Name: selectionsdes; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.selectionsdes (
    iddes integer NOT NULL,
    label character varying(255) NOT NULL,
    langid character varying(5) NOT NULL
);


ALTER TABLE public.selectionsdes OWNER TO user_p40;

--
-- Name: serviceparameter_id_seq; Type: SEQUENCE; Schema: public; Owner: user_p40
--

CREATE SEQUENCE public.serviceparameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.serviceparameter_id_seq OWNER TO user_p40;

--
-- Name: serviceparameters; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.serviceparameters (
    service integer,
    name character varying(64) NOT NULL,
    value character varying(1048) NOT NULL,
    occur character varying(1) DEFAULT '+'::character varying,
    id integer NOT NULL
);


ALTER TABLE public.serviceparameters OWNER TO user_p40;

--
-- Name: services; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.services (
    id integer NOT NULL,
    name character varying(64) NOT NULL,
    class character varying(1048) NOT NULL,
    description character varying(1048),
    explicitquery character varying(255)
);


ALTER TABLE public.services OWNER TO user_p40;

--
-- Name: settings; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.settings (
    name character varying(512) NOT NULL,
    value text,
    datatype integer,
    "position" integer,
    internal character varying(1)
);


ALTER TABLE public.settings OWNER TO user_p40;

--
-- Name: sources; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.sources (
    uuid character varying(250) NOT NULL,
    name character varying(250),
    islocal character(1) DEFAULT 'y'::bpchar
);


ALTER TABLE public.sources OWNER TO user_p40;

--
-- Name: sourcesdes; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.sourcesdes (
    iddes character varying(255) NOT NULL,
    label character varying(96) NOT NULL,
    langid character varying(5) NOT NULL
);


ALTER TABLE public.sourcesdes OWNER TO user_p40;

--
-- Name: spatialindex; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.spatialindex (
    fid integer NOT NULL,
    id character varying(250),
    the_geom public.geometry,
    CONSTRAINT enforce_dims_the_geom CHECK ((public.st_ndims(the_geom) = 2)),
    CONSTRAINT enforce_geotype_the_geom CHECK (((public.geometrytype(the_geom) = 'MULTIPOLYGON'::text) OR (the_geom IS NULL))),
    CONSTRAINT enforce_srid_the_geom CHECK ((public.st_srid(the_geom) = 4326))
);


ALTER TABLE public.spatialindex OWNER TO user_p40;

--
-- Name: statusvalues; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.statusvalues (
    id integer NOT NULL,
    name character varying(32) NOT NULL,
    reserved character(1) DEFAULT 'n'::bpchar NOT NULL,
    displayorder integer
);


ALTER TABLE public.statusvalues OWNER TO user_p40;

--
-- Name: statusvaluesdes; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.statusvaluesdes (
    iddes integer NOT NULL,
    langid character varying(5) NOT NULL,
    label character varying(96) NOT NULL
);


ALTER TABLE public.statusvaluesdes OWNER TO user_p40;

--
-- Name: thesaurus; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.thesaurus (
    id character varying(250) NOT NULL,
    activated character varying(1)
);


ALTER TABLE public.thesaurus OWNER TO user_p40;

--
-- Name: useraddress; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.useraddress (
    userid integer NOT NULL,
    addressid integer NOT NULL
);


ALTER TABLE public.useraddress OWNER TO user_p40;

--
-- Name: usergroups; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.usergroups (
    userid integer NOT NULL,
    groupid integer NOT NULL,
    profile integer NOT NULL
);


ALTER TABLE public.usergroups OWNER TO user_p40;

--
-- Name: users; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying(256) NOT NULL,
    password character varying(120) NOT NULL,
    surname character varying(32),
    name character varying(32),
    profile integer NOT NULL,
    organisation character varying(128),
    kind character varying(16),
    security character varying(128) DEFAULT ''::character varying,
    authtype character varying(32),
    lastlogindate character varying(255),
    nodeid character varying(255),
    isenabled character(1) DEFAULT 'y'::bpchar NOT NULL
);


ALTER TABLE public.users OWNER TO user_p40;

--
-- Name: usersavedselections; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.usersavedselections (
    metadatauuid character varying(255) NOT NULL,
    selectionid integer NOT NULL,
    userid integer NOT NULL
);


ALTER TABLE public.usersavedselections OWNER TO user_p40;

--
-- Name: validation; Type: TABLE; Schema: public; Owner: user_p40
--

CREATE TABLE public.validation (
    metadataid integer NOT NULL,
    valtype character varying(40) NOT NULL,
    status integer,
    tested integer,
    failed integer,
    valdate character varying(30),
    required boolean
);


ALTER TABLE public.validation OWNER TO user_p40;

--
-- Name: harves_opendata_node pk_node_id; Type: DEFAULT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.harves_opendata_node ALTER COLUMN pk_node_id SET DEFAULT nextval('catalogue.harves_opendata_node_pk_node_id_seq'::regclass);


--
-- Name: harves_opendata_node_params pk_params_id; Type: DEFAULT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.harves_opendata_node_params ALTER COLUMN pk_params_id SET DEFAULT nextval('catalogue.harves_opendata_node_params_pk_params_id_seq'::regclass);


--
-- Name: harves_opendata_node_params fk_node_id; Type: DEFAULT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.harves_opendata_node_params ALTER COLUMN fk_node_id SET DEFAULT nextval('catalogue."harves_opendata_node_params_FK_node_id_seq"'::regclass);


--
-- Name: standards_fournisseur fournisseur_id; Type: DEFAULT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.standards_fournisseur ALTER COLUMN fournisseur_id SET DEFAULT nextval('catalogue.standards_fournisseur_fournisseur_id_seq'::regclass);


--
-- Name: standards_standard standard_id; Type: DEFAULT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.standards_standard ALTER COLUMN standard_id SET DEFAULT nextval('catalogue.standards_standard_standard_id_seq'::regclass);


--
-- Data for Name: bdterr_champ_type; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_champ_type (champtype_id, champtype_nom, champtype_incontenutier) FROM stdin;
2	text	t
4	url	t
5	Image	t
6	vidéo	t
3	date	f
\.


--
-- Data for Name: bdterr_contenus_tiers; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_contenus_tiers (contenu_id, lot_id, contenu_type, contenu_nom, contenu_ordre, contenu_fiche, contenu_resultats, contenu_url) FROM stdin;
\.


--
-- Data for Name: bdterr_couche_champ; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_couche_champ (champ_id, champ_lot_id, champ_type, champ_nom, champ_alias, champ_ordre, champ_format, champ_fiche, champ_resultats, champ_identifiant, champ_label) FROM stdin;
\.


--
-- Data for Name: bdterr_donnee; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_donnee (donnee_id, donnee_lot_id, donnee_objet_id, donnee_objet_nom, donnee_geom, donnee_geom_json, donnee_attributs_fiche, donnee_attributs_resultat, donnee_ressources_resultat, donnee_ressources_fiche, donnee_insee, donnee_insee_delegue) FROM stdin;
\.


--
-- Data for Name: bdterr_lot; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_lot (lot_id, lot_theme_id, lot_referentiel_id, lot_uuid, lot_table, lot_alias, lot_carte, lot_avertissement_message, lot_statistiques, lot_visualiseur, lot_date_maj, lot_statistic, lot_layer_name) FROM stdin;
\.


--
-- Data for Name: bdterr_rapports; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_rapports (rapport_id, rapport_nom, rapport_gabarit_html, rapport_gabarit_odt) FROM stdin;
\.


--
-- Data for Name: bdterr_rapports_profils; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_rapports_profils (id, bterr_rapport_id, prodige_profil_id) FROM stdin;
\.


--
-- Data for Name: bdterr_referentiel_carto; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_referentiel_carto (referentiel_id, referentiel_table, referentiel_champs) FROM stdin;
\.


--
-- Name: bdterr_referentiel_carto_referentiel_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.bdterr_referentiel_carto_referentiel_id_seq', 1, false);


--
-- Data for Name: bdterr_referentiel_intersection; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_referentiel_intersection (referentiel_id, referentiel_table, referentiel_insee, referentiel_insee_deleguee, referentiel_nom) FROM stdin;
\.


--
-- Data for Name: bdterr_referentiel_recherche; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_referentiel_recherche (referentiel_id, referentiel_table, referentiel_insee, referentiel_insee_delegue, referentiel_commune, referentiel_nom, referentiel_api_champs) FROM stdin;
1						
\.


--
-- Data for Name: bdterr_themes; Type: TABLE DATA; Schema: bdterr; Owner: user_p40
--

COPY bdterr.bdterr_themes (theme_id, theme_parent, theme_nom, theme_alias, theme_ordre) FROM stdin;
0	\N	root	\N	0
\.


--
-- Name: champ_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.champ_id_seq', 1, false);


--
-- Name: champtype_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.champtype_id_seq', 1, false);


--
-- Name: contenu_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.contenu_id_seq', 1, false);


--
-- Name: donnee_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.donnee_id_seq', 1, false);


--
-- Name: lot_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.lot_id_seq', 1, false);


--
-- Name: rapport_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.rapport_id_seq', 1, false);


--
-- Name: rapports_profils_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.rapports_profils_id_seq', 1, false);


--
-- Name: referentiel_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.referentiel_id_seq', 1, false);


--
-- Name: referentiel_recherche_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.referentiel_recherche_id_seq', 1, false);


--
-- Name: theme_id_seq; Type: SEQUENCE SET; Schema: bdterr; Owner: user_p40
--

SELECT pg_catalog.setval('bdterr.theme_id_seq', 1, false);


--
-- Data for Name: acces_serveur; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.acces_serveur (pk_acces_serveur, ts, accs_id, accs_adresse, path_consultation, path_administration, path_carte_statique, accs_adresse_admin, accs_adresse_tele, accs_login_admin, accs_pwd_admin, accs_service_admin, accs_adresse_data) FROM stdin;
1	2007-02-28 11:07:04.150759	Serveur cartographique régional	carto-p40.alkante.com	/	/HTML_PRIVATE/index.php?map=	/CartesStatiques/	admincarto-p40.alkante.com	telecarto-p40.alkante.com	cartes	cartes	1	datacarto-p40.alkante.com
\.


--
-- Data for Name: carte_projet; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.carte_projet (pk_carte_projet, ts, cartp_id, cartp_nom, cartp_description, cartp_format, cartp_fk_stockage_carte, cartp_fk_fiche_metadonnees, cartp_utilisateur, cartep_etat, cartp_administrateur, cartp_utilisateur_email, cartp_wms) FROM stdin;
174	2009-03-06 17:53:49.462071	1_174_limites_administratives	limites_administratives	limites_administratives	0	174	331	\N	0	\N	\N	0
\.


--
-- Data for Name: competence; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.competence (pk_competence_id, competence_nom) FROM stdin;
\.


--
-- Data for Name: competence_accede_carte; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.competence_accede_carte (pk_competence_accede_carte, competencecarte_fk_competence_id, competencecarte_fk_carte_projet) FROM stdin;
\.


--
-- Data for Name: competence_accede_couche; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.competence_accede_couche (pk_competence_accede_couche, competencecouche_fk_competence_id, competencecouche_fk_couche_donnees) FROM stdin;
\.


--
-- Data for Name: couche_donnees; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.couche_donnees (pk_couche_donnees, ts, couchd_id, couchd_nom, couchd_description, couchd_type_stockage, couchd_emplacement_stockage, couchd_fk_acces_server, couchd_wms, couchd_wfs, couchd_download, couchd_restriction, couchd_restriction_exclusif, couchd_zonageid_fk, couchd_restriction_buffer, couchd_visualisable, couchd_extraction_attributaire, couchd_extraction_attributaire_champ, changedate, couchd_alert_msg_id, couchd_wfs_uuid, couchd_wms_sdom) FROM stdin;
269	2009-03-06 17:38:25.8314	1_269	departements	departements	1	departements	1	0	0	0	0	0	\N	\N	1	0		2017-04-26 11:00:16.037012+02	\N	\N	\N
\.


--
-- Data for Name: domaine; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.domaine (pk_domaine, ts, dom_id, dom_nom, dom_description, dom_rubric) FROM stdin;
48	2009-03-06 14:40:21.466063	Demonstration	Demonstration	D&amp;eacute;monstration	14
\.


--
-- Data for Name: execution_report; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.execution_report (id, scheduled_command_id, date_time, file, status) FROM stdin;
\.


--
-- Name: execution_report_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.execution_report_id_seq', 1, false);


--
-- Data for Name: fiche_metadonnees; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.fiche_metadonnees (pk_fiche_metadonnees, ts, fmeta_id, fmeta_description, fmeta_fk_couche_donnees, statut, schema) FROM stdin;
330	2009-03-06 17:38:41.802036	9	\N	269	4	\N
331	2009-03-06 17:54:02.250734	10	\N	\N	4	\N
\.


--
-- Data for Name: groupe_profil; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.groupe_profil (pk_groupe_profil, ts, grp_id, grp_nom, grp_description, grp_is_default_installation, grp_nom_ldap) FROM stdin;
200	2007-02-26 16:58:27.117093	ADM-PRODIGE	Administrateur Prodige	Administrateur principal de l'application	1	\N
219	2012-04-11 12:19:46.123956	Internet	Internet	Internet	1	\N
\.


--
-- Data for Name: grp_accede_competence; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.grp_accede_competence (pk_grp_accede_competence, fk_grp_id, fk_competence_id) FROM stdin;
\.


--
-- Data for Name: grp_accede_dom; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.grp_accede_dom (pk_grp_accede_dom, ts, grpdom_fk_domaine, grpdom_fk_groupe_profil) FROM stdin;
102	2009-03-06 15:00:07.863653	48	200
\.


--
-- Data for Name: grp_accede_ssdom; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.grp_accede_ssdom (pk_grp_accede_ssdom, ts, grpss_fk_sous_domaine, grpss_fk_groupe_profil) FROM stdin;
7	2009-03-06 15:01:05.886418	217	200
\.


--
-- Data for Name: grp_autorise_trt; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.grp_autorise_trt (pk_grp_autorise_trt, ts, grptrt_fk_traitement, grptrt_fk_groupe_profil) FROM stdin;
9	2007-02-26 17:09:47.800647	3	200
10	2007-02-26 17:09:48.392514	2	200
12	2007-02-26 17:17:51.040256	7	200
13	2007-02-26 17:17:52.077436	4	200
14	2007-02-26 17:17:57.586974	1	200
\.


--
-- Data for Name: grp_regroupe_usr; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.grp_regroupe_usr (pk_grp_regroupe_usr, ts, grpusr_fk_groupe_profil, grpusr_fk_utilisateur, grpusr_is_principal) FROM stdin;
1	2007-03-02 11:20:54.78394	200	1	0
59	2012-04-11 12:19:46.124562	219	40	0
\.


--
-- Data for Name: grp_trt_objet; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.grp_trt_objet (pk_grp_trt_objet, grptrtobj_fk_grp_id, grptrtobj_fk_trt_id, grptrtobj_fk_objet_id, grptrtobj_fk_obj_type_id, grp_trt_objet_status) FROM stdin;
\.


--
-- Data for Name: harves_opendata_node; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.harves_opendata_node (pk_node_id, node_name, node_logo, api_url, node_last_modification, url_dcat, node_log_file, command_id) FROM stdin;
\.


--
-- Data for Name: harves_opendata_node_params; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.harves_opendata_node_params (pk_params_id, fk_node_id, key, value) FROM stdin;
\.


--
-- Name: harves_opendata_node_params_FK_node_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue."harves_opendata_node_params_FK_node_id_seq"', 1, false);


--
-- Name: harves_opendata_node_params_pk_params_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.harves_opendata_node_params_pk_params_id_seq', 1, false);


--
-- Name: harves_opendata_node_pk_node_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.harves_opendata_node_pk_node_id_seq', 1, false);


--
-- Data for Name: objet_type; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.objet_type (pk_objet_type_id, objet_type_nom) FROM stdin;
1	couche
2	carte
4	modéle
5	groupe
\.


--
-- Data for Name: perimetre; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.perimetre (pk_perimetre_id, perimetre_code, perimetre_nom, perimetre_zonage_id) FROM stdin;
\.


--
-- Data for Name: prodige_carto_colors; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_carto_colors (prodige_carto_color_id, color_theme, color_white, color_light, color_dark, color_light_grey, color_tables, color_grey, color_theme_extjs) FROM stdin;
1	Bleu	e8e8ff	c8d2dc	114f88	87a6c3	003399	56779a	blue
2	Vert	edffe8	cbdcc8	198811	87c387	07461e	5b9a56	olive
3	Rouge	ffefeb	dccfc8	883011	c38787	b50000	9a5656	peppermint
4	Orange	fff3e8	dcd4c8	885711	c3a887	e17a00	9a7e56	orange
5	Violet	ffebff	dbc8dc	701188	bb87c3	3a046d	9a5696	purple
6	Marron	fff7eb	dcd1c8	884911	c3a387	422f05	9a7e56	brown
7	Gris	f1f1f1	d7d7d7	878787	c3c0be	000000	969593	grey
\.


--
-- Name: prodige_carto_colors_prodige_carto_color_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.prodige_carto_colors_prodige_carto_color_id_seq', 7, true);


--
-- Data for Name: prodige_colors; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_colors (prodige_color_id, color_theme, color_back, color_tables, color_text, color_text_light, volet_url, css_file) FROM stdin;
1	Bleu	#f5f9ff	#003399	#15428b	#333333	volet_bleu.png	\N
2	Vert	#f8ffeb	#759341	#324b06	#333333	volet_vert.png	CSS/olive/css/xtheme-olive.css
3	Rouge	#fff2f2	#FF5353	#a40e0e	#333333	volet_rouge.png	CSS/xtheme-peppermint/xtheme-peppermint.css
4	Violet	#fbfaff	#84d0ef	#260496	#333333	volet_violet.png	CSS/PurpleTheme/css/xtheme-purple.css
5	Marron	#ffffee	#422f05	#913006	#333333	volet_marron.png	CSS/xtheme-light-brown/css/xtheme-light-brown.css
6	Gris	#f1f1f1	#000000	#636060	#333333	volet_gris.png	CSS/DarkGrayTheme/css/xtheme-darkgray.css
7	Orange	#fff9ee	#e17a00	#e58700	#333333	volet_orange.png	CSS/xtheme-light-orange/css/xtheme-light-orange.css
\.


--
-- Name: prodige_colors_prodige_color_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.prodige_colors_prodige_color_id_seq', 7, true);


--
-- Data for Name: prodige_database_requests; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_database_requests (prodige_database_request_id, request_description, sql_request, viewname) FROM stdin;
\.


--
-- Name: prodige_database_requests_prodige_database_request_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.prodige_database_requests_prodige_database_request_id_seq', 47, true);


--
-- Data for Name: prodige_external_access; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_external_access (pk_prodige_external_access, prodige_extaccess_referer, prodige_extaccess_fk_utilisateur) FROM stdin;
\.


--
-- Data for Name: prodige_fonts; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_fonts (font_id, font1, font2, font3) FROM stdin;
1	Arial	Helvetica	sans-serif
2	Bookman Old Style	Helvetica	sans-serif
3	Comic Sans MS	monospace	sans-serif
4	Garamond	Helvetica	sans-serif
5	Impact	Charcoal	sans-serif
6	Palatino Linotype	Book Antiqua	Palatino
7	Symbol	Helvetica	sans-serif
8	Tahoma	Geneva	sans-serif
9	Times New Roman	Times	serif
10	Trebuchet MS	Helvetica	sans-serif
\.


--
-- Name: prodige_fonts_font_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.prodige_fonts_font_id_seq', 10, true);


--
-- Data for Name: prodige_geosource_colors; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_geosource_colors (prodige_geosource_color_id, geosource_color_theme, color_commontext, color_a, color_a_hover, color_left_menu_context, color_border, color_background_content, color_left_menu_text) FROM stdin;
1	Bleu	0263b2	064377	025090	b9d3f5	266397	eff4fa	15428b
2	Vert	105f2f	105f2f	07461e	92c95d	07461e	e8f9d7	000000
3	Rouge	822626	822626	b50000	ffa8a8	b50000	ffeeee	000000
4	Orange	915003	915003	e17a00	ffcc90	e17a00	fef0df	a03e00
5	Violet	490b82	490b82	3a046d	d2a2ff	3a046d	f2edff	000000
6	Marron	523d11	523d11	422f05	e3b98c	422f05	f2e9e0	a2753f
7	Gris	424242	424242	000000	888888	000000	ececec	ffffff
\.


--
-- Name: prodige_geosource_colors_prodige_geosource_color_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.prodige_geosource_colors_prodige_geosource_color_id_seq', 7, true);


--
-- Data for Name: prodige_help; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_help (pk_prodige_help_id, prodige_help_title, prodige_help_desc, prodige_help_url, prodige_help_row_number) FROM stdin;
4	Manuel de l&#039;outil de consultation de cartes	Ce manuel pr&eacute;sente les fonctions du module de consultation de cartes.	./ressources/Manuel Outil de consultation cartographique.pdf	0
5	Manuel de l&#039;outil d&#039;administration de cartes	Ce manuel pr&eacute;sente les fonctions du module d&#039;administration de cartes.	./ressources/Manuel Outil d&#039;administration cartographique.pdf	1
2	Manuel de l&#039;outil de gestion des m&eacute;tadonn&eacute;es	Ce manuel pr&eacute;sente les fonctions d&#039;administration et de consultation du catalogue.	./ressources/Manuel Outil de gestion des metadonnees.pdf	2
\.


--
-- Data for Name: prodige_help_group; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_help_group (hlpgrp_fk_help_id, hlpgrp_fk_groupe_profil, pk_prodige_help_group) FROM stdin;
4	200	1
4	219	2
5	200	3
2	200	4
\.


--
-- Data for Name: prodige_param; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_param (pk_prodige_param, prodige_title, prodige_subtitle, prodige_catalog_intro, logo_url, top_image_url, font_id, prodige_color_id, prodige_geosource_color_id, prodige_carto_color_id, logo_right_url) FROM stdin;
1	PRODIGE	Plate-forme mutualisée pour le partage d'information géographique	PRODIGE	Logo_Prodige_V0.jpg	bandeau_bleu.png	1	1	1	1	\N
\.


--
-- Name: prodige_param_pk_prodige_param_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.prodige_param_pk_prodige_param_seq', 1, true);


--
-- Name: prodige_server_server_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.prodige_server_server_id_seq', 2, true);


--
-- Data for Name: prodige_session_user; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_session_user (pk_session_user, date_connexion) FROM stdin;
\.


--
-- Data for Name: prodige_settings; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) FROM stdin;
4	PRO_TIMEOUT_TELE_DIRECT	\N	18000	Timeout du service de téléchargement (en ms)	textfield	\N
5	PRO_IS_MAJIC_ACTIF	\N	off	Activation du module MAJIC	checkboxfield	\N
8	PRO_IS_CARTEPERSO_ACTIF	\N	off	Activation du module carte personnelle	checkboxfield	\N
9	PRO_IMPORT_EPSG	Projection principale de la plateforme	2154	Projection principale de la plateforme (code EPSG)	textfield	\N
6	PRO_RASTER_INFO_ECW_SIZE_LIMIT	\N	500000000	Taille maximale des fichiers Raster ECW téléchargés (en octet)	textfield	\N
10	PRO_IS_REQ_JOINTURES_ACTIF	\N	off	Activation des requêtes et jointures	checkboxfield	\N
7	PRO_RASTER_INFO_GTIFF_SIZE_LIMIT	NULL	100000000000	Taille maximale des fichiers Raster GeoTIFF téléchargés (en octet)	textfield	\N
23	PRO_CATALOGUE_EMAIL_ADMIN	\N	admin@prodige.fr	Adresse mail d'administration	textfield	\N
22	PRO_CATALOGUE_NOM_EMAIL_ADMIN	\N	Administrateur Prodige	Nom de l'expéditeur des mails d'administration	textfield	\N
21	PRO_CATALOGUE_EMAIL_AUTO	\N	no_reply@prodige.fr	Adresse des mails automatiques	textfield	\N
20	PRO_CATALOGUE_NOM_EMAIL_AUTO	\N	No reply	Nom de l'expéditeur des mails automatiques	textfield	\N
19	PRO_CATALOGUE_NB_SESSION_USER	\N	off	Afficher le nombre de visiteur	checkboxfield	\N
18	PRO_CATALOGUE_CONTACT_ADMIN	\N	off	Possibilité de contacter un admininstrateur	checkboxfield	\N
17	PRO_GEONETWORK_LIST_CATALOGUE	\N		Liste des catalogues	multiselectfield	uuid,name;public.sources
16	PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL	\N		Url du fichier de licence	textfield	\N
15	PRO_CATALOGUE_TELECHARGEMENT_LICENCE	\N	off	Affichage d'une licence dans l'interface de téléchargement	checkboxfield	\N
24	PRO_ACTIVE_CARTE_PERSO	\N	off	Active le module cartes personnelles pour l'internaute	checkboxfield	\N
25	PRO_WMS_METADATA_ID	\N	40020	Identifiant de la métadonnée de service WMS	textfield	\N
26	PRO_WFS_METADATA_ID	\N	40021	Identifiant de la métadonnée de service WFS	textfield	\N
27	PRO_DOWNLOAD_METADATA_ID	\N	40022	Identifiant de la métadonnée de service de téléchargement libre	textfield	\N
28	PRO_PUBLIPOSTAGE	\N	off	Module publipostage activé	checkboxfield	\N
29	PRO_EDITION	\N	off	Module édition en ligne activé	checkboxfield	\N
30	PRO_MODULE_STANDARDS	Module qualité activé	off	Module qualité activé	checkboxfield	\N
31	PRO_MODULE_BASE_TERRITORIALE	Module Base territoriale activé	off	Module Base territoriale activé	checkboxfield	\N
32	PRO_HARVES_DCAT_ORGANIZATIONS	\N	https://www.data.gouv.fr/api/1/organizations/	Définit l'url d'API fournissant la liste des organisations disponibles	textfield	\N
33	PRO_IS_OPENDATA_ACTIF	\N	off	Module opendata activé	checkboxfield	\N
34	PRO_IS_RAWGRAPH_ACTIF	\N	off	Module graphe activé	checkboxfield	\N
35	PRO_NONGEO_METADATA_ID	\N	40787	Identifiant de la métadonnée de série de données non géographiques modèle	textfield	\N
\.


--
-- Data for Name: raster_info; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.raster_info (id, name, srid, ulx, uly, lrx, lry, bands, cols, rows, resx, resy, format, vrt_path, compression, color_interp) FROM stdin;
\.


--
-- Data for Name: rawgraph_couche_config; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.rawgraph_couche_config (id, titre, resume, serie_donnees_uuid, couche, colonnes, uuid, config) FROM stdin;
\.


--
-- Data for Name: rubric_param; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.rubric_param (rubric_id, rubric_name) FROM stdin;
14	PRODIGE
\.


--
-- Name: rubric_param_rubric_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.rubric_param_rubric_id_seq', 13, true);


--
-- Data for Name: scheduled_command; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.scheduled_command (id, name, command, arguments, cron_expression, last_execution, last_return_code, log_file, priority, execute_immediately, disabled, locked, is_periodic, year, frequency) FROM stdin;
\.


--
-- Name: scheduled_command_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.scheduled_command_id_seq', 1, false);


--
-- Name: seq_acces_serveur; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_acces_serveur', 1, true);


--
-- Name: seq_carte_projet; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_carte_projet', 171, true);


--
-- Name: seq_commune; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_commune', 1, false);


--
-- Name: seq_competence; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_competence', 2, true);


--
-- Name: seq_competence_accede_carte; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_competence_accede_carte', 1, false);


--
-- Name: seq_competence_accede_couche; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_competence_accede_couche', 1, false);


--
-- Name: seq_couche_donnees; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_couche_donnees', 266, true);


--
-- Name: seq_departement; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_departement', 1, false);


--
-- Name: seq_dico; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_dico', 1, false);


--
-- Name: seq_dico_majic2; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_dico_majic2', 1, false);


--
-- Name: seq_domaine; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_domaine', 39, true);


--
-- Name: seq_export; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_export', 1, false);


--
-- Name: seq_fiche_metadonnees; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_fiche_metadonnees', 326, true);


--
-- Name: seq_groupe_profil; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_groupe_profil', 219, true);


--
-- Name: seq_grp_accede_competence; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_grp_accede_competence', 1, false);


--
-- Name: seq_grp_accede_dom; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_grp_accede_dom', 100, true);


--
-- Name: seq_grp_accede_ssdom; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_grp_accede_ssdom', 6, true);


--
-- Name: seq_grp_autorise_trt; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_grp_autorise_trt', 116, true);


--
-- Name: seq_grp_regroupe_usr; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_grp_regroupe_usr', 59, true);


--
-- Name: seq_grp_trt_objet; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_grp_trt_objet', 50, true);


--
-- Name: seq_incoherence_perso; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_incoherence_perso', 0, false);


--
-- Name: seq_mapfile; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_mapfile', 1, false);


--
-- Name: seq_mcd_field; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_mcd_field', 1, false);


--
-- Name: seq_mcd_layer; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_mcd_layer', 1, false);


--
-- Name: seq_menu; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_menu', 1, false);


--
-- Name: seq_objet_type; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_objet_type', 1, false);


--
-- Name: seq_perimetre; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_perimetre', 1, false);


--
-- Name: seq_prodige_external_access; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_prodige_external_access', 1, false);


--
-- Name: seq_prodige_help; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_prodige_help', 5, true);


--
-- Name: seq_prodige_help_group; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_prodige_help_group', 4, true);


--
-- Name: seq_prodige_settings; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_prodige_settings', 35, true);


--
-- Name: seq_profil; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_profil', 1, false);


--
-- Name: seq_rawgraph_couche_config; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_rawgraph_couche_config', 1, false);


--
-- Name: seq_sous_domaine; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_sous_domaine', 215, true);


--
-- Name: seq_ssdom_dispose_couche; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_ssdom_dispose_couche', 365, true);


--
-- Name: seq_ssdom_dispose_metadata; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_ssdom_dispose_metadata', 2, true);


--
-- Name: seq_ssdom_visualise_carte; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_ssdom_visualise_carte', 200, true);


--
-- Name: seq_statistique; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_statistique', 1, false);


--
-- Name: seq_stockage_carte; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_stockage_carte', 171, true);


--
-- Name: seq_tache_import_donnees; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_tache_import_donnees', 1, false);


--
-- Name: seq_traitement; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_traitement', 200, false);


--
-- Name: seq_trt_autorise_objet; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_trt_autorise_objet', 1, false);


--
-- Name: seq_trt_objet; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_trt_objet', 1, false);


--
-- Name: seq_usr_accede_perimetre; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_usr_accede_perimetre', 1, false);


--
-- Name: seq_usr_accede_perimetre_edition; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_usr_accede_perimetre_edition', 1, false);


--
-- Name: seq_usr_alerte_perimetre_edition; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_usr_alerte_perimetre_edition', 1, false);


--
-- Name: seq_utilisateur; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_utilisateur', 40, true);


--
-- Name: seq_utilisateur_carte; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_utilisateur_carte', 1, false);


--
-- Name: seq_utilisateur_mapfile; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_utilisateur_mapfile', 1, false);


--
-- Name: seq_zonage; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.seq_zonage', 1, false);


--
-- Data for Name: sous_domaine; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.sous_domaine (pk_sous_domaine, ts, ssdom_id, ssdom_nom, ssdom_description, ssdom_fk_domaine, ssdom_admin_fk_groupe_profil, ssdom_createur_fk_groupe_profil, ssdom_editeur_fk_groupe_profil, ssdom_service_wms, ssdom_service_wms_uuid) FROM stdin;
217	2009-03-06 14:57:44.783952	France	France	France	48	200	200	\N	0	\N
\.


--
-- Data for Name: ssdom_dispose_couche; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.ssdom_dispose_couche (pk_ssdom_dispose_couche, ts, ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine) FROM stdin;
368	2009-03-06 17:38:41.802036	269	217
\.


--
-- Data for Name: ssdom_dispose_metadata; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.ssdom_dispose_metadata (pk_ssdom_dispose_metadata, ts, uuid, ssdcouch_fk_sous_domaine) FROM stdin;
1	2017-04-27 10:55:19.445171	3c892ef0-0a6d-11de-ad6b-00104b7907b4	217
2	2017-04-27 10:55:19.445171	617af7f0-0a6f-11de-ad6b-00104b7907b4	217
\.


--
-- Data for Name: ssdom_visualise_carte; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.ssdom_visualise_carte (pk_ssdom_visualise_carte, ts, ssdcart_fk_carte_projet, ssdcart_fk_sous_domaine) FROM stdin;
202	2009-03-06 17:54:02.315995	174	217
\.


--
-- Data for Name: standards_conformite; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.standards_conformite (metadata_id, standard_id, conformite_prefix, conformite_suffix, conformite_success, conformite_report_url) FROM stdin;
\.


--
-- Data for Name: standards_fournisseur; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.standards_fournisseur (fournisseur_id, fournisseur_name, fournisseur_url) FROM stdin;
1	Prodige	prodige.org
\.


--
-- Name: standards_fournisseur_fournisseur_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.standards_fournisseur_fournisseur_id_seq', 1, true);


--
-- Data for Name: standards_standard; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.standards_standard (standard_id, fournisseur_id, standard_name, standard_url, standard_database, standard_schema, fmeta_id) FROM stdin;
\.


--
-- Name: standards_standard_standard_id_seq; Type: SEQUENCE SET; Schema: catalogue; Owner: user_p40
--

SELECT pg_catalog.setval('catalogue.standards_standard_standard_id_seq', 1, false);


--
-- Data for Name: stockage_carte; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.stockage_carte (pk_stockage_carte, ts, stkcard_id, stkcard_path, stk_server) FROM stdin;
174	2009-03-06 17:53:49.462071	1_174_limites_administratives	limites_administratives.map	1
\.


--
-- Data for Name: tache_import_donnees; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.tache_import_donnees (pk_tache_import_donnees, uuid, pk_couche_donnees, user_id, import_date_request, import_date_execution, import_type, import_frequency, import_table, import_data_type, import_data_source, import_extra_params) FROM stdin;
\.


--
-- Data for Name: test_requests; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.test_requests (test_id, test_description) FROM stdin;
\.


--
-- Data for Name: traitement; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) FROM stdin;
1	2006-05-15 10:32:20.274767	TELECHARGEMENT	Acc&egrave;s &agrave; l'outil de t&eacute;l&eacute;chargement	Permet le t&eacute;l&eacute;chargement de donn&eacute;es g&eacute;ographiques	f	t	f
4	2006-05-15 10:32:20.285331	CMS	Acc&egrave;s &agrave; l'outil de cr&eacute;ation / modification / suppression des m&eacute;tadonn&eacute;es	Permet l'administration des fiches de m&eacute;tadonn&eacute;es	t	f	f
2	2006-05-15 10:32:20.279317	NAVIGATION	Acc&egrave;s &agrave; l'outil de navigation cartographique	Permet de consulter les cartes	f	t	f
3	2006-05-15 10:32:20.282272	ADMINISTRATION	Acc&egrave;s &agrave; l'outil d'administration	Gestion des droits PRODIGE.	f	f	f
7	2006-05-15 10:32:20.293369	PARAMETRAGE	Acc&egrave;s &agrave; l'outil de param&eacute;trage des cartes projets.	Permet d'administrer les cartes	t	f	f
8	2010-07-29 17:46:20.53123	MAJIC	Acc&egrave;s &agrave; l'outil d'administration des donn&eacute;es MAJIC.	Permet d'administrer les donn&eacute;es MAJIC	t	f	f
9	2010-11-23 17:46:20.53123	CARTEPERSO	Acc&egrave;s &agrave; l'outil Carte Personnelle	Permet de cr&eacute;er des cartes personnelles	f	f	f
10	2017-04-26 11:03:07.476403	PUBLIPOSTAGE	Acc&egrave;s &agrave; l'outil de publipostage	Permet d'envoyer des mailing &agrave; partir d'informations attributaires	f	t	f
11	2017-04-26 11:03:07.478022	EDITION EN LIGNE	Acc&egrave;s &agrave; l'outil d'édition de donnée	Permet de modifier en ligne des couches de type point, ligne et polygone	t	t	t
12	2017-04-26 11:03:07.485083	EDITION EN LIGNE (AJOUT)	Acc&egrave;s &agrave; l'outil d'édition de donnée (fonction d'ajout)	Permet de créer en ligne des objets de type point, ligne et polygone	t	t	t
13	2017-04-27 10:55:19.459418	EDITION EN LIGNE (MODIF)	l'outil de modification de donnée (fonction de modification)	Permet de modifier en ligne des objets	t	t	t
\.


--
-- Data for Name: trt_autorise_objet; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.trt_autorise_objet (pk_trt_autorise_objet, trtautoobjet_fk_trt_id, trtautoobjet_fk_obj_type_id) FROM stdin;
1	1	1
2	2	1
3	2	2
13	7	5
15	9	1
16	10	1
17	11	1
\.


--
-- Data for Name: trt_objet; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.trt_objet (pk_trt_objet, trt_id, trt_nom) FROM stdin;
1	TELECHARGEMENT	Acc&egrave;s &agrave; l'outil de t&eacute;l&eacute;chargement  
2	CONSULTATION	Acc&egrave;s &agrave; l'outil de consultation
7	ADMINISTRATION	Acc&egrave;s &agrave; l'outil d'administration
8	SUPPRESSION	Acc&egrave;s &agrave; l'outil de supression
9	EDITION	Acc&egrave;s &agrave; l'outil d'édition de donnée
10	EDITION (AJOUT)	Acc&egrave;s &agrave; l'outil d'édition de donnée (fonction d'ajout)
11	EDITION EN LIGNE (MODIF)	Acc&egrave;s &agrave; l'outil modification de donnée (fonction de modification)
\.


--
-- Data for Name: usr_accede_perimetre; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.usr_accede_perimetre (pk_usr_accede_perimetre, ts, usrperim_fk_utilisateur, usrperim_fk_perimetre) FROM stdin;
\.


--
-- Data for Name: usr_accede_perimetre_edition; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.usr_accede_perimetre_edition (pk_usr_accede_perimetre_edition, ts, usrperim_fk_utilisateur, usrperim_fk_perimetre) FROM stdin;
\.


--
-- Data for Name: usr_alerte_perimetre_edition; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.usr_alerte_perimetre_edition (pk_usr_alerte_perimetre_edition, ts, usralert_fk_utilisateur, usralert_fk_couchedonnees, usralert_fk_perimetre) FROM stdin;
\.


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.utilisateur (pk_utilisateur, ts, usr_id, usr_nom, usr_prenom, usr_email, usr_telephone, usr_telephone2, usr_service, usr_description, usr_password, usr_pwdexpire, usr_generic, usr_ldap, usr_signature, date_expiration_compte) FROM stdin;
1	2007-03-02 10:45:52.52875	admin@prodige.fr	PRODIGE_ADM	ADM	admin@prodige.fr			Service		ac6f71ff549832d6139ac2acc57e946b	2024-07-01	0	0	\N	\N
40	2012-04-11 12:19:46.123204	vinternet	Internet	Internet	NEANT	\N	\N	\N	\N	dcaa6e60155776107c638af755498759	2024-07-01	1	0	\N	\N
\.


--
-- Data for Name: utilisateur_carte; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.utilisateur_carte (id, fk_utilisateur, fk_stockage_carte) FROM stdin;
\.


--
-- Data for Name: zonage; Type: TABLE DATA; Schema: catalogue; Owner: user_p40
--

COPY catalogue.zonage (pk_zonage_id, zonage_nom, zonage_minimal, zonage_field_id, zonage_field_name, zonage_fk_couche_donnees, zonage_field_liaison) FROM stdin;
\.


--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.address (id, address, city, state, zip, country) FROM stdin;
2	\N	\N	\N	\N	\N
1	\N	\N	\N	\N	\N
3	\N	\N	\N	\N	\N
\.


--
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.categories (id, name) FROM stdin;
1	maps
2	datasets
3	interactiveResources
4	applications
5	caseStudies
6	proceedings
7	photo
8	audioVideo
9	directories
10	otherResources
11	z3950Servers
12	registers
\.


--
-- Data for Name: categoriesdes; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.categoriesdes (iddes, langid, label) FROM stdin;
1	chi	Maps & graphics
2	chi	Datasets
3	chi	Interactive resources
4	chi	Applications
5	chi	Case studies, best practices
6	chi	Conference proceedings
7	chi	Photo
8	chi	Audio/Video
9	chi	Directories
10	chi	Other information resources
1	eng	Maps & graphics
2	eng	Datasets
3	eng	Interactive resources
4	eng	Applications
5	eng	Case studies, best practices
6	eng	Conference proceedings
7	eng	Photo
8	eng	Audio/Video
9	eng	Directories
10	eng	Other information resources
2	fre	Jeux de données
1	fre	Cartes & graphiques
7	fre	Photographies
10	fre	Autres ressources
5	fre	Etude de cas, meilleures pratiques
8	fre	Vidéo/Audio
9	fre	Répertoires
4	fre	Applications
3	fre	Ressources interactives
6	fre	Conférences
11	fre	Serveurs Z3950
12	fre	Annuaires
\.


--
-- Data for Name: cswservercapabilitiesinfo; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.cswservercapabilitiesinfo (idfield, langid, field, label) FROM stdin;
13	chi	title	
14	chi	abstract	
15	chi	fees	
16	chi	accessConstraints	
9	dut	title	
10	dut	abstract	
11	dut	fees	
12	dut	accessConstraints	
1	eng	title	
2	eng	abstract	
3	eng	fees	
4	eng	accessConstraints	
21	fre	title	
22	fre	abstract	
23	fre	fees	
24	fre	accessConstraints	
17	ger	title	
18	ger	abstract	
19	ger	fees	
20	ger	accessConstraints	
29	rus	title	
30	rus	abstract	
31	rus	fees	
32	rus	accessConstraints	
\.


--
-- Data for Name: customelementset; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.customelementset (xpath, xpathhashcode) FROM stdin;
\.


--
-- Data for Name: email; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.email (user_id, email) FROM stdin;
1	\N
40102	admin@prodige.fr
40784	noreply@nomail.com
\.


--
-- Data for Name: files; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.files (_id, _content, _mimetype) FROM stdin;
\.


--
-- Data for Name: group_category; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.group_category (group_id, category_id) FROM stdin;
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.groups (id, name, description, email, referrer, logo, website, defaultcategory_id, enablecategoriesrestriction) FROM stdin;
0	Intranet			1	\N	\N	\N	n
1	All (Internet)			1	\N	\N	\N	n
-1	GUEST	self-registered users	\N	\N	\N	\N	\N	n
217	France	France	\N	\N	\N	\N	\N	n
40785	Alkante	\N	\N	\N	\N	\N	\N	n
\.


--
-- Data for Name: groupsdes; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.groupsdes (iddes, langid, label) FROM stdin;
0	chi	Intranet
1	chi	All (Internet)
0	eng	Intranet
1	eng	All (Internet)
-1	eng	GUEST
0	fre	Intranet
1	fre	All (Internet)
-1	fre	GUEST
217	ara	France
217	cat	France
217	chi	France
217	dut	France
217	eng	France
217	fin	France
217	fre	France
217	ger	France
217	nor	France
217	por	France
217	rus	France
217	spa	France
217	vie	France
\.


--
-- Data for Name: harvesterdata; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.harvesterdata (harvesteruuid, key, value, keyvalue) FROM stdin;
\.


--
-- Data for Name: harvestersettings; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.harvestersettings (id, parentid, name, value) FROM stdin;
1	\N	harvesting	\N
\.


--
-- Data for Name: harvesthistory; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.harvesthistory (id, harvestdate, harvesteruuid, harvestername, harvestertype, deleted, info, params, elapsedtime) FROM stdin;
\.


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: user_p40
--

SELECT pg_catalog.setval('public.hibernate_sequence', 40787, true);


--
-- Data for Name: inspireatomfeed; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.inspireatomfeed (id, atom, atomdatasetid, atomdatasetns, atomurl, authoremail, authorname, lang, metadataid, rights, subtitle, title) FROM stdin;
\.


--
-- Data for Name: inspireatomfeed_entrylist; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.inspireatomfeed_entrylist (inspireatomfeed_id, crs, id, lang, title, type, url) FROM stdin;
\.


--
-- Data for Name: isolanguages; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.isolanguages (id, code, shortcode) FROM stdin;
1	aar	\N
2	abk	\N
3	ace	\N
4	ach	\N
5	ada	\N
6	ady	\N
7	afa	\N
8	afh	\N
9	afr	\N
10	ain	\N
11	aka	\N
12	akk	\N
13	alb	\N
14	ale	\N
15	alg	\N
16	alt	\N
17	amh	\N
18	ang	\N
19	anp	\N
20	apa	\N
22	arc	\N
23	arg	\N
24	arm	\N
25	arn	\N
26	arp	\N
27	art	\N
28	arw	\N
29	asm	\N
30	ast	\N
31	ath	\N
32	aus	\N
33	ava	\N
34	ave	\N
35	awa	\N
36	aym	\N
37	aze	\N
38	bad	\N
39	bai	\N
40	bak	\N
41	bal	\N
42	bam	\N
43	ban	\N
44	baq	\N
45	bas	\N
46	bat	\N
47	bej	\N
48	bel	\N
49	bem	\N
50	ben	\N
51	ber	\N
52	bho	\N
53	bih	\N
54	bik	\N
55	bin	\N
56	bis	\N
57	bla	\N
58	bnt	\N
59	bos	\N
60	bra	\N
61	bre	\N
62	btk	\N
63	bua	\N
64	bug	\N
65	bul	\N
66	bur	\N
67	byn	\N
68	cad	\N
69	cai	\N
70	car	\N
72	cau	\N
73	ceb	\N
74	cel	\N
75	cha	\N
76	chb	\N
77	che	\N
78	chg	\N
80	chk	\N
81	chm	\N
82	chn	\N
83	cho	\N
84	chp	\N
85	chr	\N
86	chu	\N
87	chv	\N
88	chy	\N
89	cmc	\N
90	cop	\N
91	cor	\N
92	cos	\N
93	cpe	\N
94	cpf	\N
95	cpp	\N
96	cre	\N
97	crh	\N
98	crp	\N
99	csb	\N
100	cus	\N
101	cze	\N
102	dak	\N
103	dan	\N
104	dar	\N
105	day	\N
106	del	\N
107	den	\N
108	dgr	\N
109	din	\N
110	div	\N
111	doi	\N
112	dra	\N
113	dsb	\N
114	dua	\N
115	dum	\N
117	dyu	\N
118	dzo	\N
119	efi	\N
120	egy	\N
121	eka	\N
122	elx	\N
124	enm	\N
125	epo	\N
126	est	\N
127	ewe	\N
128	ewo	\N
129	fan	\N
130	fao	\N
131	fat	\N
132	fij	\N
133	fil	\N
135	fiu	\N
136	fon	\N
138	frm	\N
139	fro	\N
140	frr	\N
141	frs	\N
142	fry	\N
143	ful	\N
144	fur	\N
145	gaa	\N
146	gay	\N
147	gba	\N
148	gem	\N
149	geo	\N
150	deu	\N
151	gez	\N
152	gil	\N
153	gla	\N
154	gle	\N
155	glg	\N
156	glv	\N
157	gmh	\N
158	goh	\N
159	gon	\N
160	gor	\N
161	got	\N
162	grb	\N
163	grc	\N
164	gre	\N
165	grn	\N
166	gsw	\N
167	guj	\N
168	gwi	\N
169	hai	\N
170	hat	\N
171	hau	\N
172	haw	\N
173	heb	\N
174	her	\N
175	hil	\N
176	him	\N
177	hin	\N
178	hit	\N
179	hmn	\N
180	hmo	\N
181	hsb	\N
182	hun	\N
183	hup	\N
184	iba	\N
185	ibo	\N
186	ice	\N
187	ido	\N
188	iii	\N
189	ijo	\N
190	iku	\N
191	ile	\N
192	ilo	\N
193	ina	\N
194	inc	\N
195	ind	\N
196	ine	\N
197	inh	\N
198	ipk	\N
199	ira	\N
200	iro	\N
201	ita	\N
202	jav	\N
203	jbo	\N
204	jpn	\N
205	jpr	\N
206	jrb	\N
207	kaa	\N
208	kab	\N
209	kac	\N
210	kal	\N
211	kam	\N
212	kan	\N
213	kar	\N
214	kas	\N
215	kau	\N
216	kaw	\N
217	kaz	\N
218	kbd	\N
219	kha	\N
220	khi	\N
221	khm	\N
222	kho	\N
223	kik	\N
224	kin	\N
225	kir	\N
226	kmb	\N
227	kok	\N
71	cat	ca
79	chi	ch
116	dut	nl
123	eng	en
228	kom	\N
229	kon	\N
230	kor	\N
231	kos	\N
232	kpe	\N
233	krc	\N
234	krl	\N
235	kro	\N
236	kru	\N
237	kua	\N
238	kum	\N
239	kur	\N
240	kut	\N
241	lad	\N
242	lah	\N
243	lam	\N
244	lao	\N
245	lat	\N
246	lav	\N
247	lez	\N
248	lim	\N
249	lin	\N
250	lit	\N
251	lol	\N
252	loz	\N
253	ltz	\N
254	lua	\N
255	lub	\N
256	lug	\N
257	lui	\N
258	lun	\N
259	luo	\N
260	lus	\N
261	mac	\N
262	mad	\N
263	mag	\N
264	mah	\N
265	mai	\N
266	mak	\N
267	mal	\N
268	man	\N
269	mao	\N
270	map	\N
271	mar	\N
272	mas	\N
273	may	\N
274	mdf	\N
275	mdr	\N
276	men	\N
277	mga	\N
278	mic	\N
279	min	\N
280	mis	\N
281	mkh	\N
282	mlg	\N
283	mlt	\N
284	mnc	\N
285	mni	\N
286	mno	\N
287	moh	\N
288	mol	\N
289	mon	\N
290	mos	\N
291	mul	\N
292	mun	\N
293	mus	\N
294	mwl	\N
295	mwr	\N
296	myn	\N
297	myv	\N
298	nah	\N
299	nai	\N
300	nap	\N
301	nau	\N
302	nav	\N
303	nbl	\N
304	nde	\N
305	ndo	\N
306	nds	\N
307	nep	\N
308	new	\N
309	nia	\N
310	nic	\N
311	niu	\N
312	nno	\N
313	nob	\N
314	nog	\N
315	non	\N
317	nso	\N
318	nub	\N
319	nwc	\N
320	nya	\N
321	nym	\N
322	nyn	\N
323	nyo	\N
324	nzi	\N
325	oci	\N
326	oji	\N
327	ori	\N
328	orm	\N
329	osa	\N
330	oss	\N
331	ota	\N
332	oto	\N
333	paa	\N
334	pag	\N
335	pal	\N
336	pam	\N
337	pan	\N
338	pap	\N
339	pau	\N
340	peo	\N
341	per	\N
342	phi	\N
343	phn	\N
344	pli	\N
345	pol	\N
346	pon	\N
348	pra	\N
349	pro	\N
350	pus	\N
351	qaa	\N
352	que	\N
353	raj	\N
354	rap	\N
355	rar	\N
356	roa	\N
357	roh	\N
358	rom	\N
359	rum	\N
360	run	\N
361	rup	\N
363	sad	\N
364	sag	\N
365	sah	\N
366	sai	\N
367	sal	\N
368	sam	\N
369	san	\N
370	sas	\N
371	sat	\N
372	scc	\N
373	scn	\N
374	sco	\N
375	scr	\N
376	sel	\N
377	sem	\N
378	sga	\N
379	sgn	\N
380	shn	\N
381	sid	\N
382	sin	\N
383	sio	\N
384	sit	\N
385	sla	\N
386	slo	\N
387	slv	\N
388	sma	\N
389	sme	\N
390	smi	\N
391	smj	\N
392	smn	\N
393	smo	\N
394	sms	\N
395	sna	\N
396	snd	\N
397	snk	\N
398	sog	\N
399	som	\N
400	son	\N
401	sot	\N
403	srd	\N
404	srn	\N
405	srr	\N
406	ssa	\N
407	ssw	\N
408	suk	\N
409	sun	\N
410	sus	\N
411	sux	\N
412	swa	\N
413	swe	\N
414	syr	\N
415	tah	\N
416	tai	\N
417	tam	\N
418	tat	\N
419	tel	\N
420	tem	\N
421	ter	\N
422	tet	\N
423	tgk	\N
424	tgl	\N
425	tha	\N
426	tib	\N
427	tig	\N
428	tir	\N
429	tiv	\N
430	tkl	\N
431	tlh	\N
432	tli	\N
433	tmh	\N
434	tog	\N
435	ton	\N
436	tpi	\N
437	tsi	\N
438	tsn	\N
439	tso	\N
440	tuk	\N
441	tum	\N
442	tup	\N
443	tur	\N
444	tut	\N
445	tvl	\N
446	twi	\N
447	tyv	\N
448	udm	\N
449	uga	\N
450	uig	\N
451	ukr	\N
452	umb	\N
453	und	\N
347	por	po
362	rus	ru
402	spa	sp
454	urd	\N
455	uzb	\N
456	vai	\N
457	ven	\N
459	vol	\N
460	vot	\N
461	wak	\N
462	wal	\N
463	war	\N
464	was	\N
465	wel	\N
466	wen	\N
467	wln	\N
468	wol	\N
469	xal	\N
470	xho	\N
471	yao	\N
472	yap	\N
473	yid	\N
474	yor	\N
475	ypk	\N
476	zap	\N
477	zen	\N
478	zha	\N
479	znd	\N
480	zul	\N
481	zun	\N
482	zxx	\N
483	nqo	\N
484	zza	\N
21	ara	ar
134	fin	fi
137	fre	fr
316	nor	no
458	vie	vi
\.


--
-- Data for Name: isolanguagesdes; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.isolanguagesdes (iddes, langid, label) FROM stdin;
1	eng	Afar
2	eng	Abkhazian
3	eng	Achinese
4	eng	Acoli
5	eng	Adangme
6	eng	Adyghe; Adygei
7	eng	Afro-Asiatic (Other)
8	eng	Afrihili
9	eng	Afrikaans
10	eng	Ainu
11	eng	Akan
12	eng	Akkadian
13	eng	Albanian
14	eng	Aleut
15	eng	Algonquian languages
16	eng	Southern Altai
17	eng	Amharic
18	eng	English, Old (ca.450-1100)
19	eng	Angika
20	eng	Apache languages
21	eng	Arabic
22	eng	Aramaic
23	eng	Aragonese
24	eng	Armenian
25	eng	Mapudungun; Mapuche
26	eng	Arapaho
27	eng	Artificial (Other)
28	eng	Arawak
29	eng	Assamese
30	eng	Asturian; Bable
31	eng	Athapascan languages
32	eng	Australian languages
33	eng	Avaric
34	eng	Avestan
35	eng	Awadhi
36	eng	Aymara
37	eng	Azerbaijani
38	eng	Banda languages
1	fre	Afar
2	fre	Abkhaze
3	fre	Aceh
4	fre	Acoli
5	fre	Adangme
6	fre	Adyghé
7	fre	Afro-asiatiques, autres langues
8	fre	Afrihili
9	fre	Afrikaans
10	fre	Aïnou
11	fre	Akan
12	fre	Akkadien
13	fre	Albanais
14	fre	Aléoute
15	fre	Algonquines, langues
16	fre	Altai du Sud
17	fre	Amharique
18	fre	Anglo-saxon (ca.450-1100)
19	fre	Angika
20	fre	Apache
21	fre	Arabe
22	fre	Araméen
23	fre	Aragonais
24	fre	Arménien
25	fre	Mapudungun; mapuche; mapuce
26	fre	Arapaho
27	fre	Artificielles, autres langues
28	fre	Arawak
29	fre	Assamais
30	fre	Asturien; bable
31	fre	Athapascanes, langues
32	fre	Australiennes, langues
33	fre	Avar
34	fre	Avestique
35	fre	Awadhi
36	fre	Aymara
37	fre	Azéri
38	fre	Banda, langues
44	eng	Basque
45	eng	Basa
46	eng	Baltic (Other)
47	eng	Beja
48	eng	Belarusian
49	eng	Bemba
50	eng	Bengali
51	eng	Berber (Other)
52	eng	Bhojpuri
53	eng	Bihari
54	eng	Bikol
55	eng	Bini; Edo
56	eng	Bislama
57	eng	Siksika
58	eng	Bantu (Other)
59	eng	Bosnian
60	eng	Braj
61	eng	Breton
62	eng	Batak languages
63	eng	Buriat
64	eng	Buginese
65	eng	Bulgarian
66	eng	Burmese
67	eng	Blin; Bilin
68	eng	Caddo
69	eng	Central American Indian (Other)
70	eng	Galibi Carib
71	eng	Catalan; Valencian
72	eng	Caucasian (Other)
73	eng	Cebuano
74	eng	Celtic (Other)
75	eng	Chamorro
76	eng	Chibcha
77	eng	Chechen
78	eng	Chagatai
79	eng	Chinese
80	eng	Chuukese
43	fre	Balinais
44	fre	Basque
45	fre	Basa
46	fre	Baltiques, autres langues
47	fre	Bedja
48	fre	Biélorusse
49	fre	Bemba
50	fre	Bengali
51	fre	Berbères, autres langues
52	fre	Bhojpuri
53	fre	Bihari
54	fre	Bikol
55	fre	Bini; edo
56	fre	Bichlamar
57	fre	Blackfoot
58	fre	Bantoues, autres langues
59	fre	Bosniaque
60	fre	Braj
61	fre	Breton
62	fre	Batak, langues
63	fre	Bouriate
64	fre	Bugi
65	fre	Bulgare
66	fre	Birman
67	fre	Blin; bilen
68	fre	Caddo
69	fre	Indiennes d'Amérique centrale, autres langues
70	fre	Karib; galibi; carib
71	fre	Catalan; valencien
72	fre	Caucasiennes, autres langues
73	fre	Cebuano
74	fre	Celtiques, autres langues
75	fre	Chamorro
76	fre	Chibcha
77	fre	Tchétchène
78	fre	Djaghataï
79	fre	Chinois
80	fre	Chuuk
81	fre	Mari
87	eng	Chuvash
88	eng	Cheyenne
89	eng	Chamic languages
90	eng	Coptic
91	eng	Cornish
92	eng	Corsican
93	eng	Creoles and pidgins, English based (Other)
94	eng	Creoles and pidgins, French-based (Other)
95	eng	Creoles and pidgins, Portuguese-based (Other)
96	eng	Cree
97	eng	Crimean Tatar; Crimean Turkish
98	eng	Creoles and pidgins (Other)
99	eng	Kashubian
100	eng	Cushitic (Other)
101	eng	Czech
102	eng	Dakota
103	eng	Danish
104	eng	Dargwa
105	eng	Land Dayak languages
106	eng	Delaware
107	eng	Slave (Athapascan)
108	eng	Dogrib
109	eng	Dinka
110	eng	Divehi; Dhivehi; Maldivian
111	eng	Dogri
112	eng	Dravidian (Other)
113	eng	Lower Sorbian
114	eng	Duala
115	eng	Dutch, Middle (ca.1050-1350)
116	eng	Dutch; Flemish
117	eng	Dyula
118	eng	Dzongkha
119	eng	Efik
120	eng	Egyptian (Ancient)
121	eng	Ekajuk
122	eng	Elamite
86	fre	Slavon d'église; vieux slave; slavon liturgique; vieux bulgare
87	fre	Tchouvache
88	fre	Cheyenne
89	fre	Chames, langues
90	fre	Copte
91	fre	Cornique
92	fre	Corse
93	fre	Créoles et pidgins anglais, autres
94	fre	Créoles et pidgins français, autres
95	fre	Créoles et pidgins portugais, autres
96	fre	Cree
97	fre	Tatar de Crimé
98	fre	Créoles et pidgins divers
99	fre	Kachoube
100	fre	Couchitiques, autres langues
101	fre	Tchèque
102	fre	Dakota
103	fre	Danois
104	fre	Dargwa
105	fre	Dayak, langues
106	fre	Delaware
107	fre	Esclave (athapascan)
108	fre	Dogrib
109	fre	Dinka
110	fre	Maldivien
111	fre	Dogri
112	fre	Dravidiennes, autres langues
113	fre	Bas-sorabe
114	fre	Douala
115	fre	Néerlandais moyen (ca. 1050-1350)
116	fre	Néerlandais; flamand
117	fre	Dioula
118	fre	Dzongkha
119	fre	Efik
126	eng	Estonian
127	eng	Ewe
128	eng	Ewondo
129	eng	Fang
130	eng	Faroese
131	eng	Fanti
132	eng	Fijian
133	eng	Filipino; Pilipino
134	eng	Finnish
135	eng	Finno-Ugrian (Other)
136	eng	Fon
137	eng	French
138	eng	French, Middle (ca.1400-1600)
139	eng	French, Old (842-ca.1400)
140	eng	Northern Frisian
141	eng	Eastern Frisian
142	eng	Western Frisian
143	eng	Fulah
144	eng	Friulian
145	eng	Ga
146	eng	Gayo
147	eng	Gbaya
148	eng	Germanic (Other)
149	eng	Georgian
150	eng	German
151	eng	Geez
152	eng	Gilbertese
153	eng	Gaelic; Scottish Gaelic
154	eng	Irish
155	eng	Galician
156	eng	Manx
157	eng	German, Middle High (ca.1050-1500)
158	eng	German, Old High (ca.750-1050)
159	eng	Gondi
160	eng	Gorontalo
161	eng	Gothic
162	eng	Grebo
163	eng	Greek, Ancient (to 1453)
125	fre	Espéranto
126	fre	Estonien
127	fre	Éwé
128	fre	Éwondo
129	fre	Fang
130	fre	Féroïen
131	fre	Fanti
132	fre	Fidjien
133	fre	Filipino; pilipino
134	fre	Finnois
135	fre	Finno-ougriennes, autres langues
136	fre	Fon
137	fre	Français
138	fre	Français moyen (1400-1600)
139	fre	Français ancien (842-ca.1400)
140	fre	Frison septentrional
141	fre	Frison oriental
142	fre	Frison occidental
143	fre	Peul
144	fre	Frioulan
145	fre	Ga
146	fre	Gayo
147	fre	Gbaya
148	fre	Germaniques, autres langues
149	fre	Géorgien
150	fre	Allemand
151	fre	Guèze
152	fre	Kiribati
153	fre	Gaélique; gaélique écossais
154	fre	Irlandais
155	fre	Galicien
156	fre	Manx; mannois
157	fre	Allemand, moyen haut (ca. 1050-1500)
158	fre	Allemand, vieux haut (ca. 750-1050)
159	fre	Gond
167	eng	Gujarati
168	eng	Gwich'in
169	eng	Haida
170	eng	Haitian; Haitian Creole
171	eng	Hausa
172	eng	Hawaiian
173	eng	Hebrew
174	eng	Herero
175	eng	Hiligaynon
176	eng	Himachali
177	eng	Hindi
178	eng	Hittite
179	eng	Hmong
180	eng	Hiri Motu
181	eng	Upper Sorbian
182	eng	Hungarian
183	eng	Hupa
184	eng	Iban
185	eng	Igbo
186	eng	Icelandic
187	eng	Ido
188	eng	Sichuan Yi
189	eng	Ijo languages
190	eng	Inuktitut
191	eng	Interlingue
192	eng	Iloko
193	eng	Interlingua (International Auxiliary Language Association)
194	eng	Indic (Other)
195	eng	Indonesian
196	eng	Indo-European (Other)
197	eng	Ingush
198	eng	Inupiaq
199	eng	Iranian (Other)
200	eng	Iroquoian languages
201	eng	Italian
202	eng	Javanese
203	eng	Lojban
204	eng	Japanese
167	fre	Goudjrati
168	fre	Gwich'in
169	fre	Haida
170	fre	Haïtien; créole haïtien
171	fre	Haoussa
172	fre	Hawaïen
173	fre	Hébreu
174	fre	Herero
175	fre	Hiligaynon
176	fre	Himachali
177	fre	Hindi
178	fre	Hittite
179	fre	Hmong
180	fre	Hiri motu
181	fre	Haut-sorabe
182	fre	Hongrois
183	fre	Hupa
184	fre	Iban
185	fre	Igbo
186	fre	Islandais
187	fre	Ido
188	fre	Yi de Sichuan
189	fre	Ijo, langues
190	fre	Inuktitut
191	fre	Interlingue
192	fre	Ilocano
193	fre	Interlingua (langue auxiliaire internationale)
194	fre	Indo-aryennes, autres langues
195	fre	Indonésien
196	fre	Indo-européennes, autres langues
197	fre	Ingouche
198	fre	Inupiaq
199	fre	Iraniennes, autres langues
200	fre	Iroquoises, langues (famille)
201	fre	Italien
202	fre	Javanais
203	fre	Lojban
204	fre	Japonais
211	chi	Kamba
210	eng	Kalaallisut; Greenlandic
211	eng	Kamba
212	eng	Kannada
213	eng	Karen languages
214	eng	Kashmiri
215	eng	Kanuri
216	eng	Kawi
217	eng	Kazakh
218	eng	Kabardian
219	eng	Khasi
220	eng	Khoisan (Other)
221	eng	Central Khmer
222	eng	Khotanese
223	eng	Kikuyu; Gikuyu
224	eng	Kinyarwanda
225	eng	Kirghiz; Kyrgyz
226	eng	Kimbundu
227	eng	Konkani
228	eng	Komi
229	eng	Kongo
230	eng	Korean
231	eng	Kosraean
232	eng	Kpelle
233	eng	Karachay-Balkar
234	eng	Karelian
235	eng	Kru languages
236	eng	Kurukh
237	eng	Kuanyama; Kwanyama
238	eng	Kumyk
239	eng	Kurdish
240	eng	Kutenai
241	eng	Ladino
242	eng	Lahnda
243	eng	Lamba
244	eng	Lao
245	eng	Latin
246	eng	Latvian
247	eng	Lezghian
248	eng	Limburgan; Limburger; Limburgish
209	fre	Kachin; jingpho
210	fre	Groenlandais
211	fre	Kamba
212	fre	Kannada
213	fre	Karen, langues
214	fre	Kashmiri
215	fre	Kanouri
216	fre	Kawi
217	fre	Kazakh
218	fre	Kabardien
219	fre	Khasi
220	fre	Khoisan, autres langues
221	fre	Khmer central
222	fre	Khotanais
223	fre	Kikuyu
224	fre	Rwanda
225	fre	Kirghiz
226	fre	Kimbundu
227	fre	Konkani
228	fre	Kom
229	fre	Kongo
230	fre	Coréen
231	fre	Kosrae
232	fre	Kpellé
233	fre	Karatchai balkar
234	fre	Carélien
235	fre	Krou, langues
236	fre	Kurukh
237	fre	Kuanyama; kwanyama
238	fre	Koumyk
239	fre	Kurde
240	fre	Kutenai
241	fre	Judéo-espagnol
242	fre	Lahnda
243	fre	Lamba
244	fre	Lao
245	fre	Latin
246	fre	Letton
247	fre	Lezghien
253	eng	Luxembourgish; Letzeburgesch
254	eng	Luba-Lulua
255	eng	Luba-Katanga
256	eng	Ganda
257	eng	Luiseno
258	eng	Lunda
259	eng	Luo (Kenya and Tanzania)
260	eng	Lushai
261	eng	Macedonian
262	eng	Madurese
263	eng	Magahi
264	eng	Marshallese
265	eng	Maithili
266	eng	Makasar
267	eng	Malayalam
268	eng	Mandingo
269	eng	Maori
270	eng	Austronesian (Other)
271	eng	Marathi
272	eng	Masai
273	eng	Malay
274	eng	Moksha
275	eng	Mandar
276	eng	Mende
277	eng	Irish, Middle (900-1200)
278	eng	Mi'kmaq; Micmac
279	eng	Minangkabau
280	eng	Miscellaneous languages
281	eng	Mon-Khmer (Other)
282	eng	Malagasy
283	eng	Maltese
284	eng	Manchu
285	eng	Manipuri
286	eng	Manobo languages
287	eng	Mohawk
288	eng	Moldavian
289	eng	Mongolian
290	eng	Mossi
253	fre	Luxembourgeois
254	fre	Luba-lulua
255	fre	Luba-katanga
256	fre	Ganda
257	fre	Luiseno
258	fre	Lunda
259	fre	Luo (Kenya et Tanzanie)
260	fre	Lushai
261	fre	Macédonien
262	fre	Madourais
263	fre	Magahi
264	fre	Marshall
265	fre	Maithili
266	fre	Makassar
267	fre	Malayalam
268	fre	Mandingue
269	fre	Maori
270	fre	Malayo-polynésiennes, autres langues
271	fre	Marathe
272	fre	Massaï
273	fre	Malais
274	fre	Moksa
275	fre	Mandar
276	fre	Mendé
277	fre	Irlandais moyen (900-1200)
278	fre	Mi'kmaq; micmac
279	fre	Minangkabau
280	fre	Diverses, langues
281	fre	Môn-khmer, autres langues
282	fre	Malgache
283	fre	Maltais
284	fre	Mandchou
285	fre	Manipuri
286	fre	Manobo, langues
287	fre	Mohawk
288	fre	Moldave
289	fre	Mongol
290	fre	Moré
291	fre	Multilingue
295	chi	Marwari
296	eng	Mayan languages
297	eng	Erzya
298	eng	Nahuatl languages
299	eng	North American Indian
300	eng	Neapolitan
301	eng	Nauru
302	eng	Navajo; Navaho
303	eng	Ndebele, South; South Ndebele
304	eng	Ndebele, North; North Ndebele
305	eng	Ndonga
306	eng	Low German; Low Saxon; German, Low; Saxon, Low
307	eng	Nepali
308	eng	Nepal Bhasa; Newari
309	eng	Nias
310	eng	Niger-Kordofanian (Other)
311	eng	Niuean
312	eng	Norwegian Nynorsk; Nynorsk, Norwegian
313	eng	Bokmål, Norwegian; Norwegian Bokmål
314	eng	Nogai
315	eng	Norse, Old
316	eng	Norwegian
317	eng	Pedi; Sepedi; Northern Sotho
318	eng	Nubian languages
319	eng	Classical Newari; Old Newari; Classical Nepal Bhasa
320	eng	Chichewa; Chewa; Nyanja
321	eng	Nyamwezi
322	eng	Nyankole
323	eng	Nyoro
324	eng	Nzima
325	eng	Occitan (post 1500); Provençal
326	eng	Ojibwa
327	eng	Oriya
328	eng	Oromo
329	eng	Osage
295	fre	Marvari
296	fre	Maya, langues
297	fre	Erza
298	fre	Nahuatl, langues
299	fre	Indiennes d'Amérique du Nord, autres langues
300	fre	Napolitain
301	fre	Nauruan
302	fre	Navaho
303	fre	Ndébélé du Sud
304	fre	Ndébélé du Nord
305	fre	Ndonga
306	fre	Bas allemand; bas saxon; allemand, bas; saxon, bas
307	fre	Népalais
308	fre	Nepal bhasa; newari
309	fre	Nias
310	fre	Nigéro-congolaises, autres langues
311	fre	Niué
312	fre	Norvégien nynorsk; nynorsk, norvégien
313	fre	Norvégien bokmål
314	fre	Nogaï; nogay
315	fre	Norrois, vieux
316	fre	Norvégien
317	fre	Pedi; sepedi; sotho du Nord
318	fre	Nubiennes, langues
319	fre	Newari classique
320	fre	Chichewa; chewa; nyanja
321	fre	Nyamwezi
322	fre	Nyankolé
323	fre	Nyoro
324	fre	Nzema
325	fre	Occitan (après 1500); provençal
326	fre	Ojibwa
327	fre	Oriya
328	fre	Galla
329	fre	Osage
330	fre	Ossète
333	eng	Papuan (Other)
334	eng	Pangasinan
335	eng	Pahlavi
336	eng	Pampanga
337	eng	Panjabi; Punjabi
338	eng	Papiamento
339	eng	Palauan
340	eng	Persian, Old (ca.600-400 B.C.)
341	eng	Persian
342	eng	Philippine (Other)
343	eng	Phoenician
344	eng	Pali
345	eng	Polish
346	eng	Pohnpeian
347	eng	Portuguese
348	eng	Prakrit languages
349	eng	Provençal, Old (to 1500)
350	eng	Pushto
351	eng	Reserved for local use
352	eng	Quechua
353	eng	Rajasthani
354	eng	Rapanui
355	eng	Rarotongan; Cook Islands Maori
356	eng	Romance (Other)
357	eng	Romansh
358	eng	Romany
359	eng	Romanian
360	eng	Rundi
361	eng	Aromanian; Arumanian; Macedo-Romanian
362	eng	Russian
363	eng	Sandawe
364	eng	Sango
365	eng	Yakut
366	eng	South American Indian (Other)
367	eng	Salishan languages
368	eng	Samaritan Aramaic
369	eng	Sanskrit
333	fre	Papoues, autres langues
334	fre	Pangasinan
335	fre	Pahlavi
336	fre	Pampangan
337	fre	Pendjabi
338	fre	Papiamento
339	fre	Palau
340	fre	Perse, vieux (ca. 600-400 av. J.-C.)
341	fre	Persan
342	fre	Philippines, autres langues
343	fre	Phénicien
344	fre	Pali
345	fre	Polonais
346	fre	Pohnpei
347	fre	Portugais
348	fre	Prâkrit
349	fre	Provençal ancien (jusqu'à 1500)
350	fre	Pachto
351	fre	Réservée à l'usage local
352	fre	Quechua
353	fre	Rajasthani
354	fre	Rapanui
355	fre	Rarotonga; maori des îles Cook
356	fre	Romanes, autres langues
357	fre	Romanche
358	fre	Tsigane
359	fre	Roumain
360	fre	Rundi
361	fre	Aroumain; macédo-roumain
362	fre	Russe
363	fre	Sandawe
364	fre	Sango
365	fre	Iakoute
366	fre	Indiennes d'Amérique du Sud, autres langues
367	fre	Salish, langues
368	fre	Samaritain
374	eng	Scots
375	eng	Croatian
376	eng	Selkup
377	eng	Semitic (Other)
378	eng	Irish, Old (to 900)
379	eng	Sign Languages
380	eng	Shan
381	eng	Sidamo
382	eng	Sinhala; Sinhalese
383	eng	Siouan languages
384	eng	Sino-Tibetan (Other)
385	eng	Slavic (Other)
386	eng	Slovak
387	eng	Slovenian
388	eng	Southern Sami
389	eng	Northern Sami
390	eng	Sami languages (Other)
391	eng	Lule Sami
392	eng	Inari Sami
393	eng	Samoan
394	eng	Skolt Sami
395	eng	Shona
396	eng	Sindhi
397	eng	Soninke
398	eng	Sogdian
399	eng	Somali
400	eng	Songhai languages
401	eng	Sotho, Southern
402	eng	Spanish; Castilian
403	eng	Sardinian
404	eng	Sranan Tongo
405	eng	Serer
406	eng	Nilo-Saharan (Other)
407	eng	Swati
408	eng	Sukuma
409	eng	Sundanese
410	eng	Susu
411	eng	Sumerian
374	fre	Écossais
375	fre	Croate
376	fre	Selkoupe
377	fre	Sémitiques, autres langues
378	fre	Irlandais ancien (jusqu'à 900)
379	fre	Langues des signes
380	fre	Chan
381	fre	Sidamo
382	fre	Singhalais
383	fre	Sioux, langues
384	fre	Sino-tibétaines, autres langues
385	fre	Slaves, autres langues
386	fre	Slovaque
387	fre	Slovène
388	fre	Sami du Sud
389	fre	Sami du Nord
390	fre	Sami, autres langues
391	fre	Sami de Lule
392	fre	Sami d'Inari
393	fre	Samoan
394	fre	Sami skolt
395	fre	Shona
396	fre	Sindhi
397	fre	Soninké
398	fre	Sogdien
399	fre	Somali
400	fre	Songhai, langues
401	fre	Sotho du Sud
402	fre	Espagnol; castillan
403	fre	Sarde
404	fre	Sranan tongo
405	fre	Sérère
406	fre	Nilo-sahariennes, autres langues
407	fre	Swati
408	fre	Sukuma
409	fre	Soundanais
410	fre	Soussou
417	eng	Tamil
418	eng	Tatar
419	eng	Telugu
420	eng	Timne
421	eng	Tereno
422	eng	Tetum
423	eng	Tajik
424	eng	Tagalog
425	eng	Thai
426	eng	Tibetan
427	eng	Tigre
428	eng	Tigrinya
429	eng	Tiv
430	eng	Tokelau
431	eng	Klingon; tlhIngan-Hol
432	eng	Tlingit
433	eng	Tamashek
434	eng	Tonga (Nyasa)
435	eng	Tonga (Tonga Islands)
436	eng	Tok Pisin
437	eng	Tsimshian
438	eng	Tswana
439	eng	Tsonga
440	eng	Turkmen
441	eng	Tumbuka
442	eng	Tupi languages
443	eng	Turkish
444	eng	Altaic (Other)
445	eng	Tuvalu
446	eng	Twi
447	eng	Tuvinian
448	eng	Udmurt
449	eng	Ugaritic
450	eng	Uighur; Uyghur
451	eng	Ukrainian
452	eng	Umbundu
453	eng	Undetermined
454	eng	Urdu
455	eng	Uzbek
456	eng	Vai
416	fre	Thaïes, autres langues
417	fre	Tamoul
418	fre	Tatar
419	fre	Télougou
420	fre	Temne
421	fre	Tereno
422	fre	Tetum
423	fre	Tadjik
424	fre	Tagalog
425	fre	Thaï
426	fre	Tibétain
427	fre	Tigré
428	fre	Tigrigna
429	fre	Tiv
430	fre	Tokelau
431	fre	Klingon
432	fre	Tlingit
433	fre	Tamacheq
434	fre	Tonga (Nyasa)
435	fre	Tongan (Îles Tonga)
436	fre	Tok pisin
437	fre	Tsimshian
438	fre	Tswana
439	fre	Tsonga
440	fre	Turkmène
441	fre	Tumbuka
442	fre	Tupi, langues
443	fre	Turc
444	fre	Altaïques, autres langues
445	fre	Tuvalu
446	fre	Twi
447	fre	Touva
448	fre	Oudmourte
449	fre	Ougaritique
450	fre	Ouïgour
451	fre	Ukrainien
452	fre	Umbundu
453	fre	Indéterminée
454	fre	Ourdou
1	chi	Afar
2	chi	Abkhazian
3	chi	Achinese
4	chi	Acoli
5	chi	Adangme
6	chi	Adyghe; Adygei
7	chi	Afro-Asiatic (Other)
8	chi	Afrihili
9	chi	Afrikaans
10	chi	Ainu
11	chi	Akan
12	chi	Akkadian
13	chi	Albanian
14	chi	Aleut
15	chi	Algonquian languages
16	chi	Southern Altai
17	chi	Amharic
18	chi	English, Old (ca.450-1100)
19	chi	Angika
20	chi	Apache languages
21	chi	Arabic
22	chi	Aramaic
23	chi	Aragonese
24	chi	Armenian
25	chi	Mapudungun; Mapuche
26	chi	Arapaho
27	chi	Artificial (Other)
28	chi	Arawak
29	chi	Assamese
30	chi	Asturian; Bable
31	chi	Athapascan languages
32	chi	Australian languages
33	chi	Avaric
34	chi	Avestan
35	chi	Awadhi
36	chi	Aymara
37	chi	Azerbaijani
38	chi	Banda languages
39	chi	Bamileke languages
40	chi	Bashkir
41	chi	Baluchi
42	chi	Bambara
43	chi	Balinese
44	chi	Basque
45	chi	Basa
46	chi	Baltic (Other)
47	chi	Beja
48	chi	Belarusian
49	chi	Bemba
50	chi	Bengali
51	chi	Berber (Other)
52	chi	Bhojpuri
53	chi	Bihari
54	chi	Bikol
55	chi	Bini; Edo
56	chi	Bislama
57	chi	Siksika
58	chi	Bantu (Other)
59	chi	Bosnian
60	chi	Braj
61	chi	Breton
62	chi	Batak languages
63	chi	Buriat
64	chi	Buginese
65	chi	Bulgarian
66	chi	Burmese
67	chi	Blin; Bilin
68	chi	Caddo
69	chi	Central American Indian (Other)
70	chi	Galibi Carib
71	chi	Catalan; Valencian
72	chi	Caucasian (Other)
73	chi	Cebuano
461	eng	Wakashan languages
462	eng	Walamo
463	eng	Waray
464	eng	Washo
465	eng	Welsh
466	eng	Sorbian languages
467	eng	Walloon
468	eng	Wolof
469	eng	Kalmyk; Oirat
470	eng	Xhosa
471	eng	Yao
472	eng	Yapese
473	eng	Yiddish
474	eng	Yoruba
475	eng	Yupik languages
476	eng	Zapotec
477	eng	Zenaga
478	eng	Zhuang; Chuang
479	eng	Zande languages
480	eng	Zulu
481	eng	Zuni
482	eng	No linguistic content
461	fre	Wakashennes, langues
462	fre	Walamo
463	fre	Waray
464	fre	Washo
465	fre	Gallois
466	fre	Sorabes, langues
467	fre	Wallon
468	fre	Wolof
469	fre	Kalmouk; oïrat
470	fre	Xhosa
471	fre	Yao
472	fre	Yapois
473	fre	Yiddish
474	fre	Yoruba
475	fre	Yupik, langues
476	fre	Zapotèque
477	fre	Zenaga
478	fre	Zhuang; chuang
479	fre	Zandé, langues
480	fre	Zoulou
481	fre	Zuni
482	fre	Pas de contenu linguistique
483	fre	N'ko
74	chi	Celtic (Other)
75	chi	Chamorro
76	chi	Chibcha
77	chi	Chechen
78	chi	Chagatai
79	chi	Chinese
80	chi	Chuukese
81	chi	Mari
82	chi	Chinook jargon
83	chi	Choctaw
84	chi	Chipewyan
85	chi	Cherokee
86	chi	Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic
87	chi	Chuvash
88	chi	Cheyenne
89	chi	Chamic languages
90	chi	Coptic
91	chi	Cornish
92	chi	Corsican
93	chi	Creoles and pidgins, English based (Other)
94	chi	Creoles and pidgins, French-based (Other)
95	chi	Creoles and pidgins, Portuguese-based (Other)
96	chi	Cree
97	chi	Crimean Tatar; Crimean Turkish
98	chi	Creoles and pidgins (Other)
99	chi	Kashubian
100	chi	Cushitic (Other)
101	chi	Czech
102	chi	Dakota
103	chi	Danish
104	chi	Dargwa
105	chi	Land Dayak languages
106	chi	Delaware
107	chi	Slave (Athapascan)
108	chi	Dogrib
109	chi	Dinka
110	chi	Divehi; Dhivehi; Maldivian
111	chi	Dogri
112	chi	Dravidian (Other)
113	chi	Lower Sorbian
114	chi	Duala
115	chi	Dutch, Middle (ca.1050-1350)
116	chi	Dutch; Flemish
117	chi	Dyula
118	chi	Dzongkha
119	chi	Efik
120	chi	Egyptian (Ancient)
121	chi	Ekajuk
122	chi	Elamite
123	chi	English
124	chi	English, Middle (1100-1500)
125	chi	Esperanto
126	chi	Estonian
127	chi	Ewe
128	chi	Ewondo
129	chi	Fang
130	chi	Faroese
131	chi	Fanti
132	chi	Fijian
133	chi	Filipino; Pilipino
134	chi	Finnish
135	chi	Finno-Ugrian (Other)
136	chi	Fon
137	chi	French
138	chi	French, Middle (ca.1400-1600)
139	chi	French, Old (842-ca.1400)
140	chi	Northern Frisian
141	chi	Eastern Frisian
142	chi	Western Frisian
143	chi	Fulah
144	chi	Friulian
145	chi	Ga
146	chi	Gayo
147	chi	Gbaya
148	chi	Germanic (Other)
149	chi	Georgian
150	chi	German
151	chi	Geez
152	chi	Gilbertese
153	chi	Gaelic; Scottish Gaelic
154	chi	Irish
155	chi	Galician
156	chi	Manx
157	chi	German, Middle High (ca.1050-1500)
158	chi	German, Old High (ca.750-1050)
159	chi	Gondi
160	chi	Gorontalo
161	chi	Gothic
162	chi	Grebo
163	chi	Greek, Ancient (to 1453)
164	chi	Greek, Modern (1453-)
165	chi	Guarani
166	chi	Swiss German; Alemannic
167	chi	Gujarati
168	chi	Gwich'in
169	chi	Haida
170	chi	Haitian; Haitian Creole
171	chi	Hausa
172	chi	Hawaiian
173	chi	Hebrew
174	chi	Herero
175	chi	Hiligaynon
176	chi	Himachali
177	chi	Hindi
178	chi	Hittite
179	chi	Hmong
180	chi	Hiri Motu
181	chi	Upper Sorbian
182	chi	Hungarian
183	chi	Hupa
184	chi	Iban
185	chi	Igbo
186	chi	Icelandic
187	chi	Ido
188	chi	Sichuan Yi
189	chi	Ijo languages
190	chi	Inuktitut
191	chi	Interlingue
192	chi	Iloko
193	chi	Interlingua (International Auxiliary Language Association)
194	chi	Indic (Other)
195	chi	Indonesian
196	chi	Indo-European (Other)
197	chi	Ingush
198	chi	Inupiaq
199	chi	Iranian (Other)
200	chi	Iroquoian languages
201	chi	Italian
202	chi	Javanese
203	chi	Lojban
204	chi	Japanese
205	chi	Judeo-Persian
206	chi	Judeo-Arabic
207	chi	Kara-Kalpak
208	chi	Kabyle
209	chi	Kachin; Jingpho
210	chi	Kalaallisut; Greenlandic
212	chi	Kannada
213	chi	Karen languages
214	chi	Kashmiri
215	chi	Kanuri
216	chi	Kawi
217	chi	Kazakh
218	chi	Kabardian
219	chi	Khasi
220	chi	Khoisan (Other)
221	chi	Central Khmer
222	chi	Khotanese
223	chi	Kikuyu; Gikuyu
224	chi	Kinyarwanda
225	chi	Kirghiz; Kyrgyz
226	chi	Kimbundu
227	chi	Konkani
228	chi	Komi
229	chi	Kongo
230	chi	Korean
231	chi	Kosraean
232	chi	Kpelle
233	chi	Karachay-Balkar
234	chi	Karelian
235	chi	Kru languages
236	chi	Kurukh
237	chi	Kuanyama; Kwanyama
238	chi	Kumyk
239	chi	Kurdish
240	chi	Kutenai
241	chi	Ladino
242	chi	Lahnda
243	chi	Lamba
244	chi	Lao
245	chi	Latin
246	chi	Latvian
247	chi	Lezghian
248	chi	Limburgan; Limburger; Limburgish
249	chi	Lingala
250	chi	Lithuanian
251	chi	Mongo
252	chi	Lozi
253	chi	Luxembourgish; Letzeburgesch
254	chi	Luba-Lulua
255	chi	Luba-Katanga
256	chi	Ganda
257	chi	Luiseno
258	chi	Lunda
259	chi	Luo (Kenya and Tanzania)
260	chi	Lushai
261	chi	Macedonian
262	chi	Madurese
263	chi	Magahi
264	chi	Marshallese
265	chi	Maithili
266	chi	Makasar
267	chi	Malayalam
268	chi	Mandingo
269	chi	Maori
270	chi	Austronesian (Other)
271	chi	Marathi
272	chi	Masai
273	chi	Malay
274	chi	Moksha
275	chi	Mandar
276	chi	Mende
277	chi	Irish, Middle (900-1200)
278	chi	Mi'kmaq; Micmac
279	chi	Minangkabau
280	chi	Miscellaneous languages
281	chi	Mon-Khmer (Other)
282	chi	Malagasy
283	chi	Maltese
284	chi	Manchu
285	chi	Manipuri
286	chi	Manobo languages
287	chi	Mohawk
288	chi	Moldavian
289	chi	Mongolian
290	chi	Mossi
291	chi	Multiple languages
292	chi	Munda languages
293	chi	Creek
294	chi	Mirandese
296	chi	Mayan languages
297	chi	Erzya
298	chi	Nahuatl languages
299	chi	North American Indian
300	chi	Neapolitan
301	chi	Nauru
302	chi	Navajo; Navaho
303	chi	Ndebele, South; South Ndebele
304	chi	Ndebele, North; North Ndebele
305	chi	Ndonga
306	chi	Low German; Low Saxon; German, Low; Saxon, Low
307	chi	Nepali
308	chi	Nepal Bhasa; Newari
309	chi	Nias
310	chi	Niger-Kordofanian (Other)
311	chi	Niuean
312	chi	Norwegian Nynorsk; Nynorsk, Norwegian
313	chi	Bokmål, Norwegian; Norwegian Bokmål
314	chi	Nogai
315	chi	Norse, Old
316	chi	Norwegian
317	chi	Pedi; Sepedi; Northern Sotho
318	chi	Nubian languages
319	chi	Classical Newari; Old Newari; Classical Nepal Bhasa
320	chi	Chichewa; Chewa; Nyanja
321	chi	Nyamwezi
322	chi	Nyankole
323	chi	Nyoro
324	chi	Nzima
325	chi	Occitan (post 1500); Provençal
326	chi	Ojibwa
327	chi	Oriya
328	chi	Oromo
329	chi	Osage
330	chi	Ossetian; Ossetic
331	chi	Turkish, Ottoman (1500-1928)
332	chi	Otomian languages
333	chi	Papuan (Other)
334	chi	Pangasinan
335	chi	Pahlavi
336	chi	Pampanga
337	chi	Panjabi; Punjabi
338	chi	Papiamento
339	chi	Palauan
340	chi	Persian, Old (ca.600-400 B.C.)
341	chi	Persian
342	chi	Philippine (Other)
343	chi	Phoenician
344	chi	Pali
345	chi	Polish
346	chi	Pohnpeian
347	chi	Portuguese
348	chi	Prakrit languages
349	chi	Provençal, Old (to 1500)
350	chi	Pushto
351	chi	Reserved for local use
352	chi	Quechua
353	chi	Rajasthani
354	chi	Rapanui
355	chi	Rarotongan; Cook Islands Maori
356	chi	Romance (Other)
357	chi	Romansh
358	chi	Romany
359	chi	Romanian
360	chi	Rundi
361	chi	Aromanian; Arumanian; Macedo-Romanian
362	chi	Russian
363	chi	Sandawe
364	chi	Sango
365	chi	Yakut
366	chi	South American Indian (Other)
367	chi	Salishan languages
368	chi	Samaritan Aramaic
369	chi	Sanskrit
370	chi	Sasak
371	chi	Santali
372	chi	Serbian
373	chi	Sicilian
374	chi	Scots
375	chi	Croatian
376	chi	Selkup
377	chi	Semitic (Other)
378	chi	Irish, Old (to 900)
379	chi	Sign Languages
380	chi	Shan
381	chi	Sidamo
382	chi	Sinhala; Sinhalese
383	chi	Siouan languages
384	chi	Sino-Tibetan (Other)
385	chi	Slavic (Other)
386	chi	Slovak
387	chi	Slovenian
388	chi	Southern Sami
389	chi	Northern Sami
390	chi	Sami languages (Other)
391	chi	Lule Sami
392	chi	Inari Sami
393	chi	Samoan
394	chi	Skolt Sami
395	chi	Shona
396	chi	Sindhi
397	chi	Soninke
398	chi	Sogdian
399	chi	Somali
400	chi	Songhai languages
401	chi	Sotho, Southern
402	chi	Spanish; Castilian
403	chi	Sardinian
404	chi	Sranan Tongo
405	chi	Serer
406	chi	Nilo-Saharan (Other)
407	chi	Swati
408	chi	Sukuma
409	chi	Sundanese
410	chi	Susu
411	chi	Sumerian
412	chi	Swahili
413	chi	Swedish
414	chi	Syriac
415	chi	Tahitian
416	chi	Tai (Other)
417	chi	Tamil
418	chi	Tatar
419	chi	Telugu
420	chi	Timne
421	chi	Tereno
422	chi	Tetum
423	chi	Tajik
424	chi	Tagalog
425	chi	Thai
426	chi	Tibetan
427	chi	Tigre
428	chi	Tigrinya
429	chi	Tiv
430	chi	Tokelau
431	chi	Klingon; tlhIngan-Hol
432	chi	Tlingit
433	chi	Tamashek
434	chi	Tonga (Nyasa)
435	chi	Tonga (Tonga Islands)
436	chi	Tok Pisin
437	chi	Tsimshian
438	chi	Tswana
439	chi	Tsonga
440	chi	Turkmen
441	chi	Tumbuka
442	chi	Tupi languages
443	chi	Turkish
444	chi	Altaic (Other)
445	chi	Tuvalu
446	chi	Twi
447	chi	Tuvinian
448	chi	Udmurt
449	chi	Ugaritic
450	chi	Uighur; Uyghur
451	chi	Ukrainian
452	chi	Umbundu
453	chi	Undetermined
454	chi	Urdu
455	chi	Uzbek
456	chi	Vai
457	chi	Venda
458	chi	Vietnamese
459	chi	Volapük
460	chi	Votic
461	chi	Wakashan languages
462	chi	Walamo
463	chi	Waray
464	chi	Washo
465	chi	Welsh
466	chi	Sorbian languages
467	chi	Walloon
468	chi	Wolof
469	chi	Kalmyk; Oirat
470	chi	Xhosa
471	chi	Yao
472	chi	Yapese
473	chi	Yiddish
474	chi	Yoruba
475	chi	Yupik languages
476	chi	Zapotec
477	chi	Zenaga
478	chi	Zhuang; Chuang
479	chi	Zande languages
480	chi	Zulu
481	chi	Zuni
482	chi	No linguistic content
483	chi	N'Ko
484	chi	Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki
39	eng	Bamileke languages
40	eng	Bashkir
41	eng	Baluchi
42	eng	Bambara
43	eng	Balinese
81	eng	Mari
82	eng	Chinook jargon
83	eng	Choctaw
84	eng	Chipewyan
85	eng	Cherokee
86	eng	Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic
125	eng	Esperanto
123	eng	English
124	eng	English, Middle (1100-1500)
164	eng	Greek, Modern (1453-)
165	eng	Guarani
166	eng	Swiss German; Alemannic
205	eng	Judeo-Persian
206	eng	Judeo-Arabic
207	eng	Kara-Kalpak
208	eng	Kabyle
209	eng	Kachin; Jingpho
249	eng	Lingala
250	eng	Lithuanian
251	eng	Mongo
252	eng	Lozi
291	eng	Multiple languages
292	eng	Munda languages
293	eng	Creek
294	eng	Mirandese
295	eng	Marwari
330	eng	Ossetian; Ossetic
331	eng	Turkish, Ottoman (1500-1928)
332	eng	Otomian languages
370	eng	Sasak
371	eng	Santali
372	eng	Serbian
373	eng	Sicilian
412	eng	Swahili
413	eng	Swedish
414	eng	Syriac
415	eng	Tahitian
416	eng	Tai (Other)
457	eng	Venda
458	eng	Vietnamese
459	eng	Volapük
460	eng	Votic
483	eng	N'Ko
484	eng	Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki
39	fre	Bamilékés, langues
40	fre	Bachkir
41	fre	Baloutchi
42	fre	Bambara
82	fre	Chinook, jargon
83	fre	Choctaw
84	fre	Chipewyan
85	fre	Cherokee
120	fre	Égyptien
121	fre	Ekajuk
122	fre	Élamite
123	fre	Anglais
124	fre	Anglais moyen (1100-1500)
160	fre	Gorontalo
161	fre	Gothique
162	fre	Grebo
163	fre	Grec ancien (jusqu'à 1453)
164	fre	Grec moderne (après 1453)
165	fre	Guarani
166	fre	Alémanique
205	fre	Judéo-persan
206	fre	Judéo-arabe
207	fre	Karakalpak
208	fre	Kabyle
248	fre	Limbourgeois
249	fre	Lingala
250	fre	Lituanien
251	fre	Mongo
252	fre	Lozi
292	fre	Mounda, langues
293	fre	Muskogee
294	fre	Mirandais
331	fre	Turc ottoman (1500-1928)
332	fre	Otomangue, langues
369	fre	Sanskrit
370	fre	Sasak
371	fre	Santal
372	fre	Serbe
373	fre	Sicilien
411	fre	Sumérien
412	fre	Swahili
413	fre	Suédois
414	fre	Syriaque
415	fre	Tahitien
455	fre	Ouszbek
456	fre	Vaï
457	fre	Venda
458	fre	Vietnamien
459	fre	Volapük
460	fre	Vote
484	fre	Zaza; dimili; dimli; kirdki; kirmanjki; zazaki
\.


--
-- Data for Name: languages; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.languages (id, name, isinspire, isdefault) FROM stdin;
ara	العربية	n	n
cat	Català	n	n
chi	中文	n	n
dut	Nederlands	y	n
eng	English	y	y
fin	Suomi	y	n
fre	Français	y	n
ger	Deutsch	y	n
nor	Norsk	n	n
por	Português	y	n
rus	русский язык	n	n
spa	Español	y	n
vie	Tiếng Việt	n	n
\.


--
-- Data for Name: mapservers; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.mapservers (id, configurl, description, name, namespace, namespaceprefix, password, stylerurl, username, wcsurl, wfsurl, wmsurl, pushstyleinworkspace) FROM stdin;
\.


--
-- Data for Name: metadata; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) FROM stdin;
1	c4e06459-add2-47e3-86a7-14addeb70e93	iso19139	y	n	2010-03-04T15:46:22	2010-03-04T15:46:22	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink">\n  <gmd:fileIdentifier>\n    <gco:CharacterString>c4e06459-add2-47e3-86a7-14addeb70e93</gco:CharacterString>\n  </gmd:fileIdentifier>\n  <gmd:language>\n    \n  <gco:CharacterString>fre</gco:CharacterString></gmd:language>\n  <gmd:characterSet>\n    <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n  </gmd:characterSet>\n  <gmd:hierarchyLevel>\n    <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="series"/>\n  </gmd:hierarchyLevel><gmd:contact>\n    <gmd:CI_ResponsibleParty>\n      <gmd:individualName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:individualName>\n      <gmd:organisationName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:organisationName>\n      <gmd:positionName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:positionName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone>\n              <gmd:voice gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:voice>\n              <gmd:facsimile gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:facsimile>\n            </gmd:CI_Telephone>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:deliveryPoint gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:deliveryPoint>\n              <gmd:city gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:city>\n              <gmd:administrativeArea gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:administrativeArea>\n              <gmd:postalCode gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:postalCode>\n              <gmd:country gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:country>\n              <gmd:electronicMailAddress gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="originator"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gmd:contact>\n  <gmd:dateStamp>\n    <gco:DateTime>2010-06-18T16:01:29</gco:DateTime>\n  </gmd:dateStamp>\n  <gmd:metadataStandardName>\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n  </gmd:metadataStandardName>\n  <gmd:metadataStandardVersion>\n    <gco:CharacterString>1.0</gco:CharacterString>\n  </gmd:metadataStandardVersion>\n  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>\n    <gmd:MD_DataIdentification>\n      <gmd:citation>\n        <gmd:CI_Citation>\n          <gmd:title gco:nilReason="missing">\n            <gco:CharacterString>métadonnée d'ensemble de séries de données</gco:CharacterString>\n          </gmd:title><gmd:date>\n            <gmd:CI_Date>\n              <gmd:date>\n                <gco:DateTime/>\n              </gmd:date>\n              <gmd:dateType>\n                <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="publication"/>\n              </gmd:dateType>\n            </gmd:CI_Date>\n          </gmd:date>\n          \n          \n        </gmd:CI_Citation>\n      </gmd:citation>\n      <gmd:abstract gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:abstract>\n      <gmd:pointOfContact>\n        <gmd:CI_ResponsibleParty>\n          <gmd:individualName gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:individualName>\n          <gmd:organisationName gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:organisationName>\n          <gmd:contactInfo>\n            <gmd:CI_Contact>\n              <gmd:phone>\n                <gmd:CI_Telephone>\n                  <gmd:voice gco:nilReason="missing">\n                    <gco:CharacterString/>\n                  </gmd:voice>\n                </gmd:CI_Telephone>\n              </gmd:phone>\n              <gmd:address>\n                <gmd:CI_Address>\n                  <gmd:electronicMailAddress gco:nilReason="missing">\n                    <gco:CharacterString/>\n                  </gmd:electronicMailAddress>\n                </gmd:CI_Address>\n              </gmd:address>\n            </gmd:CI_Contact>\n          </gmd:contactInfo>\n          <gmd:role>\n            <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="distributor"/>\n          </gmd:role>\n        </gmd:CI_ResponsibleParty>\n      </gmd:pointOfContact>\n      <gmd:descriptiveKeywords>\n        <gmd:MD_Keywords>\n          <gmd:keyword gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:keyword>\n          <gmd:keyword gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n          </gmd:type>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:descriptiveKeywords xlink:href="https://catalogue-p40.alkante.com/geonetwork/srv/fr/xml.keyword.get?thesaurus=external.place.regions&amp;id=http://geonetwork-opensource.org/regions%2368&amp;multiple=false" xlink:title="" xlink:role="pointOfContact">\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>France</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="place"/>\n          </gmd:type>\n          <gmd:thesaurusName>\n            <gmd:CI_Citation>\n              <gmd:title>\n                <gco:CharacterString>external.place.regions</gco:CharacterString>\n              </gmd:title>\n              <gmd:date gco:nilReason="unknown"/>\n            </gmd:CI_Citation>\n          </gmd:thesaurusName>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:resourceConstraints>\n        <gmd:MD_LegalConstraints>\n          <gmd:useLimitation gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:useLimitation>\n          <gmd:accessConstraints>\n            <gmd:MD_RestrictionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode" codeListValue="restricted"/>\n          </gmd:accessConstraints>\n          <gmd:useConstraints>\n            <gmd:MD_RestrictionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode" codeListValue="copyright"/>\n          </gmd:useConstraints>\n          <gmd:otherConstraints gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:otherConstraints>\n        </gmd:MD_LegalConstraints>\n      </gmd:resourceConstraints>\n      <gmd:resourceConstraints>\n        <gmd:MD_SecurityConstraints>\n          <gmd:classification>\n            <gmd:MD_ClassificationCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ClassificationCode" codeListValue=""/>\n          </gmd:classification>\n          <gmd:userNote gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:userNote>\n        </gmd:MD_SecurityConstraints>\n      </gmd:resourceConstraints>\n      <gmd:spatialRepresentationType>\n        <gmd:MD_SpatialRepresentationTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_SpatialRepresentationTypeCode" codeListValue="vector"/>\n      </gmd:spatialRepresentationType>\n      <gmd:spatialResolution>\n        <gmd:MD_Resolution>\n          <gmd:equivalentScale>\n            <gmd:MD_RepresentativeFraction>\n              <gmd:denominator>\n                <gco:Integer/>\n              </gmd:denominator>\n            </gmd:MD_RepresentativeFraction>\n          </gmd:equivalentScale>\n        </gmd:MD_Resolution>\n      </gmd:spatialResolution>\n      <gmd:spatialResolution>\n        <gmd:MD_Resolution>\n          <gmd:distance/>\n        </gmd:MD_Resolution>\n      </gmd:spatialResolution>\n      <gmd:language>\n        <gco:CharacterString>fre</gco:CharacterString>\n      </gmd:language>\n      <gmd:characterSet>\n        <gmd:MD_CharacterSetCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n      </gmd:characterSet>\n      <gmd:topicCategory>\n        <gmd:MD_TopicCategoryCode/>\n      </gmd:topicCategory>\n      <gmd:extent>\n        <gmd:EX_Extent>\n          <gmd:description gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:description>\n          <gmd:geographicElement>\n            <gmd:EX_GeographicBoundingBox>\n              <gmd:westBoundLongitude>\n                <gco:Decimal>-4.31</gco:Decimal>\n              </gmd:westBoundLongitude>\n              <gmd:eastBoundLongitude>\n                <gco:Decimal>8.40</gco:Decimal>\n              </gmd:eastBoundLongitude>\n              <gmd:southBoundLatitude>\n                <gco:Decimal>41.24</gco:Decimal>\n              </gmd:southBoundLatitude>\n              <gmd:northBoundLatitude>\n                <gco:Decimal>51</gco:Decimal>\n              </gmd:northBoundLatitude>\n            </gmd:EX_GeographicBoundingBox>\n          </gmd:geographicElement>\n          \n        </gmd:EX_Extent>\n      </gmd:extent><gmd:supplementalInformation gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:supplementalInformation>\n      \n      \n    </gmd:MD_DataIdentification>\n  </gmd:identificationInfo>\n  <gmd:dataQualityInfo>\n    <gmd:DQ_DataQuality>\n      <gmd:scope>\n        <gmd:DQ_Scope>\n          <gmd:level>\n            <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="series"/>\n          </gmd:level>\n        </gmd:DQ_Scope>\n      </gmd:scope>\n      <gmd:lineage>\n        <gmd:LI_Lineage>\n          <gmd:statement gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:statement>\n          <gmd:source>\n            <gmd:LI_Source>\n              <gmd:description gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:description>\n            </gmd:LI_Source>\n          </gmd:source>\n        </gmd:LI_Lineage>\n      </gmd:lineage>\n    </gmd:DQ_DataQuality>\n  </gmd:dataQualityInfo>\n  \n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	gmd:MD_Metadata	\N	1	1	\N	0	0	\N	\N	\N
40085	9ba5f3e9-c0ef-4160-81c0-b633b6067601	iso19139	y	n	2012-04-06T10:42:35	2012-04-06T10:42:35	<gmd:CI_ResponsibleParty xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gml="http://www.opengis.net/gml" xmlns:geonet="http://www.fao.org/geonetwork">\r\n  <gmd:individualName>\r\n    <gco:CharacterString>Nom</gco:CharacterString>\r\n  </gmd:individualName>\r\n  <gmd:organisationName>\r\n    <gco:CharacterString>Organisation</gco:CharacterString>\r\n  </gmd:organisationName>\r\n  <gmd:positionName>\r\n    <gco:CharacterString>Fonction</gco:CharacterString>\r\n  </gmd:positionName>\r\n  <gmd:contactInfo>\r\n    <gmd:CI_Contact>\r\n      <gmd:address>\r\n        <gmd:CI_Address>\r\n          <gmd:electronicMailAddress>\r\n            <gco:CharacterString>Email</gco:CharacterString>\r\n          </gmd:electronicMailAddress>\r\n        </gmd:CI_Address>\r\n      </gmd:address>\r\n    </gmd:CI_Contact>\r\n  </gmd:contactInfo>\r\n  <gmd:role>\r\n    <gmd:CI_RoleCode codeList="./resources/codeList.xml#CI_RoleCode" codeListValue="pointOfContact" />\r\n  </gmd:role>\r\n</gmd:CI_ResponsibleParty>	dd56a2d3-8ec6-4bc4-9dca-4a04bcfc6e61	PointOfContact	gmd:CI_ResponsibleParty	\N	1	1	\N	0	1	\N	\N	\N
2	493039f0-eef0-11da-a2ce-000c2929140b	iso19139	y	n	2006-05-29T10:51:14	2006-07-27T09:02:02	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink">\n  <gmd:fileIdentifier>\n    <gco:CharacterString>493039f0-eef0-11da-a2ce-000c2929140b</gco:CharacterString>\n  </gmd:fileIdentifier>\n  <gmd:language>\n    \n  <gco:CharacterString>fre</gco:CharacterString></gmd:language>\n  <gmd:characterSet>\n    <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n  </gmd:characterSet>\n  <gmd:hierarchyLevel>\n    <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="dataset"/>\n  </gmd:hierarchyLevel><gmd:contact>\n    <gmd:CI_ResponsibleParty>\n      <gmd:individualName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:individualName>\n      <gmd:organisationName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:organisationName>\n      <gmd:positionName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:positionName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone>\n              <gmd:voice gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:voice>\n              <gmd:facsimile gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:facsimile>\n            </gmd:CI_Telephone>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:deliveryPoint gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:deliveryPoint>\n              <gmd:city gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:city>\n              <gmd:administrativeArea gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:administrativeArea>\n              <gmd:postalCode gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:postalCode>\n              <gmd:country gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:country>\n              <gmd:electronicMailAddress gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="originator"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gmd:contact>\n  <gmd:dateStamp>\n    <gco:DateTime>2010-06-18T15:55:36</gco:DateTime>\n  </gmd:dateStamp>\n  <gmd:metadataStandardName>\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n  </gmd:metadataStandardName>\n  <gmd:metadataStandardVersion>\n    <gco:CharacterString>1.0</gco:CharacterString>\n  </gmd:metadataStandardVersion>\n  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>\n    <gmd:MD_DataIdentification>\n      <gmd:citation>\n        <gmd:CI_Citation>\n          <gmd:title gco:nilReason="missing">\n            <gco:CharacterString>métadonnée de série de données standard</gco:CharacterString>\n          </gmd:title><gmd:date>\n            <gmd:CI_Date>\n              <gmd:date>\n                <gco:DateTime/>\n              </gmd:date>\n              <gmd:dateType>\n                <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="publication"/>\n              </gmd:dateType>\n            </gmd:CI_Date>\n          </gmd:date>\n          \n          \n        </gmd:CI_Citation>\n      </gmd:citation>\n      <gmd:abstract gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:abstract>\n      <gmd:pointOfContact>\n        <gmd:CI_ResponsibleParty>\n          <gmd:individualName gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:individualName>\n          <gmd:organisationName gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:organisationName>\n          <gmd:contactInfo>\n            <gmd:CI_Contact>\n              <gmd:phone>\n                <gmd:CI_Telephone>\n                  <gmd:voice gco:nilReason="missing">\n                    <gco:CharacterString/>\n                  </gmd:voice>\n                </gmd:CI_Telephone>\n              </gmd:phone>\n              <gmd:address>\n                <gmd:CI_Address>\n                  <gmd:electronicMailAddress gco:nilReason="missing">\n                    <gco:CharacterString/>\n                  </gmd:electronicMailAddress>\n                </gmd:CI_Address>\n              </gmd:address>\n            </gmd:CI_Contact>\n          </gmd:contactInfo>\n          <gmd:role>\n            <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="distributor"/>\n          </gmd:role>\n        </gmd:CI_ResponsibleParty>\n      </gmd:pointOfContact>\n      <gmd:descriptiveKeywords>\n        <gmd:MD_Keywords>\n          <gmd:keyword gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:keyword>\n          <gmd:keyword gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n          </gmd:type>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:descriptiveKeywords xlink:href="https://catalogue-p40.alkante.com/geonetwork/srv/fr/xml.keyword.get?thesaurus=external.place.regions&amp;id=http://geonetwork-opensource.org/regions%2368&amp;multiple=false" xlink:title="" xlink:role="pointOfContact">\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>France</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="place"/>\n          </gmd:type>\n          <gmd:thesaurusName>\n            <gmd:CI_Citation>\n              <gmd:title>\n                <gco:CharacterString>external.place.regions</gco:CharacterString>\n              </gmd:title>\n              <gmd:date gco:nilReason="unknown"/>\n            </gmd:CI_Citation>\n          </gmd:thesaurusName>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:resourceConstraints>\n        <gmd:MD_LegalConstraints>\n          <gmd:useLimitation gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:useLimitation>\n          <gmd:accessConstraints>\n            <gmd:MD_RestrictionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode" codeListValue="restricted"/>\n          </gmd:accessConstraints>\n          <gmd:useConstraints>\n            <gmd:MD_RestrictionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode" codeListValue="copyright"/>\n          </gmd:useConstraints>\n          <gmd:otherConstraints gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:otherConstraints>\n        </gmd:MD_LegalConstraints>\n      </gmd:resourceConstraints>\n      <gmd:resourceConstraints>\n        <gmd:MD_SecurityConstraints>\n          <gmd:classification>\n            <gmd:MD_ClassificationCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ClassificationCode" codeListValue=""/>\n          </gmd:classification>\n          <gmd:userNote gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:userNote>\n        </gmd:MD_SecurityConstraints>\n      </gmd:resourceConstraints>\n      <gmd:spatialRepresentationType>\n        <gmd:MD_SpatialRepresentationTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_SpatialRepresentationTypeCode" codeListValue="vector"/>\n      </gmd:spatialRepresentationType>\n      <gmd:spatialResolution>\n        <gmd:MD_Resolution>\n          <gmd:equivalentScale>\n            <gmd:MD_RepresentativeFraction>\n              <gmd:denominator>\n                <gco:Integer/>\n              </gmd:denominator>\n            </gmd:MD_RepresentativeFraction>\n          </gmd:equivalentScale>\n        </gmd:MD_Resolution>\n      </gmd:spatialResolution>\n      <gmd:spatialResolution>\n        <gmd:MD_Resolution>\n          <gmd:distance/>\n        </gmd:MD_Resolution>\n      </gmd:spatialResolution>\n      <gmd:language>\n        <gco:CharacterString>fre</gco:CharacterString>\n      </gmd:language>\n      <gmd:characterSet>\n        <gmd:MD_CharacterSetCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n      </gmd:characterSet>\n      <gmd:topicCategory>\n        <gmd:MD_TopicCategoryCode/>\n      </gmd:topicCategory>\n      <gmd:extent>\n        <gmd:EX_Extent>\n          <gmd:description gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:description>\n          <gmd:geographicElement>\n            <gmd:EX_GeographicBoundingBox>\n              <gmd:westBoundLongitude>\n                <gco:Decimal>-4.31</gco:Decimal>\n              </gmd:westBoundLongitude>\n              <gmd:eastBoundLongitude>\n                <gco:Decimal>8.40</gco:Decimal>\n              </gmd:eastBoundLongitude>\n              <gmd:southBoundLatitude>\n                <gco:Decimal>41.24</gco:Decimal>\n              </gmd:southBoundLatitude>\n              <gmd:northBoundLatitude>\n                <gco:Decimal>51</gco:Decimal>\n              </gmd:northBoundLatitude>\n            </gmd:EX_GeographicBoundingBox>\n          </gmd:geographicElement>\n          \n        </gmd:EX_Extent>\n      </gmd:extent><gmd:supplementalInformation gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:supplementalInformation>\n      \n      \n    </gmd:MD_DataIdentification>\n  </gmd:identificationInfo>\n  <gmd:dataQualityInfo>\n    <gmd:DQ_DataQuality>\n      <gmd:scope>\n        <gmd:DQ_Scope>\n          <gmd:level>\n            <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="dataset"/>\n          </gmd:level>\n        </gmd:DQ_Scope>\n      </gmd:scope>\n      <gmd:lineage>\n        <gmd:LI_Lineage>\n          <gmd:statement gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:statement>\n          <gmd:source>\n            <gmd:LI_Source>\n              <gmd:description gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:description>\n            </gmd:LI_Source>\n          </gmd:source>\n        </gmd:LI_Lineage>\n      </gmd:lineage>\n    </gmd:DQ_DataQuality>\n  </gmd:dataQualityInfo>\n  \n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	\N	\N	1	1	\N	0	1	\N	\N	\N
11	c4e06459-add2-47e3-86a7-14addeb70e92	iso19110	y	n	2010-03-04T15:46:22	2010-03-04T15:46:22	<gfc:FC_FeatureCatalogue xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gmd="http://www.isotc211.org/2005/gmd" xsi:schemaLocation="http://www.isotc211.org/2005/gfc ../../../../web/geonetwork/xml/schemas/iso19110/schema.xsd">\n  <gfc:name>\n    <gco:CharacterString>Template de catalogue d'attributs</gco:CharacterString>\n  </gfc:name>\n  <gfc:scope>\n    <gco:CharacterString>http://url.of.your.document</gco:CharacterString>\n  </gfc:scope>\n  <gfc:fieldOfApplication>\n    <gco:CharacterString>Dictionnaire de données pour ...</gco:CharacterString>\n  </gfc:fieldOfApplication>\n  <gfc:versionNumber>\n    <gco:CharacterString/>\n  </gfc:versionNumber>\n  <gfc:versionDate>\n    <gco:Date>2008-04-17</gco:Date>\n  </gfc:versionDate>\n  <gfc:producer>\n    <gmd:CI_ResponsibleParty>\n      <gmd:individualName>\n        <gco:CharacterString/>\n      </gmd:individualName>\n      <gmd:organisationName>\n        <gco:CharacterString/>\n      </gmd:organisationName>\n      <gmd:positionName>\n        <gco:CharacterString/>\n      </gmd:positionName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone>\n              <gmd:voice>\n                <gco:CharacterString/>\n              </gmd:voice>\n              <gmd:facsimile>\n                <gco:CharacterString/>\n              </gmd:facsimile>\n            </gmd:CI_Telephone>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:deliveryPoint>\n                <gco:CharacterString/>\n              </gmd:deliveryPoint>\n              <gmd:city>\n                <gco:CharacterString/>\n              </gmd:city>\n              <gmd:administrativeArea>\n                <gco:CharacterString/>\n              </gmd:administrativeArea>\n              <gmd:postalCode>\n                <gco:CharacterString/>\n              </gmd:postalCode>\n              <gmd:country>\n                <gco:CharacterString/>\n              </gmd:country>\n              <gmd:electronicMailAddress>\n                <gco:CharacterString/>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeListValue="principalInvestigator" codeList="CI_RoleCode"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gfc:producer>\n  <gfc:featureType>\n    <gfc:FC_FeatureType>\n      <!--id="Table attributaire associée"-->\n      <gfc:typeName>\n        <gco:LocalName>Nom de la propriété</gco:LocalName>\n      </gfc:typeName>\n      <gfc:definition>\n        <gco:CharacterString>Definition de la table d'attributs</gco:CharacterString>\n      </gfc:definition>\n      <gfc:isAbstract>\n        <gco:Boolean>false</gco:Boolean>\n      </gfc:isAbstract>\n      <gfc:aliases>\n        <gco:LocalName/>\n      </gfc:aliases>\n      <gfc:featureCatalogue/>\n      <gfc:carrierOfCharacteristics>\n        <gfc:FC_FeatureAttribute>\n          <!--id="Table attributaire associée.AREAé"-->\n          <gfc:memberName>\n            <gco:LocalName>superficie en m²</gco:LocalName>\n          </gfc:memberName>\n          <gfc:definition>\n            <gco:CharacterString>superficie en m²</gco:CharacterString>\n          </gfc:definition>\n          <gfc:cardinality>\n            <gco:Multiplicity>\n              <gco:range>\n                <gco:MultiplicityRange>\n                  <gco:lower>\n                    <gco:Integer>0</gco:Integer>\n                  </gco:lower>\n                  <gco:upper>\n                    <gco:UnlimitedInteger isInfinite="false">1</gco:UnlimitedInteger>\n                  </gco:upper>\n                </gco:MultiplicityRange>\n              </gco:range>\n            </gco:Multiplicity>\n          </gfc:cardinality>\n          <gfc:featureType/>\n          <gfc:valueMeasurementUnit>\n            <gml:UnitDefinition xmlns:gml="http://www.opengis.net/gml" gml:id="unknown">\n              <gml:description/>\n              <gml:identifier codeSpace="unknown"/>\n            </gml:UnitDefinition>\n          </gfc:valueMeasurementUnit>\n          <gfc:listedValue>\n            <gfc:FC_ListedValue>\n              <gfc:label>\n                <gco:CharacterString/>\n              </gfc:label>\n            </gfc:FC_ListedValue>\n          </gfc:listedValue>\n          <gfc:valueType>\n            <gco:TypeName>\n              <gco:aName>\n                <gco:CharacterString>Numérique</gco:CharacterString>\n              </gco:aName>\n            </gco:TypeName>\n          </gfc:valueType>\n        </gfc:FC_FeatureAttribute>\n      </gfc:carrierOfCharacteristics>\n      <gfc:carrierOfCharacteristics>\n        <gfc:FC_FeatureAttribute>\n          <!--id="Table attributaire associée.PERIMETER"-->\n          <gfc:memberName>\n            <gco:LocalName>périmètre en mètres</gco:LocalName>\n          </gfc:memberName>\n          <gfc:definition>\n            <gco:CharacterString>définition périmètre</gco:CharacterString>\n          </gfc:definition>\n          <gfc:cardinality>\n            <gco:Multiplicity>\n              <gco:range>\n                <gco:MultiplicityRange>\n                  <gco:lower>\n                    <gco:Integer>0</gco:Integer>\n                  </gco:lower>\n                  <gco:upper>\n                    <gco:UnlimitedInteger isInfinite="true" xsi:nil="true"/>\n                  </gco:upper>\n                </gco:MultiplicityRange>\n              </gco:range>\n            </gco:Multiplicity>\n          </gfc:cardinality>\n          <gfc:featureType/>\n          <gfc:valueMeasurementUnit>\n            <gml:UnitDefinition xmlns:gml="http://www.opengis.net/gml" gml:id="null">\n              <gml:identifier codeSpace="unknown"/>\n            </gml:UnitDefinition>\n          </gfc:valueMeasurementUnit>\n          <gfc:valueType>\n            <gco:TypeName>\n              <gco:aName>\n                <gco:CharacterString>Numérique</gco:CharacterString>\n              </gco:aName>\n            </gco:TypeName>\n          </gfc:valueType>\n        </gfc:FC_FeatureAttribute>\n      </gfc:carrierOfCharacteristics>\n      <gfc:carrierOfCharacteristics>\n        <gfc:FC_FeatureAttribute>\n          <!--id="Table attributaire associée.CODE_00"-->\n          <gfc:memberName>\n            <gco:LocalName>code selon nomenclature CLC niveau 3</gco:LocalName>\n          </gfc:memberName>\n          <gfc:definition>\n            <gco:CharacterString>code selon nomenclature CLC niveau 3</gco:CharacterString>\n          </gfc:definition>\n          <gfc:cardinality>\n            <gco:Multiplicity>\n              <gco:range>\n                <gco:MultiplicityRange>\n                  <gco:lower>\n                    <gco:Integer>0</gco:Integer>\n                  </gco:lower>\n                  <gco:upper>\n                    <gco:UnlimitedInteger isInfinite="true" xsi:nil="true"/>\n                  </gco:upper>\n                </gco:MultiplicityRange>\n              </gco:range>\n            </gco:Multiplicity>\n          </gfc:cardinality>\n          <gfc:featureType/>\n          <gfc:valueMeasurementUnit>\n            <gml:UnitDefinition xmlns:gml="http://www.opengis.net/gml" gml:id="unknown_1">\n              <gml:identifier codeSpace="unknown"/>\n            </gml:UnitDefinition>\n          </gfc:valueMeasurementUnit>\n          <gfc:listedValue>\n            <gfc:FC_ListedValue>\n              <gfc:label>\n                <gco:CharacterString>1.1.1.</gco:CharacterString>\n              </gfc:label>\n              <gfc:code>\n                <gco:CharacterString/>\n              </gfc:code>\n              <gfc:definition>\n                <gco:CharacterString/>\n              </gfc:definition>\n            </gfc:FC_ListedValue>\n          </gfc:listedValue>\n          <gfc:listedValue>\n            <gfc:FC_ListedValue>\n              <gfc:label>\n                <gco:CharacterString>1.1.2.</gco:CharacterString>\n              </gfc:label>\n              <gfc:code>\n                <gco:CharacterString/>\n              </gfc:code>\n              <gfc:definition>\n                <gco:CharacterString/>\n              </gfc:definition>\n            </gfc:FC_ListedValue>\n          </gfc:listedValue>\n          <gfc:valueType>\n            <gco:TypeName>\n              <gco:aName>\n                <gco:CharacterString>Chaine de caractères</gco:CharacterString>\n              </gco:aName>\n            </gco:TypeName>\n          </gfc:valueType>\n        </gfc:FC_FeatureAttribute>\n      </gfc:carrierOfCharacteristics>\n    </gfc:FC_FeatureType>\n  </gfc:featureType>\n</gfc:FC_FeatureCatalogue>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	gfc:FC_FeatureCatalogue	\N	1	1	\N	0	0	\N	\N	\N
14	274971da-ef09-4e3b-9a5e-b236fd670874	iso19110	n	n	2013-06-26T17:40:19	2013-06-26T17:40:19	<gfc:FC_FeatureCatalogue xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gmd="http://www.isotc211.org/2005/gmd" uuid="274971da-ef09-4e3b-9a5e-b236fd670874" xsi:schemaLocation="http://www.isotc211.org/2005/gfc ../../../../web/geonetwork/xml/schemas/iso19110/schema.xsd">\n  <gfc:name>\n    <gco:CharacterString>Template de catalogue d'attributs</gco:CharacterString>\n  </gfc:name>\n  <gfc:scope>\n    <gco:CharacterString>http://url.of.your.document</gco:CharacterString>\n  </gfc:scope>\n  <gfc:fieldOfApplication>\n    <gco:CharacterString>Dictionnaire de données pour ...</gco:CharacterString>\n  </gfc:fieldOfApplication>\n  <gfc:versionNumber>\n    <gco:CharacterString/>\n  </gfc:versionNumber>\n  <gfc:versionDate>\n    <gco:DateTime xmlns:gmx="http://www.isotc211.org/2005/gmx">2013-06-26T17:40:19</gco:DateTime>\n  </gfc:versionDate>\n  <gfc:producer>\n    <gmd:CI_ResponsibleParty>\n      <gmd:individualName>\n        <gco:CharacterString/>\n      </gmd:individualName>\n      <gmd:organisationName>\n        <gco:CharacterString/>\n      </gmd:organisationName>\n      <gmd:positionName>\n        <gco:CharacterString/>\n      </gmd:positionName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone>\n              <gmd:voice>\n                <gco:CharacterString/>\n              </gmd:voice>\n              <gmd:facsimile>\n                <gco:CharacterString/>\n              </gmd:facsimile>\n            </gmd:CI_Telephone>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:deliveryPoint>\n                <gco:CharacterString/>\n              </gmd:deliveryPoint>\n              <gmd:city>\n                <gco:CharacterString/>\n              </gmd:city>\n              <gmd:administrativeArea>\n                <gco:CharacterString/>\n              </gmd:administrativeArea>\n              <gmd:postalCode>\n                <gco:CharacterString/>\n              </gmd:postalCode>\n              <gmd:country>\n                <gco:CharacterString/>\n              </gmd:country>\n              <gmd:electronicMailAddress>\n                <gco:CharacterString/>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeListValue="principalInvestigator" codeList="CI_RoleCode"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gfc:producer>\n  <gfc:featureType>\n    <gfc:FC_FeatureType>\n      <!--id="Table attributaire associée"-->\n      <gfc:typeName>\n        <gco:LocalName>Table</gco:LocalName>\n      </gfc:typeName>\n      <gfc:definition>\n        <gco:CharacterString>Definition de la table d'attributs</gco:CharacterString>\n      </gfc:definition>\n      <gfc:isAbstract>\n        <gco:Boolean>false</gco:Boolean>\n      </gfc:isAbstract>\n      <gfc:aliases>\n        <gco:LocalName/>\n      </gfc:aliases>\n      <gfc:featureCatalogue/>\n      \n      \n      \n    <gfc:carrierOfCharacteristics><gfc:FC_FeatureAttribute><gfc:memberName><gco:LocalName xmlns:gco="http://www.isotc211.org/2005/gco">id_geofla</gco:LocalName></gfc:memberName><gfc:definition><gco:CharacterString xmlns:gco="http://www.isotc211.org/2005/gco"/></gfc:definition><gfc:cardinality><gco:Multiplicity xmlns:gco="http://www.isotc211.org/2005/gco"><gco:range><gco:MultiplicityRange><gco:lower><gco:Integer>0</gco:Integer></gco:lower><gco:upper><gco:UnlimitedInteger isInfinite="true" xsi:nil="true"/></gco:upper></gco:MultiplicityRange></gco:range></gco:Multiplicity></gfc:cardinality><gfc:featureType/><gfc:valueMeasurementUnit><gml:UnitDefinition xmlns:gml="http://www.opengis.net/gml" gml:id="null"><gml:identifier codeSpace="unknown"/></gml:UnitDefinition></gfc:valueMeasurementUnit><gfc:valueType><gco:TypeName xmlns:gco="http://www.isotc211.org/2005/gco"><gco:aName><gco:CharacterString>Numérique</gco:CharacterString></gco:aName></gco:TypeName></gfc:valueType></gfc:FC_FeatureAttribute></gfc:carrierOfCharacteristics><gfc:carrierOfCharacteristics><gfc:FC_FeatureAttribute><gfc:memberName><gco:LocalName xmlns:gco="http://www.isotc211.org/2005/gco">nature</gco:LocalName></gfc:memberName><gfc:definition><gco:CharacterString xmlns:gco="http://www.isotc211.org/2005/gco"/></gfc:definition><gfc:cardinality><gco:Multiplicity xmlns:gco="http://www.isotc211.org/2005/gco"><gco:range><gco:MultiplicityRange><gco:lower><gco:Integer>0</gco:Integer></gco:lower><gco:upper><gco:UnlimitedInteger isInfinite="true" xsi:nil="true"/></gco:upper></gco:MultiplicityRange></gco:range></gco:Multiplicity></gfc:cardinality><gfc:featureType/><gfc:valueMeasurementUnit><gml:UnitDefinition xmlns:gml="http://www.opengis.net/gml" gml:id="null"><gml:identifier codeSpace="unknown"/></gml:UnitDefinition></gfc:valueMeasurementUnit><gfc:valueType><gco:TypeName xmlns:gco="http://www.isotc211.org/2005/gco"><gco:aName><gco:CharacterString>Chaine de caractères</gco:CharacterString></gco:aName></gco:TypeName></gfc:valueType></gfc:FC_FeatureAttribute></gfc:carrierOfCharacteristics></gfc:FC_FeatureType>\n  </gfc:featureType>\n</gfc:FC_FeatureCatalogue>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	gfc:FC_FeatureCatalogue	\N	1	1	\N	0	1	\N		\N
40089	88cacc48-04c5-42a6-b5ed-d1d375061544	iso19139	y	n	2010-03-04T15:46:22	2010-03-04T15:46:22	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.isotc211.org/2005/fra ../../../../web/geonetwork/xml/schemas/iso19139fra/schema.xsd">\n\n  <gmd:fileIdentifier>\n\n    <gco:CharacterString>88cacc48-04c5-42a6-b5ed-d1d375061544</gco:CharacterString>\n\n  </gmd:fileIdentifier>\n\n  <gmd:language>\n\n    \n\n  <gco:CharacterString>fre</gco:CharacterString></gmd:language>\n\n  <gmd:characterSet>\n\n    <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n\n  </gmd:characterSet>\n\n  <gmd:hierarchyLevel>\n\n    <gmd:MD_ScopeCode codeList="./resources/codeList.xml#MD_ScopeCode" codeListValue="dataset"/>\n\n  </gmd:hierarchyLevel><gmd:contact>\n\n    <gmd:CI_ResponsibleParty>\n\n      <gmd:individualName>\n\n        <gco:CharacterString/>\n\n      </gmd:individualName>\n\n      <gmd:organisationName>\n\n        <gco:CharacterString/>\n\n      </gmd:organisationName>\n\n      <gmd:positionName>\n\n        <gco:CharacterString/>\n\n      </gmd:positionName>\n\n      <gmd:contactInfo>\n\n        <gmd:CI_Contact>\n\n          <gmd:phone>\n\n            <gmd:CI_Telephone>\n\n              <gmd:voice>\n\n                <gco:CharacterString/>\n\n              </gmd:voice>\n\n              <gmd:facsimile>\n\n                <gco:CharacterString/>\n\n              </gmd:facsimile>\n\n            </gmd:CI_Telephone>\n\n          </gmd:phone>\n\n          <gmd:address>\n\n            <gmd:CI_Address>\n\n              <gmd:deliveryPoint>\n\n                <gco:CharacterString/>\n\n              </gmd:deliveryPoint>\n\n              <gmd:city>\n\n                <gco:CharacterString/>\n\n              </gmd:city>\n\n              <gmd:administrativeArea>\n\n                <gco:CharacterString/>\n\n              </gmd:administrativeArea>\n\n              <gmd:postalCode>\n\n                <gco:CharacterString/>\n\n              </gmd:postalCode>\n\n              <gmd:country>\n\n                <gco:CharacterString/>\n\n              </gmd:country>\n\n              <gmd:electronicMailAddress>\n\n                <gco:CharacterString/>\n\n              </gmd:electronicMailAddress>\n\n            </gmd:CI_Address>\n\n          </gmd:address>\n\n        </gmd:CI_Contact>\n\n      </gmd:contactInfo>\n\n      <gmd:role>\n\n        <gmd:CI_RoleCode codeList="./resources/codeList.xml#CI_RoleCode" codeListValue="originator"/>\n\n      </gmd:role>\n\n    </gmd:CI_ResponsibleParty>\n\n  </gmd:contact>\n\n  <gmd:dateStamp>\n\n    <gco:DateTime>2006-07-27T09:02:02</gco:DateTime>\n\n  </gmd:dateStamp>\n\n  <gmd:metadataStandardName>\n\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n\n  </gmd:metadataStandardName>\n\n  <gmd:metadataStandardVersion>\n\n    <gco:CharacterString>1.0</gco:CharacterString>\n\n  </gmd:metadataStandardVersion>\n\n  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>\n\n    <gmd:MD_DataIdentification>\n\n      <gmd:citation>\n\n        <gmd:CI_Citation>\n\n          <gmd:title>\n\n            <gco:CharacterString>métadonnées de données MAJIC</gco:CharacterString>\n\n          </gmd:title><gmd:date>\n\n            <gmd:CI_Date>\n\n              <gmd:date>\n\n                <gco:DateTime/>\n\n              </gmd:date>\n\n              <gmd:dateType>\n\n                <gmd:CI_DateTypeCode codeList="./resources/codeList.xml#CI_DateTypeCode" codeListValue="publication"/>\n\n              </gmd:dateType>\n\n            </gmd:CI_Date>\n\n          </gmd:date>\n\n          \n\n          \n\n        </gmd:CI_Citation>\n\n      </gmd:citation>\n\n      <gmd:abstract>\n\n        <gco:CharacterString/>\n\n      </gmd:abstract>\n\n      <gmd:pointOfContact>\n\n        <gmd:CI_ResponsibleParty>\n\n          <gmd:individualName>\n\n            <gco:CharacterString/>\n\n          </gmd:individualName>\n\n          <gmd:organisationName>\n\n            <gco:CharacterString/>\n\n          </gmd:organisationName>\n\n          <gmd:contactInfo>\n\n            <gmd:CI_Contact>\n\n              <gmd:phone>\n\n                <gmd:CI_Telephone>\n\n                  <gmd:voice>\n\n                    <gco:CharacterString/>\n\n                  </gmd:voice>\n\n                </gmd:CI_Telephone>\n\n              </gmd:phone>\n\n              <gmd:address>\n\n                <gmd:CI_Address>\n\n                  <gmd:electronicMailAddress>\n\n                    <gco:CharacterString/>\n\n                  </gmd:electronicMailAddress>\n\n                </gmd:CI_Address>\n\n              </gmd:address>\n\n            </gmd:CI_Contact>\n\n          </gmd:contactInfo>\n\n          <gmd:role>\n\n            <gmd:CI_RoleCode codeList="./resources/codeList.xml#CI_RoleCode" codeListValue="distributor"/>\n\n          </gmd:role>\n\n        </gmd:CI_ResponsibleParty>\n\n      </gmd:pointOfContact>\n\n      <gmd:descriptiveKeywords>\n\n        <gmd:MD_Keywords>\n\n          <gmd:keyword>\n\n            <gco:CharacterString/>\n\n          </gmd:keyword>\n\n          <gmd:keyword>\n\n            <gco:CharacterString/>\n\n          </gmd:keyword>\n\n          <gmd:type>\n\n            <gmd:MD_KeywordTypeCode codeList="./resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n\n          </gmd:type>\n\n        </gmd:MD_Keywords>\n\n      </gmd:descriptiveKeywords>\n\n      <gmd:descriptiveKeywords>\n\n        <gmd:MD_Keywords>\n\n          <gmd:keyword gco:nilReason="missing">\n\n            <gco:CharacterString/>\n\n          </gmd:keyword>\n\n          <gmd:type>\n\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="place"/>\n\n          </gmd:type>\n\n        </gmd:MD_Keywords>\n\n      </gmd:descriptiveKeywords>\n\n      <gmd:resourceConstraints>\n\n        <gmd:MD_LegalConstraints>\n\n          <gmd:useLimitation>\n\n            <gco:CharacterString/>\n\n          </gmd:useLimitation>\n\n          <gmd:accessConstraints>\n\n            <gmd:MD_RestrictionCode codeList="./resources/codeList.xml#MD_RestrictionCode" codeListValue="restricted"/>\n\n          </gmd:accessConstraints>\n\n          <gmd:useConstraints>\n\n            <gmd:MD_RestrictionCode codeList="./resources/codeList.xml#MD_RestrictionCode" codeListValue="copyright"/>\n\n          </gmd:useConstraints>\n\n          <gmd:otherConstraints>\n\n            <gco:CharacterString/>\n\n          </gmd:otherConstraints>\n\n        </gmd:MD_LegalConstraints>\n\n      </gmd:resourceConstraints>\n\n      <gmd:resourceConstraints>\n\n        <gmd:MD_SecurityConstraints>\n\n          <gmd:classification>\n\n            <gmd:MD_ClassificationCode codeList="./resources/codeList.xml#MD_ClassificationCode" codeListValue=""/>\n\n          </gmd:classification>\n\n          <gmd:userNote>\n\n            <gco:CharacterString/>\n\n          </gmd:userNote>\n\n        </gmd:MD_SecurityConstraints>\n\n      </gmd:resourceConstraints>\n\n      <gmd:spatialRepresentationType>\n\n        <gmd:MD_SpatialRepresentationTypeCode codeList="./resources/codeList.xml#MD_SpatialRepresentationTypeCode" codeListValue="textTable"/>\n\n      </gmd:spatialRepresentationType>\n\n      <gmd:spatialResolution>\n\n        <gmd:MD_Resolution>\n\n          <gmd:equivalentScale>\n\n            <gmd:MD_RepresentativeFraction>\n\n              <gmd:denominator>\n\n                <gco:Integer/>\n\n              </gmd:denominator>\n\n            </gmd:MD_RepresentativeFraction>\n\n          </gmd:equivalentScale>\n\n        </gmd:MD_Resolution>\n\n      </gmd:spatialResolution>\n\n      <gmd:spatialResolution>\n\n        <gmd:MD_Resolution>\n\n          <gmd:distance/>\n\n        </gmd:MD_Resolution>\n\n      </gmd:spatialResolution>\n\n      <gmd:language>\n\n        <gco:CharacterString>fre</gco:CharacterString>\n\n      </gmd:language>\n\n      <gmd:characterSet>\n\n        <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n\n      </gmd:characterSet>\n\n      <gmd:topicCategory>\n\n        <gmd:MD_TopicCategoryCode/>\n\n      </gmd:topicCategory>\n\n      \n\n      \n\n      <gmd:extent>\n\n        <gmd:EX_Extent>\n\n          <gmd:description gco:nilReason="missing">\n\n            <gco:CharacterString/>\n\n          </gmd:description>\n\n          <gmd:geographicElement>\n\n            <gmd:EX_GeographicBoundingBox>\n\n              <gmd:westBoundLongitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:westBoundLongitude>\n\n              <gmd:eastBoundLongitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:eastBoundLongitude>\n\n              <gmd:southBoundLatitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:southBoundLatitude>\n\n              <gmd:northBoundLatitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:northBoundLatitude>\n\n            </gmd:EX_GeographicBoundingBox>\n\n          </gmd:geographicElement>\n\n          \n\n        </gmd:EX_Extent>\n\n      </gmd:extent><gmd:supplementalInformation>\n\n        <gco:CharacterString/>\n\n      </gmd:supplementalInformation>\n\n      \n\n    </gmd:MD_DataIdentification>\n\n  </gmd:identificationInfo>\n\n  <gmd:dataQualityInfo>\n\n    <gmd:DQ_DataQuality>\n\n      <gmd:scope>\n\n        <gmd:DQ_Scope>\n\n          <gmd:level>\n\n            <gmd:MD_ScopeCode codeList="./resources/codeList.xml#MD_ScopeCode" codeListValue="dataset"/>\n\n          </gmd:level>\n\n        </gmd:DQ_Scope>\n\n      </gmd:scope>\n\n      <gmd:lineage>\n\n        <gmd:LI_Lineage>\n\n          <gmd:statement>\n\n            <gco:CharacterString/>\n\n          </gmd:statement>\n\n          <gmd:source>\n\n            <gmd:LI_Source>\n\n              <gmd:description>\n\n                <gco:CharacterString/>\n\n              </gmd:description>\n\n            </gmd:LI_Source>\n\n          </gmd:source>\n\n        </gmd:LI_Lineage>\n\n      </gmd:lineage>\n\n    </gmd:DQ_DataQuality>\n\n  </gmd:dataQualityInfo>\n\n  \n\n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	gmd:MD_Metadata	\N	1	1	\N	0	0	\N	\N	\N
3	3f52a630-f3cc-11da-916a-000c2929140b	iso19139	y	n	2006-06-04T15:15:52	2006-06-04T15:15:58	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:srv="http://www.isotc211.org/2005/srv">\n  <gmd:fileIdentifier>\n    <gco:CharacterString>3f52a630-f3cc-11da-916a-000c2929140b</gco:CharacterString>\n  </gmd:fileIdentifier>\n  <gmd:language>\n    \n  <gco:CharacterString>fre</gco:CharacterString></gmd:language>\n  <gmd:characterSet>\n    <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n  </gmd:characterSet>\n  <gmd:hierarchyLevel>\n    <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="service"/>\n  </gmd:hierarchyLevel><gmd:hierarchyLevelName><gco:CharacterString>service</gco:CharacterString></gmd:hierarchyLevelName><gmd:contact>\n    <gmd:CI_ResponsibleParty>\n      <gmd:individualName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:individualName>\n      <gmd:organisationName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:organisationName>\n      <gmd:positionName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:positionName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone>\n              <gmd:voice gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:voice>\n              <gmd:facsimile gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:facsimile>\n            </gmd:CI_Telephone>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:deliveryPoint gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:deliveryPoint>\n              <gmd:city gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:city>\n              <gmd:administrativeArea gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:administrativeArea>\n              <gmd:postalCode gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:postalCode>\n              <gmd:country gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:country>\n              <gmd:electronicMailAddress gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="pointOfContact"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gmd:contact>\n  <gmd:dateStamp>\n    <gco:DateTime>2010-06-18T15:59:32</gco:DateTime>\n  </gmd:dateStamp>\n  <gmd:metadataStandardName>\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n  </gmd:metadataStandardName>\n  <gmd:metadataStandardVersion>\n    <gco:CharacterString>1.0</gco:CharacterString>\n  </gmd:metadataStandardVersion>\n  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>\n    <srv:SV_ServiceIdentification>\n      <gmd:citation xmlns:gmd="http://www.isotc211.org/2005/gmd">\n        <gmd:CI_Citation>\n          <gmd:title gco:nilReason="missing">\n            <gco:CharacterString>métadonnées de cartes</gco:CharacterString>\n          </gmd:title><gmd:date>\n            <gmd:CI_Date>\n              <gmd:date>\n                <gco:DateTime/>\n              </gmd:date>\n              <gmd:dateType>\n                <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="publication"/>\n              </gmd:dateType>\n            </gmd:CI_Date>\n          </gmd:date>\n          \n          \n        </gmd:CI_Citation>\n      </gmd:citation>\n      <gmd:abstract xmlns:gmd="http://www.isotc211.org/2005/gmd" gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:abstract>\n      <gmd:pointOfContact xmlns:gmd="http://www.isotc211.org/2005/gmd">\n        <gmd:CI_ResponsibleParty>\n          <gmd:individualName gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:individualName>\n          <gmd:organisationName gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:organisationName>\n          <gmd:contactInfo>\n            <gmd:CI_Contact>\n              <gmd:phone>\n                <gmd:CI_Telephone>\n                  <gmd:voice gco:nilReason="missing">\n                    <gco:CharacterString/>\n                  </gmd:voice>\n                </gmd:CI_Telephone>\n              </gmd:phone>\n              <gmd:address>\n                <gmd:CI_Address>\n                  <gmd:electronicMailAddress gco:nilReason="missing">\n                    <gco:CharacterString/>\n                  </gmd:electronicMailAddress>\n                </gmd:CI_Address>\n              </gmd:address>\n            </gmd:CI_Contact>\n          </gmd:contactInfo>\n          <gmd:role>\n            <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="pointOfContact"/>\n          </gmd:role>\n        </gmd:CI_ResponsibleParty>\n      </gmd:pointOfContact>\n      <gmd:descriptiveKeywords xmlns:gmd="http://www.isotc211.org/2005/gmd">\n        <gmd:MD_Keywords>\n          <gmd:keyword gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:keyword>\n          <gmd:keyword gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n          </gmd:type>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:descriptiveKeywords xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://catalogue-p40.alkante.com/geonetwork/srv/fr/xml.keyword.get?thesaurus=external.place.regions&amp;id=http://geonetwork-opensource.org/regions%2368&amp;multiple=false" xlink:title="" xlink:role="pointOfContact">\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>France</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="place"/>\n          </gmd:type>\n          <gmd:thesaurusName>\n            <gmd:CI_Citation>\n              <gmd:title>\n                <gco:CharacterString>external.place.regions</gco:CharacterString>\n              </gmd:title>\n              <gmd:date gco:nilReason="unknown"/>\n            </gmd:CI_Citation>\n          </gmd:thesaurusName>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:descriptiveKeywords>\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>infoMapAccessService</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n          </gmd:type>\n          <gmd:thesaurusName>\n            <gmd:CI_Citation>\n              <gmd:title>\n                <gco:CharacterString>INSPIRE Service taxonomy</gco:CharacterString>\n              </gmd:title>\n              <gmd:date>\n                <gmd:CI_Date>\n                  <gmd:date>\n                    <gco:Date>2012-04-11</gco:Date>\n                  </gmd:date>\n                  <gmd:dateType>\n                    <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication"/>\n                  </gmd:dateType>\n                </gmd:CI_Date>\n              </gmd:date>\n            </gmd:CI_Citation>\n          </gmd:thesaurusName>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords><gmd:resourceConstraints xmlns:gmd="http://www.isotc211.org/2005/gmd">\n        <gmd:MD_LegalConstraints>\n          <gmd:accessConstraints>\n            <gmd:MD_RestrictionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode" codeListValue=""/>\n          </gmd:accessConstraints>\n          <gmd:useConstraints>\n            <gmd:MD_RestrictionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode" codeListValue=""/>\n          </gmd:useConstraints>\n          <gmd:otherConstraints gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:otherConstraints>\n        </gmd:MD_LegalConstraints>\n      </gmd:resourceConstraints>\n      <gmd:resourceConstraints xmlns:gmd="http://www.isotc211.org/2005/gmd">\n        <gmd:MD_SecurityConstraints>\n          <gmd:classification>\n            <gmd:MD_ClassificationCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ClassificationCode" codeListValue=""/>\n          </gmd:classification>\n          <gmd:userNote gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:userNote>\n        </gmd:MD_SecurityConstraints>\n      </gmd:resourceConstraints>\n      \n      \n      \n      \n      \n      \n      <srv:serviceType><gco:LocalName>invoke</gco:LocalName></srv:serviceType><srv:extent>\n        <gmd:EX_Extent xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco">\n          <gmd:description gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:description>\n          <gmd:geographicElement>\n            <gmd:EX_GeographicBoundingBox>\n              <gmd:westBoundLongitude>\n                <gco:Decimal>-5.79028</gco:Decimal>\n              </gmd:westBoundLongitude>\n              <gmd:eastBoundLongitude>\n                <gco:Decimal>9.56222</gco:Decimal>\n              </gmd:eastBoundLongitude>\n              <gmd:southBoundLatitude>\n                <gco:Decimal>41.36493</gco:Decimal>\n              </gmd:southBoundLatitude>\n              <gmd:northBoundLatitude>\n                <gco:Decimal>51.09111</gco:Decimal>\n              </gmd:northBoundLatitude>\n            </gmd:EX_GeographicBoundingBox>\n          </gmd:geographicElement>\n          \n        </gmd:EX_Extent>\n      </srv:extent>\n      \n      \n    \n               <srv:couplingType>\n\t\t\t\t         <srv:SV_CouplingType codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType" codeListValue="tight"/>\n\t\t\t\t       </srv:couplingType>\n\t\t\t\t       <srv:containsOperations>\n\t\t\t\t         <srv:SV_OperationMetadata>\n\t\t\t\t           <srv:operationName>\n\t\t\t\t             <gco:CharacterString>Accès à la carte</gco:CharacterString>\n\t\t\t\t           </srv:operationName>\n\t\t\t\t           <srv:DCP>\n\t\t\t\t             <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n\t\t\t\t           </srv:DCP>\n\t\t\t\t           <srv:connectPoint>\n\t\t\t\t             <gmd:CI_OnlineResource>\n\t\t\t\t               <gmd:linkage>\n\t\t\t\t                 <gmd:URL>https://catalogue-p40.alkante.com/geosource/consultation?id=3</gmd:URL>\n\t\t\t\t               </gmd:linkage>\n\t\t\t\t               <gmd:protocol>\n\t\t\t\t                 <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>\n\t\t\t\t               </gmd:protocol>\n\t\t\t\t             </gmd:CI_OnlineResource>\n\t\t\t\t           </srv:connectPoint>\n\t\t\t\t         </srv:SV_OperationMetadata>\n\t\t\t\t       </srv:containsOperations>\n             </srv:SV_ServiceIdentification>\n  </gmd:identificationInfo>\n  \n  <gmd:dataQualityInfo>\n    <gmd:DQ_DataQuality>\n      <gmd:scope>\n        <gmd:DQ_Scope>\n          <gmd:level>\n            <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="service"/>\n          </gmd:level>\n        </gmd:DQ_Scope>\n      </gmd:scope>\n      <gmd:lineage>\n        <gmd:LI_Lineage>\n          <gmd:statement gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:statement>\n          \n        </gmd:LI_Lineage>\n      </gmd:lineage>\n    </gmd:DQ_DataQuality>\n  </gmd:dataQualityInfo>\n  \n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	\N	\N	1	1	\N	0	0	\N	\N	\N
40088	88cacc48-04c5-42a6-b5ed-d1d375214856	iso19139	y	n	2010-03-04T15:46:22	2010-03-04T15:46:22	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.isotc211.org/2005/fra ../../../../web/geonetwork/xml/schemas/iso19139fra/schema.xsd">\n\n  <gmd:fileIdentifier>\n\n    <gco:CharacterString>88cacc48-04c5-42a6-b5ed-d1d375214856</gco:CharacterString>\n\n  </gmd:fileIdentifier>\n\n  <gmd:language>\n\n    \n\n  <gco:CharacterString>fre</gco:CharacterString></gmd:language>\n\n  <gmd:characterSet>\n\n    <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n\n  </gmd:characterSet>\n\n  <gmd:hierarchyLevel>\n\n    <gmd:MD_ScopeCode codeList="./resources/codeList.xml#MD_ScopeCode" codeListValue="dataset"/>\n\n  </gmd:hierarchyLevel><gmd:contact>\n\n    <gmd:CI_ResponsibleParty>\n\n      <gmd:individualName>\n\n        <gco:CharacterString/>\n\n      </gmd:individualName>\n\n      <gmd:organisationName>\n\n        <gco:CharacterString/>\n\n      </gmd:organisationName>\n\n      <gmd:positionName>\n\n        <gco:CharacterString/>\n\n      </gmd:positionName>\n\n      <gmd:contactInfo>\n\n        <gmd:CI_Contact>\n\n          <gmd:phone>\n\n            <gmd:CI_Telephone>\n\n              <gmd:voice>\n\n                <gco:CharacterString/>\n\n              </gmd:voice>\n\n              <gmd:facsimile>\n\n                <gco:CharacterString/>\n\n              </gmd:facsimile>\n\n            </gmd:CI_Telephone>\n\n          </gmd:phone>\n\n          <gmd:address>\n\n            <gmd:CI_Address>\n\n              <gmd:deliveryPoint>\n\n                <gco:CharacterString/>\n\n              </gmd:deliveryPoint>\n\n              <gmd:city>\n\n                <gco:CharacterString/>\n\n              </gmd:city>\n\n              <gmd:administrativeArea>\n\n                <gco:CharacterString/>\n\n              </gmd:administrativeArea>\n\n              <gmd:postalCode>\n\n                <gco:CharacterString/>\n\n              </gmd:postalCode>\n\n              <gmd:country>\n\n                <gco:CharacterString/>\n\n              </gmd:country>\n\n              <gmd:electronicMailAddress>\n\n                <gco:CharacterString/>\n\n              </gmd:electronicMailAddress>\n\n            </gmd:CI_Address>\n\n          </gmd:address>\n\n        </gmd:CI_Contact>\n\n      </gmd:contactInfo>\n\n      <gmd:role>\n\n        <gmd:CI_RoleCode codeList="./resources/codeList.xml#CI_RoleCode" codeListValue="originator"/>\n\n      </gmd:role>\n\n    </gmd:CI_ResponsibleParty>\n\n  </gmd:contact>\n\n  <gmd:dateStamp>\n\n    <gco:DateTime>2006-07-27T09:02:02</gco:DateTime>\n\n  </gmd:dateStamp>\n\n  <gmd:metadataStandardName>\n\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n\n  </gmd:metadataStandardName>\n\n  <gmd:metadataStandardVersion>\n\n    <gco:CharacterString>1.0</gco:CharacterString>\n\n  </gmd:metadataStandardVersion>\n\n  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>\n\n    <gmd:MD_DataIdentification>\n\n      <gmd:citation>\n\n        <gmd:CI_Citation>\n\n          <gmd:title>\n\n            <gco:CharacterString>métadonnée de série de données tabulaire</gco:CharacterString>\n\n          </gmd:title><gmd:date>\n\n            <gmd:CI_Date>\n\n              <gmd:date>\n\n                <gco:DateTime/>\n\n              </gmd:date>\n\n              <gmd:dateType>\n\n                <gmd:CI_DateTypeCode codeList="./resources/codeList.xml#CI_DateTypeCode" codeListValue="publication"/>\n\n              </gmd:dateType>\n\n            </gmd:CI_Date>\n\n          </gmd:date>\n\n          \n\n          \n\n        </gmd:CI_Citation>\n\n      </gmd:citation>\n\n      <gmd:abstract>\n\n        <gco:CharacterString/>\n\n      </gmd:abstract>\n\n      <gmd:pointOfContact>\n\n        <gmd:CI_ResponsibleParty>\n\n          <gmd:individualName>\n\n            <gco:CharacterString/>\n\n          </gmd:individualName>\n\n          <gmd:organisationName>\n\n            <gco:CharacterString/>\n\n          </gmd:organisationName>\n\n          <gmd:contactInfo>\n\n            <gmd:CI_Contact>\n\n              <gmd:phone>\n\n                <gmd:CI_Telephone>\n\n                  <gmd:voice>\n\n                    <gco:CharacterString/>\n\n                  </gmd:voice>\n\n                </gmd:CI_Telephone>\n\n              </gmd:phone>\n\n              <gmd:address>\n\n                <gmd:CI_Address>\n\n                  <gmd:electronicMailAddress>\n\n                    <gco:CharacterString/>\n\n                  </gmd:electronicMailAddress>\n\n                </gmd:CI_Address>\n\n              </gmd:address>\n\n            </gmd:CI_Contact>\n\n          </gmd:contactInfo>\n\n          <gmd:role>\n\n            <gmd:CI_RoleCode codeList="./resources/codeList.xml#CI_RoleCode" codeListValue="distributor"/>\n\n          </gmd:role>\n\n        </gmd:CI_ResponsibleParty>\n\n      </gmd:pointOfContact>\n\n      <gmd:descriptiveKeywords>\n\n        <gmd:MD_Keywords>\n\n          <gmd:keyword>\n\n            <gco:CharacterString/>\n\n          </gmd:keyword>\n\n          <gmd:keyword>\n\n            <gco:CharacterString/>\n\n          </gmd:keyword>\n\n          <gmd:type>\n\n            <gmd:MD_KeywordTypeCode codeList="./resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n\n          </gmd:type>\n\n        </gmd:MD_Keywords>\n\n      </gmd:descriptiveKeywords>\n\n      <gmd:descriptiveKeywords>\n\n        <gmd:MD_Keywords>\n\n          <gmd:keyword gco:nilReason="missing">\n\n            <gco:CharacterString/>\n\n          </gmd:keyword>\n\n          <gmd:type>\n\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="place"/>\n\n          </gmd:type>\n\n        </gmd:MD_Keywords>\n\n      </gmd:descriptiveKeywords>\n\n      <gmd:resourceConstraints>\n\n        <gmd:MD_LegalConstraints>\n\n          <gmd:useLimitation>\n\n            <gco:CharacterString/>\n\n          </gmd:useLimitation>\n\n          <gmd:accessConstraints>\n\n            <gmd:MD_RestrictionCode codeList="./resources/codeList.xml#MD_RestrictionCode" codeListValue="restricted"/>\n\n          </gmd:accessConstraints>\n\n          <gmd:useConstraints>\n\n            <gmd:MD_RestrictionCode codeList="./resources/codeList.xml#MD_RestrictionCode" codeListValue="copyright"/>\n\n          </gmd:useConstraints>\n\n          <gmd:otherConstraints>\n\n            <gco:CharacterString/>\n\n          </gmd:otherConstraints>\n\n        </gmd:MD_LegalConstraints>\n\n      </gmd:resourceConstraints>\n\n      <gmd:resourceConstraints>\n\n        <gmd:MD_SecurityConstraints>\n\n          <gmd:classification>\n\n            <gmd:MD_ClassificationCode codeList="./resources/codeList.xml#MD_ClassificationCode" codeListValue=""/>\n\n          </gmd:classification>\n\n          <gmd:userNote>\n\n            <gco:CharacterString/>\n\n          </gmd:userNote>\n\n        </gmd:MD_SecurityConstraints>\n\n      </gmd:resourceConstraints>\n\n      <gmd:spatialRepresentationType>\n\n        <gmd:MD_SpatialRepresentationTypeCode codeList="./resources/codeList.xml#MD_SpatialRepresentationTypeCode" codeListValue="textTable"/>\n\n      </gmd:spatialRepresentationType>\n\n      <gmd:spatialResolution>\n\n        <gmd:MD_Resolution>\n\n          <gmd:equivalentScale>\n\n            <gmd:MD_RepresentativeFraction>\n\n              <gmd:denominator>\n\n                <gco:Integer/>\n\n              </gmd:denominator>\n\n            </gmd:MD_RepresentativeFraction>\n\n          </gmd:equivalentScale>\n\n        </gmd:MD_Resolution>\n\n      </gmd:spatialResolution>\n\n      <gmd:spatialResolution>\n\n        <gmd:MD_Resolution>\n\n          <gmd:distance/>\n\n        </gmd:MD_Resolution>\n\n      </gmd:spatialResolution>\n\n      <gmd:language>\n\n        <gco:CharacterString>fre</gco:CharacterString>\n\n      </gmd:language>\n\n      <gmd:characterSet>\n\n        <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n\n      </gmd:characterSet>\n\n      <gmd:topicCategory>\n\n        <gmd:MD_TopicCategoryCode/>\n\n      </gmd:topicCategory>\n\n      \n\n      \n\n      <gmd:extent>\n\n        <gmd:EX_Extent>\n\n          <gmd:description gco:nilReason="missing">\n\n            <gco:CharacterString/>\n\n          </gmd:description>\n\n          <gmd:geographicElement>\n\n            <gmd:EX_GeographicBoundingBox>\n\n              <gmd:westBoundLongitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:westBoundLongitude>\n\n              <gmd:eastBoundLongitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:eastBoundLongitude>\n\n              <gmd:southBoundLatitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:southBoundLatitude>\n\n              <gmd:northBoundLatitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:northBoundLatitude>\n\n            </gmd:EX_GeographicBoundingBox>\n\n          </gmd:geographicElement>\n\n          \n\n        </gmd:EX_Extent>\n\n      </gmd:extent><gmd:supplementalInformation>\n\n        <gco:CharacterString/>\n\n      </gmd:supplementalInformation>\n\n      \n\n    </gmd:MD_DataIdentification>\n\n  </gmd:identificationInfo>\n\n  <gmd:dataQualityInfo>\n\n    <gmd:DQ_DataQuality>\n\n      <gmd:scope>\n\n        <gmd:DQ_Scope>\n\n          <gmd:level>\n\n            <gmd:MD_ScopeCode codeList="./resources/codeList.xml#MD_ScopeCode" codeListValue="dataset"/>\n\n          </gmd:level>\n\n        </gmd:DQ_Scope>\n\n      </gmd:scope>\n\n      <gmd:lineage>\n\n        <gmd:LI_Lineage>\n\n          <gmd:statement>\n\n            <gco:CharacterString/>\n\n          </gmd:statement>\n\n          <gmd:source>\n\n            <gmd:LI_Source>\n\n              <gmd:description>\n\n                <gco:CharacterString/>\n\n              </gmd:description>\n\n            </gmd:LI_Source>\n\n          </gmd:source>\n\n        </gmd:LI_Lineage>\n\n      </gmd:lineage>\n\n    </gmd:DQ_DataQuality>\n\n  </gmd:dataQualityInfo>\n\n  \n\n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	gmd:MD_Metadata	\N	1	1	\N	0	0	\N	\N	\N
12	7d19ee05-1684-4e01-8e18-901688427dc5	iso19139	s	n	2012-04-11T12:19:46	2012-04-11T12:19:46	<gmd:CI_ResponsibleParty xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:geonet="http://www.fao.org/geonetwork">\n    <gmd:individualName>\n      <gco:CharacterString>-- Contact --</gco:CharacterString>\n    </gmd:individualName>\n    <gmd:organisationName>\n      <gco:CharacterString>-- Organisation --</gco:CharacterString>\n    </gmd:organisationName>\n    <gmd:contactInfo>\n      <gmd:CI_Contact>\n        <gmd:phone>\n          <gmd:CI_Telephone>\n            <gmd:voice>\n              <gco:CharacterString/>\n            </gmd:voice>\n            <gmd:facsimile>\n              <gco:CharacterString/>\n            </gmd:facsimile>\n          </gmd:CI_Telephone>\n        </gmd:phone>\n        <gmd:address>\n          <gmd:CI_Address>\n            <gmd:deliveryPoint>\n              <gco:CharacterString/>\n            </gmd:deliveryPoint>\n            <gmd:city>\n              <gco:CharacterString/>\n            </gmd:city>\n            <gmd:administrativeArea>\n              <gco:CharacterString/>\n            </gmd:administrativeArea>\n            <gmd:postalCode>\n              <gco:CharacterString/>\n            </gmd:postalCode>\n            <gmd:country>\n              <gco:CharacterString/>\n            </gmd:country>\n            <gmd:electronicMailAddress>\n              <gco:CharacterString/>\n            </gmd:electronicMailAddress>\n          </gmd:CI_Address>\n        </gmd:address>\n      </gmd:CI_Contact>\n    </gmd:contactInfo>\n    <gmd:role/>\n  </gmd:CI_ResponsibleParty>\n	7fc45be3-9aba-4198-920c-b8737112d522	PointOfContact	gmd:CI_ResponsibleParty	\N	1	1	\N	0	0	\N	\N	\N
40087	88cacc48-04c5-42a6-vf45-d1d375214856	iso19139	y	n	2010-03-04T15:46:22	2010-03-04T15:46:22	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.isotc211.org/2005/fra ../../../../web/geonetwork/xml/schemas/iso19139fra/schema.xsd">\n\n  <gmd:fileIdentifier>\n\n    <gco:CharacterString>88cacc48-04c5-42a6-vf45-d1d375214856</gco:CharacterString>\n\n  </gmd:fileIdentifier>\n\n  <gmd:language>\n\n    \n\n  <gco:CharacterString>fre</gco:CharacterString></gmd:language>\n\n  <gmd:characterSet>\n\n    <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n\n  </gmd:characterSet>\n\n  <gmd:hierarchyLevel>\n\n    <gmd:MD_ScopeCode codeList="./resources/codeList.xml#MD_ScopeCode" codeListValue="dataset"/>\n\n  </gmd:hierarchyLevel><gmd:contact>\n\n    <gmd:CI_ResponsibleParty>\n\n      <gmd:individualName>\n\n        <gco:CharacterString/>\n\n      </gmd:individualName>\n\n      <gmd:organisationName>\n\n        <gco:CharacterString/>\n\n      </gmd:organisationName>\n\n      <gmd:positionName>\n\n        <gco:CharacterString/>\n\n      </gmd:positionName>\n\n      <gmd:contactInfo>\n\n        <gmd:CI_Contact>\n\n          <gmd:phone>\n\n            <gmd:CI_Telephone>\n\n              <gmd:voice>\n\n                <gco:CharacterString/>\n\n              </gmd:voice>\n\n              <gmd:facsimile>\n\n                <gco:CharacterString/>\n\n              </gmd:facsimile>\n\n            </gmd:CI_Telephone>\n\n          </gmd:phone>\n\n          <gmd:address>\n\n            <gmd:CI_Address>\n\n              <gmd:deliveryPoint>\n\n                <gco:CharacterString/>\n\n              </gmd:deliveryPoint>\n\n              <gmd:city>\n\n                <gco:CharacterString/>\n\n              </gmd:city>\n\n              <gmd:administrativeArea>\n\n                <gco:CharacterString/>\n\n              </gmd:administrativeArea>\n\n              <gmd:postalCode>\n\n                <gco:CharacterString/>\n\n              </gmd:postalCode>\n\n              <gmd:country>\n\n                <gco:CharacterString/>\n\n              </gmd:country>\n\n              <gmd:electronicMailAddress>\n\n                <gco:CharacterString/>\n\n              </gmd:electronicMailAddress>\n\n            </gmd:CI_Address>\n\n          </gmd:address>\n\n        </gmd:CI_Contact>\n\n      </gmd:contactInfo>\n\n      <gmd:role>\n\n        <gmd:CI_RoleCode codeList="./resources/codeList.xml#CI_RoleCode" codeListValue="originator"/>\n\n      </gmd:role>\n\n    </gmd:CI_ResponsibleParty>\n\n  </gmd:contact>\n\n  <gmd:dateStamp>\n\n    <gco:DateTime>2006-07-27T09:02:02</gco:DateTime>\n\n  </gmd:dateStamp>\n\n  <gmd:metadataStandardName>\n\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n\n  </gmd:metadataStandardName>\n\n  <gmd:metadataStandardVersion>\n\n    <gco:CharacterString>1.0</gco:CharacterString>\n\n  </gmd:metadataStandardVersion>\n\n  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>\n\n    <gmd:MD_DataIdentification>\n\n      <gmd:citation>\n\n        <gmd:CI_Citation>\n\n          <gmd:title>\n\n            <gco:CharacterString>métadonnée de série de données par jointure</gco:CharacterString>\n\n          </gmd:title><gmd:date>\n\n            <gmd:CI_Date>\n\n              <gmd:date>\n\n                <gco:DateTime/>\n\n              </gmd:date>\n\n              <gmd:dateType>\n\n                <gmd:CI_DateTypeCode codeList="./resources/codeList.xml#CI_DateTypeCode" codeListValue="publication"/>\n\n              </gmd:dateType>\n\n            </gmd:CI_Date>\n\n          </gmd:date>\n\n          \n\n          \n\n        </gmd:CI_Citation>\n\n      </gmd:citation>\n\n      <gmd:abstract>\n\n        <gco:CharacterString/>\n\n      </gmd:abstract>\n\n      <gmd:pointOfContact>\n\n        <gmd:CI_ResponsibleParty>\n\n          <gmd:individualName>\n\n            <gco:CharacterString/>\n\n          </gmd:individualName>\n\n          <gmd:organisationName>\n\n            <gco:CharacterString/>\n\n          </gmd:organisationName>\n\n          <gmd:contactInfo>\n\n            <gmd:CI_Contact>\n\n              <gmd:phone>\n\n                <gmd:CI_Telephone>\n\n                  <gmd:voice>\n\n                    <gco:CharacterString/>\n\n                  </gmd:voice>\n\n                </gmd:CI_Telephone>\n\n              </gmd:phone>\n\n              <gmd:address>\n\n                <gmd:CI_Address>\n\n                  <gmd:electronicMailAddress>\n\n                    <gco:CharacterString/>\n\n                  </gmd:electronicMailAddress>\n\n                </gmd:CI_Address>\n\n              </gmd:address>\n\n            </gmd:CI_Contact>\n\n          </gmd:contactInfo>\n\n          <gmd:role>\n\n            <gmd:CI_RoleCode codeList="./resources/codeList.xml#CI_RoleCode" codeListValue="distributor"/>\n\n          </gmd:role>\n\n        </gmd:CI_ResponsibleParty>\n\n      </gmd:pointOfContact>\n\n      <gmd:descriptiveKeywords>\n\n        <gmd:MD_Keywords>\n\n          <gmd:keyword>\n\n            <gco:CharacterString/>\n\n          </gmd:keyword>\n\n          <gmd:keyword>\n\n            <gco:CharacterString/>\n\n          </gmd:keyword>\n\n          <gmd:type>\n\n            <gmd:MD_KeywordTypeCode codeList="./resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n\n          </gmd:type>\n\n        </gmd:MD_Keywords>\n\n      </gmd:descriptiveKeywords>\n\n<gmd:descriptiveKeywords>\n\n        <gmd:MD_Keywords>\n\n          <gmd:keyword gco:nilReason="missing">\n\n            <gco:CharacterString/>\n\n          </gmd:keyword>\n\n          <gmd:type>\n\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="place"/>\n\n          </gmd:type>\n\n        </gmd:MD_Keywords>\n\n      </gmd:descriptiveKeywords>\n\n      <gmd:resourceConstraints>\n\n        <gmd:MD_LegalConstraints>\n\n          <gmd:useLimitation>\n\n            <gco:CharacterString/>\n\n          </gmd:useLimitation>\n\n          <gmd:accessConstraints>\n\n            <gmd:MD_RestrictionCode codeList="./resources/codeList.xml#MD_RestrictionCode" codeListValue="restricted"/>\n\n          </gmd:accessConstraints>\n\n          <gmd:useConstraints>\n\n            <gmd:MD_RestrictionCode codeList="./resources/codeList.xml#MD_RestrictionCode" codeListValue="copyright"/>\n\n          </gmd:useConstraints>\n\n          <gmd:otherConstraints>\n\n            <gco:CharacterString/>\n\n          </gmd:otherConstraints>\n\n        </gmd:MD_LegalConstraints>\n\n      </gmd:resourceConstraints>\n\n      <gmd:resourceConstraints>\n\n        <gmd:MD_SecurityConstraints>\n\n          <gmd:classification>\n\n            <gmd:MD_ClassificationCode codeList="./resources/codeList.xml#MD_ClassificationCode" codeListValue=""/>\n\n          </gmd:classification>\n\n          <gmd:userNote>\n\n            <gco:CharacterString/>\n\n          </gmd:userNote>\n\n        </gmd:MD_SecurityConstraints>\n\n      </gmd:resourceConstraints>\n\n      <gmd:spatialRepresentationType>\n\n        <gmd:MD_SpatialRepresentationTypeCode codeList="./resources/codeList.xml#MD_SpatialRepresentationTypeCode" codeListValue="vector"/>\n\n      </gmd:spatialRepresentationType>\n\n      <gmd:spatialResolution>\n\n        <gmd:MD_Resolution>\n\n          <gmd:equivalentScale>\n\n            <gmd:MD_RepresentativeFraction>\n\n              <gmd:denominator>\n\n                <gco:Integer/>\n\n              </gmd:denominator>\n\n            </gmd:MD_RepresentativeFraction>\n\n          </gmd:equivalentScale>\n\n        </gmd:MD_Resolution>\n\n      </gmd:spatialResolution>\n\n      <gmd:spatialResolution>\n\n        <gmd:MD_Resolution>\n\n          <gmd:distance/>\n\n        </gmd:MD_Resolution>\n\n      </gmd:spatialResolution>\n\n      <gmd:language>\n\n        <gco:CharacterString>fre</gco:CharacterString>\n\n      </gmd:language>\n\n      <gmd:characterSet>\n\n        <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n\n      </gmd:characterSet>\n\n      <gmd:topicCategory>\n\n        <gmd:MD_TopicCategoryCode/>\n\n      </gmd:topicCategory>\n\n      \n\n      \n\n      <gmd:extent>\n\n        <gmd:EX_Extent>\n\n          <gmd:description gco:nilReason="missing">\n\n            <gco:CharacterString/>\n\n          </gmd:description>\n\n          <gmd:geographicElement>\n\n            <gmd:EX_GeographicBoundingBox>\n\n              <gmd:westBoundLongitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:westBoundLongitude>\n\n              <gmd:eastBoundLongitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:eastBoundLongitude>\n\n              <gmd:southBoundLatitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:southBoundLatitude>\n\n              <gmd:northBoundLatitude>\n\n                <gco:Decimal>0</gco:Decimal>\n\n              </gmd:northBoundLatitude>\n\n            </gmd:EX_GeographicBoundingBox>\n\n          </gmd:geographicElement>\n\n          \n\n        </gmd:EX_Extent>\n\n      </gmd:extent><gmd:supplementalInformation>\n\n        <gco:CharacterString/>\n\n      </gmd:supplementalInformation>\n\n      \n\n    </gmd:MD_DataIdentification>\n\n  </gmd:identificationInfo>\n\n  <gmd:dataQualityInfo>\n\n    <gmd:DQ_DataQuality>\n\n      <gmd:scope>\n\n        <gmd:DQ_Scope>\n\n          <gmd:level>\n\n            <gmd:MD_ScopeCode codeList="./resources/codeList.xml#MD_ScopeCode" codeListValue="dataset"/>\n\n          </gmd:level>\n\n        </gmd:DQ_Scope>\n\n      </gmd:scope>\n\n      <gmd:lineage>\n\n        <gmd:LI_Lineage>\n\n          <gmd:statement>\n\n            <gco:CharacterString/>\n\n          </gmd:statement>\n\n          <gmd:source>\n\n            <gmd:LI_Source>\n\n              <gmd:description>\n\n                <gco:CharacterString/>\n\n              </gmd:description>\n\n            </gmd:LI_Source>\n\n          </gmd:source>\n\n        </gmd:LI_Lineage>\n\n      </gmd:lineage>\n\n    </gmd:DQ_DataQuality>\n\n  </gmd:dataQualityInfo>\n\n  \n\n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	gmd:MD_Metadata	\N	1	1	\N	0	0	\N	\N	\N
40086	c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1	iso19139	y	n	2012-04-11T12:19:46	2017-04-27T12:01:53	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gml="http://www.opengis.net/gml" xmlns:geonet="http://www.fao.org/geonetwork">\n  <gmd:fileIdentifier>\n    <gco:CharacterString>c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1</gco:CharacterString>\n  </gmd:fileIdentifier>\n  <gmd:language>\n    <gco:CharacterString>fre</gco:CharacterString>\n  </gmd:language>\n  <gmd:characterSet>\n    <gmd:MD_CharacterSetCode codeListValue="utf8" codeList="MD_CharacterSetCode"/>\n  </gmd:characterSet>\n  <gmd:hierarchyLevel>\n    <gmd:MD_ScopeCode codeListValue="service" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ScopeCode"/>\n  </gmd:hierarchyLevel>\n  <gmd:hierarchyLevelName>\n    <gco:CharacterString>Service</gco:CharacterString>\n  </gmd:hierarchyLevelName>\n  <gmd:contact>\n    <gmd:CI_ResponsibleParty>\n      <gmd:organisationName>\n        <gco:CharacterString>-- Contact sur la métadonnée --</gco:CharacterString>\n      </gmd:organisationName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone/>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:electronicMailAddress>\n                <gco:CharacterString>-- Adresse email --</gco:CharacterString>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gmd:contact>\n  <gmd:dateStamp>\n    <gco:DateTime>2012-04-11T12:19:46</gco:DateTime>\n  </gmd:dateStamp>\n  <gmd:metadataStandardName>\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n  </gmd:metadataStandardName>\n  <gmd:metadataStandardVersion>\n    <gco:CharacterString>1.0</gco:CharacterString>\n  </gmd:metadataStandardVersion>\n  <gmd:referenceSystemInfo>\n    <gmd:MD_ReferenceSystem>\n      <gmd:referenceSystemIdentifier>\n        <gmd:RS_Identifier>\n          <gmd:code>\n            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>\n          </gmd:code>\n          <gmd:codeSpace>\n            <gco:CharacterString>EPSG</gco:CharacterString>\n          </gmd:codeSpace>\n          <gmd:version>\n            <gco:CharacterString>7.4</gco:CharacterString>\n          </gmd:version>\n        </gmd:RS_Identifier>\n      </gmd:referenceSystemIdentifier>\n    </gmd:MD_ReferenceSystem>\n  </gmd:referenceSystemInfo>\n  <gmd:identificationInfo>\n    <srv:SV_ServiceIdentification>\n      <gmd:citation>\n        <gmd:CI_Citation>\n          <gmd:title>\n            <gco:CharacterString>Modèle pour la saisie d'un service</gco:CharacterString>\n          </gmd:title>\n          <gmd:date>\n            <gmd:CI_Date>\n              <gmd:date>\n                <gco:Date>2010-01-01</gco:Date>\n              </gmd:date>\n              <gmd:dateType>\n                <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="creation"/>\n              </gmd:dateType>\n            </gmd:CI_Date>\n          </gmd:date>\n        </gmd:CI_Citation>\n      </gmd:citation>\n      <gmd:abstract>\n        <gco:CharacterString>-- Description du service  --</gco:CharacterString>\n      </gmd:abstract>\n      <gmd:pointOfContact>\n        <gmd:CI_ResponsibleParty>\n          <gmd:organisationName>\n            <gco:CharacterString>-- Organisation responsable du service --</gco:CharacterString>\n          </gmd:organisationName>\n          <gmd:contactInfo>\n            <gmd:CI_Contact>\n              <gmd:address>\n                <gmd:CI_Address>\n                  <gmd:electronicMailAddress>\n                    <gco:CharacterString>-- Adresse email --</gco:CharacterString>\n                  </gmd:electronicMailAddress>\n                </gmd:CI_Address>\n              </gmd:address>\n            </gmd:CI_Contact>\n          </gmd:contactInfo>\n          <gmd:role>\n            <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="owner"/>\n          </gmd:role>\n        </gmd:CI_ResponsibleParty>\n      </gmd:pointOfContact>\n      <gmd:resourceConstraints>\n        <gmd:MD_LegalConstraints/>\n      </gmd:resourceConstraints>\n      <gmd:resourceConstraints>\n        <gmd:MD_Constraints/>\n      </gmd:resourceConstraints>\n      <srv:serviceType>\n        <gco:LocalName>W3C:HTML:LINK</gco:LocalName>\n      </srv:serviceType>\n      <srv:serviceTypeVersion>\n        <gco:CharacterString>-- Version du service --</gco:CharacterString>\n      </srv:serviceTypeVersion>\n      <srv:restrictions>\n        <gmd:MD_Constraints/>\n      </srv:restrictions>\n      <srv:extent>\n        <gmd:EX_Extent>\n          <gmd:description>\n            <gco:CharacterString>-- Description de l&amp;amp;apos;étendue géographique (eg France métropolitaine) --</gco:CharacterString>\n          </gmd:description>\n          <gmd:geographicElement>\n            <gmd:EX_GeographicBoundingBox>\n              <gmd:westBoundLongitude>\n                <gco:Decimal>-5.449218749023903</gco:Decimal>\n              </gmd:westBoundLongitude>\n              <gmd:eastBoundLongitude>\n                <gco:Decimal>11.777343747890368</gco:Decimal>\n              </gmd:eastBoundLongitude>\n              <gmd:southBoundLatitude>\n                <gco:Decimal>40.1116886595881</gco:Decimal>\n              </gmd:southBoundLatitude>\n              <gmd:northBoundLatitude>\n                <gco:Decimal>51.34433865388288</gco:Decimal>\n              </gmd:northBoundLatitude>\n            </gmd:EX_GeographicBoundingBox>\n          </gmd:geographicElement>\n        </gmd:EX_Extent>\n      </srv:extent>\n      <srv:coupledResource/>\n      <srv:couplingType>\n        <srv:SV_CouplingType codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType" codeListValue="tight"/>\n      </srv:couplingType>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName gco:nilReason="missing">\n            <gco:CharacterString/>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>-- Adresse du service --</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n      <srv:operatesOn uuidref="-- Identifiant de la métadonnée de couche associée --"/>\n    </srv:SV_ServiceIdentification>\n  </gmd:identificationInfo>\n  <gmd:distributionInfo>\n    <gmd:MD_Distribution>\n      <gmd:transferOptions>\n        <gmd:MD_DigitalTransferOptions>\n          <gmd:onLine>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>-- Adresse du service --</gmd:URL>\n              </gmd:linkage>\n            </gmd:CI_OnlineResource>\n          </gmd:onLine>\n        </gmd:MD_DigitalTransferOptions>\n      </gmd:transferOptions>\n    </gmd:MD_Distribution>\n  </gmd:distributionInfo>\n  <gmd:dataQualityInfo>\n    <gmd:DQ_DataQuality>\n      <gmd:scope>\n        <gmd:DQ_Scope>\n          <gmd:level>\n            <gmd:MD_ScopeCode codeListValue="service" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ScopeCode"/>\n          </gmd:level>\n        </gmd:DQ_Scope>\n      </gmd:scope>\n      <gmd:lineage>\n        <gmd:LI_Lineage/>\n      </gmd:lineage>\n    </gmd:DQ_DataQuality>\n  </gmd:dataQualityInfo>\n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	gmd:MD_Metadata	\N	1	1	\N	0	0	\N	\N	\N
40021	83aa3f50-c5ed-4048-930f-91ee4b983e5c	iso19139	n	n	2017-04-26T11:00:46	2017-04-26T11:00:46	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gml="http://www.opengis.net/gml" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:geonet="http://www.fao.org/geonetwork">\n  <gmd:fileIdentifier>\n    <gco:CharacterString>83aa3f50-c5ed-4048-930f-91ee4b983e5c</gco:CharacterString>\n  </gmd:fileIdentifier>\n  <gmd:language>\n    <gco:CharacterString>fre</gco:CharacterString>\n  </gmd:language>\n  <gmd:characterSet>\n    <gmd:MD_CharacterSetCode codeListValue="utf8" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_CharacterSetCode"/>\n  </gmd:characterSet>\n  <gmd:hierarchyLevel>\n    <gmd:MD_ScopeCode codeListValue="service" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode"/>\n  </gmd:hierarchyLevel>\n  <gmd:hierarchyLevelName>\n    <gco:CharacterString>Service</gco:CharacterString>\n  </gmd:hierarchyLevelName>\n  <gmd:contact>\n    <gmd:CI_ResponsibleParty>\n      <gmd:organisationName>\n        <gco:CharacterString>-- Contact sur la métadonnée --</gco:CharacterString>\n      </gmd:organisationName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone/>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:electronicMailAddress>\n                <gco:CharacterString>-- Adresse email --</gco:CharacterString>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gmd:contact>\n  <gmd:dateStamp>\n    <gco:DateTime>2017-04-26T11:00:46</gco:DateTime>\n  </gmd:dateStamp>\n  <gmd:metadataStandardName>\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n  </gmd:metadataStandardName>\n  <gmd:metadataStandardVersion>\n    <gco:CharacterString>1.0</gco:CharacterString>\n  </gmd:metadataStandardVersion>\n  <gmd:referenceSystemInfo>\n    <gmd:MD_ReferenceSystem>\n      <gmd:referenceSystemIdentifier>\n        <gmd:RS_Identifier>\n          <gmd:code>\n            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>\n          </gmd:code>\n          <gmd:codeSpace>\n            <gco:CharacterString>EPSG</gco:CharacterString>\n          </gmd:codeSpace>\n          <gmd:version>\n            <gco:CharacterString>7.4</gco:CharacterString>\n          </gmd:version>\n        </gmd:RS_Identifier>\n      </gmd:referenceSystemIdentifier>\n    </gmd:MD_ReferenceSystem>\n  </gmd:referenceSystemInfo>\n  <gmd:identificationInfo>\n    <srv:SV_ServiceIdentification>\n      <gmd:citation>\n        <gmd:CI_Citation>\n          <gmd:title>\n            <gco:CharacterString>Service WFS</gco:CharacterString>\n          </gmd:title>\n          <gmd:date>\n            <gmd:CI_Date>\n              <gmd:date>\n                <gco:Date>2010-01-01</gco:Date>\n              </gmd:date>\n              <gmd:dateType>\n                <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="creation"/>\n              </gmd:dateType>\n            </gmd:CI_Date>\n          </gmd:date>\n        </gmd:CI_Citation>\n      </gmd:citation>\n      <gmd:abstract>\n        <gco:CharacterString>-- Description du service WFS --</gco:CharacterString>\n      </gmd:abstract>\n      <gmd:pointOfContact>\n        <gmd:CI_ResponsibleParty>\n          <gmd:organisationName>\n            <gco:CharacterString>-- Organisation responsable du service --</gco:CharacterString>\n          </gmd:organisationName>\n          <gmd:contactInfo>\n            <gmd:CI_Contact>\n              <gmd:address>\n                <gmd:CI_Address>\n                  <gmd:electronicMailAddress>\n                    <gco:CharacterString>-- Adresse email  --</gco:CharacterString>\n                  </gmd:electronicMailAddress>\n                </gmd:CI_Address>\n              </gmd:address>\n            </gmd:CI_Contact>\n          </gmd:contactInfo>\n          <gmd:role>\n            <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="owner"/>\n          </gmd:role>\n        </gmd:CI_ResponsibleParty>\n      </gmd:pointOfContact>\n      <gmd:descriptiveKeywords>\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>OGC:WFS</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n          </gmd:type>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:resourceConstraints>\n        <gmd:MD_LegalConstraints/>\n      </gmd:resourceConstraints>\n      <gmd:resourceConstraints>\n        <gmd:MD_Constraints/>\n      </gmd:resourceConstraints>\n      <srv:serviceType>\n        <gco:LocalName>download</gco:LocalName>\n      </srv:serviceType>\n      <srv:serviceTypeVersion>\n        <gco:CharacterString>-- Version du service WFS --</gco:CharacterString>\n      </srv:serviceTypeVersion>\n      <srv:restrictions>\n        <gmd:MD_Constraints/>\n      </srv:restrictions>\n      <srv:extent>\n        <gmd:EX_Extent>\n          <gmd:description>\n            <gco:CharacterString>-- Description de l'étendue géographique (eg France métropolitaine) --</gco:CharacterString>\n          </gmd:description>\n          <gmd:geographicElement>\n            <gmd:EX_GeographicBoundingBox>\n              <gmd:westBoundLongitude>\n                <gco:Decimal>-5.449218749023903</gco:Decimal>\n              </gmd:westBoundLongitude>\n              <gmd:eastBoundLongitude>\n                <gco:Decimal>11.777343747890368</gco:Decimal>\n              </gmd:eastBoundLongitude>\n              <gmd:southBoundLatitude>\n                <gco:Decimal>40.1116886595881</gco:Decimal>\n              </gmd:southBoundLatitude>\n              <gmd:northBoundLatitude>\n                <gco:Decimal>51.34433865388288</gco:Decimal>\n              </gmd:northBoundLatitude>\n            </gmd:EX_GeographicBoundingBox>\n          </gmd:geographicElement>\n        </gmd:EX_Extent>\n      </srv:extent>\n      <srv:couplingType>\n        <srv:SV_CouplingType codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType" codeListValue="tight"/>\n      </srv:couplingType>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>GetCapabilities</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://carto-p40.alkante.com/cgi-bin/mapservwfs?</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>DescribeFeatureType</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://carto-p40.alkante.com/cgi-bin/mapservwfs?</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>GetFeature</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://carto-p40.alkante.com/cgi-bin/mapservwfs?</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n    </srv:SV_ServiceIdentification>\n  </gmd:identificationInfo>\n  <gmd:distributionInfo>\n    <gmd:MD_Distribution>\n      <gmd:transferOptions>\n        <gmd:MD_DigitalTransferOptions>\n          <gmd:onLine>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://carto-p40.alkante.com/cgi-bin/mapservwfs?</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </gmd:onLine>\n        </gmd:MD_DigitalTransferOptions>\n      </gmd:transferOptions>\n    </gmd:MD_Distribution>\n  </gmd:distributionInfo>\n  <gmd:dataQualityInfo>\n    <gmd:DQ_DataQuality>\n      <gmd:scope>\n        <gmd:DQ_Scope>\n          <gmd:level>\n            <gmd:MD_ScopeCode codeListValue="service" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode"/>\n          </gmd:level>\n        </gmd:DQ_Scope>\n      </gmd:scope>\n      <gmd:lineage>\n        <gmd:LI_Lineage/>\n      </gmd:lineage>\n    </gmd:DQ_DataQuality>\n  </gmd:dataQualityInfo>\n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	\N	\N	1	1	\N	0	0	\N	\N	\N
40022	80a7ceef-0bd5-4238-a631-f72fb3330ded	iso19139	n	n	2017-04-26T11:00:49	2017-04-26T11:00:49	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gml="http://www.opengis.net/gml" xsi:schemaLocation="http://www.isotc211.org/2005/srv http://schemas.opengis.net/iso/19139/20060504/srv/srv.xsd">\n  <gmd:fileIdentifier>\n    <gco:CharacterString>80a7ceef-0bd5-4238-a631-f72fb3330ded</gco:CharacterString>\n  </gmd:fileIdentifier>\n  <gmd:language>\n    <gmd:LanguageCode codeList="http://www.loc.gov/standards/iso639-2/" codeListValue="fre"/>\n  </gmd:language>\n  <gmd:hierarchyLevel>\n    <gmd:MD_ScopeCode codeListValue="service" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode"/>\n  </gmd:hierarchyLevel>\n  <gmd:hierarchyLevelName>\n    <gco:CharacterString>Service de téléchargement simple "Atom-based"</gco:CharacterString>\n  </gmd:hierarchyLevelName>\n  <gmd:contact>\n    <gmd:CI_ResponsibleParty>\n      <gmd:organisationName>\n        <gco:CharacterString>-- Contact sur la métadonnée --</gco:CharacterString>\n      </gmd:organisationName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone/>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:electronicMailAddress>\n                <gco:CharacterString>-- Adresse email --</gco:CharacterString>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gmd:contact>\n  <gmd:dateStamp>\n    <gco:DateTime>2017-04-26T11:00:49</gco:DateTime>\n  </gmd:dateStamp>\n  <gmd:metadataStandardName>\n    <gco:CharacterString>ISO 19119</gco:CharacterString>\n  </gmd:metadataStandardName>\n  <gmd:metadataStandardVersion>\n    <gco:CharacterString>2005/PDAM1</gco:CharacterString>\n  </gmd:metadataStandardVersion>\n  <gmd:referenceSystemInfo>\n    <gmd:MD_ReferenceSystem>\n      <gmd:referenceSystemIdentifier>\n        <gmd:RS_Identifier>\n          <gmd:code>\n            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>\n          </gmd:code>\n          <gmd:codeSpace>\n            <gco:CharacterString>EPSG</gco:CharacterString>\n          </gmd:codeSpace>\n          <gmd:version>\n            <gco:CharacterString>7.4</gco:CharacterString>\n          </gmd:version>\n        </gmd:RS_Identifier>\n      </gmd:referenceSystemIdentifier>\n    </gmd:MD_ReferenceSystem>\n  </gmd:referenceSystemInfo>\n  <gmd:identificationInfo>\n    <srv:SV_ServiceIdentification>\n      <gmd:citation>\n        <gmd:CI_Citation>\n          <gmd:title>\n            <gco:CharacterString>Service de téléchargement simple de la plateforme</gco:CharacterString>\n          </gmd:title>\n          <gmd:date>\n            <gmd:CI_Date>\n              <gmd:date>\n                <gco:Date>2014-05-26</gco:Date>\n              </gmd:date>\n              <gmd:dateType>\n                <gmd:CI_DateTypeCode codeListValue="creation" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode"/>\n              </gmd:dateType>\n            </gmd:CI_Date>\n          </gmd:date>\n        </gmd:CI_Citation>\n      </gmd:citation>\n      <gmd:abstract>\n        <gco:CharacterString>Service de téléchargement simple</gco:CharacterString>\n      </gmd:abstract>\n      <gmd:pointOfContact>\n        <gmd:CI_ResponsibleParty>\n          <gmd:organisationName>\n            <gco:CharacterString>-- Organisation responsable du service --</gco:CharacterString>\n          </gmd:organisationName>\n          <gmd:contactInfo>\n            <gmd:CI_Contact>\n              <gmd:address>\n                <gmd:CI_Address>\n                  <gmd:electronicMailAddress>\n                    <gco:CharacterString>-- Adresse email  --</gco:CharacterString>\n                  </gmd:electronicMailAddress>\n                </gmd:CI_Address>\n              </gmd:address>\n            </gmd:CI_Contact>\n          </gmd:contactInfo>\n          <gmd:role>\n            <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="owner"/>\n          </gmd:role>\n        </gmd:CI_ResponsibleParty>\n      </gmd:pointOfContact>\n      <gmd:descriptiveKeywords>\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>Service d’accès au produit</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:keyword>\n            <gco:CharacterString>InfoProductAccessService</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeListValue="stratum" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode"/>\n          </gmd:type>\n          <gmd:thesaurusName>\n            <gmd:CI_Citation>\n              <gmd:title>\n                <gco:CharacterString>RÈGLEMENT (CE) No 1205/2008, partie D.4</gco:CharacterString>\n              </gmd:title>\n              <gmd:date>\n                <gmd:CI_Date>\n                  <gmd:date>\n                    <gco:Date>2008-12-03</gco:Date>\n                  </gmd:date>\n                  <gmd:dateType>\n                    <gmd:CI_DateTypeCode codeListValue="publication" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode"/>\n                  </gmd:dateType>\n                </gmd:CI_Date>\n              </gmd:date>\n            </gmd:CI_Citation>\n          </gmd:thesaurusName>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:descriptiveKeywords>\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>NIVEAU DE DIFFUSION</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:keyword>\n            <gco:CharacterString>GRAND PUBLIC</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n          </gmd:type>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:resourceConstraints>\n        <gmd:MD_LegalConstraints>\n          <gmd:useLimitation>\n            <gco:CharacterString>Aucune condition ne s applique</gco:CharacterString>\n          </gmd:useLimitation>\n          <gmd:accessConstraints>\n            <gmd:MD_RestrictionCode codeListValue="otherRestrictions" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_RestrictionCode"/>\n          </gmd:accessConstraints>\n          <gmd:otherConstraints>\n            <gco:CharacterString>Pas de limitation d'accès public.</gco:CharacterString>\n          </gmd:otherConstraints>\n        </gmd:MD_LegalConstraints>\n      </gmd:resourceConstraints>\n      <srv:serviceType>\n        <gco:LocalName>download</gco:LocalName>\n      </srv:serviceType>\n      <srv:extent>\n        <gmd:EX_Extent>\n          <gmd:description>\n            <gco:CharacterString>FRANCE METROPOLITAINE</gco:CharacterString>\n          </gmd:description>\n          <gmd:geographicElement>\n            <gmd:EX_GeographicDescription>\n              <gmd:geographicIdentifier>\n                <gmd:MD_Identifier>\n                  <gmd:code>\n                    <gco:CharacterString>http://id.insee.fr/geo/pays/99100</gco:CharacterString>\n                  </gmd:code>\n                </gmd:MD_Identifier>\n              </gmd:geographicIdentifier>\n            </gmd:EX_GeographicDescription>\n          </gmd:geographicElement>\n          <gmd:geographicElement>\n            <gmd:EX_GeographicBoundingBox>\n              <gmd:westBoundLongitude>\n                <gco:Decimal>-5.14</gco:Decimal>\n              </gmd:westBoundLongitude>\n              <gmd:eastBoundLongitude>\n                <gco:Decimal>9.56</gco:Decimal>\n              </gmd:eastBoundLongitude>\n              <gmd:southBoundLatitude>\n                <gco:Decimal>41.33</gco:Decimal>\n              </gmd:southBoundLatitude>\n              <gmd:northBoundLatitude>\n                <gco:Decimal>51.09</gco:Decimal>\n              </gmd:northBoundLatitude>\n            </gmd:EX_GeographicBoundingBox>\n          </gmd:geographicElement>\n        </gmd:EX_Extent>\n      </srv:extent>\n      <srv:couplingType>\n        <srv:SV_CouplingType codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType" codeListValue="tight"/>\n      </srv:couplingType>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>GetResource</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeListValue="WebServices" codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://www-p40.alkante.com/rss/atomfeed/topatom</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>WWW:DOWNLOAD-1.0-http--download</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>GetServiceDescription</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeListValue="WebServices" codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://www-p40.alkante.com/rss/atomfeed/topatom</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>WWW:DOWNLOAD-1.0-http--download</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>GetResourceDescription</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeListValue="WebServices" codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://www-p40.alkante.com/rss/atomfeed/topatom</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>WWW:DOWNLOAD-1.0-http--download</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n    </srv:SV_ServiceIdentification>\n  </gmd:identificationInfo>\n  <gmd:distributionInfo>\n    <gmd:MD_Distribution>\n      <gmd:distributionFormat>\n        <gmd:MD_Format>\n          <gmd:name>\n            <gco:CharacterString>Format variable selon le jeu de données</gco:CharacterString>\n          </gmd:name>\n          <gmd:version>\n            <gco:CharacterString>Inconnue</gco:CharacterString>\n          </gmd:version>\n        </gmd:MD_Format>\n      </gmd:distributionFormat>\n      <gmd:transferOptions>\n        <gmd:MD_DigitalTransferOptions>\n          <gmd:unitsOfDistribution>\n            <gco:CharacterString>Accès au service</gco:CharacterString>\n          </gmd:unitsOfDistribution>\n          <gmd:onLine>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://www-p40.alkante.com/rss/atomfeed/topatom</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>WWW:LINK-1.0-http--atom</gco:CharacterString>\n              </gmd:protocol>\n              <gmd:name>\n                <gco:CharacterString>Document de description du service de téléchargement</gco:CharacterString>\n              </gmd:name>\n            </gmd:CI_OnlineResource>\n          </gmd:onLine>\n        </gmd:MD_DigitalTransferOptions>\n      </gmd:transferOptions>\n    </gmd:MD_Distribution>\n  </gmd:distributionInfo>\n  <gmd:dataQualityInfo>\n    <gmd:DQ_DataQuality>\n      <gmd:scope>\n        <gmd:DQ_Scope>\n          <gmd:level>\n            <gmd:MD_ScopeCode codeListValue="service" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode"/>\n          </gmd:level>\n        </gmd:DQ_Scope>\n      </gmd:scope>\n      <gmd:report>\n        <gmd:DQ_DomainConsistency>\n          <gmd:result>\n            <gmd:DQ_ConformanceResult>\n              <gmd:specification>\n                <gmd:CI_Citation>\n                  <gmd:title>\n                    <gco:CharacterString>Règlement (UE) No 1088/2010</gco:CharacterString>\n                  </gmd:title>\n                  <gmd:date>\n                    <gmd:CI_Date>\n                      <gmd:date>\n                        <gco:Date>2010-11-23</gco:Date>\n                      </gmd:date>\n                      <gmd:dateType>\n                        <gmd:CI_DateTypeCode codeListValue="publication" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode"/>\n                      </gmd:dateType>\n                    </gmd:CI_Date>\n                  </gmd:date>\n                </gmd:CI_Citation>\n              </gmd:specification>\n              <gmd:explanation>\n                <gco:CharacterString>See the referenced specification</gco:CharacterString>\n              </gmd:explanation>\n              <gmd:pass gco:nilReason="unknown"/>\n            </gmd:DQ_ConformanceResult>\n          </gmd:result>\n        </gmd:DQ_DomainConsistency>\n      </gmd:report>\n    </gmd:DQ_DataQuality>\n  </gmd:dataQualityInfo>\n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	\N	\N	1	1	\N	0	0	\N	\N	\N
40020	1c661f3e-33b5-4410-aa0d-c85f21a8ae7d	iso19139	n	n	2017-04-26T11:00:41	2017-04-26T11:00:41	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gml="http://www.opengis.net/gml" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:geonet="http://www.fao.org/geonetwork">\n  <gmd:fileIdentifier>\n    <gco:CharacterString>1c661f3e-33b5-4410-aa0d-c85f21a8ae7d</gco:CharacterString>\n  </gmd:fileIdentifier>\n  <gmd:language>\n    <gco:CharacterString>fre</gco:CharacterString>\n  </gmd:language>\n  <gmd:characterSet>\n    <gmd:MD_CharacterSetCode codeListValue="utf8" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_CharacterSetCode"/>\n  </gmd:characterSet>\n  <gmd:hierarchyLevel>\n    <gmd:MD_ScopeCode codeListValue="service" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode"/>\n  </gmd:hierarchyLevel>\n  <gmd:hierarchyLevelName>\n    <gco:CharacterString>Service</gco:CharacterString>\n  </gmd:hierarchyLevelName>\n  <gmd:contact>\n    <gmd:CI_ResponsibleParty>\n      <gmd:organisationName>\n        <gco:CharacterString>-- Contact sur la métadonnée --</gco:CharacterString>\n      </gmd:organisationName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone/>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:electronicMailAddress>\n                <gco:CharacterString>-- Adresse email --</gco:CharacterString>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gmd:contact>\n  <gmd:dateStamp>\n    <gco:DateTime>2017-04-26T11:00:42</gco:DateTime>\n  </gmd:dateStamp>\n  <gmd:metadataStandardName>\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n  </gmd:metadataStandardName>\n  <gmd:metadataStandardVersion>\n    <gco:CharacterString>1.0</gco:CharacterString>\n  </gmd:metadataStandardVersion>\n  <gmd:referenceSystemInfo>\n    <gmd:MD_ReferenceSystem>\n      <gmd:referenceSystemIdentifier>\n        <gmd:RS_Identifier>\n          <gmd:code>\n            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>\n          </gmd:code>\n          <gmd:codeSpace>\n            <gco:CharacterString>EPSG</gco:CharacterString>\n          </gmd:codeSpace>\n          <gmd:version>\n            <gco:CharacterString>7.4</gco:CharacterString>\n          </gmd:version>\n        </gmd:RS_Identifier>\n      </gmd:referenceSystemIdentifier>\n    </gmd:MD_ReferenceSystem>\n  </gmd:referenceSystemInfo>\n  <gmd:identificationInfo>\n    <srv:SV_ServiceIdentification>\n      <gmd:citation>\n        <gmd:CI_Citation>\n          <gmd:title>\n            <gco:CharacterString>Service WMS</gco:CharacterString>\n          </gmd:title>\n          <gmd:date>\n            <gmd:CI_Date>\n              <gmd:date>\n                <gco:Date>2010-01-01</gco:Date>\n              </gmd:date>\n              <gmd:dateType>\n                <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="creation"/>\n              </gmd:dateType>\n            </gmd:CI_Date>\n          </gmd:date>\n        </gmd:CI_Citation>\n      </gmd:citation>\n      <gmd:abstract>\n        <gco:CharacterString>-- Description du service WMS --</gco:CharacterString>\n      </gmd:abstract>\n      <gmd:pointOfContact>\n        <gmd:CI_ResponsibleParty>\n          <gmd:organisationName>\n            <gco:CharacterString>-- Organisation responsable du service --</gco:CharacterString>\n          </gmd:organisationName>\n          <gmd:contactInfo>\n            <gmd:CI_Contact>\n              <gmd:address>\n                <gmd:CI_Address>\n                  <gmd:electronicMailAddress>\n                    <gco:CharacterString>-- Adresse email  --</gco:CharacterString>\n                  </gmd:electronicMailAddress>\n                </gmd:CI_Address>\n              </gmd:address>\n            </gmd:CI_Contact>\n          </gmd:contactInfo>\n          <gmd:role>\n            <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="owner"/>\n          </gmd:role>\n        </gmd:CI_ResponsibleParty>\n      </gmd:pointOfContact>\n      <gmd:descriptiveKeywords>\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>OGC:WMS</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n          </gmd:type>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:resourceConstraints>\n        <gmd:MD_LegalConstraints/>\n      </gmd:resourceConstraints>\n      <gmd:resourceConstraints>\n        <gmd:MD_Constraints/>\n      </gmd:resourceConstraints>\n      <srv:serviceType>\n        <gco:LocalName>view</gco:LocalName>\n      </srv:serviceType>\n      <srv:serviceTypeVersion>\n        <gco:CharacterString>-- Version du service WMS --</gco:CharacterString>\n      </srv:serviceTypeVersion>\n      <srv:restrictions>\n        <gmd:MD_Constraints/>\n      </srv:restrictions>\n      <srv:extent>\n        <gmd:EX_Extent>\n          <gmd:description>\n            <gco:CharacterString>-- Description de l'étendue géographique (eg France métropolitaine) --</gco:CharacterString>\n          </gmd:description>\n          <gmd:geographicElement>\n            <gmd:EX_GeographicBoundingBox>\n              <gmd:westBoundLongitude>\n                <gco:Decimal>-5.449218749023903</gco:Decimal>\n              </gmd:westBoundLongitude>\n              <gmd:eastBoundLongitude>\n                <gco:Decimal>11.777343747890368</gco:Decimal>\n              </gmd:eastBoundLongitude>\n              <gmd:southBoundLatitude>\n                <gco:Decimal>40.1116886595881</gco:Decimal>\n              </gmd:southBoundLatitude>\n              <gmd:northBoundLatitude>\n                <gco:Decimal>51.34433865388288</gco:Decimal>\n              </gmd:northBoundLatitude>\n            </gmd:EX_GeographicBoundingBox>\n          </gmd:geographicElement>\n        </gmd:EX_Extent>\n      </srv:extent>\n      <srv:couplingType>\n        <srv:SV_CouplingType codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType" codeListValue="tight"/>\n      </srv:couplingType>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>GetCapabilities</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://carto-p40.alkante.com/cgi-bin/mapserv?</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>GetMap</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://carto-p40.alkante.com/cgi-bin/mapserv?</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>GetFeatureInfo</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n          </srv:DCP>\n          <srv:connectPoint>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://carto-p40.alkante.com/cgi-bin/mapserv?</gmd:URL>\n              </gmd:linkage>\n              <gmd:protocol>\n                <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>\n              </gmd:protocol>\n            </gmd:CI_OnlineResource>\n          </srv:connectPoint>\n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n      <srv:coupledResource>\n        <srv:SV_CoupledResource>\n          <srv:operationName>\n            <gco:CharacterString>GETMAP</gco:CharacterString>\n          </srv:operationName>\n          <srv:identifier>\n            <gco:CharacterString>7a8db1d7-f5cb-455d-a911-78f93418a0ce</gco:CharacterString>\n          </srv:identifier>\n          <gco:ScopedName>TESTBF</gco:ScopedName>\n        </srv:SV_CoupledResource>\n      </srv:coupledResource>\n      <srv:operatesOn xmlns:xlink="http://www.isotc211.org/2005/xlink" uuidref="7a8db1d7-f5cb-455d-a911-78f93418a0ce" xlink:href="https://catalogue-p40.alkante.com:8180/geonetwork/srv/fre/csw?service=CSW&amp;amp;request=GetRecordById&amp;amp;version=2.0.2&amp;amp;outputSchema=http://www.isotc211.org/2005/gmd&amp;amp;elementSetName=full&amp;amp;id=7a8db1d7-f5cb-455d-a911-78f93418a0ce#MD_DataIdentification"/>\n    </srv:SV_ServiceIdentification>\n  </gmd:identificationInfo>\n  <gmd:distributionInfo>\n    <gmd:MD_Distribution>\n      <gmd:transferOptions>\n        <gmd:MD_DigitalTransferOptions>\n          <gmd:onLine>\n            <gmd:CI_OnlineResource>\n              <gmd:linkage>\n                <gmd:URL>https://carto-p40.alkante.com/cgi-bin/mapserv?</gmd:URL>\n              </gmd:linkage>\n            </gmd:CI_OnlineResource>\n          </gmd:onLine>\n        </gmd:MD_DigitalTransferOptions>\n      </gmd:transferOptions>\n    </gmd:MD_Distribution>\n  </gmd:distributionInfo>\n  <gmd:dataQualityInfo>\n    <gmd:DQ_DataQuality>\n      <gmd:scope>\n        <gmd:DQ_Scope>\n          <gmd:level>\n            <gmd:MD_ScopeCode codeListValue="service" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode"/>\n          </gmd:level>\n        </gmd:DQ_Scope>\n      </gmd:scope>\n      <gmd:lineage>\n        <gmd:LI_Lineage/>\n      </gmd:lineage>\n    </gmd:DQ_DataQuality>\n  </gmd:dataQualityInfo>\n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	\N	\N	1	1	\N	0	0	\N	\N	\N
9	3c892ef0-0a6d-11de-ad6b-00104b7907b4	iso19139	n	n	2009-03-06T17:38:35	2012-04-11T13:48:56	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:fra="http://www.cnig.gouv.fr/2005/fra" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gml="http://www.opengis.net/gml" xmlns:geonet="http://www.fao.org/geonetwork" xsi:schemaLocation="http://www.isotc211.org/2005/gmd http://www.isotc211.org/2005/gmd/gmd.xsd">\r\n  <gmd:fileIdentifier>\r\n    <gco:CharacterString>3c892ef0-0a6d-11de-ad6b-00104b7907b4</gco:CharacterString>\r\n  </gmd:fileIdentifier>\r\n  <gmd:language>\r\n    <gco:CharacterString>fre</gco:CharacterString>\r\n  </gmd:language>\r\n  <gmd:characterSet>\r\n    <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8" />\r\n  </gmd:characterSet>\r\n  <gmd:hierarchyLevel>\r\n    <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="dataset" />\r\n  </gmd:hierarchyLevel>\r\n  <gmd:contact>\r\n    <gmd:CI_ResponsibleParty>\r\n      <gmd:individualName>\r\n        <gco:CharacterString>-- Contact --</gco:CharacterString>\r\n      </gmd:individualName>\r\n      <gmd:organisationName>\r\n        <gco:CharacterString>-- Organisation --</gco:CharacterString>\r\n      </gmd:organisationName>\r\n      <gmd:positionName gco:nilReason="missing">\r\n        <gco:CharacterString />\r\n      </gmd:positionName>\r\n      <gmd:contactInfo>\r\n        <gmd:CI_Contact>\r\n          <gmd:phone>\r\n            <gmd:CI_Telephone>\r\n              <gmd:voice>\r\n                <gco:CharacterString>33 (0) 2 32 00 00 00</gco:CharacterString>\r\n              </gmd:voice>\r\n              <gmd:facsimile gco:nilReason="missing">\r\n                <gco:CharacterString />\r\n              </gmd:facsimile>\r\n            </gmd:CI_Telephone>\r\n          </gmd:phone>\r\n          <gmd:address>\r\n            <gmd:CI_Address>\r\n              <gmd:deliveryPoint gco:nilReason="missing">\r\n                <gco:CharacterString />\r\n              </gmd:deliveryPoint>\r\n              <gmd:city gco:nilReason="missing">\r\n                <gco:CharacterString />\r\n              </gmd:city>\r\n              <gmd:administrativeArea gco:nilReason="missing">\r\n                <gco:CharacterString />\r\n              </gmd:administrativeArea>\r\n              <gmd:postalCode gco:nilReason="missing">\r\n                <gco:CharacterString />\r\n              </gmd:postalCode>\r\n              <gmd:country gco:nilReason="missing">\r\n                <gco:CharacterString />\r\n              </gmd:country>\r\n              <gmd:electronicMailAddress>\r\n                <gco:CharacterString>contact@contact.fr</gco:CharacterString>\r\n              </gmd:electronicMailAddress>\r\n            </gmd:CI_Address>\r\n          </gmd:address>\r\n        </gmd:CI_Contact>\r\n      </gmd:contactInfo>\r\n      <gmd:role>\r\n        <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="originator" />\r\n      </gmd:role>\r\n    </gmd:CI_ResponsibleParty>\r\n  </gmd:contact>\r\n  <gmd:dateStamp>\r\n    <gco:DateTime>2012-04-11T13:48:56</gco:DateTime>\r\n  </gmd:dateStamp>\r\n  <gmd:metadataStandardName>\r\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\r\n  </gmd:metadataStandardName>\r\n  <gmd:metadataStandardVersion>\r\n    <gco:CharacterString>1.0</gco:CharacterString>\r\n  </gmd:metadataStandardVersion>\r\n  <gmd:referenceSystemInfo>\r\n    <gmd:MD_ReferenceSystem>\r\n      <gmd:referenceSystemIdentifier>\r\n        <gmd:RS_Identifier>\r\n          <gmd:code>\r\n            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>\r\n          </gmd:code>\r\n          <gmd:codeSpace>\r\n            <gco:CharacterString>EPSG</gco:CharacterString>\r\n          </gmd:codeSpace>\r\n          <gmd:version>\r\n            <gco:CharacterString>7.4</gco:CharacterString>\r\n          </gmd:version>\r\n        </gmd:RS_Identifier>\r\n      </gmd:referenceSystemIdentifier>\r\n    </gmd:MD_ReferenceSystem>\r\n  </gmd:referenceSystemInfo>\r\n  <gmd:identificationInfo>\r\n    <gmd:MD_DataIdentification>\r\n      <gmd:citation>\r\n        <gmd:CI_Citation>\r\n          <gmd:title>\r\n            <gco:CharacterString>departements</gco:CharacterString>\r\n          </gmd:title>\r\n          <gmd:date>\r\n            <gmd:CI_Date>\r\n              <gmd:date>\r\n                <gco:DateTime>2009-03-06T00:00:00</gco:DateTime>\r\n              </gmd:date>\r\n              <gmd:dateType>\r\n                <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="publication" />\r\n              </gmd:dateType>\r\n            </gmd:CI_Date>\r\n          </gmd:date>\r\n          <gmd:citedResponsibleParty>\r\n            <gmd:CI_ResponsibleParty>\r\n              <gmd:individualName>\r\n                <gco:CharacterString>DUPONT Patrick</gco:CharacterString>\r\n              </gmd:individualName>\r\n              <gmd:organisationName>\r\n                <gco:CharacterString>DIREN</gco:CharacterString>\r\n              </gmd:organisationName>\r\n              <gmd:role>\r\n                <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="user" />\r\n              </gmd:role>\r\n            </gmd:CI_ResponsibleParty>\r\n          </gmd:citedResponsibleParty>\r\n          <gmd:identifier>\r\n            <gmd:MD_Identifier>\r\n              <gmd:code>\r\n                <gco:CharacterString>https://www-p40.alkante.com/geonetwork/srv/3c892ef0-0a6d-11de-ad6b-00104b7907b4</gco:CharacterString>\r\n              </gmd:code>\r\n            </gmd:MD_Identifier>\r\n          </gmd:identifier>\r\n        </gmd:CI_Citation>\r\n      </gmd:citation>\r\n      <gmd:abstract>\r\n        <gco:CharacterString>departements</gco:CharacterString>\r\n      </gmd:abstract>\r\n      <gmd:language>\r\n        <gco:CharacterString>fre</gco:CharacterString>\r\n      </gmd:language>\r\n      <gmd:characterSet>\r\n        <gmd:MD_CharacterSetCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8" />\r\n      </gmd:characterSet>\r\n      <gmd:pointOfContact>\r\n        <gmd:CI_ResponsibleParty>\r\n          <gmd:individualName>\r\n            <gco:CharacterString>-- Contact --</gco:CharacterString>\r\n          </gmd:individualName>\r\n          <gmd:organisationName>\r\n            <gco:CharacterString>-- Organisation --</gco:CharacterString>\r\n          </gmd:organisationName>\r\n          <gmd:contactInfo>\r\n            <gmd:CI_Contact>\r\n              <gmd:phone>\r\n                <gmd:CI_Telephone>\r\n                  <gmd:voice>\r\n                    <gco:CharacterString>33 (0) 2 32 00 00 00</gco:CharacterString>\r\n                  </gmd:voice>\r\n                </gmd:CI_Telephone>\r\n              </gmd:phone>\r\n              <gmd:address>\r\n                <gmd:CI_Address>\r\n                  <gmd:electronicMailAddress>\r\n                    <gco:CharacterString>contact@contact.fr</gco:CharacterString>\r\n                  </gmd:electronicMailAddress>\r\n                </gmd:CI_Address>\r\n              </gmd:address>\r\n            </gmd:CI_Contact>\r\n          </gmd:contactInfo>\r\n          <gmd:role>\r\n            <gmd:CI_RoleCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode" codeListValue="distributor" />\r\n          </gmd:role>\r\n        </gmd:CI_ResponsibleParty>\r\n      </gmd:pointOfContact>\r\n      <gmd:descriptiveKeywords>\r\n        <gmd:MD_Keywords>\r\n          <gmd:keyword>\r\n            <gco:CharacterString>France</gco:CharacterString>\r\n          </gmd:keyword>\r\n          <gmd:type>\r\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="place" />\r\n          </gmd:type>\r\n        </gmd:MD_Keywords>\r\n      </gmd:descriptiveKeywords>\r\n      <gmd:descriptiveKeywords>\r\n        <gmd:MD_Keywords>\r\n          <gmd:keyword>\r\n            <gco:CharacterString>Dénominations géographiques</gco:CharacterString>\r\n          </gmd:keyword>\r\n          <gmd:keyword>\r\n            <gco:CharacterString>Répartition de la population — démographie</gco:CharacterString>\r\n          </gmd:keyword>\r\n          <gmd:keyword>\r\n            <gco:CharacterString>Unités administratives</gco:CharacterString>\r\n          </gmd:keyword>\r\n          <gmd:type>\r\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme" />\r\n          </gmd:type>\r\n          <gmd:thesaurusName>\r\n            <gmd:CI_Citation>\r\n              <gmd:title>\r\n                <gco:CharacterString>GEMET - INSPIRE themes, version 1.0</gco:CharacterString>\r\n              </gmd:title>\r\n              <gmd:date>\r\n                <gmd:CI_Date>\r\n                  <gmd:date>\r\n                    <gco:Date>2008-06-01</gco:Date>\r\n                  </gmd:date>\r\n                  <gmd:dateType>\r\n                    <gmd:CI_DateTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode" codeListValue="publication" />\r\n                  </gmd:dateType>\r\n                </gmd:CI_Date>\r\n              </gmd:date>\r\n            </gmd:CI_Citation>\r\n          </gmd:thesaurusName>\r\n        </gmd:MD_Keywords>\r\n      </gmd:descriptiveKeywords>\r\n      <gmd:extent>\r\n        <gmd:EX_Extent>\r\n          <gmd:description>\r\n            <gco:CharacterString>France</gco:CharacterString>\r\n          </gmd:description>\r\n          <gmd:geographicElement>\r\n            <gmd:EX_GeographicBoundingBox>\r\n              <gmd:westBoundLongitude>\r\n                <gco:Decimal>-4.31</gco:Decimal>\r\n              </gmd:westBoundLongitude>\r\n              <gmd:eastBoundLongitude>\r\n                <gco:Decimal>8.40</gco:Decimal>\r\n              </gmd:eastBoundLongitude>\r\n              <gmd:southBoundLatitude>\r\n                <gco:Decimal>41.24</gco:Decimal>\r\n              </gmd:southBoundLatitude>\r\n              <gmd:northBoundLatitude>\r\n                <gco:Decimal>51</gco:Decimal>\r\n              </gmd:northBoundLatitude>\r\n            </gmd:EX_GeographicBoundingBox>\r\n          </gmd:geographicElement>\r\n        </gmd:EX_Extent>\r\n      </gmd:extent>\r\n      <gmd:descriptiveKeywords xlink:href="https://www-p40.alkante.com/geonetwork/srv/fre/xml.keyword.get?thesaurus=external.theme.prodige&amp;id=https://www-p40.alkante.com/geonetwork//thesaurus/theme/domaine%23sousdomaine_217&amp;multiple=true&amp;lang=fre" xlink:show="replace" />\r\n      <gmd:resourceConstraints>\r\n        <gmd:MD_LegalConstraints>\r\n          <gmd:useLimitation>\r\n            <gco:CharacterString>(Non) Usage et diffusion libre</gco:CharacterString>\r\n          </gmd:useLimitation>\r\n          <gmd:accessConstraints>\r\n            <gmd:MD_RestrictionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode" codeListValue="restricted" />\r\n          </gmd:accessConstraints>\r\n          <gmd:useConstraints>\r\n            <gmd:MD_RestrictionCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode" codeListValue="copyright" />\r\n          </gmd:useConstraints>\r\n          <gmd:otherConstraints gco:nilReason="missing">\r\n            <gco:CharacterString />\r\n          </gmd:otherConstraints>\r\n        </gmd:MD_LegalConstraints>\r\n      </gmd:resourceConstraints>\r\n      <gmd:resourceConstraints>\r\n        <gmd:MD_SecurityConstraints>\r\n          <gmd:classification>\r\n            <gmd:MD_ClassificationCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ClassificationCode" codeListValue="unclassified" />\r\n          </gmd:classification>\r\n          <gmd:userNote gco:nilReason="missing">\r\n            <gco:CharacterString />\r\n          </gmd:userNote>\r\n        </gmd:MD_SecurityConstraints>\r\n      </gmd:resourceConstraints>\r\n      <gmd:spatialRepresentationType>\r\n        <gmd:MD_SpatialRepresentationTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_SpatialRepresentationTypeCode" codeListValue="vector" />\r\n      </gmd:spatialRepresentationType>\r\n      <gmd:spatialResolution>\r\n        <gmd:MD_Resolution>\r\n          <gmd:equivalentScale>\r\n            <gmd:MD_RepresentativeFraction>\r\n              <gmd:denominator>\r\n                <gco:Integer>10000000</gco:Integer>\r\n              </gmd:denominator>\r\n            </gmd:MD_RepresentativeFraction>\r\n          </gmd:equivalentScale>\r\n        </gmd:MD_Resolution>\r\n      </gmd:spatialResolution>\r\n      <gmd:spatialResolution>\r\n        <gmd:MD_Resolution>\r\n          <gmd:distance />\r\n        </gmd:MD_Resolution>\r\n      </gmd:spatialResolution>\r\n      <gmd:topicCategory>\r\n        <gmd:MD_TopicCategoryCode>boundaries</gmd:MD_TopicCategoryCode>\r\n      </gmd:topicCategory>\r\n      <gmd:supplementalInformation gco:nilReason="missing">\r\n        <gco:CharacterString />\r\n      </gmd:supplementalInformation>\r\n    </gmd:MD_DataIdentification>\r\n  </gmd:identificationInfo>\r\n  <gmd:dataQualityInfo>\r\n    <gmd:DQ_DataQuality>\r\n      <gmd:scope>\r\n        <gmd:DQ_Scope>\r\n          <gmd:level>\r\n            <gmd:MD_ScopeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode" codeListValue="dataset" />\r\n          </gmd:level>\r\n        </gmd:DQ_Scope>\r\n      </gmd:scope>\r\n      <gmd:lineage>\r\n        <gmd:LI_Lineage>\r\n          <gmd:statement>\r\n            <gco:CharacterString>departements</gco:CharacterString>\r\n          </gmd:statement>\r\n        </gmd:LI_Lineage>\r\n      </gmd:lineage>\r\n    </gmd:DQ_DataQuality>\r\n  </gmd:dataQualityInfo>\r\n</gmd:MD_Metadata>	7fc45be3-9aba-4198-920c-b8737112d522	\N	gmd:MD_Metadata	\N	1	217	\N	0	34	\N	\N	\N
10	617af7f0-0a6f-11de-ad6b-00104b7907b4	iso19139	n	n	2009-03-06T17:53:56	2012-04-11T13:53:40	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gml="http://www.opengis.net/gml" xsi:schemaLocation=" http://www.isotc211.org/2005/srv http://schemas.opengis.net/iso/19139/20060504/srv/srv.xsd">\n  <gmd:fileIdentifier>\n    <gco:CharacterString>617af7f0-0a6f-11de-ad6b-00104b7907b4</gco:CharacterString>\n  </gmd:fileIdentifier>\n  <gmd:language>\n    \n  <gco:CharacterString>fre</gco:CharacterString></gmd:language>\n  <gmd:characterSet>\n    <gmd:MD_CharacterSetCode codeList="./resources/codeList.xml#MD_CharacterSetCode" codeListValue="utf8"/>\n  </gmd:characterSet>\n  <gmd:hierarchyLevel>\n    <gmd:MD_ScopeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ScopeCode" codeListValue="service"/>\n  </gmd:hierarchyLevel>\n  <gmd:hierarchyLevelName>\n    <gco:CharacterString>service</gco:CharacterString>\n  </gmd:hierarchyLevelName>\n  <gmd:contact>\n    <gmd:CI_ResponsibleParty>\n      <gmd:individualName>\n        <gco:CharacterString>-- Contact --</gco:CharacterString>\n      </gmd:individualName>\n      <gmd:organisationName>\n        <gco:CharacterString>-- Organisation --</gco:CharacterString>\n      </gmd:organisationName>\n      <gmd:positionName gco:nilReason="missing">\n        <gco:CharacterString/>\n      </gmd:positionName>\n      <gmd:contactInfo>\n        <gmd:CI_Contact>\n          <gmd:phone>\n            <gmd:CI_Telephone>\n              <gmd:voice gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:voice>\n              <gmd:facsimile gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:facsimile>\n            </gmd:CI_Telephone>\n          </gmd:phone>\n          <gmd:address>\n            <gmd:CI_Address>\n              <gmd:deliveryPoint gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:deliveryPoint>\n              <gmd:city gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:city>\n              <gmd:administrativeArea gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:administrativeArea>\n              <gmd:postalCode gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:postalCode>\n              <gmd:country gco:nilReason="missing">\n                <gco:CharacterString/>\n              </gmd:country>\n              <gmd:electronicMailAddress>\n                <gco:CharacterString>contact@contact.fr</gco:CharacterString>\n              </gmd:electronicMailAddress>\n            </gmd:CI_Address>\n          </gmd:address>\n        </gmd:CI_Contact>\n      </gmd:contactInfo>\n      <gmd:role>\n        <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact"/>\n      </gmd:role>\n    </gmd:CI_ResponsibleParty>\n  </gmd:contact>\n  <gmd:dateStamp>\n    <gco:DateTime>2012-04-11T13:53:40</gco:DateTime>\n  </gmd:dateStamp>\n  <gmd:metadataStandardName>\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n  </gmd:metadataStandardName>\n  <gmd:metadataStandardVersion>\n    <gco:CharacterString>1.0</gco:CharacterString>\n  </gmd:metadataStandardVersion>\n  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>\n    <srv:SV_ServiceIdentification>\n      <gmd:citation>\n        <gmd:CI_Citation>\n          <gmd:title>\n            <gco:CharacterString>limites_administratives</gco:CharacterString>\n          </gmd:title>\n          <gmd:date>\n            <gmd:CI_Date>\n              <gmd:date>\n                <gco:DateTime>2009-03-06T00:00:00</gco:DateTime>\n              </gmd:date>\n              <gmd:dateType>\n                <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication"/>\n              </gmd:dateType>\n            </gmd:CI_Date>\n          </gmd:date>\n          \n        <gmd:identifier>\n                  \t\t\t\t<gmd:MD_Identifier>\n                     \t\t\t\t<gmd:code>\n                        \t\t\t\t<gco:CharacterString>https://www-p40.alkante.com/geonetwork/srv/617af7f0-0a6f-11de-ad6b-00104b7907b4</gco:CharacterString>\n                    \t\t\t\t</gmd:code>\n                  \t\t\t\t</gmd:MD_Identifier>\n\t\t\t\t\t\t\t\t</gmd:identifier></gmd:CI_Citation>\n      </gmd:citation>\n      <gmd:abstract>\n        <gco:CharacterString>limites_administratives</gco:CharacterString>\n      </gmd:abstract>\n      <gmd:pointOfContact>\n        <gmd:CI_ResponsibleParty>\n          <gmd:individualName>\n            <gco:CharacterString>-- Contact --</gco:CharacterString>\n          </gmd:individualName>\n          <gmd:organisationName>\n            <gco:CharacterString>-- Organisation --</gco:CharacterString>\n          </gmd:organisationName>\n          <gmd:contactInfo>\n            <gmd:CI_Contact>\n              <gmd:phone>\n                <gmd:CI_Telephone>\n                  <gmd:voice gco:nilReason="missing">\n                    <gco:CharacterString/>\n                  </gmd:voice>\n                </gmd:CI_Telephone>\n              </gmd:phone>\n              <gmd:address>\n                <gmd:CI_Address>\n                  <gmd:electronicMailAddress>\n                    <gco:CharacterString>contact@contact.fr</gco:CharacterString>\n                  </gmd:electronicMailAddress>\n                </gmd:CI_Address>\n              </gmd:address>\n            </gmd:CI_Contact>\n          </gmd:contactInfo>\n          <gmd:role>\n            <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact"/>\n          </gmd:role>\n        </gmd:CI_ResponsibleParty>\n      </gmd:pointOfContact>\n      \n      <gmd:descriptiveKeywords>\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>France</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="place"/>\n          </gmd:type>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:descriptiveKeywords>\n        <gmd:MD_Keywords>\n          <gmd:keyword>\n            <gco:CharacterString>infoMapAccessService</gco:CharacterString>\n          </gmd:keyword>\n          <gmd:type>\n            <gmd:MD_KeywordTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="theme"/>\n          </gmd:type>\n          <gmd:thesaurusName>\n            <gmd:CI_Citation>\n              <gmd:title>\n                <gco:CharacterString>INSPIRE Service taxonomy</gco:CharacterString>\n              </gmd:title>\n              <gmd:date>\n                <gmd:CI_Date>\n                  <gmd:date>\n                    <gco:Date>2012-04-11</gco:Date>\n                  </gmd:date>\n                  <gmd:dateType>\n                    <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication"/>\n                  </gmd:dateType>\n                </gmd:CI_Date>\n              </gmd:date>\n            </gmd:CI_Citation>\n          </gmd:thesaurusName>\n        </gmd:MD_Keywords>\n      </gmd:descriptiveKeywords>\n      <gmd:descriptiveKeywords xlink:href="https://www-p40.alkante.com/geonetwork/srv/fre/xml.keyword.get?thesaurus=external.theme.prodige&amp;id=https://www-p40.alkante.com/geonetwork//thesaurus/theme/domaine%23sousdomaine_217&amp;multiple=true&amp;lang=fre" xlink:show="replace"/><gmd:resourceConstraints>\n        <gmd:MD_LegalConstraints>\n          <gmd:accessConstraints>\n            <gmd:MD_RestrictionCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_RestrictionCode" codeListValue=""/>\n          </gmd:accessConstraints>\n          <gmd:useConstraints>\n            <gmd:MD_RestrictionCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_RestrictionCode" codeListValue=""/>\n          </gmd:useConstraints>\n          <gmd:otherConstraints gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:otherConstraints>\n        </gmd:MD_LegalConstraints>\n      </gmd:resourceConstraints>\n      <gmd:resourceConstraints>\n        <gmd:MD_SecurityConstraints>\n          <gmd:classification>\n            <gmd:MD_ClassificationCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ClassificationCode" codeListValue="unclassified"/>\n          </gmd:classification>\n          <gmd:userNote gco:nilReason="missing">\n            <gco:CharacterString/>\n          </gmd:userNote>\n        </gmd:MD_SecurityConstraints>\n      </gmd:resourceConstraints>\n      <srv:serviceType>\n        <gco:LocalName>invoke</gco:LocalName>\n      </srv:serviceType>\n      <srv:extent>\n        <gmd:EX_Extent>\n          <gmd:description>\n            <gco:CharacterString>FRANCE</gco:CharacterString>\n          </gmd:description>\n          <gmd:geographicElement>\n            <gmd:EX_GeographicBoundingBox>\n              <gmd:westBoundLongitude>\n                <gco:Decimal>-4.31</gco:Decimal>\n              </gmd:westBoundLongitude>\n              <gmd:eastBoundLongitude>\n                <gco:Decimal>8.40</gco:Decimal>\n              </gmd:eastBoundLongitude>\n              <gmd:southBoundLatitude>\n                <gco:Decimal>41.24</gco:Decimal>\n              </gmd:southBoundLatitude>\n              <gmd:northBoundLatitude>\n                <gco:Decimal>51</gco:Decimal>\n              </gmd:northBoundLatitude>\n            </gmd:EX_GeographicBoundingBox>\n          </gmd:geographicElement>\n        </gmd:EX_Extent>\n      </srv:extent>\n      <srv:couplingType>\n        <srv:SV_CouplingType codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType" codeListValue="tight"/>\n      </srv:couplingType>\n      <srv:containsOperations>\n        <srv:SV_OperationMetadata>\n          <srv:operationName>\n            <gco:CharacterString>Accès à la carte</gco:CharacterString>\n          </srv:operationName>\n          <srv:DCP>\n            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n          </srv:DCP>\n          \n        </srv:SV_OperationMetadata>\n      </srv:containsOperations>\n    <srv:containsOperations>\n                    <srv:SV_OperationMetadata>\n                      <srv:operationName>\n                        <gco:CharacterString>Accès à la carte</gco:CharacterString>\n                      </srv:operationName>\n                      <srv:DCP>\n                        <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>\n                      </srv:DCP>\n                      <srv:connectPoint>\n                        <gmd:CI_OnlineResource>\n                          <gmd:linkage>\n                            <gmd:URL>https://www-p40.alkante.com/geosource/consultation?id=10</gmd:URL>\n                          </gmd:linkage>\n                          <gmd:protocol>\n                            <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>\n                          </gmd:protocol>\n                        </gmd:CI_OnlineResource>\n                      </srv:connectPoint>\n                    </srv:SV_OperationMetadata>\n                  </srv:containsOperations><srv:containsOperations><srv:SV_OperationMetadata><srv:operationName><gco:CharacterString>Accès au fichier contexte OWS de la carte</gco:CharacterString></srv:operationName><srv:DCP><srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/></srv:DCP><srv:connectPoint><gmd:CI_OnlineResource><gmd:linkage><gmd:URL>https://carto-p40.alkante.com/frontcarto/context/getOws/10</gmd:URL></gmd:linkage><gmd:protocol><gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString></gmd:protocol></gmd:CI_OnlineResource></srv:connectPoint></srv:SV_OperationMetadata></srv:containsOperations></srv:SV_ServiceIdentification>\n  </gmd:identificationInfo>\n  <gmd:distributionInfo>\n                   <gmd:MD_Distribution>\n                    <gmd:transferOptions>\n                      <gmd:MD_DigitalTransferOptions>\n                        <gmd:onLine>\n                          <gmd:CI_OnlineResource>\n                            <gmd:linkage>\n                              <gmd:URL>https://www-p40.alkante.com/geosource/consultation?id=10</gmd:URL>\n                            </gmd:linkage>\n                          </gmd:CI_OnlineResource>\n                        </gmd:onLine>\n                      <gmd:onLine><gmd:CI_OnlineResource><gmd:linkage><gmd:URL>https://carto-p40.alkante.com/frontcarto/context/getOws/10</gmd:URL></gmd:linkage><gmd:protocol><gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString></gmd:protocol><gmd:name><gco:CharacterString>Accès au fichier contexte OWS de la carte</gco:CharacterString></gmd:name><gmd:description><gco:CharacterString>Accès au fichier contexte OWS de la carte</gco:CharacterString></gmd:description></gmd:CI_OnlineResource></gmd:onLine></gmd:MD_DigitalTransferOptions>\n                    </gmd:transferOptions>\n                  </gmd:MD_Distribution>\n                  </gmd:distributionInfo><gmd:dataQualityInfo>\n    <gmd:DQ_DataQuality>\n      <gmd:scope>\n        <gmd:DQ_Scope>\n          <gmd:level>\n            <gmd:MD_ScopeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ScopeCode" codeListValue="service"/>\n          </gmd:level>\n          <gmd:levelDescription>\n            <gmd:MD_ScopeDescription>\n              <gmd:other>\n                <gco:CharacterString>Carte interactive</gco:CharacterString>\n              </gmd:other>\n            </gmd:MD_ScopeDescription>\n          </gmd:levelDescription>\n        </gmd:DQ_Scope>\n      </gmd:scope>\n      <gmd:lineage>\n        <gmd:LI_Lineage>\n          <gmd:statement>\n            <gco:CharacterString>Carte interactive représentant les départements</gco:CharacterString>\n          </gmd:statement>\n        </gmd:LI_Lineage>\n      </gmd:lineage>\n    </gmd:DQ_DataQuality>\n  </gmd:dataQualityInfo>\n</gmd:MD_Metadata>\n	7fc45be3-9aba-4198-920c-b8737112d522	\N	gmd:MD_Metadata	\N	1	217	\N	0	21	\N	\N	\N
40786	8c018199-b0f9-41f9-83b0-b2b076e5bba1	iso19139	y	n	2019-05-24T17:25:18	2019-05-24T17:25:18	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:gml="http://www.opengis.net/gml" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.isotc211.org/2005/gmd http://www.isotc211.org/2005/gmd/gmd.xsd http://www.isotc211.org/2005/gmx http://www.isotc211.org/2005/gmx/gmx.xsd http://www.isotc211.org/2005/srv http://schemas.opengis.net/iso/19139/20060504/srv/srv.xsd">\r\n  <gmd:fileIdentifier>\r\n    <gco:CharacterString>8c018199-b0f9-41f9-83b0-b2b076e5bba1</gco:CharacterString>\r\n  </gmd:fileIdentifier>\r\n  <gmd:language>\r\n    <gco:CharacterString>fre</gco:CharacterString>\r\n  </gmd:language>\r\n  <gmd:characterSet>\r\n    <gmd:MD_CharacterSetCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_CharacterSetCode" codeListValue="utf8" />\r\n  </gmd:characterSet>\r\n  <gmd:hierarchyLevel>\r\n    <gmd:MD_ScopeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode" codeListValue="service" />\r\n  </gmd:hierarchyLevel>\r\n  <gmd:hierarchyLevelName>\r\n    <gco:CharacterString>service</gco:CharacterString>\r\n  </gmd:hierarchyLevelName>\r\n  <gmd:contact>\r\n    <gmd:CI_ResponsibleParty>\r\n      <gmd:individualName gco:nilReason="missing">\r\n        <gco:CharacterString />\r\n      </gmd:individualName>\r\n      <gmd:organisationName>\r\n        <gco:CharacterString>A remplir</gco:CharacterString>\r\n      </gmd:organisationName>\r\n      <gmd:contactInfo>\r\n        <gmd:CI_Contact>\r\n          <gmd:phone>\r\n            <gmd:CI_Telephone>\r\n              <gmd:voice gco:nilReason="missing">\r\n                <gco:CharacterString />\r\n              </gmd:voice>\r\n            </gmd:CI_Telephone>\r\n          </gmd:phone>\r\n          <gmd:address>\r\n            <gmd:CI_Address>\r\n              <gmd:electronicMailAddress>\r\n                <gco:CharacterString>remplir@</gco:CharacterString>\r\n              </gmd:electronicMailAddress>\r\n            </gmd:CI_Address>\r\n          </gmd:address>\r\n        </gmd:CI_Contact>\r\n      </gmd:contactInfo>\r\n      <gmd:role>\r\n        <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact" />\r\n      </gmd:role>\r\n    </gmd:CI_ResponsibleParty>\r\n  </gmd:contact>\r\n  <gmd:dateStamp>\r\n    <gco:DateTime>2018-11-15T10:54:31</gco:DateTime>\r\n  </gmd:dateStamp>\r\n  <gmd:metadataStandardName>\r\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\r\n  </gmd:metadataStandardName>\r\n  <gmd:metadataStandardVersion>\r\n    <gco:CharacterString>1.0</gco:CharacterString>\r\n  </gmd:metadataStandardVersion>\r\n  <gmd:referenceSystemInfo>\r\n    <gmd:MD_ReferenceSystem>\r\n      <gmd:referenceSystemIdentifier>\r\n        <gmd:RS_Identifier>\r\n          <gmd:code>\r\n            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>\r\n          </gmd:code>\r\n          <gmd:codeSpace>\r\n            <gco:CharacterString>EPSG</gco:CharacterString>\r\n          </gmd:codeSpace>\r\n          <gmd:version>\r\n            <gco:CharacterString>7.9</gco:CharacterString>\r\n          </gmd:version>\r\n        </gmd:RS_Identifier>\r\n      </gmd:referenceSystemIdentifier>\r\n    </gmd:MD_ReferenceSystem>\r\n  </gmd:referenceSystemInfo>\r\n  <gmd:identificationInfo>\r\n    <srv:SV_ServiceIdentification>\r\n      <srv:containsOperations>\r\n        <srv:SV_OperationMetadata>\r\n          <srv:operationName>\r\n            <gco:CharacterString>Accès au graphe</gco:CharacterString>\r\n          </srv:operationName>\r\n          <srv:DCP>\r\n            <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices" />\r\n          </srv:DCP>\r\n          <srv:connectPoint>\r\n            <gmd:CI_OnlineResource>\r\n              <gmd:linkage>\r\n                <gmd:URL />\r\n              </gmd:linkage>\r\n              <gmd:protocol>\r\n                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>\r\n              </gmd:protocol>\r\n            </gmd:CI_OnlineResource>\r\n          </srv:connectPoint>\r\n        </srv:SV_OperationMetadata>\r\n      </srv:containsOperations>\r\n      <gmd:citation>\r\n        <gmd:CI_Citation>\r\n          <gmd:title>\r\n            <gco:CharacterString>Modèle de métadonnées de graphe</gco:CharacterString>\r\n          </gmd:title>\r\n          <gmd:date>\r\n            <gmd:CI_Date>\r\n              <gmd:date>\r\n                <gco:DateTime>2018-11-15T09:56:00</gco:DateTime>\r\n              </gmd:date>\r\n              <gmd:dateType>\r\n                <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication" />\r\n              </gmd:dateType>\r\n            </gmd:CI_Date>\r\n          </gmd:date>\r\n          <gmd:identifier>\r\n            <gmd:MD_Identifier>\r\n              <gmd:code>\r\n                <gco:CharacterString />\r\n              </gmd:code>\r\n            </gmd:MD_Identifier>\r\n          </gmd:identifier>\r\n        </gmd:CI_Citation>\r\n      </gmd:citation>\r\n      <gmd:abstract>\r\n        <gco:CharacterString>A remplir</gco:CharacterString>\r\n      </gmd:abstract>\r\n      <gmd:pointOfContact>\r\n        <gmd:CI_ResponsibleParty>\r\n          <gmd:individualName gco:nilReason="missing">\r\n            <gco:CharacterString />\r\n          </gmd:individualName>\r\n          <gmd:organisationName>\r\n            <gco:CharacterString>A remplir</gco:CharacterString>\r\n          </gmd:organisationName>\r\n          <gmd:contactInfo>\r\n            <gmd:CI_Contact>\r\n              <gmd:phone>\r\n                <gmd:CI_Telephone>\r\n                  <gmd:voice gco:nilReason="missing">\r\n                    <gco:CharacterString />\r\n                  </gmd:voice>\r\n                </gmd:CI_Telephone>\r\n              </gmd:phone>\r\n              <gmd:address>\r\n                <gmd:CI_Address>\r\n                  <gmd:electronicMailAddress>\r\n                    <gco:CharacterString>remplir@</gco:CharacterString>\r\n                  </gmd:electronicMailAddress>\r\n                </gmd:CI_Address>\r\n              </gmd:address>\r\n            </gmd:CI_Contact>\r\n          </gmd:contactInfo>\r\n          <gmd:role>\r\n            <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact" />\r\n          </gmd:role>\r\n        </gmd:CI_ResponsibleParty>\r\n      </gmd:pointOfContact>\r\n      <gmd:descriptiveKeywords>\r\n        <gmd:MD_Keywords>\r\n          <gmd:keyword>\r\n            <gco:CharacterString>infoChartAccessService</gco:CharacterString>\r\n          </gmd:keyword>\r\n          <gmd:type>\r\n            <gmd:MD_KeywordTypeCode codeList="http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode" codeListValue="theme" />\r\n          </gmd:type>\r\n          <gmd:thesaurusName>\r\n            <gmd:CI_Citation>\r\n              <gmd:title>\r\n                <gco:CharacterString>INSPIRE Service taxonomy</gco:CharacterString>\r\n              </gmd:title>\r\n              <gmd:date>\r\n                <gmd:CI_Date>\r\n                  <gmd:date>\r\n                    <gco:Date>2010-04-22</gco:Date>\r\n                  </gmd:date>\r\n                  <gmd:dateType>\r\n                    <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication" />\r\n                  </gmd:dateType>\r\n                </gmd:CI_Date>\r\n              </gmd:date>\r\n              <gmd:identifier>\r\n                <gmd:MD_Identifier>\r\n                  <gmd:code>\r\n                    <gmx:Anchor xlink:href="https://www.application-prodige-mig.fr/geonetwork/srv/fre/thesaurus.download?ref=external.theme.inspire-service-taxonomy">geonetwork.thesaurus.external.theme.inspire-service-taxonomy</gmx:Anchor>\r\n                  </gmd:code>\r\n                </gmd:MD_Identifier>\r\n              </gmd:identifier>\r\n            </gmd:CI_Citation>\r\n          </gmd:thesaurusName>\r\n        </gmd:MD_Keywords>\r\n      </gmd:descriptiveKeywords>\r\n      <gmd:resourceConstraints>\r\n        <gmd:MD_LegalConstraints>\r\n          <gmd:useLimitation>\r\n            <gco:CharacterString>Utilisation libre sous réserve de mentionner la source (a minima le nom du producteur) et la date de sa dernière mise à jour</gco:CharacterString>\r\n          </gmd:useLimitation>\r\n          <gmd:accessConstraints>\r\n            <gmd:MD_RestrictionCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_RestrictionCode" codeListValue="otherRestrictions" />\r\n          </gmd:accessConstraints>\r\n          <gmd:otherConstraints>\r\n            <gco:CharacterString>Pas de restriction d’accès public selon INSPIRE</gco:CharacterString>\r\n          </gmd:otherConstraints>\r\n        </gmd:MD_LegalConstraints>\r\n      </gmd:resourceConstraints>\r\n      <srv:serviceType>\r\n        <gco:LocalName>invoke</gco:LocalName>\r\n      </srv:serviceType>\r\n      <srv:extent>\r\n        <gmd:EX_Extent xmlns:xs="http://www.w3.org/2001/XMLSchema">\r\n          <gmd:description>\r\n            <gco:CharacterString>FRANCE METROPOLITAINE</gco:CharacterString>\r\n          </gmd:description>\r\n          <gmd:geographicElement>\r\n            <gmd:EX_GeographicBoundingBox>\r\n              <gmd:westBoundLongitude>\r\n                <gco:Decimal>-5</gco:Decimal>\r\n              </gmd:westBoundLongitude>\r\n              <gmd:eastBoundLongitude>\r\n                <gco:Decimal>10</gco:Decimal>\r\n              </gmd:eastBoundLongitude>\r\n              <gmd:southBoundLatitude>\r\n                <gco:Decimal>42</gco:Decimal>\r\n              </gmd:southBoundLatitude>\r\n              <gmd:northBoundLatitude>\r\n                <gco:Decimal>51</gco:Decimal>\r\n              </gmd:northBoundLatitude>\r\n            </gmd:EX_GeographicBoundingBox>\r\n          </gmd:geographicElement>\r\n        </gmd:EX_Extent>\r\n      </srv:extent>\r\n    </srv:SV_ServiceIdentification>\r\n  </gmd:identificationInfo>\r\n  <gmd:distributionInfo>\r\n    <gmd:MD_Distribution>\r\n      <gmd:distributionFormat>\r\n        <gmd:MD_Format>\r\n          <gmd:name>\r\n            <gco:CharacterString>UTF-8</gco:CharacterString>\r\n          </gmd:name>\r\n          <gmd:version gco:nilReason="missing">\r\n            <gco:CharacterString />\r\n          </gmd:version>\r\n        </gmd:MD_Format>\r\n      </gmd:distributionFormat>\r\n      <gmd:distributor />\r\n    </gmd:MD_Distribution>\r\n  </gmd:distributionInfo>\r\n</gmd:MD_Metadata>	7fc45be3-9aba-4198-920c-b8737112d522	\N	gmd:MD_Metadata	\N	40784	\N	\N	0	0	0	\N	\N
40787	0f9a5c54-0a56-436e-b33f-a24cf270097b	iso19139	y	n	2019-05-24T17:25:25	2019-05-24T17:25:25	<gmd:MD_Metadata xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gml="http://www.opengis.net/gml" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gfc="http://www.isotc211.org/2005/gfc" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gmx="http://www.isotc211.org/2005/gmx" xmlns:gts="http://www.isotc211.org/2005/gts" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.isotc211.org/2005/gmd http://www.isotc211.org/2005/gmd/gmd.xsd http://www.isotc211.org/2005/gmx http://www.isotc211.org/2005/gmx/gmx.xsd http://www.isotc211.org/2005/srv http://schemas.opengis.net/iso/19139/20060504/srv/srv.xsd">\r\n  <gmd:fileIdentifier>\r\n    <gco:CharacterString>0f9a5c54-0a56-436e-b33f-a24cf270097b</gco:CharacterString>\r\n  </gmd:fileIdentifier>\r\n  <gmd:language>\r\n    <gmd:LanguageCode codeList="http://www.loc.gov/standards/iso639-2/" codeListValue="fre" />\r\n  </gmd:language>\r\n  <gmd:characterSet>\r\n    <gmd:MD_CharacterSetCode codeListValue="utf8" codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_CharacterSetCode" />\r\n  </gmd:characterSet>\r\n  <gmd:hierarchyLevel>\r\n    <gmd:MD_ScopeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode" codeListValue="nonGeographicDataset" />\r\n  </gmd:hierarchyLevel>\r\n  <gmd:hierarchyLevelName>\r\n    <gco:CharacterString>Série de données non géolocalisables</gco:CharacterString>\r\n  </gmd:hierarchyLevelName>\r\n  <gmd:contact>\r\n    <gmd:CI_ResponsibleParty>\r\n      <gmd:organisationName>\r\n        <gco:CharacterString>-- Nom du point de contact des métadonnées --</gco:CharacterString>\r\n      </gmd:organisationName>\r\n      <gmd:contactInfo>\r\n        <gmd:CI_Contact>\r\n          <gmd:address>\r\n            <gmd:CI_Address>\r\n              <gmd:electronicMailAddress>\r\n                <gco:CharacterString>-- Adresse email --</gco:CharacterString>\r\n              </gmd:electronicMailAddress>\r\n            </gmd:CI_Address>\r\n          </gmd:address>\r\n        </gmd:CI_Contact>\r\n      </gmd:contactInfo>\r\n      <gmd:role>\r\n        <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="pointOfContact" />\r\n      </gmd:role>\r\n    </gmd:CI_ResponsibleParty>\r\n  </gmd:contact>\r\n  <gmd:dateStamp>\r\n    <gco:DateTime>2017-12-11T12:07:56</gco:DateTime>\r\n  </gmd:dateStamp>\r\n  <gmd:metadataStandardName>\r\n    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\r\n  </gmd:metadataStandardName>\r\n  <gmd:metadataStandardVersion>\r\n    <gco:CharacterString>1.0</gco:CharacterString>\r\n  </gmd:metadataStandardVersion>\r\n  <gmd:identificationInfo>\r\n    <gmd:MD_DataIdentification>\r\n      <gmd:citation>\r\n        <gmd:CI_Citation>\r\n          <gmd:title>\r\n            <gco:CharacterString>Modèle pour la saisie d'une série de données non géolocalisables</gco:CharacterString>\r\n          </gmd:title>\r\n          <gmd:date>\r\n            <gmd:CI_Date>\r\n              <gmd:date>\r\n                <gco:Date>2011-01-01</gco:Date>\r\n              </gmd:date>\r\n              <gmd:dateType>\r\n                <gmd:CI_DateTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode" codeListValue="creation" />\r\n              </gmd:dateType>\r\n            </gmd:CI_Date>\r\n          </gmd:date>\r\n        </gmd:CI_Citation>\r\n      </gmd:citation>\r\n      <gmd:abstract>\r\n        <gco:CharacterString>-- Court résumé explicatif du contenu de la ressource --</gco:CharacterString>\r\n      </gmd:abstract>\r\n      <gmd:pointOfContact>\r\n        <gmd:CI_ResponsibleParty>\r\n          <gmd:organisationName>\r\n            <gco:CharacterString>-- Nom de l'organisation responsable de la série de données --</gco:CharacterString>\r\n          </gmd:organisationName>\r\n          <gmd:contactInfo>\r\n            <gmd:CI_Contact>\r\n              <gmd:address>\r\n                <gmd:CI_Address>\r\n                  <gmd:electronicMailAddress>\r\n                    <gco:CharacterString>-- Adresse email --</gco:CharacterString>\r\n                  </gmd:electronicMailAddress>\r\n                </gmd:CI_Address>\r\n              </gmd:address>\r\n            </gmd:CI_Contact>\r\n          </gmd:contactInfo>\r\n          <gmd:role>\r\n            <gmd:CI_RoleCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode" codeListValue="publisher" />\r\n          </gmd:role>\r\n        </gmd:CI_ResponsibleParty>\r\n      </gmd:pointOfContact>\r\n      <gmd:descriptiveKeywords>\r\n        <gmd:MD_Keywords>\r\n          <gmd:keyword>\r\n            <gco:CharacterString>-- Mots-clé --</gco:CharacterString>\r\n          </gmd:keyword>\r\n          <gmd:type>\r\n            <gmd:MD_KeywordTypeCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode" codeListValue="theme" />\r\n          </gmd:type>\r\n        </gmd:MD_Keywords>\r\n      </gmd:descriptiveKeywords>\r\n      <gmd:language>\r\n        <gco:CharacterString>fre</gco:CharacterString>\r\n      </gmd:language>\r\n      <gmd:extent>\r\n        <gmd:EX_Extent xmlns:xs="http://www.w3.org/2001/XMLSchema">\r\n          <gmd:geographicElement>\r\n            <gmd:EX_GeographicBoundingBox>\r\n              <gmd:westBoundLongitude>\r\n                <gco:Decimal>-5</gco:Decimal>\r\n              </gmd:westBoundLongitude>\r\n              <gmd:eastBoundLongitude>\r\n                <gco:Decimal>10</gco:Decimal>\r\n              </gmd:eastBoundLongitude>\r\n              <gmd:southBoundLatitude>\r\n                <gco:Decimal>42</gco:Decimal>\r\n              </gmd:southBoundLatitude>\r\n              <gmd:northBoundLatitude>\r\n                <gco:Decimal>51</gco:Decimal>\r\n              </gmd:northBoundLatitude>\r\n            </gmd:EX_GeographicBoundingBox>\r\n          </gmd:geographicElement>\r\n        </gmd:EX_Extent>\r\n      </gmd:extent>\r\n    </gmd:MD_DataIdentification>\r\n  </gmd:identificationInfo>\r\n  <gmd:distributionInfo>\r\n    <gmd:MD_Distribution>\r\n      <gmd:distributionFormat>\r\n        <gmd:MD_Format>\r\n          <gmd:name gco:nilReason="missing">\r\n            <gco:CharacterString />\r\n          </gmd:name>\r\n          <gmd:version gco:nilReason="missing">\r\n            <gco:CharacterString />\r\n          </gmd:version>\r\n        </gmd:MD_Format>\r\n      </gmd:distributionFormat>\r\n      <gmd:transferOptions>\r\n        <gmd:MD_DigitalTransferOptions />\r\n      </gmd:transferOptions>\r\n    </gmd:MD_Distribution>\r\n  </gmd:distributionInfo>\r\n</gmd:MD_Metadata>	7fc45be3-9aba-4198-920c-b8737112d522	\N	gmd:MD_Metadata	\N	40784	\N	\N	0	0	0	\N	\N
\.


--
-- Data for Name: metadatacateg; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.metadatacateg (metadataid, categoryid) FROM stdin;
\.


--
-- Data for Name: metadatafiledownloads; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.metadatafiledownloads (id, downloaddate, filename, fileuploadid, metadataid, requestercomments, requestermail, requestername, requesterorg, username) FROM stdin;
\.


--
-- Data for Name: metadatafileuploads; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.metadatafileuploads (id, deleteddate, filename, filesize, metadataid, uploaddate, username) FROM stdin;
\.


--
-- Data for Name: metadataidentifiertemplate; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.metadataidentifiertemplate (id, name, isprovided, template) FROM stdin;
\.


--
-- Data for Name: metadatanotifications; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.metadatanotifications (metadataid, notifierid, notified, metadatauuid, action, errormsg) FROM stdin;
\.


--
-- Data for Name: metadatanotifiers; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.metadatanotifiers (id, name, url, enabled, username, password) FROM stdin;
\.


--
-- Data for Name: metadatarating; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.metadatarating (metadataid, ipaddress, rating) FROM stdin;
\.


--
-- Data for Name: metadatastatus; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.metadatastatus (metadataid, statusid, userid, changedate, changemessage) FROM stdin;
\.


--
-- Data for Name: operationallowed; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.operationallowed (groupid, metadataid, operationid) FROM stdin;
1	12	0
1	14	0
1	10	5
1	10	0
1	10	1
1	9	5
1	9	0
1	9	1
1	40020	5
1	40020	0
1	40020	1
1	40021	5
1	40021	0
1	40021	1
1	40022	5
1	40022	0
1	40022	1
217	9	2
217	10	2
\.


--
-- Data for Name: operations; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.operations (id, name) FROM stdin;
0	view
1	download
3	notify
5	dynamic
6	featured
2	editing
\.


--
-- Data for Name: operationsdes; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.operationsdes (iddes, langid, label) FROM stdin;
0	chi	Publish
1	chi	Download
3	chi	Notify
5	chi	Interactive Map
6	chi	Featured
0	eng	Publish
1	eng	Download
3	eng	Notify
5	eng	Interactive Map
6	eng	Featured
2	eng	Editing
0	fre	Publish
1	fre	Télécharger
3	fre	Notifier
5	fre	Interactive Map
6	fre	Epingler
2	fre	Editer
\.


--
-- Data for Name: params; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) FROM stdin;
1	2	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
2	3	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
3	3	1	keyword	France	1.40000000000000007e-45	\N	\N	n
4	5	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
5	5	1	serviceType	invoke	1.40000000000000007e-45	\N	\N	n
6	6	1	parentUuid	617af7f0-0a6f-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
7	7	1	operatesOn	617af7f0-0a6f-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
8	8	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
9	8	1	keyword	Demonstration	1.40000000000000007e-45	\N	\N	n
10	9	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
11	9	1	keyword	Demonstration	1.40000000000000007e-45	\N	\N	n
12	9	1	serviceType	invoke	1.40000000000000007e-45	\N	\N	n
13	12	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
14	12	1	keyword	France	1.40000000000000007e-45	\N	\N	n
15	13	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
16	14	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
17	15	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
18	15	1	keyword	France	1.40000000000000007e-45	\N	\N	n
19	15	1	serviceType	invoke	1.40000000000000007e-45	\N	\N	n
20	16	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
21	16	1	keyword	France	1.40000000000000007e-45	\N	\N	n
22	16	1	serviceType	invoke	1.40000000000000007e-45	\N	\N	n
23	17	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
24	17	1	keyword	France	1.40000000000000007e-45	\N	\N	n
25	18	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
26	19	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
27	20	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
28	22	1	keyword	Unités administratives	1.40000000000000007e-45	\N	\N	n
29	24	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
30	24	1	keyword	France	1.40000000000000007e-45	\N	\N	n
31	27	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
32	28	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
33	29	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
34	30	1	hasfeaturecat	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
35	31	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
36	32	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
37	33	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
38	34	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
39	35	1	hasfeaturecat	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
40	36	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
41	37	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
42	42	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
43	42	1	keyword	France	1.40000000000000007e-45	\N	\N	n
44	43	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
45	43	1	keyword	France	1.40000000000000007e-45	\N	\N	n
46	44	1	hasfeaturecat	7a8db1d7-f5cb-455d-a911-78f93418a0ce	1.40000000000000007e-45	\N	\N	n
47	49	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
48	50	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
49	51	1	hasfeaturecat	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
50	52	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
51	53	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
52	65	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
53	65	1	keyword	France	1.40000000000000007e-45	\N	\N	n
54	66	1	hasfeaturecat	7a8db1d7-f5cb-455d-a911-78f93418a0ce	1.40000000000000007e-45	\N	\N	n
55	83	1	hasfeaturecat	7a8db1d7-f5cb-455d-a911-78f93418a0ce	1.40000000000000007e-45	\N	\N	n
56	84	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
57	84	1	keyword	France	1.40000000000000007e-45	\N	\N	n
58	85	1	hasfeaturecat	7a8db1d7-f5cb-455d-a911-78f93418a0ce	1.40000000000000007e-45	\N	\N	n
59	94	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
60	94	1	keyword	France	1.40000000000000007e-45	\N	\N	n
61	95	1	hasfeaturecat	7a8db1d7-f5cb-455d-a911-78f93418a0ce	1.40000000000000007e-45	\N	\N	n
62	100	1	hasfeaturecat	7a8db1d7-f5cb-455d-a911-78f93418a0ce	1.40000000000000007e-45	\N	\N	n
63	105	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
64	105	1	keyword	France	1.40000000000000007e-45	\N	\N	n
65	110	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
66	110	1	keyword	France	1.40000000000000007e-45	\N	\N	n
67	111	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
68	111	1	keyword	France	1.40000000000000007e-45	\N	\N	n
69	114	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
70	114	1	keyword	Demonstration	1.40000000000000007e-45	\N	\N	n
71	117	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
72	117	1	keyword	Demonstration	1.40000000000000007e-45	\N	\N	n
73	118	1	hasfeaturecat	7a8db1d7-f5cb-455d-a911-78f93418a0ce	1.40000000000000007e-45	\N	\N	n
74	119	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
75	119	1	keyword	Demonstration	1.40000000000000007e-45	\N	\N	n
76	122	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
77	123	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
78	124	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
79	125	1	hasfeaturecat	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
80	126	1	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
81	127	1	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	1.40000000000000007e-45	\N	\N	n
82	130	1	_source	7fc45be3-9aba-4198-920c-b8737112d522	1.40000000000000007e-45	\N	\N	n
40007	40006	0	_op0	1	0	\N	\N	n
40008	40006	0	_op2	1	0	\N	\N	n
40009	40006	0	_isTemplate	n	0	\N	\N	n
40011	40010	0	_op0	1	0	\N	\N	n
40012	40010	0	_op2	1	0	\N	\N	n
40013	40010	0	_source	7fc45be3-9aba-4198-920c-b8737112d522	0	\N	\N	n
40014	40010	0	type	dataset	0	\N	\N	n
40015	40010	0	type	series	0	\N	\N	n
40016	40010	0	type	service	0	\N	\N	n
40017	40010	0	type	nonGeographicDataset	0	\N	\N	n
40018	40010	0	_isTemplate	n	0	\N	\N	n
40020	40019	0	_op0	1	0	\N	\N	n
40021	40019	0	_op2	1	0	\N	\N	n
40022	40019	0	_isTemplate	n	0	\N	\N	n
40024	40023	0	_op0	1	0	\N	\N	n
40025	40023	0	_op2	1	0	\N	\N	n
40026	40023	0	_isTemplate	n	0	\N	\N	n
40031	40029	0	_op0	1	0	\N	\N	n
40035	40027	0	_op0	1	0	\N	\N	n
40030	40028	0	_op0	1	0	\N	\N	n
40032	40029	0	_op2	1	0	\N	\N	n
40033	40029	0	_isTemplate	n	0	\N	\N	n
40034	40028	0	_op2	1	0	\N	\N	n
40038	40028	0	_isTemplate	n	0	\N	\N	n
40036	40027	0	_op2	1	0	\N	\N	n
40037	40027	0	_isTemplate	n	0	\N	\N	n
40040	40039	0	_op0	1	0	\N	\N	n
40041	40039	0	_op2	1	0	\N	\N	n
40042	40039	0	_isTemplate	n	0	\N	\N	n
40044	40043	0	_op0	1	0	\N	\N	n
40045	40043	0	_op2	1	0	\N	\N	n
40046	40043	0	_isTemplate	n	0	\N	\N	n
40048	40047	0	_op0	1	0	\N	\N	n
40049	40047	0	_op2	1	0	\N	\N	n
40050	40047	0	_isTemplate	n	0	\N	\N	n
40052	40051	0	_op0	1	0	\N	\N	n
40053	40051	0	_op2	1	0	\N	\N	n
40054	40051	0	_isTemplate	n	0	\N	\N	n
40056	40055	0	_op0	1	0	\N	\N	n
40057	40055	0	_op2	1	0	\N	\N	n
40058	40055	0	parentUuid	1c661f3e-33b5-4410-aa0d-c85f21a8ae7d	0	\N	\N	n
40059	40055	0	_isTemplate	n	0	\N	\N	n
40061	40060	0	_op0	1	0	\N	\N	n
40062	40060	0	_op2	1	0	\N	\N	n
40063	40060	0	operatesOn	1c661f3e-33b5-4410-aa0d-c85f21a8ae7d	0	\N	\N	n
40064	40060	0	_isTemplate	n	0	\N	\N	n
40068	40067	0	_op0	1	0	\N	\N	n
40069	40067	0	_op2	1	0	\N	\N	n
40070	40067	0	_uuid	7a8db1d7-f5cb-455d-a911-78f93418a0ce	0	\N	\N	n
40066	40065	0	_op0	1	0	\N	\N	n
40072	40065	0	_op2	1	0	\N	\N	n
40073	40065	0	hasfeaturecat	1c661f3e-33b5-4410-aa0d-c85f21a8ae7d	0	\N	\N	n
40074	40065	0	_isTemplate	n	0	\N	\N	n
40071	40067	0	_isTemplate	n	0	\N	\N	n
40076	40075	0	_op0	1	0	\N	\N	n
40077	40075	0	_op2	1	0	\N	\N	n
40078	40075	0	agg_associated	1c661f3e-33b5-4410-aa0d-c85f21a8ae7d	0	\N	\N	n
40079	40075	0	_isTemplate	n	0	\N	\N	n
40081	40080	0	_op0	1	0	\N	\N	n
40082	40080	0	_op2	1	0	\N	\N	n
40083	40080	0	hasfeaturecat	1c661f3e-33b5-4410-aa0d-c85f21a8ae7d	0	\N	\N	n
40084	40080	0	_isTemplate	n	0	\N	\N	n
40091	40090	0	_op0	1	0	\N	\N	n
40092	40090	0	_op2	1	0	\N	\N	n
40093	40090	0	_isTemplate	n	0	\N	\N	n
40095	40094	0	_op0	1	0	\N	\N	n
40096	40094	0	_op2	1	0	\N	\N	n
40097	40094	0	_isTemplate	n	0	\N	\N	n
40099	40098	0	_op0	1	0	\N	\N	n
40100	40098	0	_op2	1	0	\N	\N	n
40101	40098	0	_isTemplate	n	0	\N	\N	n
40104	40103	0	_op0	-1	0	\N	\N	n
40105	40103	0	_op2	-1	0	\N	\N	n
40106	40103	0	_op0	0	0	\N	\N	n
40107	40103	0	_op2	0	0	\N	\N	n
40108	40103	0	_op0	1	0	\N	\N	n
40109	40103	0	_op2	1	0	\N	\N	n
40110	40103	0	_op0	217	0	\N	\N	n
40111	40103	0	_op2	217	0	\N	\N	n
40112	40103	0	_owner	40102	0	\N	\N	n
40113	40103	0	_dummy	0	0	\N	\N	n
40114	40103	0	_isTemplate	n	0	\N	\N	n
40117	40115	0	_op0	-1	0	\N	\N	n
40118	40115	0	_op2	-1	0	\N	\N	n
40119	40115	0	_op0	0	0	\N	\N	n
40120	40115	0	_op2	0	0	\N	\N	n
40121	40115	0	_op0	1	0	\N	\N	n
40122	40115	0	_op2	1	0	\N	\N	n
40123	40115	0	_op0	217	0	\N	\N	n
40124	40115	0	_op2	217	0	\N	\N	n
40125	40115	0	_owner	40102	0	\N	\N	n
40126	40115	0	_dummy	0	0	\N	\N	n
40127	40115	0	_isTemplate	n	0	\N	\N	n
40128	40116	0	_op0	-1	0	\N	\N	n
40129	40116	0	_op2	-1	0	\N	\N	n
40130	40116	0	_op0	0	0	\N	\N	n
40131	40116	0	_op2	0	0	\N	\N	n
40132	40116	0	_op0	1	0	\N	\N	n
40133	40116	0	_op2	1	0	\N	\N	n
40134	40116	0	_op0	217	0	\N	\N	n
40135	40116	0	_op2	217	0	\N	\N	n
40136	40116	0	_owner	40102	0	\N	\N	n
40137	40116	0	_dummy	0	0	\N	\N	n
40138	40116	0	_isTemplate	n	0	\N	\N	n
40141	40140	0	_op0	-1	0	\N	\N	n
40142	40140	0	_op2	-1	0	\N	\N	n
40143	40140	0	_op0	0	0	\N	\N	n
40144	40140	0	_op2	0	0	\N	\N	n
40145	40140	0	_op0	1	0	\N	\N	n
40146	40140	0	_op2	1	0	\N	\N	n
40147	40140	0	_op0	217	0	\N	\N	n
40148	40140	0	_op2	217	0	\N	\N	n
40149	40140	0	_owner	40102	0	\N	\N	n
40150	40140	0	_dummy	0	0	\N	\N	n
40151	40140	0	_isTemplate	n	0	\N	\N	n
40153	40152	0	_op0	-1	0	\N	\N	n
40154	40152	0	_op2	-1	0	\N	\N	n
40155	40152	0	_op0	0	0	\N	\N	n
40156	40152	0	_op2	0	0	\N	\N	n
40157	40152	0	_op0	1	0	\N	\N	n
40158	40152	0	_op2	1	0	\N	\N	n
40159	40152	0	_op0	217	0	\N	\N	n
40160	40152	0	_op2	217	0	\N	\N	n
40161	40152	0	_owner	40102	0	\N	\N	n
40162	40152	0	_dummy	0	0	\N	\N	n
40163	40152	0	_isTemplate	n	0	\N	\N	n
40165	40164	0	_op0	-1	0	\N	\N	n
40166	40164	0	_op2	-1	0	\N	\N	n
40167	40164	0	_op0	0	0	\N	\N	n
40168	40164	0	_op2	0	0	\N	\N	n
40169	40164	0	_op0	1	0	\N	\N	n
40170	40164	0	_op2	1	0	\N	\N	n
40171	40164	0	_op0	217	0	\N	\N	n
40172	40164	0	_op2	217	0	\N	\N	n
40173	40164	0	_owner	40102	0	\N	\N	n
40174	40164	0	_dummy	0	0	\N	\N	n
40175	40164	0	_id	9	0	\N	\N	n
40176	40164	0	_isTemplate	y	0	\N	\N	n
40177	40164	0	_isTemplate	n	0	\N	\N	n
40178	40164	0	_isTemplate	s	0	\N	\N	n
40180	40179	0	_op0	-1	0	\N	\N	n
40181	40179	0	_op2	-1	0	\N	\N	n
40182	40179	0	_op0	0	0	\N	\N	n
40183	40179	0	_op2	0	0	\N	\N	n
40184	40179	0	_op0	1	0	\N	\N	n
40185	40179	0	_op2	1	0	\N	\N	n
40186	40179	0	_op0	217	0	\N	\N	n
40187	40179	0	_op2	217	0	\N	\N	n
40188	40179	0	_owner	40102	0	\N	\N	n
40189	40179	0	_dummy	0	0	\N	\N	n
40190	40179	0	parentUuid	3c892ef0-0a6d-11de-ad6b-00104b7907b4	0	\N	\N	n
40191	40179	0	_isTemplate	n	0	\N	\N	n
40193	40192	0	_op0	-1	0	\N	\N	n
40194	40192	0	_op2	-1	0	\N	\N	n
40195	40192	0	_op0	0	0	\N	\N	n
40196	40192	0	_op2	0	0	\N	\N	n
40197	40192	0	_op0	1	0	\N	\N	n
40198	40192	0	_op2	1	0	\N	\N	n
40199	40192	0	_op0	217	0	\N	\N	n
40200	40192	0	_op2	217	0	\N	\N	n
40201	40192	0	_owner	40102	0	\N	\N	n
40202	40192	0	_dummy	0	0	\N	\N	n
40203	40192	0	agg_associated	3c892ef0-0a6d-11de-ad6b-00104b7907b4	0	\N	\N	n
40204	40192	0	_isTemplate	n	0	\N	\N	n
40206	40205	0	_op0	-1	0	\N	\N	n
40207	40205	0	_op2	-1	0	\N	\N	n
40208	40205	0	_op0	0	0	\N	\N	n
40209	40205	0	_op2	0	0	\N	\N	n
40210	40205	0	_op0	1	0	\N	\N	n
40211	40205	0	_op2	1	0	\N	\N	n
40212	40205	0	_op0	217	0	\N	\N	n
40213	40205	0	_op2	217	0	\N	\N	n
40214	40205	0	_owner	40102	0	\N	\N	n
40215	40205	0	_dummy	0	0	\N	\N	n
40216	40205	0	operatesOn	3c892ef0-0a6d-11de-ad6b-00104b7907b4	0	\N	\N	n
40217	40205	0	_isTemplate	n	0	\N	\N	n
40219	40218	0	_op0	-1	0	\N	\N	n
40220	40218	0	_op2	-1	0	\N	\N	n
40221	40218	0	_op0	0	0	\N	\N	n
40222	40218	0	_op2	0	0	\N	\N	n
40223	40218	0	_op0	1	0	\N	\N	n
40224	40218	0	_op2	1	0	\N	\N	n
40225	40218	0	_op0	217	0	\N	\N	n
40226	40218	0	_op2	217	0	\N	\N	n
40227	40218	0	_owner	40102	0	\N	\N	n
40228	40218	0	_dummy	0	0	\N	\N	n
40229	40218	0	hassource	3c892ef0-0a6d-11de-ad6b-00104b7907b4	0	\N	\N	n
40230	40218	0	_isTemplate	n	0	\N	\N	n
40232	40231	0	_op0	-1	0	\N	\N	n
40233	40231	0	_op2	-1	0	\N	\N	n
40234	40231	0	_op0	0	0	\N	\N	n
40235	40231	0	_op2	0	0	\N	\N	n
40236	40231	0	_op0	1	0	\N	\N	n
40237	40231	0	_op2	1	0	\N	\N	n
40238	40231	0	_op0	217	0	\N	\N	n
40239	40231	0	_op2	217	0	\N	\N	n
40240	40231	0	_owner	40102	0	\N	\N	n
40241	40231	0	_dummy	0	0	\N	\N	n
40242	40231	0	hasfeaturecat	3c892ef0-0a6d-11de-ad6b-00104b7907b4	0	\N	\N	n
40243	40231	0	_isTemplate	n	0	\N	\N	n
40245	40244	0	_op0	-1	0	\N	\N	n
40246	40244	0	_op2	-1	0	\N	\N	n
40247	40244	0	_op0	0	0	\N	\N	n
40248	40244	0	_op2	0	0	\N	\N	n
40249	40244	0	_op0	1	0	\N	\N	n
40250	40244	0	_op2	1	0	\N	\N	n
40251	40244	0	_op0	217	0	\N	\N	n
40252	40244	0	_op2	217	0	\N	\N	n
40253	40244	0	_owner	40102	0	\N	\N	n
40254	40244	0	_dummy	0	0	\N	\N	n
40255	40244	0	_isTemplate	n	0	\N	\N	n
40257	40256	0	_op0	-1	0	\N	\N	n
40258	40256	0	_op2	-1	0	\N	\N	n
40259	40256	0	_op0	0	0	\N	\N	n
40260	40256	0	_op2	0	0	\N	\N	n
40261	40256	0	_op0	1	0	\N	\N	n
40262	40256	0	_op2	1	0	\N	\N	n
40263	40256	0	_op0	217	0	\N	\N	n
40264	40256	0	_op2	217	0	\N	\N	n
40265	40256	0	_owner	40102	0	\N	\N	n
40266	40256	0	_dummy	0	0	\N	\N	n
40267	40256	0	_isTemplate	n	0	\N	\N	n
40269	40268	0	_op0	-1	0	\N	\N	n
40270	40268	0	_op2	-1	0	\N	\N	n
40271	40268	0	_op0	0	0	\N	\N	n
40272	40268	0	_op2	0	0	\N	\N	n
40273	40268	0	_op0	1	0	\N	\N	n
40274	40268	0	_op2	1	0	\N	\N	n
40275	40268	0	_op0	217	0	\N	\N	n
40276	40268	0	_op2	217	0	\N	\N	n
40277	40268	0	_owner	40102	0	\N	\N	n
40278	40268	0	_dummy	0	0	\N	\N	n
40279	40268	0	_isTemplate	y	0	\N	\N	n
40280	40268	0	_isTemplate	n	0	\N	\N	n
40281	40268	0	_isTemplate	s	0	\N	\N	n
40283	40282	0	_op0	-1	0	\N	\N	n
40284	40282	0	_op2	-1	0	\N	\N	n
40285	40282	0	_op0	0	0	\N	\N	n
40286	40282	0	_op2	0	0	\N	\N	n
40287	40282	0	_op0	1	0	\N	\N	n
40288	40282	0	_op2	1	0	\N	\N	n
40289	40282	0	_op0	217	0	\N	\N	n
40290	40282	0	_op2	217	0	\N	\N	n
40291	40282	0	_owner	40102	0	\N	\N	n
40292	40282	0	_dummy	0	0	\N	\N	n
40293	40282	0	_isTemplate	y	0	\N	\N	n
40294	40282	0	_isTemplate	n	0	\N	\N	n
40295	40282	0	_isTemplate	s	0	\N	\N	n
40297	40296	0	_op0	-1	0	\N	\N	n
40298	40296	0	_op2	-1	0	\N	\N	n
40299	40296	0	_op0	0	0	\N	\N	n
40300	40296	0	_op2	0	0	\N	\N	n
40301	40296	0	_op0	1	0	\N	\N	n
40302	40296	0	_op2	1	0	\N	\N	n
40303	40296	0	_op0	217	0	\N	\N	n
40304	40296	0	_op2	217	0	\N	\N	n
40305	40296	0	_owner	40102	0	\N	\N	n
40306	40296	0	_dummy	0	0	\N	\N	n
40307	40296	0	_isTemplate	n	0	\N	\N	n
40309	40308	0	_op0	-1	0	\N	\N	n
40310	40308	0	_op2	-1	0	\N	\N	n
40311	40308	0	_op0	0	0	\N	\N	n
40312	40308	0	_op2	0	0	\N	\N	n
40313	40308	0	_op0	1	0	\N	\N	n
40314	40308	0	_op2	1	0	\N	\N	n
40315	40308	0	_op0	217	0	\N	\N	n
40316	40308	0	_op2	217	0	\N	\N	n
40317	40308	0	_owner	40102	0	\N	\N	n
40318	40308	0	_dummy	0	0	\N	\N	n
40319	40308	0	_isTemplate	y	0	\N	\N	n
40321	40320	0	_op0	-1	0	\N	\N	n
40322	40320	0	_op2	-1	0	\N	\N	n
40323	40320	0	_op0	0	0	\N	\N	n
40324	40320	0	_op2	0	0	\N	\N	n
40325	40320	0	_op0	1	0	\N	\N	n
40326	40320	0	_op2	1	0	\N	\N	n
40327	40320	0	_op0	217	0	\N	\N	n
40328	40320	0	_op2	217	0	\N	\N	n
40329	40320	0	_owner	40102	0	\N	\N	n
40330	40320	0	_dummy	0	0	\N	\N	n
40331	40320	0	_isTemplate	y	0	\N	\N	n
40332	40320	0	_isTemplate	n	0	\N	\N	n
40333	40320	0	_isTemplate	s	0	\N	\N	n
40335	40334	0	_op0	-1	0	\N	\N	n
40336	40334	0	_op2	-1	0	\N	\N	n
40337	40334	0	_op0	0	0	\N	\N	n
40338	40334	0	_op2	0	0	\N	\N	n
40339	40334	0	_op0	1	0	\N	\N	n
40340	40334	0	_op2	1	0	\N	\N	n
40341	40334	0	_op0	217	0	\N	\N	n
40342	40334	0	_op2	217	0	\N	\N	n
40343	40334	0	_owner	40102	0	\N	\N	n
40344	40334	0	_dummy	0	0	\N	\N	n
40345	40334	0	_id	40086	0	\N	\N	n
40346	40334	0	_isTemplate	y	0	\N	\N	n
40347	40334	0	_isTemplate	n	0	\N	\N	n
40348	40334	0	_isTemplate	s	0	\N	\N	n
40350	40349	0	_op0	-1	0	\N	\N	n
40351	40349	0	_op2	-1	0	\N	\N	n
40352	40349	0	_op0	0	0	\N	\N	n
40363	40362	0	_op0	-1	0	\N	\N	n
40364	40362	0	_op2	-1	0	\N	\N	n
40365	40362	0	_op0	0	0	\N	\N	n
40366	40362	0	_op2	0	0	\N	\N	n
40367	40362	0	_op0	1	0	\N	\N	n
40368	40362	0	_op2	1	0	\N	\N	n
40369	40362	0	_op0	217	0	\N	\N	n
40370	40362	0	_op2	217	0	\N	\N	n
40371	40362	0	_owner	40102	0	\N	\N	n
40372	40362	0	_dummy	0	0	\N	\N	n
40373	40362	0	agg_associated	c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1	0	\N	\N	n
40374	40362	0	_isTemplate	n	0	\N	\N	n
40376	40375	0	_op0	-1	0	\N	\N	n
40377	40375	0	_op2	-1	0	\N	\N	n
40378	40375	0	_op0	0	0	\N	\N	n
40379	40375	0	_op2	0	0	\N	\N	n
40380	40375	0	_op0	1	0	\N	\N	n
40381	40375	0	_op2	1	0	\N	\N	n
40382	40375	0	_op0	217	0	\N	\N	n
40383	40375	0	_op2	217	0	\N	\N	n
40384	40375	0	_owner	40102	0	\N	\N	n
40385	40375	0	_dummy	0	0	\N	\N	n
40386	40375	0	operatesOn	c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1	0	\N	\N	n
40387	40375	0	_isTemplate	n	0	\N	\N	n
40389	40388	0	_op0	-1	0	\N	\N	n
40390	40388	0	_op2	-1	0	\N	\N	n
40391	40388	0	_op0	0	0	\N	\N	n
40392	40388	0	_op2	0	0	\N	\N	n
40393	40388	0	_op0	1	0	\N	\N	n
40394	40388	0	_op2	1	0	\N	\N	n
40395	40388	0	_op0	217	0	\N	\N	n
40396	40388	0	_op2	217	0	\N	\N	n
40397	40388	0	_owner	40102	0	\N	\N	n
40398	40388	0	_dummy	0	0	\N	\N	n
40399	40388	0	_uuid	-- Identifiant de la métadonnée de couche associée --	0	\N	\N	n
40400	40388	0	_isTemplate	n	0	\N	\N	n
40402	40401	0	_op0	-1	0	\N	\N	n
40403	40401	0	_op2	-1	0	\N	\N	n
40404	40401	0	_op0	0	0	\N	\N	n
40405	40401	0	_op2	0	0	\N	\N	n
40406	40401	0	_op0	1	0	\N	\N	n
40407	40401	0	_op2	1	0	\N	\N	n
40408	40401	0	_op0	217	0	\N	\N	n
40409	40401	0	_op2	217	0	\N	\N	n
40410	40401	0	_owner	40102	0	\N	\N	n
40411	40401	0	_dummy	0	0	\N	\N	n
40412	40401	0	hassource	c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1	0	\N	\N	n
40413	40401	0	_isTemplate	n	0	\N	\N	n
40415	40414	0	_op0	-1	0	\N	\N	n
40416	40414	0	_op2	-1	0	\N	\N	n
40417	40414	0	_op0	0	0	\N	\N	n
40418	40414	0	_op2	0	0	\N	\N	n
40419	40414	0	_op0	1	0	\N	\N	n
40420	40414	0	_op2	1	0	\N	\N	n
40421	40414	0	_op0	217	0	\N	\N	n
40422	40414	0	_op2	217	0	\N	\N	n
40423	40414	0	_owner	40102	0	\N	\N	n
40424	40414	0	_dummy	0	0	\N	\N	n
40425	40414	0	hasfeaturecat	c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1	0	\N	\N	n
40426	40414	0	_isTemplate	n	0	\N	\N	n
40353	40349	0	_op2	0	0	\N	\N	n
40354	40349	0	_op0	1	0	\N	\N	n
40355	40349	0	_op2	1	0	\N	\N	n
40356	40349	0	_op0	217	0	\N	\N	n
40357	40349	0	_op2	217	0	\N	\N	n
40358	40349	0	_owner	40102	0	\N	\N	n
40359	40349	0	_dummy	0	0	\N	\N	n
40360	40349	0	parentUuid	c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1	0	\N	\N	n
40361	40349	0	_isTemplate	n	0	\N	\N	n
40428	40427	0	_op0	-1	0	\N	\N	n
40429	40427	0	_op2	-1	0	\N	\N	n
40430	40427	0	_op0	0	0	\N	\N	n
40431	40427	0	_op2	0	0	\N	\N	n
40432	40427	0	_op0	1	0	\N	\N	n
40433	40427	0	_op2	1	0	\N	\N	n
40434	40427	0	_op0	217	0	\N	\N	n
40435	40427	0	_op2	217	0	\N	\N	n
40436	40427	0	_owner	40102	0	\N	\N	n
40437	40427	0	_dummy	0	0	\N	\N	n
40438	40427	0	_isTemplate	y	0	\N	\N	n
40439	40427	0	_isTemplate	n	0	\N	\N	n
40440	40427	0	_isTemplate	s	0	\N	\N	n
40442	40441	0	_op0	-1	0	\N	\N	n
40443	40441	0	_op2	-1	0	\N	\N	n
40444	40441	0	_op0	0	0	\N	\N	n
40445	40441	0	_op2	0	0	\N	\N	n
40446	40441	0	_op0	1	0	\N	\N	n
40447	40441	0	_op2	1	0	\N	\N	n
40448	40441	0	_op0	217	0	\N	\N	n
40449	40441	0	_op2	217	0	\N	\N	n
40450	40441	0	_owner	40102	0	\N	\N	n
40451	40441	0	_dummy	0	0	\N	\N	n
40452	40441	0	_id	40085	0	\N	\N	n
40453	40441	0	_isTemplate	y	0	\N	\N	n
40454	40441	0	_isTemplate	n	0	\N	\N	n
40455	40441	0	_isTemplate	s	0	\N	\N	n
40457	40456	0	_op0	-1	0	\N	\N	n
40458	40456	0	_op2	-1	0	\N	\N	n
40459	40456	0	_op0	0	0	\N	\N	n
40460	40456	0	_op2	0	0	\N	\N	n
40461	40456	0	_op0	1	0	\N	\N	n
40462	40456	0	_op2	1	0	\N	\N	n
40463	40456	0	_op0	217	0	\N	\N	n
40464	40456	0	_op2	217	0	\N	\N	n
40465	40456	0	_owner	40102	0	\N	\N	n
40466	40456	0	_dummy	0	0	\N	\N	n
40467	40456	0	_isTemplate	y	0	\N	\N	n
40468	40456	0	_isTemplate	n	0	\N	\N	n
40469	40456	0	_isTemplate	s	0	\N	\N	n
40471	40470	0	_op0	-1	0	\N	\N	n
40472	40470	0	_op2	-1	0	\N	\N	n
40473	40470	0	_op0	0	0	\N	\N	n
40474	40470	0	_op2	0	0	\N	\N	n
40475	40470	0	_op0	1	0	\N	\N	n
40476	40470	0	_op2	1	0	\N	\N	n
40477	40470	0	_op0	217	0	\N	\N	n
40478	40470	0	_op2	217	0	\N	\N	n
40479	40470	0	_owner	40102	0	\N	\N	n
40480	40470	0	_dummy	0	0	\N	\N	n
40481	40470	0	_isTemplate	n	0	\N	\N	n
40483	40482	0	_op0	-1	0	\N	\N	n
40484	40482	0	_op2	-1	0	\N	\N	n
40485	40482	0	_op0	0	0	\N	\N	n
40486	40482	0	_op2	0	0	\N	\N	n
40487	40482	0	_op0	1	0	\N	\N	n
40488	40482	0	_op2	1	0	\N	\N	n
40489	40482	0	_op0	217	0	\N	\N	n
40490	40482	0	_op2	217	0	\N	\N	n
40491	40482	0	_owner	40102	0	\N	\N	n
40492	40482	0	_dummy	0	0	\N	\N	n
40493	40482	0	_isTemplate	n	0	\N	\N	n
40496	40495	0	_op0	-1	0	\N	\N	n
40499	40495	0	_op2	-1	0	\N	\N	n
40502	40495	0	_op0	0	0	\N	\N	n
40497	40494	0	_op0	-1	0	\N	\N	n
40498	40494	0	_op2	-1	0	\N	\N	n
40500	40494	0	_op0	0	0	\N	\N	n
40501	40494	0	_op2	0	0	\N	\N	n
40503	40494	0	_op0	1	0	\N	\N	n
40506	40494	0	_op2	1	0	\N	\N	n
40508	40494	0	_op0	217	0	\N	\N	n
40510	40494	0	_op2	217	0	\N	\N	n
40512	40494	0	_owner	40102	0	\N	\N	n
40504	40495	0	_op2	0	0	\N	\N	n
40505	40495	0	_op0	1	0	\N	\N	n
40507	40495	0	_op2	1	0	\N	\N	n
40509	40495	0	_op0	217	0	\N	\N	n
40511	40495	0	_op2	217	0	\N	\N	n
40514	40495	0	_owner	40102	0	\N	\N	n
40515	40495	0	_dummy	0	0	\N	\N	n
40517	40495	0	_isTemplate	n	0	\N	\N	n
40513	40494	0	_dummy	0	0	\N	\N	n
40516	40494	0	_isTemplate	n	0	\N	\N	n
40519	40518	0	_op0	-1	0	\N	\N	n
40520	40518	0	_op2	-1	0	\N	\N	n
40521	40518	0	_op0	0	0	\N	\N	n
40522	40518	0	_op2	0	0	\N	\N	n
40523	40518	0	_op0	1	0	\N	\N	n
40524	40518	0	_op2	1	0	\N	\N	n
40525	40518	0	_op0	217	0	\N	\N	n
40526	40518	0	_op2	217	0	\N	\N	n
40527	40518	0	_owner	40102	0	\N	\N	n
40528	40518	0	_dummy	0	0	\N	\N	n
40529	40518	0	_isTemplate	y	0	\N	\N	n
40530	40518	0	_isTemplate	n	0	\N	\N	n
40531	40518	0	_isTemplate	s	0	\N	\N	n
40533	40532	0	_op0	-1	0	\N	\N	n
40534	40532	0	_op2	-1	0	\N	\N	n
40535	40532	0	_op0	0	0	\N	\N	n
40536	40532	0	_op2	0	0	\N	\N	n
40537	40532	0	_op0	1	0	\N	\N	n
40538	40532	0	_op2	1	0	\N	\N	n
40539	40532	0	_op0	217	0	\N	\N	n
40540	40532	0	_op2	217	0	\N	\N	n
40541	40532	0	_owner	40102	0	\N	\N	n
40542	40532	0	_dummy	0	0	\N	\N	n
40543	40532	0	_isTemplate	n	0	\N	\N	n
40545	40544	0	_op0	-1	0	\N	\N	n
40546	40544	0	_op2	-1	0	\N	\N	n
40547	40544	0	_op0	0	0	\N	\N	n
40548	40544	0	_op2	0	0	\N	\N	n
40549	40544	0	_op0	1	0	\N	\N	n
40550	40544	0	_op2	1	0	\N	\N	n
40551	40544	0	_op0	217	0	\N	\N	n
40552	40544	0	_op2	217	0	\N	\N	n
40553	40544	0	_owner	40102	0	\N	\N	n
40554	40544	0	_dummy	0	0	\N	\N	n
40555	40544	0	_isTemplate	y	0	\N	\N	n
40556	40544	0	_isTemplate	n	0	\N	\N	n
40557	40544	0	_uuid	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40559	40558	0	_op0	-1	0	\N	\N	n
40560	40558	0	_op2	-1	0	\N	\N	n
40561	40558	0	_op0	0	0	\N	\N	n
40562	40558	0	_op2	0	0	\N	\N	n
40563	40558	0	_op0	1	0	\N	\N	n
40564	40558	0	_op2	1	0	\N	\N	n
40565	40558	0	_op0	217	0	\N	\N	n
40566	40558	0	_op2	217	0	\N	\N	n
40567	40558	0	_owner	40102	0	\N	\N	n
40568	40558	0	_dummy	0	0	\N	\N	n
40569	40558	0	parentUuid	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40570	40558	0	_isTemplate	n	0	\N	\N	n
40572	40571	0	_op0	-1	0	\N	\N	n
40573	40571	0	_op2	-1	0	\N	\N	n
40574	40571	0	_op0	0	0	\N	\N	n
40575	40571	0	_op2	0	0	\N	\N	n
40576	40571	0	_op0	1	0	\N	\N	n
40577	40571	0	_op2	1	0	\N	\N	n
40578	40571	0	_op0	217	0	\N	\N	n
40604	40571	0	_op2	217	0	\N	\N	n
40605	40571	0	_owner	40102	0	\N	\N	n
40581	40580	0	_op0	-1	0	\N	\N	n
40582	40580	0	_op2	-1	0	\N	\N	n
40584	40580	0	_op0	0	0	\N	\N	n
40606	40571	0	_dummy	0	0	\N	\N	n
40607	40571	0	operatesOn	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40608	40571	0	_isTemplate	n	0	\N	\N	n
40583	40579	0	_op0	-1	0	\N	\N	n
40592	40579	0	_op2	-1	0	\N	\N	n
40594	40579	0	_op0	0	0	\N	\N	n
40585	40580	0	_op2	0	0	\N	\N	n
40586	40580	0	_op0	1	0	\N	\N	n
40587	40580	0	_op2	1	0	\N	\N	n
40596	40579	0	_op2	0	0	\N	\N	n
40597	40579	0	_op0	1	0	\N	\N	n
40598	40579	0	_op2	1	0	\N	\N	n
40588	40580	0	_op0	217	0	\N	\N	n
40589	40580	0	_op2	217	0	\N	\N	n
40590	40580	0	_owner	40102	0	\N	\N	n
40599	40579	0	_op0	217	0	\N	\N	n
40600	40579	0	_op2	217	0	\N	\N	n
40601	40579	0	_owner	40102	0	\N	\N	n
40591	40580	0	_dummy	0	0	\N	\N	n
40593	40580	0	agg_associated	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40595	40580	0	_isTemplate	n	0	\N	\N	n
40602	40579	0	_dummy	0	0	\N	\N	n
40603	40579	0	hasfeaturecat	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40609	40579	0	_isTemplate	n	0	\N	\N	n
40611	40610	0	_op0	-1	0	\N	\N	n
40612	40610	0	_op2	-1	0	\N	\N	n
40613	40610	0	_op0	0	0	\N	\N	n
40614	40610	0	_op2	0	0	\N	\N	n
40615	40610	0	_op0	1	0	\N	\N	n
40616	40610	0	_op2	1	0	\N	\N	n
40617	40610	0	_op0	217	0	\N	\N	n
40618	40610	0	_op2	217	0	\N	\N	n
40619	40610	0	_owner	40102	0	\N	\N	n
40620	40610	0	_dummy	0	0	\N	\N	n
40621	40610	0	hasfeaturecat	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40622	40610	0	_isTemplate	n	0	\N	\N	n
40624	40623	0	_op0	-1	0	\N	\N	n
40625	40623	0	_op2	-1	0	\N	\N	n
40626	40623	0	_op0	0	0	\N	\N	n
40641	40640	0	_op0	-1	0	\N	\N	n
40642	40640	0	_op2	-1	0	\N	\N	n
40643	40640	0	_op0	0	0	\N	\N	n
40644	40640	0	_op2	0	0	\N	\N	n
40645	40640	0	_op0	1	0	\N	\N	n
40646	40640	0	_op2	1	0	\N	\N	n
40647	40640	0	_op0	217	0	\N	\N	n
40648	40640	0	_op2	217	0	\N	\N	n
40649	40640	0	_owner	40102	0	\N	\N	n
40650	40640	0	_dummy	0	0	\N	\N	n
40651	40640	0	operatesOn	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40652	40640	0	_isTemplate	n	0	\N	\N	n
40627	40623	0	_op2	0	0	\N	\N	n
40628	40623	0	_op0	1	0	\N	\N	n
40629	40623	0	_op2	1	0	\N	\N	n
40630	40623	0	_op0	217	0	\N	\N	n
40631	40623	0	_op2	217	0	\N	\N	n
40632	40623	0	_owner	40102	0	\N	\N	n
40633	40623	0	_dummy	0	0	\N	\N	n
40634	40623	0	parentUuid	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40635	40623	0	_isTemplate	n	0	\N	\N	n
40675	40674	0	_op0	-1	0	\N	\N	n
40676	40674	0	_op2	-1	0	\N	\N	n
40677	40674	0	_op0	0	0	\N	\N	n
40639	40636	0	_op0	-1	0	\N	\N	n
40654	40636	0	_op2	-1	0	\N	\N	n
40655	40636	0	_op0	0	0	\N	\N	n
40656	40636	0	_op2	0	0	\N	\N	n
40658	40636	0	_op0	1	0	\N	\N	n
40660	40636	0	_op2	1	0	\N	\N	n
40662	40636	0	_op0	217	0	\N	\N	n
40669	40636	0	_op2	217	0	\N	\N	n
40670	40636	0	_owner	40102	0	\N	\N	n
40678	40674	0	_op2	0	0	\N	\N	n
40679	40674	0	_op0	1	0	\N	\N	n
40680	40674	0	_op2	1	0	\N	\N	n
40671	40636	0	_dummy	0	0	\N	\N	n
40672	40636	0	hasfeaturecat	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40673	40636	0	_isTemplate	n	0	\N	\N	n
40681	40674	0	_op0	217	0	\N	\N	n
40682	40674	0	_op2	217	0	\N	\N	n
40683	40674	0	_owner	40102	0	\N	\N	n
40684	40674	0	_dummy	0	0	\N	\N	n
40685	40674	0	hasfeaturecat	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40686	40674	0	_isTemplate	n	0	\N	\N	n
40691	40688	0	_op0	-1	0	\N	\N	n
40692	40688	0	_op2	-1	0	\N	\N	n
40694	40688	0	_op0	0	0	\N	\N	n
40696	40688	0	_op2	0	0	\N	\N	n
40698	40688	0	_op0	1	0	\N	\N	n
40700	40688	0	_op2	1	0	\N	\N	n
40702	40688	0	_op0	217	0	\N	\N	n
40704	40688	0	_op2	217	0	\N	\N	n
40706	40688	0	_owner	40102	0	\N	\N	n
40708	40688	0	_dummy	0	0	\N	\N	n
40710	40688	0	_isTemplate	n	0	\N	\N	n
40690	40689	0	_op0	-1	0	\N	\N	n
40693	40689	0	_op2	-1	0	\N	\N	n
40695	40689	0	_op0	0	0	\N	\N	n
40697	40689	0	_op2	0	0	\N	\N	n
40699	40689	0	_op0	1	0	\N	\N	n
40701	40689	0	_op2	1	0	\N	\N	n
40703	40689	0	_op0	217	0	\N	\N	n
40705	40689	0	_op2	217	0	\N	\N	n
40707	40689	0	_owner	40102	0	\N	\N	n
40709	40689	0	_dummy	0	0	\N	\N	n
40711	40689	0	_isTemplate	n	0	\N	\N	n
40638	40637	0	_op0	-1	0	\N	\N	n
40653	40637	0	_op2	-1	0	\N	\N	n
40657	40637	0	_op0	0	0	\N	\N	n
40659	40637	0	_op2	0	0	\N	\N	n
40661	40637	0	_op0	1	0	\N	\N	n
40663	40637	0	_op2	1	0	\N	\N	n
40664	40637	0	_op0	217	0	\N	\N	n
40665	40637	0	_op2	217	0	\N	\N	n
40666	40637	0	_owner	40102	0	\N	\N	n
40667	40637	0	_dummy	0	0	\N	\N	n
40668	40637	0	agg_associated	9ba5f3e9-c0ef-4160-81c0-b633b6067601	0	\N	\N	n
40687	40637	0	_isTemplate	n	0	\N	\N	n
40713	40712	0	_op0	-1	0	\N	\N	n
40714	40712	0	_op2	-1	0	\N	\N	n
40715	40712	0	_op0	0	0	\N	\N	n
40716	40712	0	_op2	0	0	\N	\N	n
40717	40712	0	_op0	1	0	\N	\N	n
40718	40712	0	_op2	1	0	\N	\N	n
40719	40712	0	_op0	217	0	\N	\N	n
40720	40712	0	_op2	217	0	\N	\N	n
40721	40712	0	_owner	40102	0	\N	\N	n
40722	40712	0	_dummy	0	0	\N	\N	n
40723	40712	0	_isTemplate	n	0	\N	\N	n
40725	40724	0	_op0	-1	0	\N	\N	n
40726	40724	0	_op2	-1	0	\N	\N	n
40727	40724	0	_op0	0	0	\N	\N	n
40728	40724	0	_op2	0	0	\N	\N	n
40729	40724	0	_op0	1	0	\N	\N	n
40730	40724	0	_op2	1	0	\N	\N	n
40731	40724	0	_op0	217	0	\N	\N	n
40732	40724	0	_op2	217	0	\N	\N	n
40733	40724	0	_owner	40102	0	\N	\N	n
40734	40724	0	_dummy	0	0	\N	\N	n
40735	40724	0	_isTemplate	y	0	\N	\N	n
40736	40724	0	_isTemplate	n	0	\N	\N	n
40737	40724	0	_isTemplate	s	0	\N	\N	n
40739	40738	0	_op0	-1	0	\N	\N	n
40740	40738	0	_op2	-1	0	\N	\N	n
40741	40738	0	_op0	0	0	\N	\N	n
40742	40738	0	_op2	0	0	\N	\N	n
40743	40738	0	_op0	1	0	\N	\N	n
40744	40738	0	_op2	1	0	\N	\N	n
40745	40738	0	_op0	217	0	\N	\N	n
40746	40738	0	_op2	217	0	\N	\N	n
40747	40738	0	_owner	40102	0	\N	\N	n
40748	40738	0	_dummy	0	0	\N	\N	n
40749	40738	0	_isTemplate	n	0	\N	\N	n
40756	40751	0	_op0	1	0	\N	\N	n
40753	40752	0	_op0	1	0	\N	\N	n
40759	40750	0	_op0	1	0	\N	\N	n
40757	40751	0	_op2	1	0	\N	\N	n
40760	40750	0	_op2	1	0	\N	\N	n
40754	40752	0	_op2	1	0	\N	\N	n
40758	40751	0	_isTemplate	n	0	\N	\N	n
40755	40752	0	_isTemplate	n	0	\N	\N	n
40761	40750	0	_isTemplate	n	0	\N	\N	n
40769	40762	0	_op0	1	0	\N	\N	n
40775	40774	0	_op0	1	0	\N	\N	n
40765	40764	0	_op0	1	0	\N	\N	n
40766	40763	0	_op0	1	0	\N	\N	n
40770	40762	0	_op2	1	0	\N	\N	n
40773	40762	0	_isTemplate	n	0	\N	\N	n
40776	40774	0	_op2	1	0	\N	\N	n
40777	40774	0	_isTemplate	n	0	\N	\N	n
40771	40764	0	_op2	1	0	\N	\N	n
40767	40763	0	_op2	1	0	\N	\N	n
40768	40763	0	_isTemplate	n	0	\N	\N	n
40772	40764	0	_isTemplate	n	0	\N	\N	n
\.


--
-- Data for Name: regions; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.regions (id, north, south, west, east) FROM stdin;
2	38.4719800000000021	29.4061100000000017	60.504170000000002	74.9157399999999996
338	38.2000000000000028	-34.6000000000000014	-17.3000000000000007	51.1000000000000014
3	42.6603399999999979	39.6450000000000031	19.2885400000000011	21.053329999999999
4	37.0898600000000016	18.9763899999999985	-8.66722000000000037	11.9864800000000002
1220	65	-65	-180	180
5	-14.2543100000000003	-14.3755600000000001	-170.823229999999995	-170.561869999999999
6	42.6559700000000035	42.4363900000000029	1.42138999999999993	1.78171999999999997
7	-4.38898999999999972	-18.0163900000000012	11.7312499999999993	24.0844400000000007
258	18.2729800000000004	18.164439999999999	-63.1677800000000005	-62.9727099999999993
30	-60.5033299999999983	-90	-180	180
8	17.7242999999999995	16.9897199999999984	-61.8911099999999976	-61.6669499999999999
9	-21.7805199999999992	-55.0516700000000014	-73.5823000000000036	-53.6500100000000018
1	41.2970499999999987	38.841149999999999	43.4541600000000017	46.6205399999999983
1008	90	48	-180	180
22	12.6277799999999996	12.4111100000000008	-70.0596599999999938	-69.8748599999999982
337	83.5	10.8000000000000007	31	179.900000000000006
1016	-50	-78.1136000000000053	-68.5052000000000021	-68.3546000000000049
1012	36	-6.13210000000000033	-16.434429999999999	-16
1010	90	36	55	-27.3408100000000012
1009	78.1700000000000017	34.9500000000000028	58.2012900000000002	-84.3583299999999952
1015	-6	-50	-20	30
1014	5	-60	-69.6307999999999936	-20
1011	36	5	-97.8097000000000065	-40
10	-10.1356999999999999	-54.7538899999999984	112.907210000000006	158.960370000000012
11	49.0187499999999972	46.4074900000000028	9.53356999999999921	17.1663899999999998
52	41.8970600000000033	38.3891500000000008	44.7788600000000017	50.3749899999999968
12	26.9291699999999992	20.9152799999999992	-78.9788999999999959	-72.7388899999999978
13	26.2888899999999985	25.5719400000000014	50.4533300000000011	50.7963900000000024
16	26.6261399999999995	20.7448200000000007	88.0438699999999983	92.6693400000000054
14	13.3370800000000003	13.0505499999999994	-59.6594499999999996	-59.4270899999999997
57	56.1674900000000008	51.2518499999999975	23.1654000000000018	32.7400599999999997
255	51.5012499999999989	49.5088799999999978	2.54166999999999987	6.39820000000000011
23	18.4898999999999987	15.8898499999999991	-89.216399999999993	-87.7795899999999989
53	12.3966600000000007	6.21872000000000025	0.776669999999999972	3.85499999999999998
17	32.3795100000000033	32.2605500000000021	-64.8230599999999981	-64.6768100000000032
18	28.3249999999999993	26.7036100000000012	88.7519400000000047	92.1142200000000031
19	-9.6791999999999998	-22.9011099999999992	-69.6561899999999952	-57.5211200000000034
80	45.2659499999999966	42.5658299999999983	15.7405899999999992	19.6197899999999983
20	-17.7820900000000002	-26.8755600000000001	19.9961100000000016	29.373619999999999
31	-54.3836099999999973	-54.4627800000000022	3.34236000000000022	3.48417000000000021
21	5.27271000000000001	-33.7411200000000022	-74.0045899999999932	-34.7929200000000023
24	-7.23346999999999962	-7.43625000000000025	72.3579000000000008	72.4942900000000066
239	18.5048600000000008	18.383890000000001	-64.6984800000000035	-64.3245200000000068
26	5.05304999999999982	4.01818999999999971	114.095079999999996	115.360259999999997
27	44.2247199999999978	41.2430499999999967	22.3652799999999985	28.6051399999999987
233	15.0827799999999996	9.3956900000000001	-5.52083000000000013	2.39792000000000005
29	-2.30155999999999983	-4.4480599999999999	28.9849999999999994	30.8531900000000014
115	14.7086199999999998	10.4227399999999992	102.346500000000006	107.636380000000003
32	13.0850000000000009	1.65416999999999992	8.50235999999999947	16.2070100000000004
33	83.1138799999999947	41.6755500000000012	-141.002990000000011	-52.6173599999999979
35	17.1923600000000008	14.8111099999999993	-25.3605599999999995	-22.6661099999999998
317	29.3500000000000014	8.10999999999999943	-88.8100000000000023	-56.8299999999999983
36	19.3541600000000003	19.2650000000000006	-81.4008400000000023	-81.0930599999999941
312	25.1600000000000001	-19.3599999999999994	-9.11999999999999922	46.7100000000000009
37	11.0008300000000006	2.22126000000000001	14.4188899999999993	27.4597200000000008
316	36.9099999999999966	3.20999999999999996	-120.170000000000002	-74.8299999999999983
39	23.4505499999999998	7.45854000000000017	13.4619400000000002	24.0027499999999989
40	-17.5052799999999991	-55.902230000000003	-109.446110000000004	-66.4206300000000027
41	53.5537399999999977	18.1688800000000015	73.6200500000000062	134.768460000000005
214	25.2836099999999995	21.9277699999999989	118.278599999999997	122.000399999999999
42	-10.3840800000000009	-10.5109700000000004	105.629000000000005	105.751900000000006
43	-12.1304200000000009	-12.1994399999999992	96.8174900000000065	96.8648500000000041
44	12.5902799999999999	-4.23686999999999969	-81.7201500000000038	-66.8704500000000053
45	-11.3669499999999992	-12.3830600000000004	43.2140199999999979	44.5304199999999994
250	2.10000000000000009	-9.22000000000000064	10.0299999999999994	34.7700000000000031
46	3.71111000000000013	-5.01499999999999968	11.1406600000000005	18.6436099999999989
47	-10.8813200000000005	-21.9408299999999983	-165.848340000000007	-157.703769999999992
48	11.2128499999999995	8.02566999999999986	-85.9113899999999973	-82.5614000000000061
98	46.5358299999999971	42.3999900000000025	13.5047899999999998	19.4250000000000007
49	23.1940300000000015	19.8219400000000014	-84.9529299999999949	-74.1312599999999975
50	35.6886099999999971	34.640270000000001	32.2698600000000013	34.586039999999997
167	51.0524899999999988	48.5813800000000029	12.0937000000000001	18.8522199999999991
54	57.7459699999999998	54.5619399999999999	8.09291999999999945	15.1491699999999998
72	12.7083300000000001	10.9422200000000007	41.7598600000000033	43.4204099999999968
55	15.6319400000000002	15.1980599999999999	-61.4913900000000027	-61.2507000000000019
56	19.9308300000000003	17.6041699999999999	-72.0030699999999939	-68.3229299999999995
324	55.0200000000000031	-12.7300000000000004	40.25	158.710000000000008
176	-8.01999999999999957	-9.38000000000000078	124.840000000000003	127.349999999999994
313	19.2199999999999989	-28.1099999999999994	13.5500000000000007	70.5
334	55.4600000000000009	39.5499999999999972	9.78999999999999915	31.9100000000000001
58	1.43778000000000006	-5.00030999999999981	-91.6638999999999982	-75.2168400000000048
59	31.6469400000000007	21.9941600000000008	24.7068000000000012	36.8958299999999966
60	14.4319799999999994	13.15639	-90.1080599999999947	-87.6946700000000021
61	3.76332999999999984	0.930159999999999987	8.42417000000000016	11.3538899999999998
178	17.9948799999999984	12.3638899999999996	36.4432800000000015	43.121380000000002
63	59.6647200000000026	57.5226299999999995	21.8373600000000003	28.1940899999999992
238	14.8836099999999991	3.40667000000000009	32.9917999999999978	47.9882399999999976
335	81.4000000000000057	35.2999999999999972	-11.5	43.2000000000000028
427	82.6400000000000006	29.3999999999999986	-29.629999999999999	45.0499999999999972
64	62.3575000000000017	61.3883300000000034	-7.4334699999999998	-6.38971999999999962
65	-51.2494500000000031	-52.3430600000000013	-61.148060000000001	-57.7331999999999965
322	56.1499999999999986	-12.4700000000000006	52.9500000000000028	148.509999999999991
66	-16.1534699999999987	-19.1627800000000015	175	180
67	70.0886100000000027	59.8068000000000026	19.5113899999999987	31.5819599999999987
69	5.75541999999999998	2.11346999999999996	-54.6037800000000004	-51.648060000000001
70	-8.7782	-17.8708300000000015	-151.497770000000003	-138.809750000000008
71	-46.3276400000000024	-49.7250099999999975	51.6508299999999991	70.5674900000000065
68	51.0911100000000005	41.3649300000000011	-5.79028000000000009	9.56221999999999994
74	2.31789999999999985	-3.92527999999999988	8.70082999999999984	14.5195799999999995
75	13.82639	13.0599799999999995	-16.821670000000001	-13.79861
76	31.5960999999999999	31.2165399999999984	34.2166599999999974	34.5588899999999981
73	43.5847199999999972	41.0480400000000003	40.0029699999999977	46.7108199999999982
79	55.0565300000000022	47.2747200000000021	5.86500000000000021	15.0338200000000004
81	11.1556899999999999	4.72707999999999995	-3.24888999999999983	1.20277999999999996
82	36.1633100000000027	36.1120700000000028	-5.35616999999999965	-5.33450999999999986
84	41.7477700000000027	34.9305499999999967	19.6400000000000006	28.2380500000000012
85	83.6235999999999962	59.7902800000000028	-73.053600000000003	-12.1576400000000007
86	12.2371499999999997	11.9969400000000004	-61.7851799999999969	-61.5963899999999995
87	16.5129200000000012	15.8699999999999992	-61.7961099999999988	-61.1870800000000017
88	13.6522900000000007	13.2349999999999994	144.634160000000008	144.953309999999988
89	17.8211100000000009	13.7458299999999998	-92.2467800000000011	-88.2147400000000061
90	12.6775000000000002	7.19392999999999994	-15.0808300000000006	-7.65336999999999978
175	12.6847200000000004	10.9251000000000005	-16.7177700000000016	-13.6438900000000007
91	8.5352800000000002	1.18687999999999994	-61.3897300000000001	-56.4706299999999999
93	20.0914600000000014	18.0227800000000009	-74.4677899999999937	-71.6291800000000052
92	-52.9651500000000013	-53.1994499999999988	73.2347100000000069	73.7738800000000055
95	16.4358299999999993	12.9851700000000001	-89.3504899999999935	-83.13185
97	48.5761800000000008	45.7483300000000028	16.1118099999999984	22.8948
99	66.5361000000000047	63.3900000000000006	-24.5383999999999993	-13.4994499999999995
100	35.5056200000000004	6.74582999999999977	68.1442299999999932	97.3805399999999963
1019	-45	-70.0180699999999945	148.004199999999997	148.389800000000008
1018	22.757200000000001	-55	77	150
1017	30.5061	-45	30	80
101	5.91347000000000023	-10.9296500000000005	95.2109499999999969	141.007020000000011
307	23.3399999999999999	-5.78000000000000025	18.4499999999999993	54.9099999999999966
102	39.7791599999999974	25.0759700000000016	44.034950000000002	63.3302699999999987
103	37.3836799999999982	29.0616599999999998	38.7946999999999989	48.560690000000001
104	55.3799899999999994	51.4455499999999972	-10.4747199999999996	-6.01306000000000029
264	54.4163899999999998	54.0555499999999967	-4.78714999999999957	-4.30867999999999984
105	33.2702699999999965	29.4867099999999986	34.2675800000000024	35.6811099999999968
106	47.0945800000000006	36.649160000000002	6.62396999999999991	18.5144400000000005
107	10.7352600000000002	4.34471999999999969	-8.6063799999999997	-2.48777999999999988
109	18.5225000000000009	17.6972200000000015	-78.3739000000000061	-76.2211199999999991
110	45.4863799999999969	24.2513900000000007	123.67886	145.81241
112	33.3775899999999979	29.1888900000000007	34.9604199999999992	39.3011100000000013
108	55.4426300000000012	40.5944399999999987	46.4991600000000034	87.3482099999999946
114	4.62249999999999961	-4.6696200000000001	33.9072200000000024	41.9051699999999983
83	2.0330499999999998	1.70500000000000007	-157.581700000000012	-157.172550000000001
116	43.0061000000000035	37.6713799999999992	124.323949999999996	130.697419999999994
117	38.625239999999998	33.1922100000000029	126.099010000000007	129.586870000000005
118	30.0841600000000007	28.5388799999999989	46.5469399999999993	48.4165899999999993
113	43.2169000000000025	39.1954700000000003	69.2494999999999976	80.2815899999999942
120	22.4999299999999991	13.92666	100.091369999999998	107.695250000000001
315	33.1700000000000017	-25.4400000000000013	-120.170000000000002	-58.7800000000000011
348	32.7000000000000028	-55.3999999999999986	-117	-33.7999999999999972
119	58.0832600000000028	55.6748400000000032	20.9686100000000017	28.2359699999999982
121	34.6475000000000009	33.0620800000000017	35.100830000000002	36.623739999999998
122	-28.5706999999999987	-30.6505299999999998	27.0139700000000005	29.4555499999999988
123	8.51277999999999935	4.34360999999999997	-11.4923300000000008	-7.36840000000000028
124	33.1711400000000012	19.4990699999999997	9.31138999999999939	25.1516699999999993
125	47.2745400000000018	47.057459999999999	9.47464000000000084	9.63388999999999918
126	56.4498499999999979	53.8903400000000019	20.9428300000000007	26.8130500000000005
256	50.1818099999999987	49.4484700000000004	5.7344400000000002	6.52402999999999977
154	42.3589500000000001	40.8558900000000023	20.4588199999999993	23.0309699999999999
129	-11.9455600000000004	-25.5883399999999988	43.2368200000000016	50.5013900000000007
130	-9.37667000000000073	-17.1352800000000016	32.6818700000000035	35.920969999999997
131	7.35292000000000012	0.852779999999999982	99.6419400000000053	119.275819999999996
132	7.02777999999999992	-0.641669999999999963	72.8633899999999954	73.6372800000000041
133	25.0002800000000001	10.1421500000000009	-12.2448300000000003	4.25138999999999978
134	35.9919399999999996	35.7999999999999972	14.3291000000000004	14.5700000000000003
127	14.5940300000000001	5.6002799999999997	162.324970000000008	169.971620000000001
135	14.8801400000000008	14.4027799999999999	-61.2315299999999993	-60.8169499999999985
136	27.2904599999999995	14.7256400000000003	-17.0755599999999994	-4.80611000000000033
137	-19.6733399999999996	-20.5205599999999997	57.3063100000000034	63.4957599999999971
270	-12.6624999999999996	-12.9924999999999997	45.0391600000000025	45.2297200000000004
1013	47.2719000000000023	30.2735999999999983	-5.61000000000000032	41.7586000000000013
138	32.7184600000000003	14.5505499999999994	-118.404169999999993	-86.7386199999999974
145	6.97764000000000006	5.26166999999999963	158.120100000000008	163.04289
139	28.2215200000000017	28.1841599999999985	-177.395839999999993	-177.360549999999989
146	48.4683199999999985	45.4486500000000007	26.6350000000000016	30.1287100000000017
140	43.7683000000000035	43.7275500000000008	7.39090000000000025	7.43928999999999974
141	52.1427699999999987	41.586660000000002	87.761099999999999	119.931510000000003
142	16.8123600000000017	16.6713899999999988	-62.2369500000000002	-62.1388900000000035
143	35.9191700000000012	27.6642399999999995	-13.1749600000000004	-1.0118100000000001
144	-10.4711099999999995	-26.8602799999999995	30.2130200000000002	40.846110000000003
28	28.546520000000001	9.83957999999999977	92.2049899999999951	101.169430000000006
163	15.2681900000000006	14.9080499999999994	145.572679999999991	145.818090000000012
147	-16.9541700000000013	-28.9618800000000007	11.7163900000000005	25.2644300000000008
148	-0.493329999999999991	-0.552220000000000044	166.904419999999988	166.95705000000001
319	51.1599999999999966	-5.09999999999999964	6.71999999999999975	77.5
325	62.2000000000000028	-6.46999999999999975	-17.129999999999999	78.6899999999999977
320	34.259999999999998	2.27000000000000002	4.12999999999999989	44.1300000000000026
321	48.4200000000000017	5.66999999999999993	23.5500000000000007	77.0300000000000011
149	30.4247200000000007	26.3683599999999991	80.0521999999999991	88.1945599999999956
151	12.3838899999999992	12.0205599999999997	-69.1636199999999945	-68.1929200000000009
150	53.4658299999999969	50.7538800000000023	3.37087000000000003	7.21096999999999966
156	-32.4147200000000026	-52.5780600000000007	170	180
153	-20.0879200000000004	-22.6738900000000001	163.982740000000007	168.130509999999987
157	15.0222200000000008	10.7096900000000002	-87.6898300000000006	-83.13185
158	23.5223100000000009	11.6932700000000001	0.166670000000000013	15.9966699999999999
159	13.8915000000000006	4.27285000000000004	2.69249999999999989	14.6496499999999994
160	-18.9633299999999991	-19.1455599999999997	-169.952239999999989	-169.781560000000013
161	-29.0005600000000001	-29.0811099999999989	167.910950000000014	167.998870000000011
421	46.2000000000000028	10.4000000000000004	-15.3000000000000007	39.1000000000000014
428	85	7.20000000000000018	-166.300000000000011	-14.4000000000000004
310	39.2800000000000011	17.1999999999999993	-18.4499999999999993	13.3000000000000007
162	71.1547099999999944	57.9879200000000026	4.78957999999999995	31.0735400000000013
221	26.3687100000000001	16.6427799999999984	51.999290000000002	59.8470799999999983
1026	-60	-78.5666999999999973	150	-105.224199999999996
1023	40.5	-25	-175	-77.8889599999999973
1021	66.1336000000000013	40	-175	-122.226399999999998
1020	66.5	15	105.623599999999996	-175
1025	7.21119000000000021	-60	-120	-65.7199999999999989
1024	-25	-60	149.891359999999992	-120
1022	20	-28.1499999999999986	99.158600000000007	-175
165	37.0607899999999972	23.6880500000000005	60.8663000000000025	77.8239300000000043
166	9.62013999999999925	7.20610999999999979	-83.0302899999999937	-77.1983299999999986
168	-1.35528000000000004	-11.6425000000000001	140.858859999999993	155.966839999999991
169	-19.2968100000000007	-27.5847200000000008	-62.6437700000000035	-54.2438999999999965
170	-0.03687	-18.3485499999999995	-81.3551499999999947	-68.6739000000000033
171	19.3911100000000012	5.04917000000000016	116.950000000000003	126.598039999999997
172	-24.3258399999999995	-25.0822299999999991	-130.105060000000009	-128.286120000000011
173	54.836039999999997	49.00291	14.1476400000000009	24.1434700000000007
174	44	35	-10	-6.19045000000000023
177	18.5194399999999995	17.9222199999999994	-67.2664000000000044	-65.3011199999999974
179	26.1524999999999999	24.5560399999999994	50.7519399999999976	51.6158300000000025
182	-20.8565299999999993	-21.3738899999999994	55.2205500000000029	55.8530500000000032
183	48.2638900000000035	43.6233099999999965	20.2610199999999985	29.6722199999999994
185	81.8519299999999959	41.1965799999999973	-36	180
184	-1.05445000000000011	-2.82548999999999984	28.8544400000000003	30.8932600000000015
244	-13.4605599999999992	-14.0574999999999992	-172.780030000000011	-171.429200000000009
192	43.9868700000000032	43.8986799999999988	12.4069400000000005	12.5111100000000004
193	1.70124999999999993	0.0183299999999999991	6.46513999999999989	7.46347000000000005
194	32.1549400000000034	15.6169399999999996	34.5721500000000006	55.6661100000000033
195	16.6906199999999991	12.3017500000000002	-17.5327799999999989	-11.3699300000000001
196	-4.55166999999999966	-9.46306000000000047	46.205689999999997	55.5405500000000032
197	9.9975000000000005	6.92361000000000004	-13.2956099999999999	-10.26431
200	1.4452799999999999	1.25903000000000009	103.640950000000004	103.997950000000003
199	49.600830000000002	47.7374999999999972	16.8447199999999988	22.5580500000000015
198	46.8762499999999989	45.4258200000000016	13.3834700000000009	16.6078699999999984
25	-6.60552000000000028	-11.8458299999999994	155.671300000000002	166.931839999999994
201	11.9791699999999999	-1.67487000000000008	40.9886100000000013	51.4113200000000035
202	-22.1363899999999987	-46.9697299999999984	14.4093099999999996	37.8922200000000018
318	13	-55.3999999999999986	-82.0999999999999943	-33.7999999999999972
323	38.7800000000000011	-2.20000000000000018	57.4399999999999977	100.670000000000002
390	30.3099999999999987	-33.1899999999999977	-27.8900000000000006	65.269999999999996
314	-14.9000000000000004	-32.5600000000000023	10.8900000000000006	33.009999999999998
271	-53.9897199999999984	-58.4986099999999993	-38.0237499999999997	-26.2413899999999991
203	43.7642999999999986	27.6374999999999993	-18.1698699999999995	4.31693999999999978
38	9.82818999999999932	5.91805999999999965	79.6960899999999981	81.8916600000000017
187	-15.9037500000000005	-16.0219500000000004	-5.7927799999999996	-5.64527999999999963
188	17.4101399999999984	17.2088900000000002	-62.8627800000000008	-62.6225099999999983
189	14.1092999999999993	13.7094400000000007	-61.0795900000000032	-60.8780599999999978
190	47.1358299999999986	46.7798500000000033	-56.3977799999999974	-56.2326400000000035
191	13.3831900000000008	13.1302800000000008	-61.2801400000000029	-61.1202899999999971
206	22.2322200000000016	3.49339000000000022	21.8291000000000004	38.6075000000000017
207	6.00180999999999987	1.83624999999999994	-58.071399999999997	-53.9861199999999997
260	80.7641600000000039	74.3430500000000052	10.4879099999999994	33.6375000000000028
209	-25.7283399999999993	-27.3163899999999984	30.79833	32.1334000000000017
210	69.060299999999998	55.3391700000000029	11.1133299999999995	24.1670100000000012
211	47.8066600000000008	45.8294399999999982	5.96701000000000015	10.4882100000000005
212	37.29054	32.3136099999999971	35.6144600000000011	42.3783299999999983
208	41.0492599999999968	36.6718400000000031	67.3646999999999991	75.1874899999999968
215	-0.997219999999999995	-11.7404200000000003	29.3408300000000004	40.4368100000000013
216	20.45458	5.63346999999999998	97.3472799999999978	105.639290000000003
217	11.1385400000000008	6.10055000000000014	-0.149760000000000004	1.79780000000000006
218	-9.17062999999999917	-9.21889000000000003	-171.862719999999996	-171.843770000000006
219	-18.5680599999999991	-21.2680599999999984	-175.360000000000014	-173.906830000000014
220	11.3455499999999994	10.0403500000000001	-61.921599999999998	-60.5208399999999997
222	37.3404099999999985	30.2343900000000012	7.49221999999999966	11.5816700000000008
223	42.1099900000000034	35.8184400000000025	25.6658299999999997	44.8205499999999972
213	42.7961699999999965	35.1459899999999976	52.4400699999999986	66.6708799999999968
224	21.9577799999999996	21.7397199999999984	-72.0314599999999956	-71.6336199999999934
227	-6.08943999999999974	-8.56128999999999962	176.295260000000013	179.232290000000006
229	60.8433300000000017	49.9552800000000019	-8.17167000000000066	1.74944000000000011
231	71.3514399999999966	18.9254800000000003	-178.216550000000012	-68
232	28.2215200000000017	-0.398060000000000025	166.608980000000003	-177.395839999999993
240	17.7925000000000004	17.6766700000000014	-64.8961199999999963	-64.562579999999997
226	4.2227800000000002	-1.47611000000000003	29.5743000000000009	35.0097200000000015
230	52.3785999999999987	44.3791500000000028	22.1514400000000009	40.1787500000000009
225	26.0838900000000002	22.6333300000000008	51.5833299999999966	56.3816599999999966
234	-30.0966699999999996	-34.9438200000000023	-58.4386099999999971	-53.0983000000000018
469	88.230000000000004	32.1799999999999997	16.1000000000000014	183.389999999999986
235	45.5705999999999989	37.1849899999999991	55.9974899999999991	73.1675500000000056
155	-13.7072199999999995	-20.2541699999999985	166.521639999999991	169.893859999999989
236	12.1974999999999998	0.649170000000000025	-73.3780699999999939	-59.8030600000000021
237	23.3241599999999991	8.55924000000000085	102.140749999999997	109.464839999999995
242	19.324580000000001	19.279440000000001	166.608980000000003	166.662200000000013
243	-13.2148599999999998	-14.3238900000000005	-178.190280000000001	-176.121929999999992
245	32.5463900000000024	31.3506900000000002	34.8881900000000016	35.5706100000000021
311	29.0399999999999991	-17.0599999999999987	-33.4500000000000028	17.5599999999999987
303	82.9899999999999949	26.9200000000000017	-37.3200000000000003	39.240000000000002
205	27.6669599999999996	20.7640999999999991	-17.1015300000000003	-8.66638999999999982
9999	90	-90	-180	180
249	18.9993400000000001	12.1447199999999995	42.5559700000000021	54.4734699999999989
251	-8.19167000000000023	-18.0749199999999988	21.9963900000000017	33.7022800000000018
181	-15.6165299999999991	-22.4147600000000011	25.237919999999999	33.0715900000000005
\.


--
-- Data for Name: regionsdes; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.regionsdes (iddes, langid, label) FROM stdin;
1	eng	Armenia
2	eng	Afghanistan
3	eng	Albania
4	eng	Algeria
5	eng	American Samoa
6	eng	Andorra
7	eng	Angola
8	eng	Antigua and Barbuda
9	eng	Argentina
10	eng	Australia
11	eng	Austria
12	eng	Bahamas
13	eng	Bahrain
14	eng	Barbados
16	eng	Bangladesh
17	eng	Bermuda
18	eng	Bhutan
19	eng	Bolivia
20	eng	Botswana
21	eng	Brazil
22	eng	Aruba
23	eng	Belize
24	eng	British Indian Ocean Ter
25	eng	Solomon Islands
26	eng	Brunei Darussalam
27	eng	Bulgaria
28	eng	Myanmar
29	eng	Burundi
30	eng	Antarctica
31	eng	Bouvet Island
32	eng	Cameroon
33	eng	Canada
35	eng	Cape Verde
36	eng	Cayman Island
37	eng	Central African Republic
38	eng	Sri Lanka
39	eng	Chad
40	eng	Chile
41	eng	China, Mainland
45	eng	Comoros
1	fre	Arménie
2	fre	Afghanistan
3	fre	Albanie
4	fre	Algérie
5	fre	Samoa américaines
6	fre	Andorre
7	fre	Angola
8	fre	Antigua-et-Barbuda
9	fre	Argentine
10	fre	Australie
11	fre	Autriche
12	fre	Bahamas
13	fre	Bahreïn
14	fre	Barbade
16	fre	Bangladesch
17	fre	Bermudes
18	fre	Bhoutan
19	fre	Bolivie
20	fre	Botswana
21	fre	Brésil
22	fre	Aruba
23	fre	Belize
24	fre	British Indian Ocean Ter
25	fre	Salomon, Îles
26	fre	Brunei Darussalam
27	fre	Bulgarie
28	fre	Myanmar
29	fre	Burundi
30	fre	Antartique
31	fre	Bouvet, Île
32	fre	Cameroun
33	fre	Canada
35	fre	Cap-Vert
36	fre	Cayman Island
37	fre	Centrafricaine, République
38	fre	Sri Lanka
39	fre	Tchad
40	fre	Chili
46	eng	Congo, Republic of
47	eng	Cook Islands
48	eng	Costa Rica
49	eng	Cuba
50	eng	Cyprus
52	eng	Azerbaijan, Republic of
53	eng	Benin
54	eng	Denmark
55	eng	Dominica
56	eng	Dominican Republic
57	eng	Belarus
58	eng	Ecuador
59	eng	Egypt
60	eng	El Salvador
61	eng	Equatorial Guinea
63	eng	Estonia
64	eng	Faeroe Islands
65	eng	Falkland Islands
66	eng	Fiji Islands
67	eng	Finland
68	eng	France
69	eng	French Guiana
70	eng	French Polynesia
71	eng	French South Terr
72	eng	Djibouti
73	eng	Georgia
74	eng	Gabon
75	eng	Gambia
76	eng	Gaza Strip
79	eng	Germany
80	eng	Bosnia and Herzegovina
81	eng	Ghana
82	eng	Gibraltar
83	eng	Kiribati
84	eng	Greece
85	eng	Greenland
86	eng	Grenada
46	fre	Congo
47	fre	Cook, Îles
48	fre	Costa Rica
49	fre	Cuba
50	fre	Chypre
52	fre	Azerbaijan, Republique de
53	fre	Bénin
54	fre	Danemark
55	fre	Dominique
56	fre	Dominicaine, République
57	fre	Bélarus
58	fre	Équateur
59	fre	Égypte
60	fre	El Salvador
61	fre	Guinée équatoriale
63	fre	Estonie
64	fre	Faeroe Islands
65	fre	Falkland, Iles
66	fre	Fiji, Iles
67	fre	Finlande
68	fre	France
69	fre	Guyane française
70	fre	Polynésie française
71	fre	French South Terr
72	fre	Djibouti
73	fre	Géorgie
74	fre	Gabon
75	fre	Gambie
76	fre	Bande de Gaza
79	fre	Allemagne
80	fre	Bosnie-et-Herzégovine
81	fre	Ghana
82	fre	Gibraltar
83	fre	Kiribati
84	fre	Grèce
85	fre	Groenland
86	fre	Grenade
87	fre	Guadeloupe
93	eng	Haiti
95	eng	Honduras
97	eng	Hungary
98	eng	Croatia
99	eng	Iceland
100	eng	India
101	eng	Indonesia
102	eng	Iran, Islamic Rep of
103	eng	Iraq
104	eng	Ireland
105	eng	Israel
106	eng	Italy
107	eng	Ivory Coast
108	eng	Kazakhstan
109	eng	Jamaica
110	eng	Japan
112	eng	Jordan
113	eng	Kyrgyzstan
114	eng	Kenya
115	eng	Cambodia
116	eng	Korea, Dem People's Rep
117	eng	Korea, Republic of
118	eng	Kuwait
119	eng	Latvia
120	eng	Laos
121	eng	Lebanon
122	eng	Lesotho
123	eng	Liberia
124	eng	Libyan Arab Jamahiriya
125	eng	Liechtensten
126	eng	Lithuania
127	eng	Marshall Island
129	eng	Madagascar
130	eng	Malawi
131	eng	Malaysia
132	eng	Maldives
133	eng	Mali
134	eng	Malta
135	eng	Martinique
136	eng	Mauritania
93	fre	Haïti
95	fre	Honduras
97	fre	Hongrie
98	fre	Croatie
99	fre	Islande
100	fre	Inde
101	fre	Indonésie
102	fre	Iran
103	fre	Irak
104	fre	Irlande
105	fre	Israël
106	fre	Italie
107	fre	Côte d'Ivoire
108	fre	Kazakhstan
109	fre	Jamaïque
110	fre	Japon
112	fre	Jordanie
113	fre	Kirghizistan
114	fre	Kenya
115	fre	Cambodge
116	fre	Corée du nord
117	fre	Corée du sud
118	fre	Koweït
119	fre	Lettonie
120	fre	Lao, République démocratique pop
121	fre	Liban
122	fre	Lesotho
123	fre	Liberia
124	fre	Libyan Arab Jamahiriya
125	fre	Liechtensten
126	fre	Lituanie
127	fre	Marshall Island
129	fre	Madagascar
130	fre	Malawi
131	fre	Malaisie
132	fre	Maldives
133	fre	Mali
134	fre	Malte
135	fre	Martinique
141	eng	Mongolia
142	eng	Montserrat
143	eng	Morocco
144	eng	Mozambique
145	eng	Micronesia,Fed States of
146	eng	Moldova, Republic of
147	eng	Namibia
148	eng	Nauru
149	eng	Nepal
150	eng	Netherlands
151	eng	Neth Antilles
153	eng	NewCaledonia
154	eng	Macedonia
155	eng	Vanuatu
156	eng	New Zealand
157	eng	Nicaragua
158	eng	Niger
159	eng	Nigeria
160	eng	Niue
161	eng	Norfolk Island
162	eng	Norway
163	eng	Northern Mariana Is
165	eng	Pakistan
166	eng	Panama
167	eng	Czech Republic
168	eng	Papua New Guinea
169	eng	Paraguay
170	eng	Peru
171	eng	Philippines
172	eng	Pitcairn Islands
173	eng	Poland
174	eng	Portugal
175	eng	Guinea-Bissau
176	eng	East Timor
177	eng	Puerto Rico
178	eng	Eritrea
179	eng	Qatar
181	eng	Zimbabwe
182	eng	Reunion
141	fre	Mongolie
142	fre	Montserrat
143	fre	Maroc
144	fre	Mozambique
145	fre	Micronesia,Fed States of
146	fre	Moldova, Republic of
147	fre	Namibie
148	fre	Nauru
149	fre	Népal
150	fre	Pays-Bas
151	fre	Neth Antilles
153	fre	NewCaledonia
154	fre	Macédoine, L'ex-République yougo
155	fre	Vanuatu
156	fre	Nouvelle-Zélande
157	fre	Nicaragua
158	fre	Niger
159	fre	Nigeria
160	fre	Niué
161	fre	Norfolk, Île
162	fre	Norvège
163	fre	Northern Mariana Is
165	fre	Pakistan
166	fre	Panama
167	fre	Tchèque, République
168	fre	Papouasie-Nouvelle-Guinée
169	fre	Paraguay
170	fre	Pérou
171	fre	Philippines
172	fre	Pitcairn Islands
173	fre	Pologne
174	fre	Portugal
175	fre	Guinée-Bissau
176	fre	Timor est
177	fre	Porto Rico
178	fre	Érythrée
179	fre	Qatar
188	eng	Saint Kitts and Nevis
189	eng	Saint Lucia
190	eng	Saint Pierre & Miquelon
191	eng	Saint Vincent/Grenadines
192	eng	San Marino
193	eng	Sao Tome and Principe
194	eng	Saudi Arabia
195	eng	Senegal
196	eng	Seychelles
197	eng	Sierra Leone
198	eng	Slovenia
199	eng	Slovakia
200	eng	Singapore
201	eng	Somalia
202	eng	South Africa
203	eng	Spain
205	eng	Western Sahara
206	eng	Sudan
207	eng	Suriname
208	eng	Tajikistan
209	eng	Swaziland
210	eng	Sweden
211	eng	Switzerland
212	eng	Syrian Arab Republic
213	eng	Turkmenistan
214	eng	China, Taiwan Prov of
215	eng	Tanzania, United Rep of
216	eng	Thailand
217	eng	Togo
218	eng	Tokelau
219	eng	Tonga
220	eng	Trinidad and Tobago
221	eng	Oman
222	eng	Tunisia
223	eng	Turkey
224	eng	Turks and Caicos Is
225	eng	United Arab Emirates
187	fre	Sainte-Hélène
188	fre	Saint-Kitts-et-Nevis
189	fre	Sainte-Lucie
190	fre	Saint Pierre & Miquelon
191	fre	Saint Vincent/Grenadines
192	fre	Saint-Marin
193	fre	Sao Tomé-et-Principe
194	fre	Saudi Arabia
195	fre	Sénégal
196	fre	Seychelles
197	fre	Sierra Leone
198	fre	Slovénie
199	fre	Slovaquie
200	fre	Singapour
201	fre	Somalie
202	fre	Afrique du Sud
203	fre	Espagne
205	fre	Sahara occidental
206	fre	Soudan
207	fre	Suriname
208	fre	Tadjikistan
209	fre	Swaziland
210	fre	Suède
211	fre	Suisse
212	fre	Syrie
213	fre	Turkménistan
214	fre	China, Taiwan Prov of
215	fre	Tanzanie
216	fre	Thaïlande
217	fre	Togo
218	fre	Tokelau
219	fre	Tonga
220	fre	Trinité-et-Tobago
221	fre	Oman
222	fre	Tunisie
223	fre	Turquie
224	fre	Turks and Caicos Is
225	fre	Émirats arabes unis
232	eng	US Minor Outlying Is
233	eng	Burkina Faso
234	eng	Uruguay
235	eng	Uzbekistan
236	eng	Venezuela
237	eng	Viet Nam
238	eng	Ethiopia
239	eng	British Virgin Island
240	eng	US Virgin Islands
242	eng	Wake Island
243	eng	Wallis and Futuna Is
244	eng	Samoa
245	eng	West Bank
249	eng	Yemen
250	eng	Congo, Dem Republic of
251	eng	Zambia
255	eng	Belgium
256	eng	Luxembourg
258	eng	Anguilla
260	eng	Svalbard Is
264	eng	Isle of Man
270	eng	Mayotte
271	eng	SouthGeorgia/Sandwich Is
303	eng	Western Europe
307	eng	Intergvt Author Devpment
310	eng	North Western Africa
311	eng	Western Africa
312	eng	Central Africa
313	eng	Eastern Africa
314	eng	Southern Africa
315	eng	Latin Amer & Caribbean
316	eng	Central America
317	eng	Caribbean
318	eng	South America
319	eng	Near East
320	eng	Near East in Africa
322	eng	Far East
231	fre	États-Unis
232	fre	US Minor Outlying Is
233	fre	Burkina Faso
234	fre	Uruguay
235	fre	Ouzbékistan
236	fre	Venezuela
237	fre	Viet Nam
238	fre	Éthiopie
239	fre	British Virgin Island
240	fre	US Virgin Islands
242	fre	Wake Island
243	fre	Wallis and Futuna Is
244	fre	Samoa
245	fre	West Bank
249	fre	Yemen
250	fre	Congo, République démocratique
251	fre	Zambie
255	fre	Belgique
256	fre	Luxembourg
258	fre	Anguilla
260	fre	Svalbard Is
264	fre	Île de Man
270	fre	Mayotte
271	fre	Ile Sandwich
303	fre	Europe de l'ouest
307	fre	Intergvt Author Devpment
310	fre	Afrique - nord-ouest
311	fre	Afrique de l'ouest
312	fre	Afrique - centre
313	fre	Afrique de l'est
314	fre	Afrique - sud
315	fre	Caraïbes & Amérique latine
316	fre	Amérique centrale
317	fre	Caraïbes
318	fre	Amérique du sud
1	chi	Armenia
2	chi	Afghanistan
3	chi	Albania
4	chi	Algeria
5	chi	American Samoa
6	chi	Andorra
7	chi	Angola
8	chi	Antigua and Barbuda
9	chi	Argentina
10	chi	Australia
11	chi	Austria
12	chi	Bahamas
13	chi	Bahrain
14	chi	Barbados
16	chi	Bangladesh
17	chi	Bermuda
18	chi	Bhutan
19	chi	Bolivia
20	chi	Botswana
21	chi	Brazil
22	chi	Aruba
325	eng	Near East and North Africa
334	eng	Eastern Europe
335	eng	Europe
337	eng	Asia
338	eng	Africa
348	eng	Latin America
390	eng	South of Sahara
421	eng	North Africa
427	eng	Europe, Non-EU Countries
428	eng	North America
469	eng	USSR, Former Area of
1008	eng	Arctic Sea
1009	eng	Atlantic, Northwest
1010	eng	Atlantic, Northeast
1011	eng	Atlantic, Western Central
1012	eng	Atlantic, Eastern Central
1013	eng	Mediterran and Black Sea
1014	eng	Atlantic, Southwest
1015	eng	Atlantic, Southeast
1016	eng	Atlantic, Antarctic
1017	eng	Indian Ocean, Western
1018	eng	Indian Ocean, Eastern
1019	eng	Indian Ocean, Antarctic
1020	eng	Pacific, Northwest
1021	eng	Pacific, Northeast
1022	eng	Pacific, Western Central
1023	eng	Pacific, Eastern Central
1024	eng	Pacific, Southwest
1025	eng	Pacific, Southeast
324	fre	Asie - est et sud-est
325	fre	Afrique - nord et proche orient
334	fre	Europe de l'Est
335	fre	Europe
337	fre	Asie
338	fre	Afrique
348	fre	Amérique Latine
390	fre	Sud du Sahara
421	fre	Afrique du Nord
427	fre	Europe, Pays hors EU
428	fre	Amérique du Nord
469	fre	URSS, Ancienne
1008	fre	Mer Arctique
1009	fre	Atlantique, Nord-ouest
1010	fre	Atlantique, Nord-est
1011	fre	Atlantique, Centre-ouest
1012	fre	Atlantique, Centre-est
1013	fre	Méditerranée et Mer noire
1014	fre	Atlantique, Sud-ouest
1015	fre	Atlantique, Sud-est
1016	fre	Atlantique, Antarctique
1017	fre	Ocean Indien, Ouest
1018	fre	Ocean Indien, Est
1019	fre	Ocean Indien, Antarctique
1020	fre	Pacifique, Nord-ouest
1021	fre	Pacifique, Nord-est
1022	fre	Pacifique, Centre-ouest
1023	fre	Pacific, Centre-est
1024	fre	Pacifique, Sud-ouest
23	chi	Belize
24	chi	British Indian Ocean Ter
25	chi	Solomon Islands
26	chi	Brunei Darussalam
27	chi	Bulgaria
28	chi	Myanmar
29	chi	Burundi
30	chi	Antarctica
31	chi	Bouvet Island
32	chi	Cameroon
33	chi	Canada
35	chi	Cape Verde
36	chi	Cayman Island
37	chi	Central African Republic
38	chi	Sri Lanka
39	chi	Chad
40	chi	Chile
41	chi	China, Mainland
42	chi	Christmas Island
43	chi	Cocos Islands
44	chi	Colombia
45	chi	Comoros
46	chi	Congo, Republic of
47	chi	Cook Islands
48	chi	Costa Rica
49	chi	Cuba
50	chi	Cyprus
52	chi	Azerbaijan, Republic of
53	chi	Benin
54	chi	Denmark
55	chi	Dominica
56	chi	Dominican Republic
57	chi	Belarus
58	chi	Ecuador
59	chi	Egypt
60	chi	El Salvador
61	chi	Equatorial Guinea
63	chi	Estonia
64	chi	Faeroe Islands
65	chi	Falkland Islands
66	chi	Fiji Islands
67	chi	Finland
68	chi	France
69	chi	French Guiana
70	chi	French Polynesia
71	chi	French South Terr
72	chi	Djibouti
73	chi	Georgia
74	chi	Gabon
75	chi	Gambia
76	chi	Gaza Strip
79	chi	Germany
80	chi	Bosnia and Herzegovina
81	chi	Ghana
82	chi	Gibraltar
83	chi	Kiribati
84	chi	Greece
85	chi	Greenland
86	chi	Grenada
87	chi	Guadeloupe
88	chi	Guam
89	chi	Guatemala
90	chi	Guinea
91	chi	Guyana
92	chi	Heard and McDonald Is
93	chi	Haiti
95	chi	Honduras
97	chi	Hungary
98	chi	Croatia
99	chi	Iceland
100	chi	India
101	chi	Indonesia
102	chi	Iran, Islamic Rep of
103	chi	Iraq
104	chi	Ireland
105	chi	Israel
106	chi	Italy
107	chi	Ivory Coast
108	chi	Kazakhstan
109	chi	Jamaica
110	chi	Japan
112	chi	Jordan
113	chi	Kyrgyzstan
114	chi	Kenya
115	chi	Cambodia
116	chi	Korea, Dem People's Rep
117	chi	Korea, Republic of
118	chi	Kuwait
119	chi	Latvia
120	chi	Laos
121	chi	Lebanon
122	chi	Lesotho
123	chi	Liberia
124	chi	Libyan Arab Jamahiriya
125	chi	Liechtensten
126	chi	Lithuania
127	chi	Marshall Island
129	chi	Madagascar
130	chi	Malawi
131	chi	Malaysia
132	chi	Maldives
133	chi	Mali
134	chi	Malta
135	chi	Martinique
136	chi	Mauritania
137	chi	Mauritius
138	chi	Mexico
139	chi	Midway Islands
140	chi	Monaco
141	chi	Mongolia
142	chi	Montserrat
143	chi	Morocco
144	chi	Mozambique
145	chi	Micronesia,Fed States of
146	chi	Moldova, Republic of
147	chi	Namibia
148	chi	Nauru
149	chi	Nepal
150	chi	Netherlands
151	chi	Neth Antilles
153	chi	NewCaledonia
154	chi	Macedonia
155	chi	Vanuatu
156	chi	New Zealand
157	chi	Nicaragua
158	chi	Niger
159	chi	Nigeria
160	chi	Niue
161	chi	Norfolk Island
162	chi	Norway
163	chi	Northern Mariana Is
165	chi	Pakistan
166	chi	Panama
167	chi	Czech Republic
168	chi	Papua New Guinea
169	chi	Paraguay
170	chi	Peru
171	chi	Philippines
172	chi	Pitcairn Islands
173	chi	Poland
174	chi	Portugal
175	chi	Guinea-Bissau
176	chi	East Timor
177	chi	Puerto Rico
178	chi	Eritrea
179	chi	Qatar
181	chi	Zimbabwe
182	chi	Reunion
183	chi	Romania
184	chi	Rwanda
185	chi	Russian Federation
187	chi	Saint Helena
188	chi	Saint Kitts and Nevis
189	chi	Saint Lucia
190	chi	Saint Pierre & Miquelon
191	chi	Saint Vincent/Grenadines
192	chi	San Marino
193	chi	Sao Tome and Principe
194	chi	Saudi Arabia
195	chi	Senegal
196	chi	Seychelles
197	chi	Sierra Leone
198	chi	Slovenia
199	chi	Slovakia
200	chi	Singapore
201	chi	Somalia
202	chi	South Africa
203	chi	Spain
205	chi	Western Sahara
206	chi	Sudan
207	chi	Suriname
208	chi	Tajikistan
209	chi	Swaziland
210	chi	Sweden
211	chi	Switzerland
212	chi	Syrian Arab Republic
213	chi	Turkmenistan
214	chi	China, Taiwan Prov of
215	chi	Tanzania, United Rep of
216	chi	Thailand
217	chi	Togo
218	chi	Tokelau
219	chi	Tonga
220	chi	Trinidad and Tobago
221	chi	Oman
222	chi	Tunisia
223	chi	Turkey
224	chi	Turks and Caicos Is
225	chi	United Arab Emirates
226	chi	Uganda
227	chi	Tuvalu
229	chi	United Kingdom
230	chi	Ukraine
231	chi	United States of America
232	chi	US Minor Outlying Is
233	chi	Burkina Faso
234	chi	Uruguay
235	chi	Uzbekistan
236	chi	Venezuela
237	chi	Viet Nam
238	chi	Ethiopia
239	chi	British Virgin Island
240	chi	US Virgin Islands
242	chi	Wake Island
243	chi	Wallis and Futuna Is
244	chi	Samoa
245	chi	West Bank
249	chi	Yemen
250	chi	Congo, Dem Republic of
251	chi	Zambia
255	chi	Belgium
256	chi	Luxembourg
258	chi	Anguilla
260	chi	Svalbard Is
264	chi	Isle of Man
270	chi	Mayotte
271	chi	SouthGeorgia/Sandwich Is
303	chi	Western Europe
307	chi	Intergvt Author Devpment
310	chi	North Western Africa
311	chi	Western Africa
312	chi	Central Africa
313	chi	Eastern Africa
314	chi	Southern Africa
315	chi	Latin Amer & Caribbean
316	chi	Central America
317	chi	Caribbean
318	chi	South America
319	chi	Near East
320	chi	Near East in Africa
321	chi	Near East in Asia
322	chi	Far East
323	chi	South Asia
324	chi	East & South East Asia
325	chi	Near East and North Africa
334	chi	Eastern Europe
335	chi	Europe
337	chi	Asia
338	chi	Africa
348	chi	Latin America
390	chi	South of Sahara
421	chi	North Africa
427	chi	Europe, Non-EU Countries
428	chi	North America
469	chi	USSR, Former Area of
1008	chi	Arctic Sea
1009	chi	Atlantic, Northwest
1010	chi	Atlantic, Northeast
1011	chi	Atlantic, Western Central
1012	chi	Atlantic, Eastern Central
1013	chi	Mediterran and Black Sea
1014	chi	Atlantic, Southwest
1015	chi	Atlantic, Southeast
1016	chi	Atlantic, Antarctic
1017	chi	Indian Ocean, Western
1018	chi	Indian Ocean, Eastern
1019	chi	Indian Ocean, Antarctic
1020	chi	Pacific, Northwest
1021	chi	Pacific, Northeast
1022	chi	Pacific, Western Central
1023	chi	Pacific, Eastern Central
1024	chi	Pacific, Southwest
1025	chi	Pacific, Southeast
1026	chi	Pacific, Antarctic
1220	chi	All fishing areas
9999	chi	World
42	eng	Christmas Island
43	eng	Cocos Islands
44	eng	Colombia
87	eng	Guadeloupe
88	eng	Guam
89	eng	Guatemala
90	eng	Guinea
91	eng	Guyana
92	eng	Heard and McDonald Is
137	eng	Mauritius
138	eng	Mexico
139	eng	Midway Islands
140	eng	Monaco
183	eng	Romania
184	eng	Rwanda
185	eng	Russian Federation
187	eng	Saint Helena
226	eng	Uganda
227	eng	Tuvalu
229	eng	United Kingdom
230	eng	Ukraine
231	eng	United States of America
321	eng	Near East in Asia
323	eng	South Asia
324	eng	East & South East Asia
1026	eng	Pacific, Antarctic
1220	eng	All fishing areas
9999	eng	World
41	fre	Chine
42	fre	Île de Christmas
43	fre	Cocos Islands
44	fre	Colombie
45	fre	Comores
88	fre	Guam
89	fre	Guatemala
90	fre	Guinée
91	fre	Guyana
92	fre	Heard, Île, et îles McDonald
136	fre	Mauritanie
137	fre	Maurice
138	fre	Mexique
139	fre	Midway Islands
140	fre	Monaco
181	fre	Zimbabwe
182	fre	Réunion
183	fre	Roumanie
184	fre	Rwanda
185	fre	Fédération de Russie
226	fre	Ouganda
227	fre	Tuvalu
229	fre	Royaume-Uni
230	fre	Ukraine
319	fre	Proche orient
320	fre	Proche orient en Afrique
321	fre	Proche orient en Asie
322	fre	Moyen orient
323	fre	Asie - sud
1025	fre	Pacifique, Sud-est
1026	fre	Pacifique, Antarctique
1220	fre	Toutes zones de pêches
9999	fre	Monde
\.


--
-- Data for Name: relations; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.relations (id, relatedid) FROM stdin;
\.


--
-- Data for Name: requests; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) FROM stdin;
40006	2015-03-30T13:19:42	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	q	f	t
1	2012-04-11T12:22:00	192.168.0.216	+(_op0:1 _op2:1) +_isTemplate:n	2	fr	null ASC	\N	all	q	\N	\N
4	2012-04-11T13:30:03	192.168.0.216	+(_op0:1 _op2:1) +_isTemplate:n	2	fr	null ASC	\N	all	q	\N	\N
11	2012-04-11T13:55:39	192.168.0.209	+(_op0:1 _op2:1) +_isTemplate:n	2	fr	null ASC	\N	all	q	\N	\N
65	2013-06-26T17:40:35	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n	2	fre	_title ASC,null ASC	\N		q	\N	\N
66	2013-06-26T17:40:39	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
127	2013-07-01T12:00:19	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	null ASC	\N		mef.export	\N	\N
14	2012-04-11T13:55:59	192.168.0.209	+(_op0:1 _op2:1) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fr	null ASC	\N		xml.relation	\N	\N
15	2012-04-11T13:56:19	192.168.0.209	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:service +serviceType:invoke +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
16	2012-04-11T13:56:44	192.168.0.209	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +serviceType:invoke +type:service +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
17	2012-04-11T13:57:33	192.168.0.209	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +(type:dataset type:series) +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
18	2012-04-11T13:57:45	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fr	null ASC	\N		mef.export	\N	\N
19	2012-04-11T13:57:45	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fr	null ASC	\N		mef.export	\N	\N
20	2012-04-11T13:59:25	192.168.0.209	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +type:dataset +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
22	2012-04-11T13:59:44	192.168.0.209	+(_op0:1 _op2:1) +keyword:Unités administratives +_isTemplate:n	1	fr	null ASC	\N		q	\N	\N
24	2012-04-11T14:26:54	192.168.0.209	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +(type:dataset type:series) +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
27	2013-06-26T17:34:47	193.251.20.2	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n	4	fre	_title ASC,null ASC	\N		q	\N	\N
28	2013-06-26T17:37:04	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
29	2013-06-26T17:37:05	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
30	2013-06-26T17:37:05	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
31	2013-06-26T17:37:07	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	null ASC	\N		mef.export	\N	\N
32	2013-06-26T17:37:07	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	null ASC	\N		mef.export	\N	\N
33	2013-06-26T17:38:12	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
34	2013-06-26T17:38:12	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
35	2013-06-26T17:38:12	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
36	2013-06-26T17:38:13	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	null ASC	\N		mef.export	\N	\N
37	2013-06-26T17:38:13	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	null ASC	\N		mef.export	\N	\N
130	2013-07-01T16:17:09	193.251.20.2	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n	4	fre	_title ASC,null ASC	\N		q	\N	\N
38	2013-06-26T17:39:32	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
39	2013-06-26T17:39:32	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
40	2013-06-26T17:39:32	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
41	2013-06-26T17:39:32	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
40010	2015-03-30T13:20:14	172.16.64.216	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n	5	fre	_title ASC,null ASC	\N	TERM	q	f	f
116	2013-07-01T11:29:24	193.251.20.2	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	all	q	\N	\N
2	2012-04-11T12:22:01	192.168.0.216	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n	2	fr	_title ASC,null ASC	\N		q	\N	\N
42	2013-06-26T17:39:44	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n	2	fre	_title ASC,null ASC	\N		q	\N	\N
43	2013-06-26T17:39:50	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n	2	fre	_title ASC,null ASC	\N		q	\N	\N
44	2013-06-26T17:39:54	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
85	2013-06-26T17:42:05	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
94	2013-06-26T17:42:30	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n	2	fre	_title ASC,null ASC	\N		q	\N	\N
95	2013-06-26T17:42:36	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
100	2013-06-26T17:42:46	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
111	2013-06-26T17:45:02	193.251.20.2	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n	2	fre	_title ASC,null ASC	\N		q	\N	\N
83	2013-06-26T17:41:20	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
84	2013-06-26T17:42:01	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n	2	fre	_title ASC,null ASC	\N		q	\N	\N
112	2013-06-26T17:45:42	193.251.20.2	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	all	q	\N	\N
113	2013-07-01T10:48:45	193.251.20.2	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	all	q	\N	\N
115	2013-07-01T11:20:09	192.168.1.254	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	all	q	\N	\N
120	2013-07-01T11:31:30	193.251.20.2	+(_op0:1 _op2:1) +_isTemplate:n	5	fre	null ASC	\N	all	q	\N	\N
121	2013-07-01T11:54:42	193.251.20.2	+(_op0:1 _op2:1) +_isTemplate:n	5	fre	null ASC	\N	all	q	\N	\N
128	2013-07-01T16:16:22	193.251.20.2	+(_op0:1 _op2:1) +_isTemplate:n	5	fre	null ASC	\N	all	q	\N	\N
129	2013-07-01T16:17:07	193.251.20.2	+(_op0:1 _op2:1) +_isTemplate:n	5	fre	null ASC	\N	all	q	\N	\N
105	2013-06-26T17:43:02	193.251.20.2	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +(type:dataset type:series type:serv) +_isTemplate:n	2	fre	_title ASC,null ASC	\N		q	\N	\N
110	2013-06-26T17:44:36	193.251.20.2	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n	2	fre	_title ASC,null ASC	\N		q	\N	\N
114	2013-07-01T10:53:23	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +type:map +_isTemplate:n	1	fre	_title ASC,null ASC	\N		q	\N	\N
117	2013-07-01T11:30:56	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +type:dataset +_isTemplate:n	2	fre	_title ASC,null ASC	\N		q	\N	\N
119	2013-07-01T11:31:14	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +type:dataset +_isTemplate:n	1	fre	_title ASC,null ASC	\N		q	\N	\N
118	2013-07-01T11:31:03	193.251.20.2	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
122	2013-07-01T11:54:43	193.251.20.2	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n	4	fre	_title ASC,null ASC	\N		q	\N	\N
123	2013-07-01T12:00:18	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
124	2013-07-01T12:00:18	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
125	2013-07-01T12:00:19	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
126	2013-07-01T12:00:19	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	null ASC	\N		mef.export	\N	\N
3	2012-04-11T13:29:58	192.168.0.216	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +(type:dataset type:series) +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
5	2012-04-11T13:32:23	192.168.0.209	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +type:service +serviceType:invoke +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
6	2012-04-11T13:32:35	192.168.0.209	+(_op0:1 _op2:1) +parentUuid:617af7f0-0a6f-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fr	null ASC	\N		xml.relation	\N	\N
7	2012-04-11T13:32:35	192.168.0.209	+(_op0:1 _op2:1) +operatesOn:617af7f0-0a6f-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fr	null ASC	\N		xml.relation	\N	\N
8	2012-04-11T13:35:16	192.168.0.209	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +type:dataset +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
9	2012-04-11T13:49:11	192.168.0.209	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +serviceType:invoke +type:service +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
12	2012-04-11T13:55:56	192.168.0.209	+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n	1	fr	_title ASC,null ASC	\N		q	\N	\N
13	2012-04-11T13:55:59	192.168.0.209	+(_op0:1 _op2:1) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fr	null ASC	\N		xml.relation	\N	\N
49	2013-06-26T17:40:03	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
50	2013-06-26T17:40:03	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
51	2013-06-26T17:40:03	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		xml.relation	\N	\N
52	2013-06-26T17:40:03	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	null ASC	\N		mef.export	\N	\N
53	2013-06-26T17:40:03	127.0.0.1	+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	null ASC	\N		mef.export	\N	\N
21	2012-04-11T13:59:37	192.168.0.209	+(_op0:1 _op2:1) +_isTemplate:n	2	fr	null ASC	\N	all	q	\N	\N
45	2013-06-26T17:39:56	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
46	2013-06-26T17:39:56	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
47	2013-06-26T17:39:57	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
48	2013-06-26T17:39:57	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
54	2013-06-26T17:40:20	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +parentUuid:274971da-ef09-4e3b-9a5e-b236fd670874 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
55	2013-06-26T17:40:20	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +operatesOn:274971da-ef09-4e3b-9a5e-b236fd670874 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
56	2013-06-26T17:40:20	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:274971da-ef09-4e3b-9a5e-b236fd670874 +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
57	2013-06-26T17:40:20	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
58	2013-06-26T17:40:20	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
59	2013-06-26T17:40:20	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
60	2013-06-26T17:40:20	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
61	2013-06-26T17:40:21	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
62	2013-06-26T17:40:21	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
63	2013-06-26T17:40:21	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
64	2013-06-26T17:40:21	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
67	2013-06-26T17:40:44	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
68	2013-06-26T17:40:44	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
69	2013-06-26T17:40:44	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
70	2013-06-26T17:40:44	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
72	2013-06-26T17:40:55	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
73	2013-06-26T17:40:55	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
74	2013-06-26T17:40:55	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
77	2013-06-26T17:41:12	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
78	2013-06-26T17:41:12	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
98	2013-06-26T17:42:41	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
99	2013-06-26T17:42:41	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
103	2013-06-26T17:42:49	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
104	2013-06-26T17:42:49	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
40047	2017-04-27T11:39:17	172.16.64.209	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_changeDate DESC,null ASC	\N	TERM	q	f	t
71	2013-06-26T17:40:55	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
75	2013-06-26T17:41:12	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
76	2013-06-26T17:41:12	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
79	2013-06-26T17:41:13	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
80	2013-06-26T17:41:13	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
81	2013-06-26T17:41:13	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
82	2013-06-26T17:41:13	127.0.0.1	+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
86	2013-06-26T17:42:16	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
87	2013-06-26T17:42:17	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
88	2013-06-26T17:42:17	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
89	2013-06-26T17:42:17	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
90	2013-06-26T17:42:17	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:27879905-936b-41ba-87d2-7f843cd5d1eb +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
91	2013-06-26T17:42:17	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:27879905-936b-41ba-87d2-7f843cd5d1eb +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
92	2013-06-26T17:42:17	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	1	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
93	2013-06-26T17:42:17	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:27879905-936b-41ba-87d2-7f843cd5d1eb +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
96	2013-06-26T17:42:41	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
97	2013-06-26T17:42:41	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	1	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
101	2013-06-26T17:42:49	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
102	2013-06-26T17:42:49	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	1	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
106	2013-06-26T17:43:09	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
107	2013-06-26T17:43:09	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	1	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
108	2013-06-26T17:43:09	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
109	2013-06-26T17:43:09	127.0.0.1	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N		metadata.show	\N	\N
10	2012-04-11T13:55:04	192.168.0.209	+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_isTemplate:s	2	fr	null ASC	\N	all	xml.search	\N	\N
23	2012-04-11T13:59:51	192.168.0.209	+(_op0:1 _op2:1) +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n	2	fr	_title ASC,null ASC	\N	all	q	\N	\N
25	2013-06-26T17:33:56	193.251.20.2	+(_op0:1 _op2:1) +_isTemplate:n	4	fre	null ASC	\N	all	q	\N	\N
26	2013-06-26T17:34:45	193.251.20.2	+(_op0:1 _op2:1) +_isTemplate:n	4	fre	null ASC	\N	all	q	\N	\N
40019	2017-04-26T10:51:18	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	q	f	t
40023	2017-04-26T11:04:02	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	q	f	t
40029	2017-04-27T11:39:01	172.16.64.209	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	t
40027	2017-04-27T11:39:01	172.16.64.209	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_changeDate DESC,null ASC	\N	TERM	q	f	t
40028	2017-04-27T11:39:01	172.16.64.209	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_popularity DESC,null ASC	\N	TERM	q	f	t
40039	2017-04-27T11:39:17	172.16.64.209	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	t
40043	2017-04-27T11:39:17	172.16.64.209	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	q	f	t
40051	2017-04-27T11:39:17	172.16.64.209	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_popularity DESC,null ASC	\N	TERM	q	f	t
40090	2017-04-27T11:44:53	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	t
40103	2017-04-27T11:50:17	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	f
40152	2017-04-27T11:59:12	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	f
40218	2017-04-27T11:59:19	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hassource:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40231	2017-04-27T11:59:19	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40256	2017-04-27T12:00:57	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	f
40308	2017-04-27T12:01:21	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:y	9	fre	null ASC	\N	TERM	qi	f	f
40571	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +operatesOn:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40637	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +agg_associated:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40055	2017-04-27T11:39:24	172.16.64.209	+(_op0:1 _op2:1) +parentUuid:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40060	2017-04-27T11:39:24	172.16.64.209	+(_op0:1 _op2:1) +operatesOn:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40067	2017-04-27T11:39:24	172.16.64.209	+(_op0:1 _op2:1) +_uuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40065	2017-04-27T11:39:24	172.16.64.209	+(_op0:1 _op2:1) +hasfeaturecat:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40075	2017-04-27T11:39:24	172.16.64.209	+(_op0:1 _op2:1) +agg_associated:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40080	2017-04-27T11:39:24	172.16.64.209	+(_op0:1 _op2:1) +hasfeaturecat:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40349	2017-04-27T12:01:48	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +parentUuid:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40362	2017-04-27T12:01:48	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +agg_associated:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40375	2017-04-27T12:01:48	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +operatesOn:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40388	2017-04-27T12:01:48	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_uuid:-- Identifiant de la métadonnée de couche associée -- +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40401	2017-04-27T12:01:48	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hassource:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40414	2017-04-27T12:01:48	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40495	2017-04-27T12:02:28	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	f
40094	2017-04-27T11:44:53	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_changeDate DESC,null ASC	\N	TERM	q	f	t
40116	2017-04-27T11:50:17	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	_changeDate DESC,null ASC	\N	TERM	q	f	f
40179	2017-04-27T11:59:19	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40205	2017-04-27T11:59:19	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40282	2017-04-27T12:01:10	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)	16	fre	_changeDate DESC,null ASC	\N	TERM	q	f	f
40296	2017-04-27T12:01:11	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	f
40427	2017-04-27T12:01:53	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)	16	fre	_changeDate DESC,null ASC	\N	TERM	q	f	f
40441	2017-04-27T12:02:02	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_id:40085 +(_isTemplate:y _isTemplate:n _isTemplate:s)	1	fre	null ASC	\N	TERM	q	f	f
40456	2017-04-27T12:02:07	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)	16	fre	_changeDate DESC,null ASC	\N	TERM	q	f	f
40470	2017-04-27T12:02:27	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	_changeDate DESC,null ASC	\N	TERM	q	f	f
40482	2017-04-27T12:02:27	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	_popularity DESC,null ASC	\N	TERM	q	f	f
40494	2017-04-27T12:02:28	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	q	f	f
40098	2017-04-27T11:44:53	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_popularity DESC,null ASC	\N	TERM	q	f	t
40115	2017-04-27T11:50:17	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	_popularity DESC,null ASC	\N	TERM	q	f	f
40192	2017-04-27T11:59:19	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +agg_associated:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40244	2017-04-27T12:00:08	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	f
40268	2017-04-27T12:01:05	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)	16	fre	null ASC	\N	TERM	qi	f	f
40320	2017-04-27T12:01:41	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)	16	fre	_changeDate DESC,null ASC	\N	TERM	q	f	f
40334	2017-04-27T12:01:46	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_id:40086 +(_isTemplate:y _isTemplate:n _isTemplate:s)	1	fre	null ASC	\N	TERM	q	f	f
40558	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +parentUuid:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40139	2017-04-27T11:51:39	172.16.64.209	+(+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n) +ConstantScore(type_facet:typedataset)^0.0	1	fre	null ASC	\N	MATCH_ALL_DOCS	q	f	t
40140	2017-04-27T11:51:53	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	q	f	f
40164	2017-04-27T11:59:12	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_id:9 +(_isTemplate:y _isTemplate:n _isTemplate:s)	1	fre	null ASC	\N	TERM	q	f	f
40518	2017-04-27T12:04:53	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)	16	fre	_changeDate DESC,null ASC	\N	TERM	q	f	f
40532	2017-04-27T12:04:55	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	f
40544	2017-04-27T12:05:01	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n) +_uuid:9ba5f3e9-c0ef-4160-81c0-b633b6067601	1	fre	null ASC	\N	TERM	q	f	f
40580	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +agg_associated:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40579	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40610	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40623	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +parentUuid:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40640	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +operatesOn:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40636	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40674	2017-04-27T12:05:04	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n	0	fre	_title ASC,null ASC	\N	TERM	Api	f	f
40688	2017-04-27T12:05:05	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	_changeDate DESC,null ASC	\N	TERM	q	f	f
40689	2017-04-27T12:05:05	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	_popularity DESC,null ASC	\N	TERM	q	f	f
40712	2017-04-27T12:05:05	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	f
40724	2017-04-27T12:05:17	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)	16	fre	_changeDate DESC,null ASC	\N	TERM	q	f	f
40738	2017-04-27T12:05:18	172.16.64.209	+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	f
40750	2019-05-24T11:18:39	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_changeDate DESC,null ASC	\N	TERM	q	f	t
40751	2019-05-24T11:18:39	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	t
40752	2019-05-24T11:18:39	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_popularity DESC,null ASC	\N	TERM	q	f	t
40762	2019-05-24T12:01:59	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	qi	f	t
40774	2019-05-24T12:01:59	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	null ASC	\N	TERM	q	f	t
40763	2019-05-24T12:01:59	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_popularity DESC,null ASC	\N	TERM	q	f	t
40764	2019-05-24T12:01:59	172.16.64.216	+(_op0:1 _op2:1) +_isTemplate:n	6	fre	_changeDate DESC,null ASC	\N	TERM	q	f	t
\.


--
-- Data for Name: schematron; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.schematron (id, displaypriority, filename, schemaname) FROM stdin;
40000	0	schematron-rules-inspire.xsl	iso19139
40778	1	schematron-rules-inspire-strict.disabled.xsl	iso19139
40780	2	schematron-rules-inspire-sds.disabled.xsl	iso19139
40782	3	schematron-rules-url-check.report_only.xsl	iso19139
40002	4	schematron-rules-geonetwork.xsl	iso19139
40004	5	schematron-rules-iso.xsl	iso19139
\.


--
-- Data for Name: schematroncriteria; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.schematroncriteria (id, type, uitype, uivalue, value, group_name, group_schematronid) FROM stdin;
40001	ALWAYS_ACCEPT	\N	\N	_ignored_	*Generated*	40000
40003	ALWAYS_ACCEPT	\N	\N	_ignored_	*Generated*	40002
40005	ALWAYS_ACCEPT	\N	\N	_ignored_	*Generated*	40004
40779	ALWAYS_ACCEPT	\N	\N	_ignored_	*Generated*	40778
40781	ALWAYS_ACCEPT	\N	\N	_ignored_	*Generated*	40780
40783	ALWAYS_ACCEPT	\N	\N	_ignored_	*Generated*	40782
\.


--
-- Data for Name: schematroncriteriagroup; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.schematroncriteriagroup (name, schematronid, requirement) FROM stdin;
*Generated*	40000	REQUIRED
*Generated*	40002	REQUIRED
*Generated*	40004	REQUIRED
*Generated*	40778	DISABLED
*Generated*	40780	DISABLED
*Generated*	40782	REPORT_ONLY
\.


--
-- Data for Name: schematrondes; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.schematrondes (iddes, label, langid) FROM stdin;
40000	schematron-rules-inspire	eng
40002	schematron-rules-geonetwork	eng
40004	schematron-rules-iso	eng
40778	schematron-rules-inspire-strict	eng
40780	schematron-rules-inspire-sds	eng
40782	schematron-rules-url-check	eng
\.


--
-- Data for Name: selections; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.selections (id, name, iswatchable) FROM stdin;
0	PreferredList	n
1	WatchList	y
\.


--
-- Data for Name: selectionsdes; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.selectionsdes (iddes, label, langid) FROM stdin;
0	Preferred records	ara
1	Watch list	ara
0	Preferred records	cat
1	Watch list	cat
0	Preferred records	chi
1	Watch list	chi
0	Preferred records	dut
1	Watch list	dut
0	Preferred records	eng
1	Watch list	eng
0	Preferred records	fin
1	Watch list	fin
0	Fiches préférées	fre
1	Fiches observées	fre
0	Preferred records	ger
1	Watch list	ger
0	Preferred records	ita
1	Watch list	ita
0	Preferred records	nor
1	Watch list	nor
0	Preferred records	pol
1	Watch list	pol
0	Preferred records	por
1	Watch list	por
0	Preferred records	rus
1	Watch list	rus
0	Preferred records	spa
1	Watch list	spa
0	Preferred records	tur
1	Watch list	tur
0	Preferred records	vie
1	Watch list	vie
\.


--
-- Name: serviceparameter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: user_p40
--

SELECT pg_catalog.setval('public.serviceparameter_id_seq', 1, false);


--
-- Data for Name: serviceparameters; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.serviceparameters (service, name, value, occur, id) FROM stdin;
\.


--
-- Data for Name: services; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.services (id, name, class, description, explicitquery) FROM stdin;
\.


--
-- Data for Name: settings; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.settings (name, value, datatype, "position", internal) FROM stdin;
system/intranet/network	127.0.0.1	0	310	y
system/intranet/netmask	255.0.0.0	0	320	y
system/proxy/use	false	2	510	y
system/proxy/host	\N	0	520	y
system/proxy/port	\N	1	530	y
system/proxy/username	\N	0	540	y
system/proxy/password	\N	0	550	y
system/feedback/email	\N	0	610	y
system/feedback/mailServer/host	\N	0	630	y
system/feedback/mailServer/port	25	1	640	y
system/csw/enable	true	2	1210	y
system/csw/contactId	\N	0	1220	y
system/site/svnUuid	f900dee6-0873-4a41-b726-48d36166754a	0	170	y
system/site/name	PRODIGE	0	110	n
system/site/siteId	7fc45be3-9aba-4198-920c-b8737112d522	0	120	n
system/site/organization	PRODIGE	0	130	n
system/server/port	8180	1	220	n
system/userSelfRegistration/enable	false	2	1910	n
system/searchStats/enable	true	2	2510	n
system/inspire/enableSearchPanel	true	2	7220	n
system/harvester/enableEditing	false	2	9010	n
system/metadataprivs/usergrouponly	false	2	9180	n
system/hidewithheldelements/enable	false	2	9570	n
system/server/securePort	8443	1	240	y
system/selectionmanager/maxrecords	1000	1	910	y
system/csw/metadataPublic	false	2	1310	y
system/clickablehyperlinks/enable	true	2	2010	y
system/localrating/enable	false	2	2110	y
system/downloadservice/withdisclaimer	false	0	2230	y
system/autofixing/enable	true	2	2410	y
system/indexoptimizer/enable	true	2	6010	y
system/indexoptimizer/at/hour	0	1	6030	y
system/indexoptimizer/at/min	0	1	6040	y
system/indexoptimizer/at/sec	0	1	6050	y
system/indexoptimizer/interval		0	6060	y
system/indexoptimizer/interval/day	0	1	6070	y
system/indexoptimizer/interval/hour	24	1	6080	y
system/indexoptimizer/interval/min	0	1	6090	y
system/oai/mdmode	1	0	7010	y
system/oai/tokentimeout	3600	1	7020	y
system/oai/cachesize	60	1	7030	y
system/inspire/enable	false	2	7210	y
system/threadedindexing/maxthreads	1	1	9210	y
system/autodetect/enable	true	2	9510	y
system/requestedLanguage/only	false	0	9530	y
system/requestedLanguage/sorted	false	2	9540	y
system/feedback/mailServer/username		0	642	y
system/feedback/mailServer/password		0	643	y
system/feedback/mailServer/ssl	false	2	641	y
system/harvesting/mail/recipient	\N	0	9020	y
system/harvesting/mail/template		0	9021	y
system/harvesting/mail/templateError	There was an error on the harvesting: $$errorMsg$$	0	9022	y
system/harvesting/mail/templateWarning		0	9023	y
system/harvesting/mail/subject	[$$harvesterType$$] $$harvesterName$$ finished harvesting	0	9024	y
system/harvesting/mail/enabled	false	2	9025	y
system/harvesting/mail/level1	false	2	9026	y
system/harvesting/mail/level2	false	2	9027	y
system/harvesting/mail/level3	false	2	9028	y
system/requestedLanguage/ignorechars		0	9590	y
system/requestedLanguage/preferUiLanguage	true	2	9595	y
system/csw/transactionUpdateCreateXPath	true	2	1320	y
system/downloadservice/leave	false	0	2210	y
system/downloadservice/simple	false	0	2220	y
system/server/host	www-p40.alkante.com	0	210	n
system/server/protocol	https	0	230	n
system/userFeedback/enable	false	2	1911	n
system/xlinkResolver/enable	true	2	2310	n
region/getmap/background	osm	0	9590	n
region/getmap/width	500	0	9590	n
region/getmap/summaryWidth	500	0	9590	n
region/getmap/mapproj	EPSG:3857	0	9590	n
system/proxy/ignorehostlist	\N	0	560	y
system/inspire/atom	disabled	0	7230	y
system/inspire/atomSchedule	0 0 0/24 ? * *	0	7240	y
system/inspire/atomProtocol	INSPIRE-ATOM	0	7250	y
system/metadata/prefergrouplogo	true	2	9111	y
system/metadata/allThesaurus	false	2	9160	n
system/ui/defaultView	default	0	10100	n
system/server/log	log4j.xml	0	250	y
metadata/workflow/draftWhenInGroup		0	100002	n
system/oai/maxrecords	10	1	7040	y
metadata/workflow/allowPublishInvalidMd	true	2	100003	n
metadata/workflow/automaticUnpublishInvalidMd	false	2	100004	n
metadata/workflow/forceValidationOnMdSave	false	2	100005	n
system/feedback/mailServer/tls	false	2	644	y
system/feedback/mailServer/ignoreSslCertificateErrors	false	2	645	y
system/xlinkResolver/ignore	operatesOn,featureCatalogueCitation,Anchor,source	0	2312	n
system/cors/allowedHosts	*	0	561	y
metadata/resourceIdentifierPrefix	https://www-p40.alkante.com/geonetwork/srv/	0	10001	n
system/hidewithheldelements/enableLogging	false	2	2320	y
system/xlinkResolver/localXlinkEnable	true	2	2311	n
system/metadatacreate/generateUuid	true	2	9100	n
system/csw/enabledWhenIndexing	true	2	1211	y
metadata/import/restrict		0	11000	y
system/xlinkResolver/referencedDeletionAllowed	true	2	2313	n
metadata/backuparchive/enable	false	2	12000	n
system/userSelfRegistration/recaptcha/enable	false	2	1910	n
system/userSelfRegistration/recaptcha/publickey		0	1910	n
system/userSelfRegistration/recaptcha/secretkey		0	1910	y
ui/config	{"langDetector":{"fromHtmlTag":false,"regexp":"^/[a-zA-Z0-9_-]+/[a-zA-Z0-9_-]+/([a-z]{3})/","default":"eng"},"nodeDetector":{"regexp":"^/[a-zA-Z0-9_-]+/([a-zA-Z0-9_-]+)/[a-z]{3}/","default":"srv"},"mods":{"header":{"enabled":true,"languages":{"eng":"en","dut":"nl","fre":"fr","ger":"ge","kor":"ko","spa":"es","cze":"cz","cat":"ca","fin":"fi","ice":"is", "rus": "ru", "chi": "zh"}},"home":{"enabled":true,"appUrl":"../../srv/{{lang}}/catalog.search#/home"},"search":{"enabled":true,"appUrl":"../../srv/{{lang}}/catalog.search#/search","hitsperpageValues":[10,50,100],"paginationInfo":{"hitsPerPage":20},"facetsSummaryType":"details","facetConfig":[],"facetTabField":"","filters":{},"sortbyValues":[{"sortBy":"relevance","sortOrder":""},{"sortBy":"changeDate","sortOrder":""},{"sortBy":"title","sortOrder":"reverse"},{"sortBy":"rating","sortOrder":""},{"sortBy":"popularity","sortOrder":""},{"sortBy":"denominatorDesc","sortOrder":""},{"sortBy":"denominatorAsc","sortOrder":"reverse"}],"sortBy":"relevance","resultViewTpls":[{"tplUrl":"../../catalog/components/search/resultsview/partials/viewtemplates/grid.html","tooltip":"Grid","icon":"fa-th"}],"resultTemplate":"../../catalog/components/search/resultsview/partials/viewtemplates/grid.html","formatter":{"list":[{"label":"full","url":"../api/records/{{uuid}}/formatters/xsl-view?root=div&view=advanced"}]},"grid":{"related":["parent","children","services","datasets"]},"linkTypes":{"links":["LINK","kml"],"downloads":["DOWNLOAD"],"layers":["OGC"],"maps":["ows"]},"isFilterTagsDisplayedInSearch":false},"map":{"enabled":true,"appUrl":"../../srv/{{lang}}/catalog.search#/map","is3DModeAllowed":true,"isSaveMapInCatalogAllowed":true,"isExportMapAsImageEnabled":true,"bingKey":"AnElW2Zqi4fI-9cYx1LHiQfokQ9GrNzcjOh_p_0hkO1yo78ba8zTLARcLBIf8H6D","storage":"sessionStorage","map":"../../map/config-viewer.xml","listOfServices":{"wms":[],"wmts":[]},"useOSM":true,"context":"","layer":{"url":"http://www2.demis.nl/mapserver/wms.asp?","layers":"Countries","version":"1.1.1"},"projection":"EPSG:3857","projectionList":[{"code":"EPSG:4326","label":"WGS84(EPSG:4326)"},{"code":"EPSG:3857","label":"Googlemercator(EPSG:3857)"}],"disabledTools":{"processes":false,"addLayers":false,"layers":false,"filter":false,"contexts":false,"print":false,"mInteraction":false,"graticule":false,"syncAllLayers":false,"drawVector":false},"searchMapLayers":[],"viewerMapLayers":[]},"editor":{"enabled":true,"appUrl":"../../srv/{{lang}}/catalog.edit","isUserRecordsOnly":false,"isFilterTagsDisplayed":false,"createPageTpl": "../../catalog/templates/editor/new-metadata-horizontal.html"},"admin":{"enabled":true,"appUrl":"../../srv/{{lang}}/admin.console"},"signin":{"enabled":true,"appUrl":"../../srv/{{lang}}/catalog.signin"},"signout":{"appUrl":"../../signout"}}}	3	10000	n
system/metadata/validation/removeSchemaLocation	false	2	9170	n
system/platform/version	3.4.1	0	150	n
system/platform/subVersion	0	0	160	n
system/userFeedback/lastNotificationDate	2019-06-02T04:00:00	0	1912	y
\.


--
-- Data for Name: sources; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.sources (uuid, name, islocal) FROM stdin;
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	y
\.


--
-- Data for Name: sourcesdes; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.sourcesdes (iddes, label, langid) FROM stdin;
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	ara
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	cat
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	chi
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	dut
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	eng
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	fin
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	fre
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	ger
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	nor
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	por
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	rus
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	spa
7fc45be3-9aba-4198-920c-b8737112d522	PRODIGE	vie
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: spatialindex; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.spatialindex (fid, id, the_geom) FROM stdin;
1	-6	0106000020E61000000100000001030000000100000005000000143BEFFFFFCB15C07B96FC4913AC4940DFE0EDFFFF8D27407B96FC4913AC4940DFE0EDFFFF8D2740EA2162D04B0E4440143BEFFFFFCB15C0EA2162D04B0E4440143BEFFFFFCB15C07B96FC4913AC4940
2	-5	0106000020E61000000100000001030000000100000005000000143BEFFFFFCB15C07B96FC4913AC4940DFE0EDFFFF8D27407B96FC4913AC4940DFE0EDFFFF8D2740EA2162D04B0E4440143BEFFFFFCB15C0EA2162D04B0E4440143BEFFFFFCB15C07B96FC4913AC4940
3	-4	0106000020E61000000100000001030000000100000005000000143BEFFFFFCB15C07B96FC4913AC4940DFE0EDFFFF8D27407B96FC4913AC4940DFE0EDFFFF8D2740EA2162D04B0E4440143BEFFFFFCB15C0EA2162D04B0E4440143BEFFFFFCB15C07B96FC4913AC4940
4	-3	0106000020E610000001000000010300000001000000050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
5	-2	0106000020E610000001000000010300000001000000050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
6	1	0106000020E610000001000000010300000001000000050000003D0AD7A3703D11C00000000000804940CDCCCCCCCCCC20400000000000804940CDCCCCCCCCCC20401F85EB51B89E44403D0AD7A3703D11C01F85EB51B89E44403D0AD7A3703D11C00000000000804940
7	2	0106000020E610000001000000010300000001000000050000003D0AD7A3703D11C00000000000804940CDCCCCCCCCCC20400000000000804940CDCCCCCCCCCC20401F85EB51B89E44403D0AD7A3703D11C01F85EB51B89E44403D0AD7A3703D11C00000000000804940
8	3	0106000020E6100000010000000103000000010000000500000045BB0A293F2917C0562B137EA98B49407250C24CDB1F2340562B137EA98B49407250C24CDB1F234026AAB706B6AE444045BB0A293F2917C026AAB706B6AE444045BB0A293F2917C0562B137EA98B4940
9	9	0106000020E610000001000000010300000001000000050000003D0AD7A3703D11C00000000000804940CDCCCCCCCCCC20400000000000804940CDCCCCCCCCCC20401F85EB51B89E44403D0AD7A3703D11C01F85EB51B89E44403D0AD7A3703D11C00000000000804940
10	10	0106000020E610000001000000010300000001000000050000003D0AD7A3703D11C00000000000804940CDCCCCCCCCCC20400000000000804940CDCCCCCCCCCC20401F85EB51B89E44403D0AD7A3703D11C01F85EB51B89E44403D0AD7A3703D11C00000000000804940
\.


--
-- Data for Name: statusvalues; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.statusvalues (id, name, reserved, displayorder) FROM stdin;
0	unknown	y	0
1	draft	y	1
2	approved	y	3
3	retired	y	5
4	submitted	y	2
5	rejected	y	4
\.


--
-- Data for Name: statusvaluesdes; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.statusvaluesdes (iddes, langid, label) FROM stdin;
0	fre	Inconnu
1	fre	Brouillon
2	fre	Validé
3	fre	Retiré
4	fre	A valider
5	fre	Rejeté
\.


--
-- Data for Name: thesaurus; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.thesaurus (id, activated) FROM stdin;
\.


--
-- Data for Name: useraddress; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.useraddress (userid, addressid) FROM stdin;
1	1
\.


--
-- Data for Name: usergroups; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.usergroups (userid, groupid, profile) FROM stdin;
1	217	2
40102	217	0
40784	40785	0
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.users (id, username, password, surname, name, profile, organisation, kind, security, authtype, lastlogindate, nodeid, isenabled) FROM stdin;
40102	admin@prodige.fr		ADM	PRODIGE_ADM	0		\N		LDAP	2017-04-27T11:50:14	srv	y
1	administrateur	1cb5662aa3cc6ce81f14426f0e04451a9f4288eebc847d9813bde81568c437dabb3b0e1e0dee13c0	admin	admin	0	\N	\N		\N	2017-04-26T11:00:48	srv	y
40	Internet		Internet	Internet	4	\N	\N		LDAP		srv	y
40784	admincli			admincli	0		\N		LDAP	\N	srv	y
\.


--
-- Data for Name: usersavedselections; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.usersavedselections (metadatauuid, selectionid, userid) FROM stdin;
\.


--
-- Data for Name: validation; Type: TABLE DATA; Schema: public; Owner: user_p40
--

COPY public.validation (metadataid, valtype, status, tested, failed, valdate, required) FROM stdin;
10	schematron-rules-geonetwork	1	1	0	2012-04-11T13:53:41	\N
10	xsd	0	0	0	2012-04-11T13:53:41	\N
10	schematron-rules-iso	1	41	0	2012-04-11T13:53:41	\N
2	schematron-rules-geonetwork	1	1	0	2012-04-11T13:54:36	\N
2	xsd	0	0	0	2012-04-11T13:54:36	\N
2	schematron-rules-inspire	0	20	7	2012-04-11T13:54:36	\N
2	schematron-rules-iso	0	44	3	2012-04-11T13:54:36	\N
9	schematron-rules-geonetwork	1	1	0	2012-04-11T13:48:57	t
9	xsd	0	0	0	2012-04-11T13:48:57	t
9	schematron-rules-inspire	1	38	0	2012-04-11T13:48:57	t
9	schematron-rules-iso	1	49	0	2012-04-11T13:48:57	t
\.


--
-- Name: bdterr_champ_type bdterr_champ_type_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_champ_type
    ADD CONSTRAINT bdterr_champ_type_pkey PRIMARY KEY (champtype_id);


--
-- Name: bdterr_contenus_tiers bdterr_contenus_tiers_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_contenus_tiers
    ADD CONSTRAINT bdterr_contenus_tiers_pkey PRIMARY KEY (contenu_id);


--
-- Name: bdterr_couche_champ bdterr_couche_champ_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_couche_champ
    ADD CONSTRAINT bdterr_couche_champ_pkey PRIMARY KEY (champ_id);


--
-- Name: bdterr_donnee bdterr_donnee_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_donnee
    ADD CONSTRAINT bdterr_donnee_pkey PRIMARY KEY (donnee_id);


--
-- Name: bdterr_lot bdterr_lot_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_lot
    ADD CONSTRAINT bdterr_lot_pkey PRIMARY KEY (lot_id);


--
-- Name: bdterr_rapports bdterr_rapports_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_rapports
    ADD CONSTRAINT bdterr_rapports_pkey PRIMARY KEY (rapport_id);


--
-- Name: bdterr_rapports_profils bdterr_rapports_profils_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_rapports_profils
    ADD CONSTRAINT bdterr_rapports_profils_pkey PRIMARY KEY (id);


--
-- Name: bdterr_referentiel_carto bdterr_referentiel_carto_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_referentiel_carto
    ADD CONSTRAINT bdterr_referentiel_carto_pkey PRIMARY KEY (referentiel_id);


--
-- Name: bdterr_referentiel_intersection bdterr_referentiel_intersection_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_referentiel_intersection
    ADD CONSTRAINT bdterr_referentiel_intersection_pkey PRIMARY KEY (referentiel_id);


--
-- Name: bdterr_referentiel_recherche bdterr_referentiel_recherche_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_referentiel_recherche
    ADD CONSTRAINT bdterr_referentiel_recherche_pkey PRIMARY KEY (referentiel_id);


--
-- Name: bdterr_themes bdterr_themes_pkey; Type: CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_themes
    ADD CONSTRAINT bdterr_themes_pkey PRIMARY KEY (theme_id);


--
-- Name: prodige_carto_colors carto_colors_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_carto_colors
    ADD CONSTRAINT carto_colors_pkey PRIMARY KEY (prodige_carto_color_id);


--
-- Name: prodige_colors colors_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_colors
    ADD CONSTRAINT colors_pkey PRIMARY KEY (prodige_color_id);


--
-- Name: competence_accede_carte competence_accede_carte_pk; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.competence_accede_carte
    ADD CONSTRAINT competence_accede_carte_pk PRIMARY KEY (pk_competence_accede_carte);


--
-- Name: competence_accede_couche competence_accede_couche_pk; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.competence_accede_couche
    ADD CONSTRAINT competence_accede_couche_pk PRIMARY KEY (pk_competence_accede_couche);


--
-- Name: competence competence_pk; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.competence
    ADD CONSTRAINT competence_pk PRIMARY KEY (pk_competence_id);


--
-- Name: execution_report execution_report_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.execution_report
    ADD CONSTRAINT execution_report_pkey PRIMARY KEY (id);


--
-- Name: prodige_fonts fonts_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_fonts
    ADD CONSTRAINT fonts_pkey PRIMARY KEY (font_id);


--
-- Name: prodige_geosource_colors geosource_colors_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_geosource_colors
    ADD CONSTRAINT geosource_colors_pkey PRIMARY KEY (prodige_geosource_color_id);


--
-- Name: grp_accede_competence grp_accede_competence_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_competence
    ADD CONSTRAINT grp_accede_competence_pkey PRIMARY KEY (pk_grp_accede_competence);


--
-- Name: grp_trt_objet grp_trt_objet_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_trt_objet
    ADD CONSTRAINT grp_trt_objet_pkey PRIMARY KEY (pk_grp_trt_objet);


--
-- Name: harves_opendata_node_params harves_opendata_node_params_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.harves_opendata_node_params
    ADD CONSTRAINT harves_opendata_node_params_pkey PRIMARY KEY (pk_params_id);


--
-- Name: harves_opendata_node harves_opendata_node_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.harves_opendata_node
    ADD CONSTRAINT harves_opendata_node_pkey PRIMARY KEY (pk_node_id);


--
-- Name: perimetre perimetre_pk; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.perimetre
    ADD CONSTRAINT perimetre_pk PRIMARY KEY (pk_perimetre_id);


--
-- Name: acces_serveur pk_acces_serveur; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.acces_serveur
    ADD CONSTRAINT pk_acces_serveur PRIMARY KEY (pk_acces_serveur);


--
-- Name: couche_donnees pk_couche_donnees; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.couche_donnees
    ADD CONSTRAINT pk_couche_donnees PRIMARY KEY (pk_couche_donnees);


--
-- Name: fiche_metadonnees pk_fiche_metadonnees; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.fiche_metadonnees
    ADD CONSTRAINT pk_fiche_metadonnees PRIMARY KEY (pk_fiche_metadonnees);


--
-- Name: grp_autorise_trt pk_grp_autorise_trt; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_autorise_trt
    ADD CONSTRAINT pk_grp_autorise_trt PRIMARY KEY (pk_grp_autorise_trt);


--
-- Name: objet_type pk_objet_type_id_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.objet_type
    ADD CONSTRAINT pk_objet_type_id_pkey PRIMARY KEY (pk_objet_type_id);


--
-- Name: carte_projet pk_pk_carte_projet; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.carte_projet
    ADD CONSTRAINT pk_pk_carte_projet PRIMARY KEY (pk_carte_projet);


--
-- Name: domaine pk_pk_domaine; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.domaine
    ADD CONSTRAINT pk_pk_domaine PRIMARY KEY (pk_domaine);


--
-- Name: groupe_profil pk_pk_groupe_profil; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.groupe_profil
    ADD CONSTRAINT pk_pk_groupe_profil PRIMARY KEY (pk_groupe_profil);


--
-- Name: grp_accede_dom pk_pk_grp_accede_dom; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_dom
    ADD CONSTRAINT pk_pk_grp_accede_dom PRIMARY KEY (pk_grp_accede_dom);


--
-- Name: grp_accede_ssdom pk_pk_grp_accede_ssdom; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_ssdom
    ADD CONSTRAINT pk_pk_grp_accede_ssdom PRIMARY KEY (pk_grp_accede_ssdom);


--
-- Name: grp_regroupe_usr pk_pk_grp_regroupe_usr; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_regroupe_usr
    ADD CONSTRAINT pk_pk_grp_regroupe_usr PRIMARY KEY (pk_grp_regroupe_usr);


--
-- Name: sous_domaine pk_pk_sous_domaine; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.sous_domaine
    ADD CONSTRAINT pk_pk_sous_domaine PRIMARY KEY (pk_sous_domaine);


--
-- Name: stockage_carte pk_pk_stockage_carte; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.stockage_carte
    ADD CONSTRAINT pk_pk_stockage_carte PRIMARY KEY (pk_stockage_carte);


--
-- Name: traitement pk_pk_traitement; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.traitement
    ADD CONSTRAINT pk_pk_traitement PRIMARY KEY (pk_traitement);


--
-- Name: utilisateur pk_pk_utilisateur; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.utilisateur
    ADD CONSTRAINT pk_pk_utilisateur PRIMARY KEY (pk_utilisateur);


--
-- Name: prodige_external_access pk_prodige_external_access; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_external_access
    ADD CONSTRAINT pk_prodige_external_access PRIMARY KEY (pk_prodige_external_access);


--
-- Name: prodige_help pk_prodige_help; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_help
    ADD CONSTRAINT pk_prodige_help PRIMARY KEY (pk_prodige_help_id);


--
-- Name: prodige_help_group pk_prodige_help_group; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_help_group
    ADD CONSTRAINT pk_prodige_help_group PRIMARY KEY (pk_prodige_help_group);


--
-- Name: prodige_settings pk_prodige_settings; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_settings
    ADD CONSTRAINT pk_prodige_settings PRIMARY KEY (pk_prodige_settings_id);


--
-- Name: rawgraph_couche_config pk_rawgraph_couche_config; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.rawgraph_couche_config
    ADD CONSTRAINT pk_rawgraph_couche_config PRIMARY KEY (id);


--
-- Name: ssdom_dispose_couche pk_ssdom_dispose_couche; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_dispose_couche
    ADD CONSTRAINT pk_ssdom_dispose_couche PRIMARY KEY (pk_ssdom_dispose_couche);


--
-- Name: ssdom_dispose_metadata pk_ssdom_dispose_metadata; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_dispose_metadata
    ADD CONSTRAINT pk_ssdom_dispose_metadata PRIMARY KEY (pk_ssdom_dispose_metadata);


--
-- Name: ssdom_visualise_carte pk_ssdom_visualise_carte; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_visualise_carte
    ADD CONSTRAINT pk_ssdom_visualise_carte PRIMARY KEY (pk_ssdom_visualise_carte);


--
-- Name: standards_conformite pk_standards_conformite; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.standards_conformite
    ADD CONSTRAINT pk_standards_conformite PRIMARY KEY (metadata_id);


--
-- Name: standards_fournisseur pk_standards_fournisseur; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.standards_fournisseur
    ADD CONSTRAINT pk_standards_fournisseur PRIMARY KEY (fournisseur_id);


--
-- Name: standards_standard pk_standards_standard; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.standards_standard
    ADD CONSTRAINT pk_standards_standard PRIMARY KEY (standard_id);


--
-- Name: tache_import_donnees pk_tache_import_donnees; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.tache_import_donnees
    ADD CONSTRAINT pk_tache_import_donnees PRIMARY KEY (pk_tache_import_donnees);


--
-- Name: trt_objet pk_trt_objet_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.trt_objet
    ADD CONSTRAINT pk_trt_objet_pkey PRIMARY KEY (pk_trt_objet);


--
-- Name: prodige_param pkey_prodige_param; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_param
    ADD CONSTRAINT pkey_prodige_param PRIMARY KEY (pk_prodige_param);


--
-- Name: prodige_session_user prodige_session_user_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_session_user
    ADD CONSTRAINT prodige_session_user_pkey PRIMARY KEY (pk_session_user);


--
-- Name: raster_info raster_info_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.raster_info
    ADD CONSTRAINT raster_info_pkey PRIMARY KEY (id);


--
-- Name: prodige_database_requests request_pk; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_database_requests
    ADD CONSTRAINT request_pk PRIMARY KEY (prodige_database_request_id);


--
-- Name: rubric_param rubric_param_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.rubric_param
    ADD CONSTRAINT rubric_param_pkey PRIMARY KEY (rubric_id);


--
-- Name: scheduled_command scheduled_command_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.scheduled_command
    ADD CONSTRAINT scheduled_command_pkey PRIMARY KEY (id);


--
-- Name: ssdom_visualise_carte ssdom_visualise_carte_ssdcart_fk_carte_projet_key; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_visualise_carte
    ADD CONSTRAINT ssdom_visualise_carte_ssdcart_fk_carte_projet_key UNIQUE (ssdcart_fk_carte_projet, ssdcart_fk_sous_domaine);


--
-- Name: trt_autorise_objet trt_autorise_objet_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.trt_autorise_objet
    ADD CONSTRAINT trt_autorise_objet_pkey PRIMARY KEY (pk_trt_autorise_objet);


--
-- Name: acces_serveur un_accs_id; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.acces_serveur
    ADD CONSTRAINT un_accs_id UNIQUE (accs_id);


--
-- Name: carte_projet un_cartp_id; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.carte_projet
    ADD CONSTRAINT un_cartp_id UNIQUE (cartp_id);


--
-- Name: couche_donnees un_couchd_id; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.couche_donnees
    ADD CONSTRAINT un_couchd_id UNIQUE (couchd_id);


--
-- Name: domaine un_dom_id; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.domaine
    ADD CONSTRAINT un_dom_id UNIQUE (dom_id);


--
-- Name: domaine un_dom_nom; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.domaine
    ADD CONSTRAINT un_dom_nom UNIQUE (dom_nom);


--
-- Name: grp_accede_dom un_grp_accede_dom_fks; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_dom
    ADD CONSTRAINT un_grp_accede_dom_fks UNIQUE (grpdom_fk_domaine, grpdom_fk_groupe_profil);


--
-- Name: grp_accede_ssdom un_grp_accede_ssdom_fks; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_ssdom
    ADD CONSTRAINT un_grp_accede_ssdom_fks UNIQUE (grpss_fk_sous_domaine, grpss_fk_groupe_profil);


--
-- Name: groupe_profil un_grp_id; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.groupe_profil
    ADD CONSTRAINT un_grp_id UNIQUE (grp_id);


--
-- Name: groupe_profil un_grp_nom; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.groupe_profil
    ADD CONSTRAINT un_grp_nom UNIQUE (grp_nom);


--
-- Name: grp_regroupe_usr un_grp_regroupe_usr_fks; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_regroupe_usr
    ADD CONSTRAINT un_grp_regroupe_usr_fks UNIQUE (grpusr_fk_groupe_profil, grpusr_fk_utilisateur);


--
-- Name: ssdom_dispose_couche un_ssdom_dispose_couche_fks; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_dispose_couche
    ADD CONSTRAINT un_ssdom_dispose_couche_fks UNIQUE (ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine);


--
-- Name: ssdom_dispose_metadata un_ssdom_dispose_metadata_fks; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_dispose_metadata
    ADD CONSTRAINT un_ssdom_dispose_metadata_fks UNIQUE (uuid, ssdcouch_fk_sous_domaine);


--
-- Name: sous_domaine un_ssdom_id; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.sous_domaine
    ADD CONSTRAINT un_ssdom_id UNIQUE (ssdom_id);


--
-- Name: sous_domaine un_ssdom_nom; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.sous_domaine
    ADD CONSTRAINT un_ssdom_nom UNIQUE (ssdom_nom);


--
-- Name: stockage_carte un_stkcard_id; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.stockage_carte
    ADD CONSTRAINT un_stkcard_id UNIQUE (stkcard_id);


--
-- Name: traitement un_trt_id; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.traitement
    ADD CONSTRAINT un_trt_id UNIQUE (trt_id);


--
-- Name: traitement un_trt_nom; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.traitement
    ADD CONSTRAINT un_trt_nom UNIQUE (trt_nom);


--
-- Name: utilisateur un_usr_id; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.utilisateur
    ADD CONSTRAINT un_usr_id UNIQUE (usr_id);


--
-- Name: usr_accede_perimetre_edition usr_accede_perimetre_edit_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.usr_accede_perimetre_edition
    ADD CONSTRAINT usr_accede_perimetre_edit_pkey PRIMARY KEY (pk_usr_accede_perimetre_edition);


--
-- Name: usr_alerte_perimetre_edition usr_alerte_perim_edit_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition
    ADD CONSTRAINT usr_alerte_perim_edit_pkey PRIMARY KEY (pk_usr_alerte_perimetre_edition);


--
-- Name: utilisateur_carte utilisateur_carte_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.utilisateur_carte
    ADD CONSTRAINT utilisateur_carte_pkey PRIMARY KEY (id);


--
-- Name: zonage zonage_pk; Type: CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.zonage
    ADD CONSTRAINT zonage_pk PRIMARY KEY (pk_zonage_id);


--
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: categories categories_name_key; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_name_key UNIQUE (name);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: categoriesdes categoriesdes_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.categoriesdes
    ADD CONSTRAINT categoriesdes_pkey PRIMARY KEY (iddes, langid);


--
-- Name: cswservercapabilitiesinfo cswservercapabilitiesinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.cswservercapabilitiesinfo
    ADD CONSTRAINT cswservercapabilitiesinfo_pkey PRIMARY KEY (idfield);


--
-- Name: email email_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.email
    ADD CONSTRAINT email_pkey PRIMARY KEY (user_id);


--
-- Name: files files_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT files_pkey PRIMARY KEY (_id);


--
-- Name: groups groups_name_key; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_name_key UNIQUE (name);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: groupsdes groupsdes_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.groupsdes
    ADD CONSTRAINT groupsdes_pkey PRIMARY KEY (iddes, langid);


--
-- Name: harvesterdata harvesterdata_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.harvesterdata
    ADD CONSTRAINT harvesterdata_pkey PRIMARY KEY (harvesteruuid, key);


--
-- Name: harvestersettings harvestersettings_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.harvestersettings
    ADD CONSTRAINT harvestersettings_pkey PRIMARY KEY (id);


--
-- Name: harvesthistory harvesthistory_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.harvesthistory
    ADD CONSTRAINT harvesthistory_pkey PRIMARY KEY (id);


--
-- Name: inspireatomfeed inspireatomfeed_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.inspireatomfeed
    ADD CONSTRAINT inspireatomfeed_pkey PRIMARY KEY (id);


--
-- Name: isolanguages isolanguages_code_key; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.isolanguages
    ADD CONSTRAINT isolanguages_code_key UNIQUE (code);


--
-- Name: isolanguages isolanguages_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.isolanguages
    ADD CONSTRAINT isolanguages_pkey PRIMARY KEY (id);


--
-- Name: isolanguagesdes isolanguagesdes_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.isolanguagesdes
    ADD CONSTRAINT isolanguagesdes_pkey PRIMARY KEY (iddes, langid);


--
-- Name: languages languages_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.languages
    ADD CONSTRAINT languages_pkey PRIMARY KEY (id);


--
-- Name: mapservers mapservers_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.mapservers
    ADD CONSTRAINT mapservers_pkey PRIMARY KEY (id);


--
-- Name: metadata metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadata
    ADD CONSTRAINT metadata_pkey PRIMARY KEY (id);


--
-- Name: metadata metadata_uuid_key; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadata
    ADD CONSTRAINT metadata_uuid_key UNIQUE (uuid);


--
-- Name: metadatacateg metadatacateg_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatacateg
    ADD CONSTRAINT metadatacateg_pkey PRIMARY KEY (metadataid, categoryid);


--
-- Name: metadatafiledownloads metadatafiledownloads_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatafiledownloads
    ADD CONSTRAINT metadatafiledownloads_pkey PRIMARY KEY (id);


--
-- Name: metadatafileuploads metadatafileuploads_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatafileuploads
    ADD CONSTRAINT metadatafileuploads_pkey PRIMARY KEY (id);


--
-- Name: metadataidentifiertemplate metadataidentifiertemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadataidentifiertemplate
    ADD CONSTRAINT metadataidentifiertemplate_pkey PRIMARY KEY (id);


--
-- Name: metadatanotifications metadatanotifications_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatanotifications
    ADD CONSTRAINT metadatanotifications_pkey PRIMARY KEY (metadataid, notifierid);


--
-- Name: metadatanotifiers metadatanotifiers_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatanotifiers
    ADD CONSTRAINT metadatanotifiers_pkey PRIMARY KEY (id);


--
-- Name: metadatarating metadatarating_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatarating
    ADD CONSTRAINT metadatarating_pkey PRIMARY KEY (metadataid, ipaddress);


--
-- Name: metadatastatus metadatastatus_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatastatus
    ADD CONSTRAINT metadatastatus_pkey PRIMARY KEY (metadataid, statusid, userid, changedate);


--
-- Name: operationallowed operationallowed_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.operationallowed
    ADD CONSTRAINT operationallowed_pkey PRIMARY KEY (groupid, metadataid, operationid);


--
-- Name: operations operations_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.operations
    ADD CONSTRAINT operations_pkey PRIMARY KEY (id);


--
-- Name: operationsdes operationsdes_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.operationsdes
    ADD CONSTRAINT operationsdes_pkey PRIMARY KEY (iddes, langid);


--
-- Name: params params_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.params
    ADD CONSTRAINT params_pkey PRIMARY KEY (id);


--
-- Name: regions regions_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (id);


--
-- Name: regionsdes regionsdes_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.regionsdes
    ADD CONSTRAINT regionsdes_pkey PRIMARY KEY (iddes, langid);


--
-- Name: relations relations_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT relations_pkey PRIMARY KEY (id, relatedid);


--
-- Name: requests requests_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.requests
    ADD CONSTRAINT requests_pkey PRIMARY KEY (id);


--
-- Name: schematron schematron_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.schematron
    ADD CONSTRAINT schematron_pkey PRIMARY KEY (id);


--
-- Name: schematroncriteria schematroncriteria_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.schematroncriteria
    ADD CONSTRAINT schematroncriteria_pkey PRIMARY KEY (id);


--
-- Name: schematroncriteriagroup schematroncriteriagroup_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.schematroncriteriagroup
    ADD CONSTRAINT schematroncriteriagroup_pkey PRIMARY KEY (name, schematronid);


--
-- Name: schematrondes schematrondes_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.schematrondes
    ADD CONSTRAINT schematrondes_pkey PRIMARY KEY (iddes, langid);


--
-- Name: selections selections_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.selections
    ADD CONSTRAINT selections_pkey PRIMARY KEY (id);


--
-- Name: selectionsdes selectionsdes_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.selectionsdes
    ADD CONSTRAINT selectionsdes_pkey PRIMARY KEY (iddes, langid);


--
-- Name: serviceparameters serviceparameters_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.serviceparameters
    ADD CONSTRAINT serviceparameters_pkey PRIMARY KEY (id);


--
-- Name: services services_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.services
    ADD CONSTRAINT services_pkey PRIMARY KEY (id);


--
-- Name: settings settings_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (name);


--
-- Name: sources sources_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.sources
    ADD CONSTRAINT sources_pkey PRIMARY KEY (uuid);


--
-- Name: sourcesdes sourcesdes_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.sourcesdes
    ADD CONSTRAINT sourcesdes_pkey PRIMARY KEY (iddes, langid);


--
-- Name: spatialindex spatialindex_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.spatialindex
    ADD CONSTRAINT spatialindex_pkey PRIMARY KEY (fid);


--
-- Name: statusvalues statusvalues_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.statusvalues
    ADD CONSTRAINT statusvalues_pkey PRIMARY KEY (id);


--
-- Name: statusvaluesdes statusvaluesdes_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.statusvaluesdes
    ADD CONSTRAINT statusvaluesdes_pkey PRIMARY KEY (iddes, langid);


--
-- Name: thesaurus thesaurus_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.thesaurus
    ADD CONSTRAINT thesaurus_pkey PRIMARY KEY (id);


--
-- Name: useraddress uk_8te6nqcuovmv45ej1oej73hg3; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.useraddress
    ADD CONSTRAINT uk_8te6nqcuovmv45ej1oej73hg3 UNIQUE (addressid);


--
-- Name: schematron uk_k7c29i3x0x6p5hbvb0qsdmuek; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.schematron
    ADD CONSTRAINT uk_k7c29i3x0x6p5hbvb0qsdmuek UNIQUE (schemaname, filename);


--
-- Name: schematron uk_mlb74anygnag9es7yexj3qj0d; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.schematron
    ADD CONSTRAINT uk_mlb74anygnag9es7yexj3qj0d UNIQUE (schemaname, filename);


--
-- Name: useraddress useraddress_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.useraddress
    ADD CONSTRAINT useraddress_pkey PRIMARY KEY (userid, addressid);


--
-- Name: usergroups usergroups_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.usergroups
    ADD CONSTRAINT usergroups_pkey PRIMARY KEY (userid, groupid, profile);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- Name: usersavedselections usersavedselections_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.usersavedselections
    ADD CONSTRAINT usersavedselections_pkey PRIMARY KEY (metadatauuid, selectionid, userid);


--
-- Name: validation validation_pkey; Type: CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.validation
    ADD CONSTRAINT validation_pkey PRIMARY KEY (metadataid, valtype);


--
-- Name: idx_30055129141f7bf0; Type: INDEX; Schema: bdterr; Owner: user_p40
--

CREATE INDEX idx_30055129141f7bf0 ON bdterr.bdterr_donnee USING btree (donnee_lot_id);


--
-- Name: idx_4674d3dfa9ee0a03; Type: INDEX; Schema: bdterr; Owner: user_p40
--

CREATE INDEX idx_4674d3dfa9ee0a03 ON bdterr.bdterr_lot USING btree (lot_referentiel_id);


--
-- Name: idx_4674d3dffbc885b3; Type: INDEX; Schema: bdterr; Owner: user_p40
--

CREATE INDEX idx_4674d3dffbc885b3 ON bdterr.bdterr_lot USING btree (lot_theme_id);


--
-- Name: idx_4819e6af8ae06949; Type: INDEX; Schema: bdterr; Owner: user_p40
--

CREATE INDEX idx_4819e6af8ae06949 ON bdterr.bdterr_contenus_tiers USING btree (contenu_type);


--
-- Name: idx_4819e6afa8cba5f7; Type: INDEX; Schema: bdterr; Owner: user_p40
--

CREATE INDEX idx_4819e6afa8cba5f7 ON bdterr.bdterr_contenus_tiers USING btree (lot_id);


--
-- Name: idx_51faf077d1eae64c; Type: INDEX; Schema: bdterr; Owner: user_p40
--

CREATE INDEX idx_51faf077d1eae64c ON bdterr.bdterr_rapports_profils USING btree (bterr_rapport_id);


--
-- Name: idx_52a0ca003dc30acd; Type: INDEX; Schema: bdterr; Owner: user_p40
--

CREATE INDEX idx_52a0ca003dc30acd ON bdterr.bdterr_couche_champ USING btree (champ_lot_id);


--
-- Name: idx_52a0ca00d43aa457; Type: INDEX; Schema: bdterr; Owner: user_p40
--

CREATE INDEX idx_52a0ca00d43aa457 ON bdterr.bdterr_couche_champ USING btree (champ_type);


--
-- Name: idx_a06003abedb65433; Type: INDEX; Schema: bdterr; Owner: user_p40
--

CREATE INDEX idx_a06003abedb65433 ON bdterr.bdterr_themes USING btree (theme_parent);


--
-- Name: competence_accede_carte_pkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE UNIQUE INDEX competence_accede_carte_pkey ON catalogue.competence_accede_carte USING btree (pk_competence_accede_carte);


--
-- Name: competence_accede_couche_pkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE UNIQUE INDEX competence_accede_couche_pkey ON catalogue.competence_accede_couche USING btree (pk_competence_accede_couche);


--
-- Name: competence_pkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE UNIQUE INDEX competence_pkey ON catalogue.competence USING btree (pk_competence_id);


--
-- Name: competencecarte_fk_carte_projet_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX competencecarte_fk_carte_projet_fkey ON catalogue.competence_accede_carte USING btree (competencecarte_fk_carte_projet);


--
-- Name: competencecarte_fk_competence_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX competencecarte_fk_competence_id_fkey ON catalogue.competence_accede_carte USING btree (competencecarte_fk_competence_id);


--
-- Name: competencecouche_fk_competence_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX competencecouche_fk_competence_id_fkey ON catalogue.competence_accede_couche USING btree (competencecouche_fk_competence_id);


--
-- Name: competencecouche_fk_couche_donnees_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX competencecouche_fk_couche_donnees_fkey ON catalogue.competence_accede_couche USING btree (competencecouche_fk_couche_donnees);


--
-- Name: couche_donnees_zonage_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX couche_donnees_zonage_fkey ON catalogue.couche_donnees USING btree (couchd_zonageid_fk);


--
-- Name: fk_competence_id_fkey_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fk_competence_id_fkey_fkey ON catalogue.grp_accede_competence USING btree (fk_competence_id);


--
-- Name: fk_grp_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fk_grp_id_fkey ON catalogue.grp_accede_competence USING btree (fk_grp_id);


--
-- Name: fk_hlpgrp_fk_groupe_profil_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fk_hlpgrp_fk_groupe_profil_fkey ON catalogue.prodige_help_group USING btree (hlpgrp_fk_groupe_profil);


--
-- Name: fk_hlpgrp_fk_help_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fk_hlpgrp_fk_help_id_fkey ON catalogue.prodige_help_group USING btree (hlpgrp_fk_help_id);


--
-- Name: fkey_usr_accede_perim_edit_usrperim_fk_perimetre_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fkey_usr_accede_perim_edit_usrperim_fk_perimetre_fkey ON catalogue.usr_accede_perimetre_edition USING btree (usrperim_fk_perimetre);


--
-- Name: fkey_usr_accede_perim_edit_usrperim_fk_utilisateur_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fkey_usr_accede_perim_edit_usrperim_fk_utilisateur_fkey ON catalogue.usr_accede_perimetre_edition USING btree (usrperim_fk_utilisateur);


--
-- Name: fkey_usr_accede_perim_usrperim_fk_perimetre_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fkey_usr_accede_perim_usrperim_fk_perimetre_fkey ON catalogue.usr_accede_perimetre USING btree (usrperim_fk_perimetre);


--
-- Name: fkey_usr_accede_perim_usrperim_fk_utilisateur_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fkey_usr_accede_perim_usrperim_fk_utilisateur_fkey ON catalogue.usr_accede_perimetre USING btree (usrperim_fk_utilisateur);


--
-- Name: fkey_usr_alerte_perim_edit_fk_perimetre_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fkey_usr_alerte_perim_edit_fk_perimetre_fkey ON catalogue.usr_alerte_perimetre_edition USING btree (usralert_fk_perimetre);


--
-- Name: fkey_usr_alerte_perim_edit_usrperim_fk_couchedonnees_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fkey_usr_alerte_perim_edit_usrperim_fk_couchedonnees_fkey ON catalogue.usr_alerte_perimetre_edition USING btree (usralert_fk_couchedonnees);


--
-- Name: fkey_usr_alerte_perim_edit_usrperim_fk_utilisateur_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX fkey_usr_alerte_perim_edit_usrperim_fk_utilisateur_fkey ON catalogue.usr_alerte_perimetre_edition USING btree (usralert_fk_utilisateur);


--
-- Name: grptrtobj_fk_grp_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX grptrtobj_fk_grp_id_fkey ON catalogue.grp_trt_objet USING btree (grptrtobj_fk_grp_id);


--
-- Name: grptrtobj_fk_obj_type_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX grptrtobj_fk_obj_type_id_fkey ON catalogue.grp_trt_objet USING btree (grptrtobj_fk_obj_type_id);


--
-- Name: grptrtobj_fk_objet_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX grptrtobj_fk_objet_id_fkey ON catalogue.grp_trt_objet USING btree (grptrtobj_fk_objet_id);


--
-- Name: grptrtobj_fk_trt_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX grptrtobj_fk_trt_id_fkey ON catalogue.grp_trt_objet USING btree (grptrtobj_fk_trt_id);


--
-- Name: idx_8e1b7bbcad93e023; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX idx_8e1b7bbcad93e023 ON catalogue.execution_report USING btree (scheduled_command_id);


--
-- Name: idx_utilisateur_carte_fk_stockage_carte; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX idx_utilisateur_carte_fk_stockage_carte ON catalogue.utilisateur_carte USING btree (fk_stockage_carte);


--
-- Name: idx_utilisateur_carte_fk_utilisateur; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX idx_utilisateur_carte_fk_utilisateur ON catalogue.utilisateur_carte USING btree (fk_utilisateur);


--
-- Name: perimetre_fk_zonage_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX perimetre_fk_zonage_fkey ON catalogue.perimetre USING btree (perimetre_zonage_id);


--
-- Name: perimetre_pkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE UNIQUE INDEX perimetre_pkey ON catalogue.perimetre USING btree (pk_perimetre_id);


--
-- Name: pk_objet_type_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE UNIQUE INDEX pk_objet_type_id_fkey ON catalogue.objet_type USING btree (pk_objet_type_id);


--
-- Name: pk_usr_accede_perimetre_edit_pkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX pk_usr_accede_perimetre_edit_pkey ON catalogue.usr_accede_perimetre_edition USING btree (pk_usr_accede_perimetre_edition);


--
-- Name: pk_usr_accede_perimetre_pkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX pk_usr_accede_perimetre_pkey ON catalogue.usr_accede_perimetre USING btree (pk_usr_accede_perimetre);


--
-- Name: pk_usr_alerte_perim_edit_pkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX pk_usr_alerte_perim_edit_pkey ON catalogue.usr_alerte_perimetre_edition USING btree (pk_usr_alerte_perimetre_edition);


--
-- Name: prodige_extaccess_fk_utilisateur_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX prodige_extaccess_fk_utilisateur_fkey ON catalogue.prodige_external_access USING btree (prodige_extaccess_fk_utilisateur);


--
-- Name: trtautoobjet_fk_obj_type_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX trtautoobjet_fk_obj_type_id_fkey ON catalogue.trt_autorise_objet USING btree (trtautoobjet_fk_obj_type_id);


--
-- Name: trtautoobjet_fk_trt_id_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX trtautoobjet_fk_trt_id_fkey ON catalogue.trt_autorise_objet USING btree (trtautoobjet_fk_trt_id);


--
-- Name: zonage_pkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE UNIQUE INDEX zonage_pkey ON catalogue.zonage USING btree (pk_zonage_id);


--
-- Name: zonaget_fk_couche_donnees_fkey; Type: INDEX; Schema: catalogue; Owner: user_p40
--

CREATE INDEX zonaget_fk_couche_donnees_fkey ON catalogue.zonage USING btree (zonage_fk_couche_donnees);


--
-- Name: harvesthistoryndx1; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX harvesthistoryndx1 ON public.harvesthistory USING btree (harvestdate);


--
-- Name: metadatandx1; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX metadatandx1 ON public.metadata USING btree (source);


--
-- Name: paramsndx1; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX paramsndx1 ON public.params USING btree (requestid);


--
-- Name: paramsndx2; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX paramsndx2 ON public.params USING btree (querytype);


--
-- Name: paramsndx3; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX paramsndx3 ON public.params USING btree (termfield);


--
-- Name: paramsndx4; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX paramsndx4 ON public.params USING btree (termtext);


--
-- Name: requestsndx1; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX requestsndx1 ON public.requests USING btree (requestdate);


--
-- Name: requestsndx2; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX requestsndx2 ON public.requests USING btree (ip);


--
-- Name: requestsndx3; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX requestsndx3 ON public.requests USING btree (hits);


--
-- Name: requestsndx4; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX requestsndx4 ON public.requests USING btree (lang);


--
-- Name: spatialindexndx1; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX spatialindexndx1 ON public.spatialindex USING btree (id);


--
-- Name: spatialindexndx2; Type: INDEX; Schema: public; Owner: user_p40
--

CREATE INDEX spatialindexndx2 ON public.spatialindex USING gist (the_geom);


--
-- Name: bdterr_donnee fk_30055129141f7bf0; Type: FK CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_donnee
    ADD CONSTRAINT fk_30055129141f7bf0 FOREIGN KEY (donnee_lot_id) REFERENCES bdterr.bdterr_lot(lot_id);


--
-- Name: bdterr_lot fk_4674d3dfa9ee0a03; Type: FK CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_lot
    ADD CONSTRAINT fk_4674d3dfa9ee0a03 FOREIGN KEY (lot_referentiel_id) REFERENCES bdterr.bdterr_referentiel_intersection(referentiel_id);


--
-- Name: bdterr_lot fk_4674d3dffbc885b3; Type: FK CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_lot
    ADD CONSTRAINT fk_4674d3dffbc885b3 FOREIGN KEY (lot_theme_id) REFERENCES bdterr.bdterr_themes(theme_id);


--
-- Name: bdterr_contenus_tiers fk_4819e6af8ae06949; Type: FK CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_contenus_tiers
    ADD CONSTRAINT fk_4819e6af8ae06949 FOREIGN KEY (contenu_type) REFERENCES bdterr.bdterr_champ_type(champtype_id);


--
-- Name: bdterr_contenus_tiers fk_4819e6afa8cba5f7; Type: FK CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_contenus_tiers
    ADD CONSTRAINT fk_4819e6afa8cba5f7 FOREIGN KEY (lot_id) REFERENCES bdterr.bdterr_lot(lot_id);


--
-- Name: bdterr_rapports_profils fk_51faf077d1eae64c; Type: FK CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_rapports_profils
    ADD CONSTRAINT fk_51faf077d1eae64c FOREIGN KEY (bterr_rapport_id) REFERENCES bdterr.bdterr_rapports(rapport_id);


--
-- Name: bdterr_couche_champ fk_52a0ca003dc30acd; Type: FK CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_couche_champ
    ADD CONSTRAINT fk_52a0ca003dc30acd FOREIGN KEY (champ_lot_id) REFERENCES bdterr.bdterr_lot(lot_id);


--
-- Name: bdterr_couche_champ fk_52a0ca00d43aa457; Type: FK CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_couche_champ
    ADD CONSTRAINT fk_52a0ca00d43aa457 FOREIGN KEY (champ_type) REFERENCES bdterr.bdterr_champ_type(champtype_id);


--
-- Name: bdterr_themes fk_a06003abedb65433; Type: FK CONSTRAINT; Schema: bdterr; Owner: user_p40
--

ALTER TABLE ONLY bdterr.bdterr_themes
    ADD CONSTRAINT fk_a06003abedb65433 FOREIGN KEY (theme_parent) REFERENCES bdterr.bdterr_themes(theme_id);


--
-- Name: carte_projet carte_projet_cartp_fk_fiche_metadonnees_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.carte_projet
    ADD CONSTRAINT carte_projet_cartp_fk_fiche_metadonnees_fkey FOREIGN KEY (cartp_fk_fiche_metadonnees) REFERENCES catalogue.fiche_metadonnees(pk_fiche_metadonnees);


--
-- Name: carte_projet carte_projet_cartp_fk_stockage_carte_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.carte_projet
    ADD CONSTRAINT carte_projet_cartp_fk_stockage_carte_fkey FOREIGN KEY (cartp_fk_stockage_carte) REFERENCES catalogue.stockage_carte(pk_stockage_carte) MATCH FULL ON DELETE CASCADE;


--
-- Name: prodige_param carto_colors_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_param
    ADD CONSTRAINT carto_colors_fkey FOREIGN KEY (prodige_carto_color_id) REFERENCES catalogue.prodige_carto_colors(prodige_carto_color_id);


--
-- Name: carte_projet cartp_fk_utilisateur; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.carte_projet
    ADD CONSTRAINT cartp_fk_utilisateur FOREIGN KEY (cartp_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) ON DELETE CASCADE;


--
-- Name: prodige_param colors_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_param
    ADD CONSTRAINT colors_fkey FOREIGN KEY (prodige_color_id) REFERENCES catalogue.prodige_colors(prodige_color_id);


--
-- Name: competence_accede_carte competencecarte_fk_carte_projet_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.competence_accede_carte
    ADD CONSTRAINT competencecarte_fk_carte_projet_fkey FOREIGN KEY (competencecarte_fk_carte_projet) REFERENCES catalogue.carte_projet(pk_carte_projet) MATCH FULL ON DELETE CASCADE;


--
-- Name: competence_accede_carte competencecarte_fk_competence_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.competence_accede_carte
    ADD CONSTRAINT competencecarte_fk_competence_id_fkey FOREIGN KEY (competencecarte_fk_competence_id) REFERENCES catalogue.competence(pk_competence_id);


--
-- Name: competence_accede_couche competencecouche_fk_competence_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.competence_accede_couche
    ADD CONSTRAINT competencecouche_fk_competence_id_fkey FOREIGN KEY (competencecouche_fk_competence_id) REFERENCES catalogue.competence(pk_competence_id);


--
-- Name: competence_accede_couche competencecouche_fk_couche_donnees_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.competence_accede_couche
    ADD CONSTRAINT competencecouche_fk_couche_donnees_fkey FOREIGN KEY (competencecouche_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) MATCH FULL ON DELETE CASCADE;


--
-- Name: couche_donnees couche_donnees_couchd_fk_acces_server_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.couche_donnees
    ADD CONSTRAINT couche_donnees_couchd_fk_acces_server_fkey FOREIGN KEY (couchd_fk_acces_server) REFERENCES catalogue.acces_serveur(pk_acces_serveur) MATCH FULL ON DELETE CASCADE;


--
-- Name: couche_donnees couche_donnees_fk_zonage_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.couche_donnees
    ADD CONSTRAINT couche_donnees_fk_zonage_fkey FOREIGN KEY (couchd_zonageid_fk) REFERENCES catalogue.zonage(pk_zonage_id);


--
-- Name: domaine dom_rubric_fk; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.domaine
    ADD CONSTRAINT dom_rubric_fk FOREIGN KEY (dom_rubric) REFERENCES catalogue.rubric_param(rubric_id);


--
-- Name: fiche_metadonnees fiche_metadonnees_fmeta_fk_couche_donnees_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.fiche_metadonnees
    ADD CONSTRAINT fiche_metadonnees_fmeta_fk_couche_donnees_fkey FOREIGN KEY (fmeta_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees);


--
-- Name: execution_report fk_8e1b7bbcad93e023; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.execution_report
    ADD CONSTRAINT fk_8e1b7bbcad93e023 FOREIGN KEY (scheduled_command_id) REFERENCES catalogue.scheduled_command(id);


--
-- Name: grp_accede_competence fk_competence_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_competence
    ADD CONSTRAINT fk_competence_id_fkey FOREIGN KEY (fk_competence_id) REFERENCES catalogue.competence(pk_competence_id);


--
-- Name: standards_conformite fk_conformite_metadata; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.standards_conformite
    ADD CONSTRAINT fk_conformite_metadata FOREIGN KEY (metadata_id) REFERENCES public.metadata(id);


--
-- Name: standards_conformite fk_conformite_standard; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.standards_conformite
    ADD CONSTRAINT fk_conformite_standard FOREIGN KEY (standard_id) REFERENCES catalogue.standards_standard(standard_id);


--
-- Name: grp_accede_competence fk_grp_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_competence
    ADD CONSTRAINT fk_grp_id_fkey FOREIGN KEY (fk_grp_id) REFERENCES catalogue.groupe_profil(pk_groupe_profil) ON DELETE CASCADE;


--
-- Name: perimetre fk_perimetre_zonage_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.perimetre
    ADD CONSTRAINT fk_perimetre_zonage_id_fkey FOREIGN KEY (perimetre_zonage_id) REFERENCES catalogue.zonage(pk_zonage_id) ON DELETE CASCADE;


--
-- Name: standards_standard fk_standard_fournisseur; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.standards_standard
    ADD CONSTRAINT fk_standard_fournisseur FOREIGN KEY (fournisseur_id) REFERENCES catalogue.standards_fournisseur(fournisseur_id);


--
-- Name: standards_standard fk_standard_metadata; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.standards_standard
    ADD CONSTRAINT fk_standard_metadata FOREIGN KEY (fmeta_id) REFERENCES public.metadata(id);


--
-- Name: stockage_carte fk_stk_server_acces_server; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.stockage_carte
    ADD CONSTRAINT fk_stk_server_acces_server FOREIGN KEY (stk_server) REFERENCES catalogue.acces_serveur(pk_acces_serveur) ON DELETE CASCADE;


--
-- Name: utilisateur_carte fk_utilisateur_carte_fk_stockage_carte; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.utilisateur_carte
    ADD CONSTRAINT fk_utilisateur_carte_fk_stockage_carte FOREIGN KEY (fk_stockage_carte) REFERENCES catalogue.stockage_carte(pk_stockage_carte);


--
-- Name: utilisateur_carte fk_utilisateur_carte_fk_utilisateur; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.utilisateur_carte
    ADD CONSTRAINT fk_utilisateur_carte_fk_utilisateur FOREIGN KEY (fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur);


--
-- Name: prodige_param fonts_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_param
    ADD CONSTRAINT fonts_fkey FOREIGN KEY (font_id) REFERENCES catalogue.prodige_fonts(font_id);


--
-- Name: prodige_param geosource_colors_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_param
    ADD CONSTRAINT geosource_colors_fkey FOREIGN KEY (prodige_geosource_color_id) REFERENCES catalogue.prodige_geosource_colors(prodige_geosource_color_id);


--
-- Name: grp_accede_dom grp_accede_dom_grpdom_fk_domaine_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_dom
    ADD CONSTRAINT grp_accede_dom_grpdom_fk_domaine_fkey FOREIGN KEY (grpdom_fk_domaine) REFERENCES catalogue.domaine(pk_domaine) MATCH FULL ON DELETE CASCADE;


--
-- Name: grp_accede_dom grp_accede_dom_grpdom_fk_groupe_profil_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_dom
    ADD CONSTRAINT grp_accede_dom_grpdom_fk_groupe_profil_fkey FOREIGN KEY (grpdom_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE;


--
-- Name: grp_accede_ssdom grp_accede_ssdom_grpss_fk_groupe_profil_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_ssdom
    ADD CONSTRAINT grp_accede_ssdom_grpss_fk_groupe_profil_fkey FOREIGN KEY (grpss_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE;


--
-- Name: grp_accede_ssdom grp_accede_ssdom_grpss_fk_sous_domaine_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_accede_ssdom
    ADD CONSTRAINT grp_accede_ssdom_grpss_fk_sous_domaine_fkey FOREIGN KEY (grpss_fk_sous_domaine) REFERENCES catalogue.sous_domaine(pk_sous_domaine) MATCH FULL ON DELETE CASCADE;


--
-- Name: grp_autorise_trt grp_autorise_trt_grptrt_fk_groupe_profil_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_autorise_trt
    ADD CONSTRAINT grp_autorise_trt_grptrt_fk_groupe_profil_fkey FOREIGN KEY (grptrt_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE;


--
-- Name: grp_autorise_trt grp_autorise_trt_grptrt_fk_traitement_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_autorise_trt
    ADD CONSTRAINT grp_autorise_trt_grptrt_fk_traitement_fkey FOREIGN KEY (grptrt_fk_traitement) REFERENCES catalogue.traitement(pk_traitement) MATCH FULL ON DELETE CASCADE;


--
-- Name: grp_regroupe_usr grp_regroupe_usr_grpusr_fk_groupe_profil_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_regroupe_usr
    ADD CONSTRAINT grp_regroupe_usr_grpusr_fk_groupe_profil_fkey FOREIGN KEY (grpusr_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE;


--
-- Name: grp_regroupe_usr grp_regroupe_usr_grpusr_fk_utilisateur_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_regroupe_usr
    ADD CONSTRAINT grp_regroupe_usr_grpusr_fk_utilisateur_fkey FOREIGN KEY (grpusr_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE;


--
-- Name: grp_trt_objet grptrtobj_fk_grp_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_trt_objet
    ADD CONSTRAINT grptrtobj_fk_grp_id_fkey FOREIGN KEY (grptrtobj_fk_grp_id) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE;


--
-- Name: grp_trt_objet grptrtobj_fk_obj_type_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_trt_objet
    ADD CONSTRAINT grptrtobj_fk_obj_type_id_fkey FOREIGN KEY (grptrtobj_fk_obj_type_id) REFERENCES catalogue.objet_type(pk_objet_type_id);


--
-- Name: grp_trt_objet grptrtobj_fk_trt_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.grp_trt_objet
    ADD CONSTRAINT grptrtobj_fk_trt_id_fkey FOREIGN KEY (grptrtobj_fk_trt_id) REFERENCES catalogue.trt_objet(pk_trt_objet);


--
-- Name: harves_opendata_node_params harves_opendata_node_params_FK_node_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.harves_opendata_node_params
    ADD CONSTRAINT "harves_opendata_node_params_FK_node_id_fkey" FOREIGN KEY (fk_node_id) REFERENCES catalogue.harves_opendata_node(pk_node_id);


--
-- Name: prodige_external_access prodige_extaccess_fk_utilisateur_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_external_access
    ADD CONSTRAINT prodige_extaccess_fk_utilisateur_fkey FOREIGN KEY (prodige_extaccess_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) ON DELETE CASCADE;


--
-- Name: prodige_help_group prodige_help_group_hlpgrp_fk_groupe_profil_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_help_group
    ADD CONSTRAINT prodige_help_group_hlpgrp_fk_groupe_profil_fkey FOREIGN KEY (hlpgrp_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) ON DELETE CASCADE;


--
-- Name: prodige_help_group prodige_help_group_hlpgrp_fk_help_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.prodige_help_group
    ADD CONSTRAINT prodige_help_group_hlpgrp_fk_help_id_fkey FOREIGN KEY (hlpgrp_fk_help_id) REFERENCES catalogue.prodige_help(pk_prodige_help_id) ON DELETE CASCADE;


--
-- Name: sous_domaine sous_domaine_ssdom_fk_domaine_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.sous_domaine
    ADD CONSTRAINT sous_domaine_ssdom_fk_domaine_fkey FOREIGN KEY (ssdom_fk_domaine) REFERENCES catalogue.domaine(pk_domaine) MATCH FULL ON DELETE CASCADE;


--
-- Name: ssdom_dispose_couche ssdom_dispose_couche_ssdcouch_fk_couche_donnees_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_dispose_couche
    ADD CONSTRAINT ssdom_dispose_couche_ssdcouch_fk_couche_donnees_fkey FOREIGN KEY (ssdcouch_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) MATCH FULL ON DELETE CASCADE;


--
-- Name: ssdom_dispose_couche ssdom_dispose_couche_ssdcouch_fk_sous_domaine_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_dispose_couche
    ADD CONSTRAINT ssdom_dispose_couche_ssdcouch_fk_sous_domaine_fkey FOREIGN KEY (ssdcouch_fk_sous_domaine) REFERENCES catalogue.sous_domaine(pk_sous_domaine) MATCH FULL ON DELETE CASCADE;


--
-- Name: ssdom_dispose_metadata ssdom_dispose_metadata_ssdcouch_fk_sous_domaine_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_dispose_metadata
    ADD CONSTRAINT ssdom_dispose_metadata_ssdcouch_fk_sous_domaine_fkey FOREIGN KEY (ssdcouch_fk_sous_domaine) REFERENCES catalogue.sous_domaine(pk_sous_domaine) MATCH FULL ON DELETE CASCADE;


--
-- Name: ssdom_visualise_carte ssdom_visualise_carte_ssdcart_fk_carte_projet_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_visualise_carte
    ADD CONSTRAINT ssdom_visualise_carte_ssdcart_fk_carte_projet_fkey FOREIGN KEY (ssdcart_fk_carte_projet) REFERENCES catalogue.carte_projet(pk_carte_projet) MATCH FULL ON DELETE CASCADE;


--
-- Name: ssdom_visualise_carte ssdom_visualise_carte_ssdcart_fk_sous_domaine_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.ssdom_visualise_carte
    ADD CONSTRAINT ssdom_visualise_carte_ssdcart_fk_sous_domaine_fkey FOREIGN KEY (ssdcart_fk_sous_domaine) REFERENCES catalogue.sous_domaine(pk_sous_domaine) MATCH FULL ON DELETE CASCADE;


--
-- Name: tache_import_donnees tacheimportdonnees_fk_couchedonnees; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.tache_import_donnees
    ADD CONSTRAINT tacheimportdonnees_fk_couchedonnees FOREIGN KEY (pk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) ON DELETE CASCADE;


--
-- Name: trt_autorise_objet trtautoobjet_fk_obj_type_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.trt_autorise_objet
    ADD CONSTRAINT trtautoobjet_fk_obj_type_id_fkey FOREIGN KEY (trtautoobjet_fk_obj_type_id) REFERENCES catalogue.objet_type(pk_objet_type_id);


--
-- Name: trt_autorise_objet trtautoobjet_fk_trt_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.trt_autorise_objet
    ADD CONSTRAINT trtautoobjet_fk_trt_id_fkey FOREIGN KEY (trtautoobjet_fk_trt_id) REFERENCES catalogue.trt_objet(pk_trt_objet);


--
-- Name: usr_accede_perimetre_edition usr_accede_perim_usrperim_edit_fk_perimetre_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.usr_accede_perimetre_edition
    ADD CONSTRAINT usr_accede_perim_usrperim_edit_fk_perimetre_fkey FOREIGN KEY (usrperim_fk_perimetre) REFERENCES catalogue.perimetre(pk_perimetre_id) MATCH FULL ON DELETE CASCADE;


--
-- Name: usr_accede_perimetre usr_accede_perim_usrperim_edit_fk_utilisateur_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.usr_accede_perimetre
    ADD CONSTRAINT usr_accede_perim_usrperim_edit_fk_utilisateur_fkey FOREIGN KEY (usrperim_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE;


--
-- Name: usr_accede_perimetre usr_accede_perim_usrperim_fk_perimetre_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.usr_accede_perimetre
    ADD CONSTRAINT usr_accede_perim_usrperim_fk_perimetre_fkey FOREIGN KEY (usrperim_fk_perimetre) REFERENCES catalogue.perimetre(pk_perimetre_id) MATCH FULL ON DELETE CASCADE;


--
-- Name: usr_accede_perimetre usr_accede_perim_usrperim_fk_utilisateur_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.usr_accede_perimetre
    ADD CONSTRAINT usr_accede_perim_usrperim_fk_utilisateur_fkey FOREIGN KEY (usrperim_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE;


--
-- Name: usr_alerte_perimetre_edition usr_alerte_perim_edit_fk_perimetre_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition
    ADD CONSTRAINT usr_alerte_perim_edit_fk_perimetre_fkey FOREIGN KEY (usralert_fk_perimetre) REFERENCES catalogue.perimetre(pk_perimetre_id) MATCH FULL ON DELETE CASCADE;


--
-- Name: usr_alerte_perimetre_edition usr_alerte_perim_edit_usrperim_fk_couchedonnees_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition
    ADD CONSTRAINT usr_alerte_perim_edit_usrperim_fk_couchedonnees_fkey FOREIGN KEY (usralert_fk_couchedonnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) MATCH FULL ON DELETE CASCADE;


--
-- Name: usr_alerte_perimetre_edition usr_alerte_perim_edit_usrperim_fk_utilisateur_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition
    ADD CONSTRAINT usr_alerte_perim_edit_usrperim_fk_utilisateur_fkey FOREIGN KEY (usralert_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE;


--
-- Name: zonage zonaget_fk_couche_donnees_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_p40
--

ALTER TABLE ONLY catalogue.zonage
    ADD CONSTRAINT zonaget_fk_couche_donnees_fkey FOREIGN KEY (zonage_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees);


--
-- Name: categoriesdes categoriesdes_iddes_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.categoriesdes
    ADD CONSTRAINT categoriesdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.categories(id);


--
-- Name: categoriesdes categoriesdes_langid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.categoriesdes
    ADD CONSTRAINT categoriesdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id);


--
-- Name: cswservercapabilitiesinfo cswservercapabilitiesinfo_langid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.cswservercapabilitiesinfo
    ADD CONSTRAINT cswservercapabilitiesinfo_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id);


--
-- Name: email email_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.email
    ADD CONSTRAINT email_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: statusvaluesdes fk_2vkxyjsd2d3tdwn38p5yjhb71; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.statusvaluesdes
    ADD CONSTRAINT fk_2vkxyjsd2d3tdwn38p5yjhb71 FOREIGN KEY (iddes) REFERENCES public.statusvalues(id);


--
-- Name: params fk_6d52bqoq3c2eitpdq5r7y872g; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.params
    ADD CONSTRAINT fk_6d52bqoq3c2eitpdq5r7y872g FOREIGN KEY (requestid) REFERENCES public.requests(id);


--
-- Name: schematroncriteriagroup fk_atfj71dq82he6n77lqofjxui6; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.schematroncriteriagroup
    ADD CONSTRAINT fk_atfj71dq82he6n77lqofjxui6 FOREIGN KEY (schematronid) REFERENCES public.schematron(id);


--
-- Name: groups fk_balio8qkvhnitbdw241e4ryb8; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT fk_balio8qkvhnitbdw241e4ryb8 FOREIGN KEY (defaultcategory_id) REFERENCES public.categories(id);


--
-- Name: sourcesdes fk_c3jxktm4qwai73lddsm5fiecb; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.sourcesdes
    ADD CONSTRAINT fk_c3jxktm4qwai73lddsm5fiecb FOREIGN KEY (iddes) REFERENCES public.sources(uuid);


--
-- Name: schematroncriteria fk_dh2vjs226vjp2anrvj3nuvt8x; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.schematroncriteria
    ADD CONSTRAINT fk_dh2vjs226vjp2anrvj3nuvt8x FOREIGN KEY (group_name, group_schematronid) REFERENCES public.schematroncriteriagroup(name, schematronid);


--
-- Name: inspireatomfeed_entrylist fk_eyt177hveh5vlxyxq6wpl3vqi; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.inspireatomfeed_entrylist
    ADD CONSTRAINT fk_eyt177hveh5vlxyxq6wpl3vqi FOREIGN KEY (inspireatomfeed_id) REFERENCES public.inspireatomfeed(id);


--
-- Name: usersavedselections fk_f558oonrxskvsnoc835m73sru; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.usersavedselections
    ADD CONSTRAINT fk_f558oonrxskvsnoc835m73sru FOREIGN KEY (selectionid) REFERENCES public.selections(id);


--
-- Name: group_category fk_j8nj5ssnar3byh882nuf38tqw; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.group_category
    ADD CONSTRAINT fk_j8nj5ssnar3byh882nuf38tqw FOREIGN KEY (category_id) REFERENCES public.categories(id);


--
-- Name: metadatanotifications fk_jbkvo3w3g4twk2bo1b8jn0sw8; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatanotifications
    ADD CONSTRAINT fk_jbkvo3w3g4twk2bo1b8jn0sw8 FOREIGN KEY (notifierid) REFERENCES public.metadatanotifiers(id);


--
-- Name: usersavedselections fk_m7d8cfojvibxag0wtg6pqe3d; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.usersavedselections
    ADD CONSTRAINT fk_m7d8cfojvibxag0wtg6pqe3d FOREIGN KEY (userid) REFERENCES public.users(id);


--
-- Name: selectionsdes fk_r0y9hytqn3nodmwn86hn2vsgf; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.selectionsdes
    ADD CONSTRAINT fk_r0y9hytqn3nodmwn86hn2vsgf FOREIGN KEY (iddes) REFERENCES public.selections(id);


--
-- Name: group_category fk_r1y7atocbww201qaj87h9j62e; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.group_category
    ADD CONSTRAINT fk_r1y7atocbww201qaj87h9j62e FOREIGN KEY (group_id) REFERENCES public.groups(id);


--
-- Name: schematrondes fk_sh1xwulyb1jeoc6puqpiuc5d2; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.schematrondes
    ADD CONSTRAINT fk_sh1xwulyb1jeoc6puqpiuc5d2 FOREIGN KEY (iddes) REFERENCES public.schematron(id);


--
-- Name: groups groups_referrer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_referrer_fkey FOREIGN KEY (referrer) REFERENCES public.users(id);


--
-- Name: groupsdes groupsdes_iddes_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.groupsdes
    ADD CONSTRAINT groupsdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.groups(id);


--
-- Name: groupsdes groupsdes_langid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.groupsdes
    ADD CONSTRAINT groupsdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id);


--
-- Name: harvestersettings harvestersettings_parentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.harvestersettings
    ADD CONSTRAINT harvestersettings_parentid_fkey FOREIGN KEY (parentid) REFERENCES public.harvestersettings(id);


--
-- Name: isolanguagesdes isolanguagesdes_iddes_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.isolanguagesdes
    ADD CONSTRAINT isolanguagesdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.isolanguages(id);


--
-- Name: isolanguagesdes isolanguagesdes_langid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.isolanguagesdes
    ADD CONSTRAINT isolanguagesdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id);


--
-- Name: metadata metadata_groupowner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadata
    ADD CONSTRAINT metadata_groupowner_fkey FOREIGN KEY (groupowner) REFERENCES public.groups(id);


--
-- Name: metadata metadata_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadata
    ADD CONSTRAINT metadata_owner_fkey FOREIGN KEY (owner) REFERENCES public.users(id);


--
-- Name: metadatacateg metadatacateg_categoryid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatacateg
    ADD CONSTRAINT metadatacateg_categoryid_fkey FOREIGN KEY (categoryid) REFERENCES public.categories(id);


--
-- Name: metadatacateg metadatacateg_metadataid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatacateg
    ADD CONSTRAINT metadatacateg_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id);


--
-- Name: metadatarating metadatarating_metadataid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatarating
    ADD CONSTRAINT metadatarating_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id);


--
-- Name: metadatastatus metadatastatus_metadataid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatastatus
    ADD CONSTRAINT metadatastatus_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id);


--
-- Name: metadatastatus metadatastatus_statusid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatastatus
    ADD CONSTRAINT metadatastatus_statusid_fkey FOREIGN KEY (statusid) REFERENCES public.statusvalues(id);


--
-- Name: metadatastatus metadatastatus_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.metadatastatus
    ADD CONSTRAINT metadatastatus_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(id);


--
-- Name: operationallowed operationallowed_groupid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.operationallowed
    ADD CONSTRAINT operationallowed_groupid_fkey FOREIGN KEY (groupid) REFERENCES public.groups(id);


--
-- Name: operationallowed operationallowed_metadataid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.operationallowed
    ADD CONSTRAINT operationallowed_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id);


--
-- Name: operationallowed operationallowed_operationid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.operationallowed
    ADD CONSTRAINT operationallowed_operationid_fkey FOREIGN KEY (operationid) REFERENCES public.operations(id);


--
-- Name: operationsdes operationsdes_iddes_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.operationsdes
    ADD CONSTRAINT operationsdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.operations(id);


--
-- Name: operationsdes operationsdes_langid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.operationsdes
    ADD CONSTRAINT operationsdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id);


--
-- Name: regionsdes regionsdes_iddes_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.regionsdes
    ADD CONSTRAINT regionsdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.regions(id);


--
-- Name: regionsdes regionsdes_langid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.regionsdes
    ADD CONSTRAINT regionsdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id);


--
-- Name: serviceparameters serviceparameters_service_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.serviceparameters
    ADD CONSTRAINT serviceparameters_service_fkey FOREIGN KEY (service) REFERENCES public.services(id);


--
-- Name: useraddress useraddress_addressid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.useraddress
    ADD CONSTRAINT useraddress_addressid_fkey FOREIGN KEY (addressid) REFERENCES public.address(id);


--
-- Name: useraddress useraddress_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.useraddress
    ADD CONSTRAINT useraddress_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(id);


--
-- Name: usergroups usergroups_groupid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.usergroups
    ADD CONSTRAINT usergroups_groupid_fkey FOREIGN KEY (groupid) REFERENCES public.groups(id);


--
-- Name: usergroups usergroups_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.usergroups
    ADD CONSTRAINT usergroups_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(id);


--
-- Name: validation validation_metadataid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: user_p40
--

ALTER TABLE ONLY public.validation
    ADD CONSTRAINT validation_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id);


--
-- Name: SCHEMA catalogue; Type: ACL; Schema: -; Owner: user_p40
--

GRANT ALL ON SCHEMA catalogue TO postgres;
GRANT ALL ON SCHEMA catalogue TO PUBLIC;


--
-- Name: TABLE geography_columns; Type: ACL; Schema: public; Owner: postgres
--

REVOKE SELECT ON TABLE public.geography_columns FROM PUBLIC;
GRANT ALL ON TABLE public.geography_columns TO user_p40;


--
-- Name: TABLE geometry_columns; Type: ACL; Schema: public; Owner: postgres
--

REVOKE SELECT ON TABLE public.geometry_columns FROM PUBLIC;
GRANT ALL ON TABLE public.geometry_columns TO user_p40;


--
-- Name: TABLE spatial_ref_sys; Type: ACL; Schema: public; Owner: postgres
--

REVOKE SELECT ON TABLE public.spatial_ref_sys FROM PUBLIC;
GRANT ALL ON TABLE public.spatial_ref_sys TO user_p40;


--
-- PostgreSQL database dump complete
--

