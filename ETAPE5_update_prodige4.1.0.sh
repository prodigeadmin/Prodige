#!/bin/bash
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`

error_exit()
{
echo "Annulation du script : $1" && exit 1
}
check()
{
case "$2" in
    var) [ -z $1 ] && error_exit "$3";;
    file) [ -f $1 ] || error_exit "Le fichier $1 n'existe pas";;
    dir) [ -d $1 ] || error_exit "Le répertoire $1 n'existe pas";;
esac
}

check "${SCRIPTDIR}" "dir"
OLDIR="${SCRIPTDIR}/old"
check "$OLDIR" "dir"
YEAR=`date '+%Y'`
DATE=`date '+%y%m%d'`
NUM="4.1.0"
NUM_old="4.0.15"
VERSIONTXT_old="$OLDIR/sites/prodigecatalogue/web/version.txt"
[ -f $VERSIONTXT_old ] || VERSIONTXT_old="$OLDIR/sites/prodigecatalogue_/web/version.txt"
PATCH_NAME="update_prodige${NUM}_13022019.tar.gz"
PATCH_TAG="prodige_${NUM}"
PATCH_DESC="Patch correctif prodige_${NUM}
- migration vers Prodige ${NUM}
"
user_tampon="user_tampon"
DB_tampon="tampon_tpl"



requisites()
{
check "${VERSIONTXT_old}" file
if [ "`egrep "^Patch correctif prodige_${NUM}$" $VERSIONTXT_old`" ]
  then echo "Version Prodige déjà égale à ${NUM} : fin du patch ";exit
fi
if [ "`egrep "^Patch correctif prodige_${NUM_old}$" $VERSIONTXT_old`" ]
  then echo "Version Prodige supérieure ou égale à ${NUM_old} : OK ";echo
  else echo "Prodige est en version inférieure à ${NUM_old}, fin du patch"; exit
fi
}

requisites_debian()
{
echo "Waiting for apt mirrors..."
apt-get -qq update
if [ "$?" -ne 0 ]
  then echo "Problème avec le mirroir Debian, fin du script" && exit
fi
for dir in \
${SCRIPTDIR}/prodigeadmincarto \
${SCRIPTDIR}/prodigeadminsite \
${SCRIPTDIR}/prodigecatalogue \
${SCRIPTDIR}/prodigefrontcarto \
${SCRIPTDIR}/prodigetelecarto \
${SCRIPTDIR}/prodigedatacarto \
${SCRIPTDIR}/prodigegeosource \
${SCRIPTDIR}/cas \
${SCRIPTDIR}/cas-bundle \
${SCRIPTDIR}/prodige-bundle \
${SCRIPTDIR}/dist \
${SCRIPTDIR}/tasks \
${OLDIR}/sites/prodigeadmincarto \
${OLDIR}/sites/prodigeadminsite \
${OLDIR}/sites/prodigecatalogue \
${OLDIR}/sites/prodigefrontcarto \
${OLDIR}/sites/prodigetelecarto \
${OLDIR}/sites/prodigedatacarto \
${OLDIR}/sites/prodigegeosource \
;do check "$dir" "dir";done
for file in \
${SCRIPTDIR}/dist/editables_parameters.yml \
${SCRIPTDIR}/cas/etc/cas/cas.properties \
${SCRIPTDIR}/cas/WEB-INF/classes/services/prodige4-proxy.json \
${SCRIPTDIR}/cas/etc/cas/log4j2.xml \
${SCRIPTDIR}/prodigegeosource/WEB-INF/config-overrides-prod.xml \
${SCRIPTDIR}/prodigegeosource/WEB-INF/config-db/jdbc.properties \
${SCRIPTDIR}/prodigegeosource/WEB-INF/config-security/config-security.properties \
${SCRIPTDIR}/prodigeadmincarto/carmenwsback/app/config/parameters.yml \
${SCRIPTDIR}/prodigegeosource/WEB-INF/classes/log4j.xml \
${SCRIPTDIR}/conf/geonetwork.xml \
${SCRIPTDIR}/conf/geonetwork_log4j.xml \
${SCRIPTDIR}/conf/start.ini \
${SCRIPTDIR}/conf/cas_log4j2.xml \
${SCRIPTDIR}/conf/server.xml \
${SCRIPTDIR}/conf/alkante.ldif \
${SCRIPTDIR}/conf/import_init.ldif \
${SCRIPTDIR}/conf/logrotate_prodige \
${SCRIPTDIR}/conf/0.conf \
${SCRIPTDIR}/files/ppolicy-conf.ldif \
${SCRIPTDIR}/files/ppolicy-default.ldif \
${SCRIPTDIR}/files/ppolicy-module.ldif \
${SCRIPTDIR}/files/Formats_Administration.xml \
${SCRIPTDIR}/rights.sh \
${SCRIPTDIR}/uprod.sh \
;do check "$file" file ;done
which rsync > /dev/null 2>&1
[ $? -eq "0" ]  ||  error_exit "rsync n'est pas installé"
which pwgen > /dev/null 2>&1
[ $? -eq "0" ]  ||  error_exit "pwgen n'est pas installé"
pass_tampon=`pwgen -s 10 1`
check "$pass_tampon" "var" "pass_tampon n'a pu être généré"
which composer > /dev/null 2>&1
[ $? -eq "0" ]  ||  error_exit "composer n'est pas installé"
}

get_vars()
{
# WWWURL= www-domain.tld ou www.domain.tld ou www-xyz.domain.tld ou www.xyz.domain.tld
WWWURL=`awk -F "\"" '/PRO_GEONETWORK_URLBASE/ { print $2 }' /home/sites/editables_parameters.yml | awk -F "/" '{ print $3 }'`
# DNS_SUFFIX= -domain.tld ou .domain.tld ou -xyz.domain.tld ou .xyz.domain.tld
DNS_SUFFIX=`awk -F "\"" '/PRODIGE_URL_ADMINSITE/ { print $2 }' /home/sites/editables_parameters.yml | sed 's|https://adminsite||'`
# DOMAIN= domain.tld ou xyz.domain.tld
DOMAIN=`echo $DNS_SUFFIX | sed 's/^-\?[a-z0-9]*\.//'`
# DNS_PREFIX_SEP = . ou -
DNS_PREFIX_SEP=`echo $WWWURL | sed 's/\(www\|catalogue\)\([\.-]\).*$/\2/'`
user_bdd=`awk '/catalogue_user/ { print $2 }' /home/sites/editables_parameters.yml`
pass_bdd=`awk '/catalogue_password/ { print $2 }' /home/sites/editables_parameters.yml`
host_bdd=`awk '/prodige_host/ { print $2 }' /home/sites/editables_parameters.yml`
#sur prodige CATALOGUE="CATALOGUE" / puis CATALOGUE=alk_respire
DB=`awk '/catalogue_name/ { print $2 }' /home/sites/editables_parameters.yml`
SSL_PWD=`cat /home/sites/editables_parameters.yml| awk '/ldap_password/ { print $NF }'`
LDAP_PWD="${SSL_PWD}"
for param in DNS_SUFFIX DOMAIN DNS_PREFIX_SEP user_bdd pass_bdd host_bdd DB SSL_PWD LDAP_PWD; do
     check "${!param}" "var" "Variable $param vide"
done
}

echo "Démarrage migration Prodige $NUM_old vers $NUM"
echo
egrep -q "^127\.0\.0\.1.*$WWWURL" /etc/hosts || echo '!!!WARNING!!! : '$WWWURL' should point to 127.0.0.1 in /etc/hosts'

requisites
requisites_debian
get_vars
read -ep "La migration va démarrer avec les paramètres suivants:
- WWWURL : $WWWURL
- DNS_SUFFIX : $DNS_SUFFIX
- DOMAIN : $DOMAIN
- DNS_PREFIX_SEP : $DNS_PREFIX_SEP
- user_bdd: $user_bdd
- pass_bdd: $pass_bdd
- host_bdd: $host_bdd
- DB: $DB
- SSL_PWD: $SSL_PWD
- LDAP_PWD: $LDAP_PWD
Appuyez sur Entrée pour continuer ou contrôle-C pour annuler"
/etc/init.d/tomcat8 stop
/etc/init.d/jetty9 stop
/etc/init.d/apache2 stop

chmod +r -R ${SCRIPTDIR}/
echo "$host_bdd:5432:PRODIGE:$user_bdd:$pass_bdd" > /root/.pgpass
echo "$host_bdd:5432:$DB:$user_bdd:$pass_bdd" >> /root/.pgpass
chmod 600 /root/.pgpass

echo
echo "Mise en place des sources"
echo "########################################################"
#${SCRIPTDIR}/prodigeadmincarto/carmenwsback/app/config/parameters.yml
cd ${SCRIPTDIR}/
[ -d /home/sites/vendor ] && rm -rf /home/sites/vendor
for dir in prodigeadmincarto prodigeadminsite prodigecatalogue prodigefrontcarto prodigetelecarto prodigedatacarto cas cas-bundle dist prodige-bundle prodigegeosource
  do
    echo "Installation de /home/sites/$dir ..."
    rsync -a --delete $dir /home/sites/
  done
chmod -R 775 /home/sites
chmod 755 /home/sites
chown -R www-data: /home/sites
echo

for dir in \
prodigecatalogue/web/thumbnails \
prodigegeosource/WEB-INF/data/data \
prodigegeosource/WEB-INF/data/config/codelist \
prodigecatalogue/web/upload \
;do
  echo "Reprise des données de $dir ..."
  rsync -a --delete $OLDIR/sites/$dir/ /home/sites/$dir/
done
dir=""
echo "Reprise des données de DOWNLOAD ..."
[ -d /home/sites/prodigetelecarto/web/DOWNLOAD ] && rm -rf /home/sites/prodigetelecarto/web/DOWNLOAD
mv $OLDIR/sites/prodigetelecarto/web/DOWNLOAD /home/sites/prodigetelecarto/web/
echo "Copie du nouveau fichier inspire-service-taxonomy.rdf"
cp ${SCRIPTDIR}/prodigegeosource/WEB-INF/data/config/codelist/external/thesauri/theme/inspire-service-taxonomy.rdf /home/sites/prodigegeosource/WEB-INF/data/config/codelist/external/thesauri/theme/
echo "Copie du nouveau fichier Formats_Administration.xml"
cp ${SCRIPTDIR}/files/Formats_Administration.xml /home/prodige/cartes/Publication/
echo
echo "Création de nouveaux dossiers"
mkdir -p /home/prodige/logs/scheduler > /dev/null 2>&1
mkdir -p /home/prodige/logs/opendata > /dev/null 2>&1
echo
echo "Composer"
SITES="prodigeadmincarto/carmenwsback
prodigeadmincarto/carmenwsmapserv
prodige-bundle
cas-bundle
prodigeadminsite
prodigecatalogue
prodigedatacarto
prodigefrontcarto
prodigetelecarto
"
for SITE in $SITES
do
  [ -d /home/sites/$SITE ] && cd "/home/sites/$SITE" && composer self-update && composer install
done


echo "Edition des paramètres"
echo "########################################################"
sed -i "
s/^overrides.ldap.security.credentials=.*$/overrides.ldap.security.credentials=${LDAP_PWD}/
s/^overrides.cas.baseURL=.*$/overrides.cas.baseURL=https:\/\/${WWWURL}\/cas/
s/^overrides.geonetwork.https.url=.*$/overrides.geonetwork.https.url=https:\/\/${WWWURL}\/geonetwork/
s/^overrides.jdbc.database=.*$/overrides.jdbc.database=${DB}/
s/^overrides.jdbc.username=.*$/overrides.jdbc.username=${user_bdd}/
s/^overrides.jdbc.password=.*$/overrides.jdbc.password=${pass_bdd}/
" /home/sites/prodigegeosource/WEB-INF/config-overrides.properties
sed -i "s/^jdbc.basic.maxWait.*$/jdbc.basic.maxWait=10000/
    s/^jdbc.basic.maxActive.*$/jdbc.basic.maxActive=66/
    s/^jdbc.host.*$/jdbc.host=${host_bdd}/" /home/sites/prodigegeosource/WEB-INF/config-db/jdbc.properties
sed -i "s/dns_url_prefix_sep.*$/dns_url_prefix_sep: \"$DNS_PREFIX_SEP\"/" /home/sites/prodigeadmincarto/carmenwsback/app/config/parameters.yml

for file in \
cas/WEB-INF/classes/services/prodige4-proxy.json \
prodigegeosource/WEB-INF/config-overrides-prod.xml \
;do cp ${OLDIR}/sites/$file /home/sites/$file; done
sed -i "s/adminsite)${DNS_SUFFIX}/adminsite|telecarto)${DNS_SUFFIX}/" /home/sites/cas/WEB-INF/classes/services/prodige4-proxy.json
sed -i "
s/#DOMAIN#/${DNS_SUFFIX}/g
s/#LDAP_PWD#/${SSL_PWD}/
" /home/sites/cas/etc/cas/cas.properties
sed -i "/<url>https:\/\/carto${DNS_SUFFIX}/ a\                    <url>https:\/\/telecarto${DNS_SUFFIX}<\/url>" /home/sites/prodigegeosource/WEB-INF/config-overrides-prod.xml
sed -i "
/PRODIGE_VERSION/ s|\"4.0\"|\"4.1\"|
/cas_proxy_chain/ s|\[ \"\/\.\*|\[ \"\/^https?:\\\\\\\/\\\\\\\/|
/cas_proxy_chain/ s|\.\*\/\" \]|\\\\\\\/\.\*\/\" \]|
/cas_proxy_chain/ s|)${DNS_SUFFIX}|\|telecarto)${DNS_SUFFIX}|
/PRO_GEONETWORK_DIRECTORY/ s/\(^[[:blank:]]*\)\(.*$\)/\1PRO_GEONETWORK_DIRECTORY  : \"%PATH_COMMUN%\/prodigegeosource\"/
" /home/sites/editables_parameters.yml
echo

echo "Edition des fichiers de configuration tomcat"
echo "########################################################"
cp ${SCRIPTDIR}/conf/geonetwork_log4j.xml /home/sites/prodigegeosource/WEB-INF/classes/log4j.xml
cp ${SCRIPTDIR}/conf/geonetwork.xml /etc/tomcat8/Catalina/localhost/
chmod +r /etc/tomcat8/Catalina/localhost/*.xml
cp ${SCRIPTDIR}/conf/server.xml /etc/tomcat8/
sed -i "s/#SSL_PWD#/$SSL_PWD/g" /etc/tomcat8/server.xml
[ -d  /var/lib/tomcat8/webapps/ROOT ] && rm -rf /var/lib/tomcat8/webapps/ROOT
cp ${SCRIPTDIR}/conf/server.xml /etc/tomcat8/
sed -i "s/#SSL_PWD#/$SSL_PWD/g" /etc/tomcat8/server.xml
echo

echo "Edition des fichiers de configuration jetty"
echo "########################################################"
echo "Make jetty starting after slapd"
mkdir -p /etc/systemd/system/jetty9.service.d > /dev/null 2>&1
echo '[Unit]
After=slapd.service' > /etc/systemd/system/jetty9.service.d/order.conf
systemctl daemon-reload
echo
cp ${SCRIPTDIR}/conf/start.ini /etc/jetty9/start.d/
sed -i "s/#SSL_PWD#/$SSL_PWD/g" /etc/jetty9/start.d/start.ini
cp ${SCRIPTDIR}/conf/cas_log4j2.xml /home/sites/cas/WEB-INF/classes/log4j2.xml
chmod 750 /var/log/jetty9
chown www-data.adm /var/log/jetty9
echo


echo "Gestion de la rotation des logs applicatifs apache / prodige"
echo "########################################################"
cp ${SCRIPTDIR}/conf/logrotate_prodige /etc/logrotate.d/
chown root: /etc/logrotate.d/prodige
chmod 644 /etc/logrotate.d/prodige
logrotate -f /etc/logrotate.d/prodige > /dev/null 2>&1

echo
echo "Configuration des virtualhosts Apache"
echo "########################################################"
cp $OLDIR/etc/apache2/sites-available/*.conf /etc/apache2/sites-available/
cp  ${SCRIPTDIR}/conf/ssl.conf /etc/apache2/ssl-cert/
cp  ${SCRIPTDIR}/conf/5_prodige*4.1.conf /etc/apache2/sites-available/
sed -i "s/#DOMAIN#/$DNS_SUFFIX/g" /etc/apache2/sites-available/5_prodige*4.1.conf
cd /etc/apache2/sites-available/
for conf in 5_prodige*4.1.conf; do a2ensite $conf; done
[ -f /etc/apache2/sites-available/0.conf ] && rm /etc/apache2/sites-available/0.conf
cp  ${SCRIPTDIR}/conf/0.conf /etc/apache2/sites-available/
sed -i "s/#DOMAIN#/$DNS_SUFFIX/g" /etc/apache2/sites-available/0.conf
[ -d /home/sites/nothing ] || mkdir -p /home/sites/nothing
a2ensite 0.conf
echo


echo "Reprise des certificats SSL"
echo "########################################################"
#ssl global:
cp "$OLDIR/etc/apache2/ssl-cert/CA_intermediate.pem" /usr/local/share/ca-certificates/CA_intermediate.crt
update-ca-certificates -v

#ssl apache
cd /etc/apache2
[ -d ssl-cert ] || mkdir ssl-cert
cd ssl-cert
[ -d ssl.crt ] || mkdir ssl.crt
[ -d ssl.key ] || mkdir ssl.key
cp "$OLDIR/etc/apache2/ssl-cert/ssl.crt/wildcard.prodige4.crt" ssl.crt/wildcard.prodige4.crt.$YEAR
cd ssl.crt && ln -s wildcard.prodige4.crt.$YEAR wildcard.prodige4.crt
cd ..
cp "$OLDIR/etc/apache2/ssl-cert/ssl.key/wildcard.prodige4.key" ssl.key/wildcard.prodige4.key.$YEAR
cd ssl.key && ln -s wildcard.prodige4.key.$YEAR wildcard.prodige4.key
cd ..
cp "$OLDIR/etc/apache2/ssl-cert/CA_intermediate.pem" CA_intermediate.pem.$YEAR
ln -s CA_intermediate.pem.$YEAR CA_intermediate.pem
chmod 644 CA_intermediate.pem.$YEAR

# ssl pour keystore tomcat
mkdir /etc/tomcat8/ssl-cert
cp "$OLDIR/etc/tomcat7/keystore/prodige.keystore" /etc/tomcat8/ssl-cert/
echo

 sed -i 's|^SLAPD_SERVICES=.*$|SLAPD_SERVICES="ldap://127.0.0.1:389/ ldapi:///"|' /etc/default/slapd

/etc/init.d/slapd restart
/etc/init.d/apache2 restart
rm -rf /home/sites/prodigegeosource/WEB-INF/data/index/*
/etc/init.d/tomcat8 start
/etc/init.d/jetty9 restart

echo "Attente 60 secondes du redemarrage complet de geosource" ; sleep 60
echo

echo "Ajout du tampon postgres"
echo "########################################################"
md5pass=`echo -n "${pass_tampon}${user_tampon}"|md5sum|awk '{ print $1 }'`
LOGtampon="${SCRIPTDIR}/tampon.log"
echo "Creating user ${user_tampon}..." | tee -a $LOGtampon
sudo -i -u postgres  psql -c "CREATE ROLE ${user_tampon} ENCRYPTED PASSWORD 'md5${md5pass}' NOSUPERUSER CREATEDB NOCREATEROLE INHERIT LOGIN;" >> $LOGtampon 2>&1
echo "Creating database $DB_tampon..." | tee -a $LOGtampon
sudo -i -u postgres createdb -T template0 --encoding='UTF-8' --lc-collate='fr_FR.UTF-8' --lc-ctype='fr_FR.UTF-8' -O "$user_tampon" "$DB_tampon" >> $LOGtampon 2>&1
sudo -i -u postgres createlang plpgsql "$DB_tampon" >> $LOGtampon 2>&1
sudo -i -u postgres psql -d "$DB_tampon" -c "CREATE EXTENSION postgis;" >> $LOGtampon 2>&1
sudo -i -u postgres psql -d "$DB_tampon" -c "grant all on geometry_columns to \"$user_tampon\";" >> $LOGtampon 2>&1
sudo -i -u postgres psql -d "$DB_tampon" -c "grant all on spatial_ref_sys to \"$user_tampon\";" >> $LOGtampon 2>&1
sudo -i -u postgres psql -d "$DB_tampon" -c "grant all on geography_columns to \"$user_tampon\";" >> $LOGtampon 2>&1
sed -i "/prodige_charset/a\\\n    #USER TAMPON FOR DATABASE CREATION\n    tampon_user: ${user_tampon}\n    tampon_password: ${pass_tampon}\n" /home/sites/editables_parameters.yml
echo


echo "Ajout des politiques des mots de passe"
echo "########################################################"
ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/ppolicy.ldif
ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f ${SCRIPTDIR}/files/ppolicy-module.ldif
ldapadd -Q -Y EXTERNAL -H ldapi:/// -f ${SCRIPTDIR}/files/ppolicy-conf.ldif
ldapadd -x -H ldap://localhost/ -D "cn=admin,dc=alkante,dc=domprodige" -w "$LDAP_PWD" -f ${SCRIPTDIR}/files/ppolicy-default.ldif
echo


if [ -d /home/sites/prodigecatalogue ]; then
  echo "Mise à jour de la base $DB, consulter ${SCRIPTDIR}/update_${NUM}_${DB}.log"
  echo "########################################################"
  psql -U ${user_bdd} -d ${DB} -h ${host_bdd} < "/home/sites/prodigecatalogue/src/ProdigeCatalogue/AdminBundle/Resources/sql/upgrade_${NUM}.sql" > ${SCRIPTDIR}/update_${NUM}_${DB}.log 2>&1
fi
if [ -d /home/sites/prodigeadmincarto/carmenwsback ]; then
  echo "Mise à jour de la base PRODIGE, consulter ${SCRIPTDIR}/update_${NUM}_PRODIGE.log"
  echo "########################################################"
  psql -U ${user_bdd} -d PRODIGE -h ${host_bdd} < "/home/sites/prodigeadmincarto/carmenwsback/src/Carmen/ApiBundle/Resources/sql/upgrade_${NUM}.sql" > ${SCRIPTDIR}/update_${NUM}_PRODIGE.log 2>&1
fi
echo

echo "migration PHP prodige:migration_41"
echo "########################################################"
cd /home/sites/prodigecatalogue &&  php app/console prodige:migration_41 -vvv
echo
#echo "migration PHP prodige:migration_metadata_41"
#echo "########################################################"
#cd /home/sites/prodigecatalogue &&  php app/console prodige:migration_metadata_41
#echo
echo "migration PHP prodige411:migration"
echo "########################################################"
cd /home/sites/prodigeadmincarto/carmenwsback &&  php app/console prodige411:migration -vvv
echo
echo "migration PHP carmen:Generatecontext"
echo "########################################################"
cd /home/sites/prodigeadmincarto/carmenwsback &&  php app/console carmen:Generatecontext 1
echo

echo "Installation des favicon.ico"
echo "########################################################"
for ico in \
/home/sites/prodigegeosource/images/logos/favicon.ico \
/home/sites/prodigeadmincarto/carmenwsback/web/favicon.ico \
/home/sites/prodigeadmincarto/carmenwsmapserv/web/favicon.ico \
/home/sites/prodigeadminsite/web/favicon.ico \
/home/sites/prodigecatalogue/web/favicon.ico \
/home/sites/prodigedatacarto/web/favicon.ico \
/home/sites/prodigefrontcarto/web/favicon.ico \
/home/sites/prodigetelecarto/web/favicon.ico \
;do
[ -f "${ico}" ] && rm "${ico}" && ln -sf /home/sites/favicon.ico "${ico}"
done
cp /home/sites/favicon.ico /home/sites/cas/favicon.ico
echo
echo "Installation des logos"
echo "########################################################"
for png in \
/home/sites/vendor/prodige/prodige/Prodige/ProdigeBundle/Resources/public/images/logo-client.png \
;do
[ -f "${png}" ] && rm "${png}" && ln -s /home/sites/logo-client.png "${png}"
done
cp /home/sites/logo-client.png /home/sites/cas/images/logo-client.png
echo

echo "Mise à jour des tâches planifiées"
echo "########################################################"
cd /etc/cron.d
echo '00 03           * * *   www-data    cd /home/sites/prodigeadminsite/ && /usr/bin/php app/console adminsite:generate_mapcache > /dev/null 2>&1' > p41_adminsite
echo '00 23           * * *   www-data    cd /home/sites/prodigeadmincarto/carmenwsback && /usr/bin/php app/console carmen:importdata > /dev/null 2>&1
30 23           * * *    www-data   cd /home/sites/prodigeadmincarto/carmenwsback && /usr/bin/php app/console carmen:cleanMaps > /dev/null 2>&1' > p41_admincarto
echo '00 00           * * *   www-data    cd /home/sites/prodigecatalogue/ && /usr/bin/php app/console prodige:password:lockexpired > /dev/null 2>&1
* *                * * *    www-data   cd /home/sites/prodigecatalogue/ && /usr/bin/php app/console scheduler:execute > /dev/null 2>&1' > p41_catalogue
echo '00,30 *         * * *   www-data    cd /home/sites/prodigeadminsite/ && /usr/bin/php app/console adminsite:populate_session_logs > /dev/null 2>&1' > p41_adminsite
echo '00 01         * * 6   www-data    cd /home/sites/prodigefrontcarto &&  /usr/bin/php app/console prodigefrontcarto:alertedition > /dev/null 2>&1' > p41_frontcarto
echo '10 01         * * 6   www-data    cd /home/sites/prodigefrontcarto &&  /usr/bin/php app/console prodigefrontcarto:sendmailingqueue > /dev/null 2>&1' > p41_frontcarto
echo '*/5 *           * * *   root        /home/tasks/queue_download.sh
30 00           * * *   root        /home/tasks/clean.sh' > p41
cp "${SCRIPTDIR}/tasks/"* /home/tasks/
chown root: /home/tasks/*.sh
chmod 700 /home/tasks/*.sh
echo

echo "Génération du fichiers version.txt"
echo "########################################################"
VERSIONTXT="/home/sites/prodigecatalogue/web/version.txt"
echo -e "Patch ${PATCH_NAME} (${PATCH_TAG}) applied `date`\n${PATCH_DESC}\n-----------------------------------------------" > "${VERSIONTXT}"
cat "${VERSIONTXT_old}" >> "${VERSIONTXT}"
echo

rm /root/.pgpass

uprod()
{
echo "Running symfony  php app/console, please wait ...."
echo "########################################################"
cd /home/sites
bash ./uprod.sh > ${SCRIPTDIR}/uprod.log 2>&1
echo "Ending symfony jobs"
echo
}

rights()
{
echo "Changing rights on filesystem, please wait ...."
echo "########################################################"
[ -f /home/rights.sh ] && [ -z "`ps ax | grep "/home/rights.sh" | grep -v grep`" ] && /home/rights.sh > /dev/null 2>&1
echo
}

cp ${SCRIPTDIR}/rights.sh /home/
cp ${SCRIPTDIR}/uprod.sh /home/sites/
uprod
rights

echo
echo "Fin de la mise à jour"
echo

exit 0
