#!/bin/bash
#Installation Composants système pour Prodige 4.1
#24/09/2018
#http://adullact.net/projects/prodige/

#Definition du repertoire d'install source
SCRIPTDIR=`dirname $(readlink -f $0)`
DATEINST=`date '+%y%m%d-%H%M'`
Netapes="7"
#adresse du depot debian stretch
SOURCES_LIST1="deb http://ftp.fr.debian.org/debian/ stretch main contrib non-free"
SOURCES_LIST2="deb http://security.debian.org/ stretch/updates  main contrib non-free"
SOURCES_LIST3="deb http://ftp.fr.debian.org/debian/ stretch-updates main"
NUM="4.1.0"
NUM_old_major="4.0"
NUM_old="${NUM_old_major}.15"
VERSIONTXT="/home/sites/prodigecatalogue/web/version.txt"
[ -f $VERSIONTXT ] || VERSIONTXT="/home/sites/prodigecatalogue_/web/version.txt"
SOURCES_LIST4="deb [arch=amd64] https://packagecloud.io/phalcon/stable/debian/ stretch main"

change_mirror()
{
#Configurer les depôts Debian stretch
cp /etc/apt/sources.list /etc/apt/sources.list.bak.$DATEINST
echo "${SOURCES_LIST1}" > /etc/apt/sources.list
echo "${SOURCES_LIST2}" >> /etc/apt/sources.list
echo "${SOURCES_LIST3}" >> /etc/apt/sources.list
}

################################################
#Verifications
################################################
verif()
{
echo
#Verification version Debian
echo -ne "Version Debian Stretch "
if [ "`egrep "^9\." /etc/debian_version`" ] 
  then color_echo "OK" $green
  else color_echo "NOK, annulation de l'installation" $red;exit
fi

echo -ne "Architecture amd64 "
if [ "`dpkg --print-architecture |grep amd64`" ] 
  then color_echo "OK" $green
  else color_echo "NOK i386 détectée, annulation de l'installation" $red;exit
fi

#Verification depot Debian
SOURCES_LIST1_URL=`echo "${SOURCES_LIST1}" |awk '{ print $2 "dists/" $3 }'`
echo -ne "Vérification du dépôt Debian "
wget -O /dev/null -q "${SOURCES_LIST1_URL}"
if [ $? -eq 0 ]
 then color_echo "OK" $green
else color_echo "NOK
Veuillez mettre à jour manuellement l'adresse du dépôt dans la variable SOURCES_LIST1 de ce script
Puis relancez l'installation" $red;exit
fi
}


start()
{
apt-get -qq update

echo "Europe/Paris" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

echo -ne "Locales systême "
if [ "`egrep "^fr_FR ISO-8859-1" /etc/locale.gen`" ] && [ "`egrep "^fr_FR.UTF-8 UTF-8" /etc/locale.gen`" ] && [ `locale charmap` == "UTF-8" ]
    then color_echo "OK" $green
    else
         echo "Reconfiguration des paramétrages régionaux actifs"
         echo "Activation de FR utf8 et de FR latin1, avec utf8 par défaut"
         cp /etc/locale.gen /etc/locale.gen.$DATEINST
         echo "LANG=fr_FR.UTF-8" > /etc/default/locale 
         echo "fr_FR ISO-8859-1" > /etc/locale.gen
         echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen
         locale-gen
         echo "La machine doit redémarrer, veuillez relancer l'install APRES REBOOT..."
         read -p "Appuyer sur Entrée pour redémarrer ou contrôle-C pour l'en empêcher"
         reboot &
	 exit 0
fi
}

################################################
#Fonctions
################################################

red='\033[31m';green='\033[32m';yellow='\033[33m';blue='\033[34m'
magenta='\033[35m';cyan='\033[36m';white='\033[37m'

color_echo()
{
message="$1";color="$2"
echo -en "$color";echo -e "$message"
tput sgr0
return
}  
echo_titre()
{
color_echo "\n\t${1}/$Netapes ${2}\n" "$cyan"
}


################################################
#Install paquets
################################################
install_utils()
{
apt-get -qqy remove mailx exim4*
apt-get -qqy install openssh-server ssh ftp unzip zip screen ntpdate time vim wget make autoconf libstdc++6 gcc cpp gnu-standards g++ sendmail sendmail-base sensible-mda sudo rsync bzip2 ed pwgen ca-certificates sharutils psmisc openssl python-numpy apt-transport-https git
curl -L "https://packagecloud.io/phalcon/stable/gpgkey" 2> /dev/null | apt-key add - &>/dev/null
echo "${SOURCES_LIST4}" > /etc/apt/sources.list.d/packagecloud_io_phalcon_stable_debian.list 
apt-get -qq update

#wkhtmltopdf 0.12.5.1 #phantomjs 2.1.1+dfsg-2
apt-get -qqy install xfonts-75dpi xfonts-base fontconfig libfontconfig phantomjs
cd /usr/local/src
rm /usr/bin/wkhtmltopdf > /dev/null 2>&1
rm /usr/local/bin/wkhtmltopdf > /dev/null 2>&1
wget -nv 'https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.stretch_amd64.deb' -O wkhtmltox_0.12.5-1.stretch_amd64.deb
dpkg -i wkhtmltox_0.12.5-1.stretch_amd64.deb
ln -s /usr/local/bin/wkhtmltopdf /usr/bin/wkhtmltopdf
}

install_apache()
{
apt-get -qqy install libapache2-mod-php7.0 php php-common php-http php-imagick php-memcache php-pear php-pecl-http php-propro php-raphf php7.0 php7.0-cli php7.0-common php7.0-curl php7.0-dev php7.0-gd php7.0-intl php7.0-json php7.0-ldap php7.0-mbstring php7.0-mcrypt php7.0-opcache php7.0-pgsql php7.0-readline php7.0-sqlite3 php7.0-xml php7.0-xsl php7.0-zip php7.0-ldap memcached apache2 file php7.0-phalcon php-yaml
}

conf_apache()
{
#Configuration de  php et d'Apache
cp /etc/php/7.0/apache2/php.ini /etc/php/7.0/apache2/php.ini.dist
#memory_limit
sed -i "s/^memory_limit = /;memory_limit = /g" /etc/php/7.0/apache2/php.ini
sed -i "/^;memory_limit = / i\memory_limit = 512M" /etc/php/7.0/apache2/php.ini
#post_max_size
sed -i "s/^post_max_size = /;post_max_size = /g" /etc/php/7.0/apache2/php.ini
sed -i "/^;post_max_size = / i\post_max_size = 500M" /etc/php/7.0/apache2/php.ini
#upload_max_filesize
sed -i "s/^upload_max_filesize = /;upload_max_filesize = /g" /etc/php/7.0/apache2/php.ini
sed -i "/^;upload_max_filesize = / i\upload_max_filesize = 500M" /etc/php/7.0/apache2/php.ini
#Reglage des sessions
sed -i "s/session.gc_maxlifetime = 1440/session.gc_maxlifetime = 3600/" /etc/php/7.0/apache2/php.ini
#maxclient 512
sed -i 's/MaxRequestWorkers[[:blank:]]*150/ServerLimit 512\n\tMaxRequestWorkers 512/' /etc/apache2/mods-available/mpm_prefork.conf

#secu
sed -i "s/^expose_php = On/;expose_php = On\nexpose_php = Off/" /etc/php/7.0/apache2/php.ini
sed -i "s/^ServerSignature.*$/ServerSignature Off/
s/^ServerTokens OS.*$/ServerTokens Prod/" /etc/apache2/conf-available/security.conf
echo 'umask 007' >> /etc/apache2/envvars
mkdir -p /etc/apache2/ssl-cert > /dev/null 2>&1
openssl dhparam -dsaparam -out /etc/apache2/ssl-cert/dhparam.pem 4096
sed -i "/SSLStrictSNIVHostCheck/ a\
SSLStaplingCache shmcb:/var/run/ocsp(128000)" /etc/apache2/mods-available/ssl.conf

#activation des modules et des sites
a2enmod proxy_http
a2enmod proxy
a2enmod rewrite
a2enmod ssl
a2enmod headers
a2enmod cgi
a2dissite 000-default.conf

apt-get -qqy install php-pecl-http

sed -i "/^APACHE_ULIMIT_MAX_FILES=/d
$ a\APACHE_ULIMIT_MAX_FILES='ulimit -n 65536'" /etc/apache2/envvars

#install composer
echo "Install composer"
php $SCRIPTDIR/files/composer-setup.php --install-dir="/usr/local/bin"
ln -s /usr/local/bin/composer.phar /usr/local/bin/composer
}

install_tomcat()
{
apt-get -qqy install tomcat8
/etc/init.d/tomcat8 stop

sed -i 's/TOMCAT8_GROUP=tomcat8/TOMCAT8_GROUP=www-data/
s/TOMCAT8_USER=tomcat8/TOMCAT8_USER=www-data/
s/^JAVA_OPTS.*$/JAVA_OPTS=\"-Xms1024m -Xmx2048m -Xss2M -Xmn128M -XX:MaxPermSize=256m -XX:PermSize=256m -server -Djava.awt.headless=true -XX:+UseParallelGC -XX:+AggressiveOpts\"/' /etc/default/tomcat8
find -L /var/cache/tomcat8/ -user tomcat8 -exec chown www-data {} \;
find -L /var/lib/tomcat8/ -user tomcat8 -exec chown www-data {} \;
find -L /var/cache/tomcat8/ -group tomcat8 -exec chgrp www-data {} \;
find -L /var/lib/tomcat8/ -group tomcat8 -exec chgrp www-data {} \;
mkdir -p /usr/share/tomcat8/shared/classes
mkdir -p /usr/share/tomcat8/server/classes
sed -i '/^ulimit /d
2 a\ulimit -n 65536' /usr/share/tomcat8/bin/catalina.sh
echo

echo "Modification des rétentions de journaux tomcat8"
cp $SCRIPTDIR/conf/logrotate_tomcat8 /etc/logrotate.d/tomcat8
chmod 600 /etc/logrotate.d/tomcat8
chown root: /etc/logrotate.d/tomcat8
echo

echo "Make tomcat starting after postgres"
mkdir -p /etc/systemd/system/tomcat8.service.d > /dev/null 2>&1
echo '[Unit]
After=postgresql@9.6-main.service' > /etc/systemd/system/tomcat8.service.d/order.conf
echo

echo "Changing file limits"
sed -ir '/www-data (soft|hard) nofile/d' /etc/security/limits.conf
sed -i '$ i\www-data soft nofile 65536\nwww-data hard nofile 65536' /etc/security/limits.conf
sed -i '/^fs.file-max = .*$/d' /etc/sysctl.conf
echo 'fs.file-max = 65536' > /etc/sysctl.d/60-file-max.conf
sysctl --system
echo

apt-get -qqy install jetty9
cp /etc/default/jetty9 /etc/default/jetty9.bak
sed -i 's/NO_START=1/NO_START=0/
s/^.*LOGFILE_DAYS.$/LOGFILE_DAYS=7/
s/^.*JETTY_PORT.*$/JETTY_PORT=8380/
s/^.*JETTY_HOST.*$/JETTY_HOST=127.0.0.1/
s/^.*JETTY_USER.*$/JETTY_USER=www-data/' /etc/default/jetty9
cd /etc/jetty9 && mkdir ssl-cert > /dev/null 2>&1
cd ssl-cert
ln -sf /etc/tomcat8/ssl-cert/prodige.keystore

/etc/init.d/jetty9 stop
pkill -9 -f jetty9
rm /var/lib/jetty9/jetty.state
[ -f /var/run/jetty9.pid ] && rm /var/run/jetty9.pid
chown www-data: /var/lib/jetty9
chmod 750 /var/log/jetty9
chown www-data.adm /var/log/jetty9
mv /var/lib/jetty9/webapps /var/lib/jetty9/webapps.dist
mkdir /var/lib/jetty9/webapps && cd /var/lib/jetty9/webapps
ln -s /home/sites/cas
echo 

}

install_carto()
{
#install postgres
apt-get -qqy install  postgresql-9.6 postgresql-contrib postgis postgresql-9.6-postgis-scripts postgresql-9.6-postgis-2.3
#Install paquets compiles par Alkante
#dep gdal
apt-get -qqy install curl libcurl3 libcurl3-gnutls libgeos-c1v5 libgif7 libjpeg62-turbo libpng16-16 python libsqlite3-0 libxerces-c3.1 libpq5 libxml2 libkmlbase1 libfreexl1 libexpat1 libopenjp2-7 libpcre3 libkmldom1 libkmlengine1 libkmlconvenience1
#dep mapserver
apt-get -qqy install libcairo2 libcurl3-gnutls libfcgi0ldbl libgd3 php7.0 proj-bin libproj12 libapache2-mod-fcgid
cd "${SCRIPTDIR}/deb"
dpkg -i libsvg_0.1.14-1stretch_amd64.deb libsvg-cairo_0.1.6-1stretch_amd64.deb
[ -f  /etc/apache2/mods-available/fcgid.conf ] && mv /etc/apache2/mods-available/fcgid.conf /etc/apache2/mods-available/fcgid.conf.bak
cp $SCRIPTDIR/conf/fcgid.conf /etc/apache2/mods-available/fcgid.conf
a2enmod fcgid
#dep mapcache
apt-get -qqy install curl libcurl3 libcurl3-gnutls libpng16-16 libjpeg62-turbo libtiff5 libpixman-1-0 libpcre3 libfcgi0ldbl

echo
cd "${SCRIPTDIR}/deb"
dpkg -i libecwj2_3.3-1stretch_amd64.deb
echo
dpkg -i gdal_2.2.3-1stretch_amd64.deb
echo
dpkg -i mapserv_7.0.7-1stretch_amd64.deb
mkdir /usr/lib/cgi-bin/ > /dev/null 2>&1
cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapserv
cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapservwfs
cp /usr/local/bin/mapserv /usr/lib/cgi-bin/mapserv.fcgi
chmod 755 /usr/lib/cgi-bin/mapserv*
echo "extension=php_mapscript.so" > /etc/php/7.0/mods-available/mapscript.ini
phpenmod mapscript
#php-ogr
echo
dpkg -i mapcache_1.4.2-1stretch_amd64.deb
ldconfig
echo 'LoadModule mapcache_module    /usr/lib/apache2/modules/mod_mapcache.so' > /etc/apache2/mods-available/mapcache.load
a2enmod mapcache

#Epsg
echo "Install epsg"
cp -f $SCRIPTDIR/files/epsg /usr/share/proj/
ln -s /usr/lib/x86_64-linux-gnu/libproj.so.12 /usr/lib/x86_64-linux-gnu/libproj.so
ldconfig
}


#Redemarrages
################################################
restart()
{
/etc/init.d/postgresql restart
/etc/init.d/apache2 restart
/etc/init.d/tomcat8 restart
#/etc/init.d/jetty9 start
}



echo "
#################################"
echo -e "Démarrage installation des composants systême pour Prodige ${NUM}"
echo "#################################"
echo

verif
change_mirror

start
read -ep "`color_echo "\nL'installation va démarrer
Appuyez sur Entrée pour continuer ou contrôle-C pour annuler" $yellow`"

echo_titre "1" "INSTALL DES PAQUETS DEBIAN"
install_utils
echo_titre "2" "INSTALL APACHE-PHP"
install_apache
echo_titre "3" "CONFIG APACHE-PHP"
conf_apache
echo_titre "4" "INSTALL TOMCAT"
install_tomcat
echo_titre "5" "INSTALL PAQUETS carto"
install_carto


echo_titre "7" "Redémarrage des services"
restart

color_echo "
L'installation des composants systême est terminée" $yellow
echo
exit 0

