#!/bin/bash
# repertoire d'execution du script actuel :
SCRIPTDIR=`dirname "$(readlink -f "$0")"`
DATE=`date '+%y%m%d-%H%M%S'`
LOG="$SCRIPTDIR/ETAPE1_export_${DATE}.log"
OLDIR="$SCRIPTDIR/old"
mkdir -p "${OLDIR}/postgres" 2> /dev/null
mkdir -p "${OLDIR}/ldap" 2> /dev/null
NUM="4.1.0"
NUM_old_major="4.0"
NUM_old="${NUM_old_major}.15"
VERSIONTXT="/home/sites/prodigecatalogue/web/version.txt"
[ -f $VERSIONTXT ] || VERSIONTXT="/home/sites/prodigecatalogue_/web/version.txt"


#######################################################
error_exit()
{
echo "Annulation du script : $1" && exit 1
}
check()
{
case "$2" in
    var) [ -z $1 ] && error_exit "$3";;
    file) [ -f $1 ] || error_exit "Le fichier $1 n'existe pas";;
    dir) [ -d $1 ] || error_exit "Le répertoire $1 n'existe pas";;
esac
}

#######################################################

requisites()
{
echo "Vérification du numéro de version de prodige actuel"
for file in \
$VERSIONTXT \
;do check "$file" file ;done
if [ "`grep ${NUM_old} $VERSIONTXT`" ] 
  then echo " ${NUM_old} OK"
  else echo " ERREUR : Prodige n'est pas en version ${NUM_old}" ; exit
fi
for dir in \
${OLDIR}/postgres \
;do check "$dir" "dir";done
}

get_vars()
{
for file in \
/home/sites/editables_parameters.yml \
;do check "$file" file ;done
# WWWURL= www-domain.tld ou www.domain.tld ou www-xyz.domain.tld ou www.xyz.domain.tld
WWWURL=`awk -F "\"" '/PRO_GEONETWORK_URLBASE/ { print $2 }' /home/sites/editables_parameters.yml | awk -F "/" '{ print $3 }'`
# DNS_SUFFIX= -domain.tld ou .domain.tld ou -xyz.domain.tld ou .xyz.domain.tld
DNS_SUFFIX=`awk -F "\"" '/PRODIGE_URL_ADMINSITE/ { print $2 }' /home/sites/editables_parameters.yml | sed 's|https://adminsite||'`
# DOMAIN= domain.tld ou xyz.domain.tld
DOMAIN=`echo $DNS_SUFFIX | sed 's/^-\?[a-z0-9]*\.//'`
# DNS_PREFIX_SEP = . ou -
DNS_PREFIX_SEP=`echo $WWWURL | sed 's/\(www\)\([\.-]\).*$/\2/'`
user_bdd=`awk '/catalogue_user/ { print $2 }' /home/sites/editables_parameters.yml`
pass_bdd=`awk '/catalogue_password/ { print $2 }' /home/sites/editables_parameters.yml`
host_bdd=`awk '/prodige_host/ { print $2 }' /home/sites/editables_parameters.yml`
#sur prodige CATALOGUE="CATALOGUE" / puis CATALOGUE=alk_respire 
DB=`awk '/catalogue_name/ { print $2 }' /home/sites/editables_parameters.yml`
SSL_PWD=`cat /home/sites/editables_parameters.yml| awk '/ldap_password/ { print $NF }'`
LDAP_PWD="${SSL_PWD}"
for param in DNS_SUFFIX DOMAIN DNS_PREFIX_SEP user_bdd pass_bdd host_bdd DB SSL_PWD LDAP_PWD; do
     check "${!param}" "var" "Variable $param vide"
done
}

backup_etc()
{
echo "Backup config /etc vers le répertoire ${OLDIR}/etc"
echo "************************************************"
cp -a /etc "${OLDIR}/"
#desactivation taches planifiees
sed -i '/\/home\/tasks/d' /etc/crontab
sed -i '/\/home\/sites/d' /etc/crontab
echo
echo "Copie de proj/epsg"
echo "************************************************"
cp /usr/share/proj/epsg ${OLDIR}/
echo
}

backup_sites()
{
echo "Backup sites"
echo "************************************************"
mkdir "${OLDIR}/sites"
cd /home/sites
for dir in `find -mindepth 1 -maxdepth 1 -type d | sed 's/^\.\///' | egrep "^prodige.+$|^cas|^vendor"`
  do
    if [ -d /home/sites/${dir} ]; then
      echo -e "\tDéplacement de ${dir}"
      mv ${dir} "${OLDIR}/sites/"
    fi
  done
echo
}

backup_postgres()
{
echo "Dump local de la base ${DB} vers ${OLDIR}/postgres/"
echo "************************************************"
su - postgres -c "pg_dump -C -E UTF8 -Fc ${DB}" > ${OLDIR}/postgres/${DB}.dmp
echo "Dump local de la base PRODIGE vers ${OLDIR}/postgres/"
echo "************************************************"
su - postgres -c "pg_dump -C -E UTF8 -Fc -N carmen PRODIGE" > ${OLDIR}/postgres/PRODIGE.dmp
su - postgres -c "pg_dump -C -E UTF8 -Fc -n carmen PRODIGE" > ${OLDIR}/postgres/PRODIGEcarmen.dmp
echo "Dump local des privilèges Postgres vers ${OLDIR}/postgres/"
su - postgres -c "pg_dumpall --globals-only" > ${OLDIR}/postgres/privileges.dmp
echo
echo "Fin de l'export `date`"
}

backup_ldap()
{
echo "Début de l'export LDAP vers ${OLDIR}/ldap"
echo "************************************************"
ldapdumpbin=`which slapcat  2> /dev/null`
if [ $? -eq 0 ]
    then
        $ldapdumpbin -b cn=config -l "${OLDIR}/ldap/LDAP_cn=config.ldif"
        for base in `$ldapdumpbin -b cn=config | egrep "^olcSuffix" | awk -F ":"  '{ print $2}'`; do
            echo "Backup $base"
            $ldapdumpbin -b "$base" -l "${OLDIR}/ldap/LDAP_${base}.ldif"
        done
    else echo "Backup LDAP NOK, or no slapcat"
fi
echo "Fin de l'export LDAP"
}

main()
{
echo "
#################################"
echo -e "Démarrage de l'export des données de Prodige ${NUM_old_major} localement.
Le script va :
* Effectuer une copie de /etc pour backup dans ${OLDIR}/etc
* Effectuer un déplacement des sites dans ${OLDIR}/sites
* Effectuer le dump des bases de l'application Prodige UNIQUEMENT (effectuez AU PREALABLE vous même la sauvegarde des éventuelles bases tierces)
Attention, cela peut nécessiter plusieurs Go d'espace disque dans le répertoire $OLDIR/postgres et $OLDIR/sites"

read -p "Appuyez sur Entrée pour continuer
#################################"

echo
for daemon in  apache2 tomcat7 jetty8; do
    [ -f /etc/init.d/$daemon ] &&  /etc/init.d/$daemon stop
done
/etc/init.d/postgresql restart
for daemon in  tomcat7 apache2 jetty8; do
    [ -f /etc/init.d/$daemon ] &&  /etc/init.d/$daemon start
done
echo
}

requisites
get_vars
main
backup_etc
backup_sites
backup_postgres
backup_ldap

echo
echo "Fin de la mise à jour"
echo

exit 0
