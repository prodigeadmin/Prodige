# Documentation d'installation/migration Prodige4.1

## Récupération des sources Prodige4.1 à partir du dépôt

```
mkdir -p /usr/local/src/prodige41
git clone -b 4.1.x https://gitlab.adullact.net/cpalkante/Prodige.git /usr/local/src/prodige41
cd /usr/local/src/prodige41
tar -xzf install-prodige4-4.1.tgz
rm install-prodige4-4.1/uprod.sh
mv install-prodige4-4.1/* .
rmdir install-prodige4-4.1
cp patches/LayerAddToWebServiceController.php prodigecatalogue/src/ProdigeCatalogue/GeosourceBundle/Controller/LayerAddToWebServiceController.php
```
### Pré-requis:
- Les minimums matériels sont : 4 CPU et 8 Go
- Vous devez disposer d'une connexion internet afin de réaliser l'installation de composants à partir de mirroirs Debian.
- le système doit être en Debian Stretch 64bit pour une installation nouvelle
- le mirroir Debian doit être joignable (cf les variables SOURCES_LIST en début de script)
- Espace disque. Le site a besoin d'espace disque notamment 
   - pour les données cartographiques dans /home/prodige (plusieurs dizaines à plusieurs centaines de Go) 
   - les bases de données Postgres remplissent le tablespace situé dans /var/lib/postgresql/ (quelques Go à plusieurs dizaines de Go)
   - En cas de migration, prévoir un espace libre au moins égal à l'espace occupé par /var/lib/postgresql/9.4/main/base/)
- Prodige doit être en 4.0.15 en cas de migration


## Installation Prodige4.1
L'installation reconfigure éventuellement les locales du système et peut demander de rebooter la machine si c'est nécessaire. 

Dans ce cas, relancer manuellement l'installation après le redémarrage du serveur.

Il est demandé à l'utilisateur de choisir:
1. L'adresse d'accès qui sera configurée lors de l'installation, par exemple: www.domaine.fr
2. les paramètres de connexion aux bases de données de l'application qui seront configurés par l'installation (utilisateur et mot de passe).
3. le chemin d'accès vers le dossier où se trouvent les certificats SSL.

Vous pouvez suivre le déroulement des opérations dans les fichiers logs indiqués à l'écran. Ils sont générés à la racine du script de migration.
L'import est terminé au redémarrage des services apache, tomcat et postgres.

```
cd /usr/local/src/prodige41
bash install_PRODIGEV4.1.sh
```

Les identifiants de connexion à l'application par défaut sont:
```
utilisateur : admin@prodige.fr
mot de passe : prodige01
```
Il est vivement conseillé de changer le mot de passe par défaut lors de la première connexion à l'application.

## Migration Prodige4.0 vers Prodige4.1

### Principe
Le tableau suivant liste les différentes caractéristiques d'une plate-forme en fonction de la version de PRODIGE :

| Version PRODIGE | Version Système Exploitation | Architecture Serveur |
| --- | --- | --- |
|4.0|Debian 8 jessie|64 bit|
|4.1|Debian 9 stretch|64 bit|

Le passage en 4.1 consiste donc en 2 changements distincts :
- l'upgrade du Système Exploitation de Debian 8 vers Debian 9
- la migration de l'application PRODIGE de 4.0 vers 4.1



### Etapes
Exécuter les scripts dans l'ordre suivant avec le compte "root"

#### ETAPE1 (ETAPE1_export_prodige4.0.sh)

arrêt des services PRODIGE 4.0, export des données PRODIGE 4.0.


#### ETAPE2 (ETAPE2_maj_debian.sh)

Mise à jour de l'OS jessie vers stretch. Ce script ne s'exécute pas, il est a jouer commande par commande par des administrateurs système.
    
#### ETAPE3 (ETAPE3_install_sys.sh)

Installation des composants système (paquets debian)



#### ETAPE4 (ETAPE4_import_prodige4.0.sh)

Les données de  PRODIGE 4.0 sont importées dans l'arborescence et dans les bases de données Postgres
   
#### ETAPE5 (ETAPE5_update_prodige4.1.0.sh)

Les données sont mises à jour vers PRODIGE 4.1



## Notes concernant l'utilisation des certificats SSL
Les services web de Prodige 4.1 sont sécurisés par l'emploi de certificats. Un certificat de type wildcard peut couvrir l'ensemble des sous-sites de l'application.
Ansi, dans le cas où vous utilisez le domaine domaine.fr pour accèder à prodige, si votre adresse principale d'accès est:
- www.`domaine.fr`      => vous devez fournir un certificat SSL wildcard pour      *.`domaine.fr`
- www-test.`domaine.fr` => vous devez fournir un certificat SSL wildcard pour      *.`domaine.fr`
- www.`test.domaine.fr` => vous devez fournir un certificat SSL wildcard pour *.`test.domaine.fr`

Les scripts demanderont d'indiquer le répertoire où se trouvent les fichiers du certificat SSL :
- wildcard.prodige4.crt, le certificat signé
- wildcard.prodige4.key, la clé privée
- CA_intermediate.pem, le certificat du CA.

## Notes concernant les mises à jour de Prodige

Se référer aux instructions dans patches/Readme.md

