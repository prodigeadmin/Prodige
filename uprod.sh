 #!/bin/bash
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`

SITES="prodigeadmincarto/carmenwsback
prodigeadmincarto/carmenwsmapserv
prodigeadminsite
prodigecatalogue
prodigedatacarto
prodigefrontcarto
prodigetelecarto"


for SITE in $SITES
do 
    cd $SCRIPTDIR
    if [ -d "$SCRIPTDIR/$SITE" ]; then
        echo "Running uprod for $SCRIPTDIR/$SITE"
        cd $SITE && sh bin/uprod.sh --symlink
    fi
done;

