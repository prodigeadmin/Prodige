Procédure du passage des patchs pour Prodige 4.1

# Pré-requis:

- La plate-forme doit au moins être en version 4.1.0. C'est le cas après une migration à partir de Prodige 4.0 ou lors d'une nouvelle installation vierge
- Lors du passage des patchs successifs, il est vérifié que le précédent patch a été appliqué.
- une connexion internet doit être disponible afin de réaliser l'installation de composants à partir de mirroirs Debian.

# Application du patch
Se placer dans le dossier du serveur, par exemple /usr/local/src/prodige41, et effectuer les commandes suivantes en tant que "root"

Pour appliquer le patch Y:
```
cd /usr/local/src/prodige41
cd patches
tar -xzf patch_4.1.Y.tar.gz
cd patch_4.1.Y
bash update_prodige4.1.Y.sh
``` 
