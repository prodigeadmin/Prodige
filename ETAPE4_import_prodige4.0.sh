#!/bin/bash



#Definition du repertoire d'install source
SCRIPTDIR=`dirname $(readlink -f $0)`
DATEINST=`date '+%y%m%d'`
OLDIR="$SCRIPTDIR/old"


################################################
#Fonctions
################################################

red='\033[31m';green='\033[32m';yellow='\033[33m';blue='\033[34m'
magenta='\033[35m';cyan='\033[36m';white='\033[37m'

color_echo ()
{
message="$1";color="$2"
echo -en "$color";echo -e "$message"
tput sgr0
return
}  
echo_titre()
{
color_echo "\n\t${1}/$Netapes ${2}\n" "$cyan"
}
wait_exec()
{
rep="0"
[ "$rep" == "0" ]
while [ "$?" != "1" ] ; do
   echo -ne ". "
   sleep "$2"
   pgrep -f "$1" > /dev/null
done
echo
}
error_exit()
{
echo "Annulation du script : $1" && exit 1
}
check()
{
case "$2" in
    var) [ -z $1 ] && error_exit "$3";;
    file) [ -f $1 ] || error_exit "Le fichier $1 n'existe pas";;
    dir) [ -d $1 ] || error_exit "Le répertoire $1 n'existe pas";;
esac
}
################################################
#Verifications
################################################

verif()
{
#Verification présence des donnees 4.0 à importer
for dir in \
${OLDIR}/postgres \
;do check "$dir" "dir";done
for file in \
$OLDIR/postgres/PRODIGE.dmp \
$OLDIR/postgres/PRODIGEcarmen.dmp \
$OLDIR/postgres/$DB.dmp \
;do check "$file" file ;done
}

#Récuperation des paramètres de prodige 4.0
get_vars()
{
for file in \
/home/sites/editables_parameters.yml \
;do check "$file" file ;done
# WWWURL= www-domain.tld ou www.domain.tld ou www-xyz.domain.tld ou www.xyz.domain.tld
WWWURL=`awk -F "\"" '/PRO_GEONETWORK_URLBASE/ { print $2 }' /home/sites/editables_parameters.yml | awk -F "/" '{ print $3 }'`
# DNS_SUFFIX= -domain.tld ou .domain.tld ou -xyz.domain.tld ou .xyz.domain.tld
DNS_SUFFIX=`awk -F "\"" '/PRODIGE_URL_ADMINSITE/ { print $2 }' /home/sites/editables_parameters.yml | sed 's|https://adminsite||'`
# DOMAIN= domain.tld ou xyz.domain.tld
DOMAIN=`echo $DNS_SUFFIX | sed 's/^-\?[a-z0-9]*\.//'`
# DNS_PREFIX_SEP = . ou -
DNS_PREFIX_SEP=`echo $WWWURL | sed 's/\(www\)\([\.-]\).*$/\2/'`
user_bdd=`awk '/catalogue_user/ { print $2 }' /home/sites/editables_parameters.yml`
pass_bdd=`awk '/catalogue_password/ { print $2 }' /home/sites/editables_parameters.yml`
host_bdd=`awk '/prodige_host/ { print $2 }' /home/sites/editables_parameters.yml`
#sur prodige CATALOGUE="CATALOGUE" / puis CATALOGUE=alk_respire 
DB=`awk '/catalogue_name/ { print $2 }' /home/sites/editables_parameters.yml`
SSL_PWD=`cat /home/sites/editables_parameters.yml| awk '/ldap_password/ { print $NF }'`
LDAP_PWD="${SSL_PWD}"
for param in DNS_SUFFIX DOMAIN DNS_PREFIX_SEP user_bdd pass_bdd host_bdd DB SSL_PWD LDAP_PWD; do
     check "${!param}" "var" "Variable $param vide"
done
}

import_base()
{
echo
SOURCE="$OLDIR/postgres/${1}.dmp"
echo "Import de la base à partir de ${SOURCE}, cf log dans ${2}"
echo  "Re-création de la structure" | tee  -a "${2}" 
sudo -i -u postgres createdb -O "$user_bdd" ${3} "${1}" >> "${2}" 2>&1
sudo -i -u postgres psql -d "${1}" -c "CREATE EXTENSION postgis;"
sudo -i -u postgres /usr/share/postgresql/9.6/contrib/postgis-2.3/postgis_restore.pl "${SOURCE}" > "${SOURCE}.tmp"
echo -ne "Import de la base "  | tee  -a "${2}"
psql  -U "${user_bdd}" -h localhost -f "${SOURCE}.tmp" "${1}" >> "${2}" 2>&1 &
wait_exec "${SOURCE}.tmp" "5"
sudo -i -u postgres psql -d "${1}" -c "
grant all on geometry_columns to ${user_bdd};
grant all on spatial_ref_sys to ${user_bdd};
grant all on geography_columns to ${user_bdd};
" >> "${2}" 2>&1
}

postgres()
{
/etc/init.d/postgresql restart || { echo "Erreur postgres n'a pu démarrer, le script ne peut être lancé"; exit; }
echo "localhost:5432:PRODIGE:$user_bdd:$pass_bdd
localhost:5432:$DB:$user_bdd:$pass_bdd
localhost:5432:postgres:$user_bdd:$pass_bdd" >> /root/.pgpass
chmod 600 /root/.pgpass

chmod 755 ${SCRIPTDIR}
chown -R postgres: "$OLDIR/postgres"
md5pass=`echo -n "${pass_bdd}${user_bdd}"|md5sum|awk '{ print $1 }'`
sudo -i -u postgres psql -c "CREATE ROLE ${user_bdd} ENCRYPTED PASSWORD 'md5${md5pass}' SUPERUSER CREATEDB CREATEROLE INHERIT LOGIN;"
su - postgres -c "psql -c \"ALTER ROLE $user_bdd SUPERUSER;\""

#Reglage de postgres pour la phase import
sed -i 's/#checkpoint_segments = 3/checkpoint_segments = 100/' /etc/postgresql/9.6/main/postgresql.conf
/etc/init.d/postgresql restart

LOG_CATALOGUE="$SCRIPTDIR/import_$DB.log"
import_base "${DB}" "$LOG_CATALOGUE" "--encoding=UTF8"
LOG_PRODIGE="$SCRIPTDIR/import_PRODIGE.log"
import_base "PRODIGE" "$LOG_PRODIGE" "--encoding=UTF8"
echo
echo "Import carmen, cf log dans $SCRIPTDIR/import_PRODIGEcarmen.log"
sudo -i -u postgres pg_restore -v -d PRODIGE $OLDIR/postgres/PRODIGEcarmen.dmp >> "$SCRIPTDIR/import_PRODIGEcarmen.log" 2>&1

su - postgres -c "psql -c \"ALTER ROLE $user_bdd NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN;\""
rm /root/.pgpass
}

echo "
##################################################################"
echo -e "Démarrage de l'import des données Prodige4.0
##################################################################
"

get_vars
verif

read -ep "`color_echo "\nL'importation peut commencer
Appuyez sur Entrée pour continuer ou contrôle-C pour annuler" $yellow`"

echo_titre "1" "Import des bases de données"
postgres


color_echo "L'importation des données est terminée
Vous pouvez passer à l'étape suivante" $yellow
echo
exit 0
