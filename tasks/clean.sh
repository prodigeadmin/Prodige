#!/bin/bash
DATE=`date '+%y%m%d'`
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`
LOGDIR="${SCRIPTDIR}/log"; [ -d ${LOGDIR} ] || mkdir -p "${LOGDIR}"
LOG="${LOGDIR}/${SCRIPTNAME%.*}_${DATE}.log"

echolog()
{
echo "`date '+%b %d %H:%M:%S'`  ${1}"
}

mainjob()
{
[ -d /home/prodige/cartes/Publication ] && find /home/prodige/cartes/Publication/ -type f -name "temp_admin_*" -mtime +7 -exec rm -f {} \;
[ -d /home/prodige/cartes/Publication/temp ] &&  find /home/prodige/cartes/Publication/temp/ -type f -mtime +30 -exec rm -f {} \;
[ -d /home/prodige/mapimage ] && find /home/prodige/mapimage/ -type f -mtime +1 -exec rm -f {} \;

[ -d /home/prodige/cartes/Publication/wfs/seven_days ] && find /home/prodige/cartes/Publication/wfs/seven_days/ -type f -mtime +7 -exec rm -f {} \;
[ -d /home/prodige/cartes/Publication/wfs/one_month ] &&  find /home/prodige/cartes/Publication/wfs/one_month/ -type f -mtime +31 -exec rm -f {} \;
[ -d /home/prodige/cartes/Publication/wfs/one_day ] && find /home/prodige/cartes/Publication/wfs/one_day/ -type f -mtime +1 -exec rm -f {} \;
[ -d /home/prodige/cartes/Publication/wfs/temp ] && find /home/prodige/cartes/Publication/wfs/temp/ -mtime +3 -exec rm -f {} \;

[ -d /home/sites/prodigetelecarto/web/DOWNLOAD ] && find /home/sites/prodigetelecarto/web/DOWNLOAD/ -mindepth 1 -mtime +7 -exec rm -rf {} \;
[ -d /home/sites/prodigetelecarto/web/DOWNLOAD ] && find /home/sites/prodigetelecarto/web/DOWNLOAD/ -mindepth 1 -maxdepth 1 -type d -mtime +1 -exec rm -rf {} \;
[ -d /home/sites/prodigetelecarto/web/DOWNLOAD ] && find /home/sites/prodigetelecarto/web/DOWNLOAD/ -maxdepth 1 -type f -mtime +1 -name "*.zip.*" -delete
find /tmp -maxdepth 1 -regex '/tmp/PDF_Complet_[a-f0-9][a-f0-9]*.zip' -mtime +5 -delete
find /tmp -maxdepth 1 -name "*.tmp.gml" -mtime +1 -delete
find /tmp -maxdepth 1 -name "*.tmp.gfs" -mtime +1 -delete
find /tmp -maxdepth 1 -name "*._filter.map" -mtime +1 -delete
find /tmp -maxdepth 1 -name "*geometrytype_*" -mtime +1 -delete
find /tmp -maxdepth 1 -name "*layerfieldvalues_*" -mtime +1 -delete
find /tmp -maxdepth 1 -name "*.pdf" -mtime +1 -delete

}

exec 6>&1 7>&2 && exec >> ${LOG} 2>&1
echolog "Starting ${SCRIPTDIR}/${SCRIPTNAME}"
mainjob
find ${LOGDIR}/ -name "${SCRIPTNAME%.*}_*.log" -type f -mtime +10  -maxdepth 1 -delete
echolog "End of ${SCRIPTDIR}/${SCRIPTNAME}"
exec 1>&6 6>&- 2>&7 7>&-
