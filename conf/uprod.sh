 #!/bin/bash
SCRIPTDIR=`dirname $(readlink -f $0)`
SCRIPTNAME=`basename $(readlink -f $0)`

SITES="prodigeadmincarto/carmenwsback
prodigeadmincarto/carmenwsmapserv
prodigeadminsite
prodigecatalogue
prodigedatacarto
prodigefrontcarto
prodigetelecarto"


for SITE in $SITES
do 
    cd $SCRIPTDIR
    if [ -d "$SCRIPTDIR/$SITE" ]; then
        echo "cleaning $SCRIPTDIR/$SITE"
        chmod u+x $SITE/bin/uprod.sh
        cd $SITE && ./bin/uprod.sh
    fi
done;

